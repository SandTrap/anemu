#ifndef EMU_TAINT_PROP_TYPES_H
#define EMU_TAINT_PROP_TYPES_H

#include "mambo/dbm.h"
#include "emu_intercept.h"

#define POST_INDEX 0
#define PRE_INDEX 1

#define OFFSET_DOWN 0
#define OFFSET_UP 1

// A taint_block_info starts with a header word followed by a variable
// number of words depending on header. If the header is 0, then
// there's nothing left to parse in the taint_block_info.
//
// The format of the header is as follows:
//
// [bits                 Taint direction      CC1   CC2   CC3   CC4
//  31 to 24]           (16 possibilities)
//                   ____  ____  ____  ____,  ____  ____  ____  ____
//
// [bits                  Memory access type (256 possibilities)
//  23 to 16]        ____  ____  ____  ____,  ____  ____  ____  ____
//
// [bits              ARM register bitfield. Set only if ARM registers
//  15 to 0          are the source or destination of a taint operation.
//
//  Taint direction: Should be set based on the taint_direction_t enum.
//
//  CC1, CC2, CC3, CC4: the conditional code of the instruction. If
//  the instruction is not unconditional(anything other than AL/ALT),
//  then the word following the header should be the CPSR.
//
//  Memory access type: should be set based on the mem_access_type_t enum.

// No more entries may be added to this taint_direction enum. If you
// need to add an entry, and you see an entry with prefixed with
// "UNUSED", you may remove it and add your new entry. However, if you
// see no "UNUSED" entries, then there is no more space.
typedef enum taint_direction {
    REG_TO_REG = 0,
    MEM_TO_REG = 1,
    REG_TO_MEM = 2,
    EXTREG_TO_EXTREG = 3,
    EXTREG_TO_MEM = 4,
    MEM_TO_EXTREG = 5,
    REG_TO_EXTREG = 6,
    EXTREG_TO_REG = 7,
    UNUSED_008 = 8,
    UNUSED_009 = 9,
    UNUSED_010 = 10,
    UNUSED_011 = 11,
    UNUSED_012 = 12,
    FUNC_LEVEL_TAINT = 13,
    LINK_NEW_BLOCK  = 14,
    CUSTOM_TP  = 15
} taint_direction_t;

typedef enum mem_access_type {
    MEM_NOT_ACCESSED = 0,
    MEM_RN,
    MEM_RN_IMM,
    MEM_RN_REGLIST,
    MEM_RN_RM_SHIFT,
    MEM_VLD1_M,
    MEM_VLD2_M,
    MEM_VLD3_M,
    MEM_VLD4_M,
    MEM_VST1_M,
    MEM_VST2_M,
    MEM_VST3_M,
    MEM_VST4_M,
    MEM_VLD1_SO,
    MEM_VLD2_SO,
    MEM_VLD3_SO,
    MEM_VLD4_SO,
    MEM_VST1_SO,
    MEM_VST2_SO,
    MEM_VST3_SO,
    MEM_VST4_SO,
    MEM_VLD1_SA,
    MEM_VLD2_SA,
    MEM_VLD3_SA,
    MEM_VLD4_SA,
    MEM_VPUSH_DP,
    MEM_VPOP_DP,
    MEM_VLDM_DP,
    MEM_VSTM_DP,
    MEM_VSTM_SP,
} mem_access_type_t;

#define HEADER_TAINT_DIRECTION_OFFSET 28
#define HEADER_CC_OFFSET 24
#define HEADER_MEM_ACCESS_OFFSET 16
#define HEADER_NUM_REGISTER_OFFSET 12
#define HEADER_NUM_EXT_REGISTER_OFFSET 14

#define HEADER_TAINT_DIRECTION_MASK 0xF0000000
#define HEADER_CC_MASK 0x0F000000
#define HEADER_MEM_ACCESS_MASK 0x00FF0000
#define HEADER_REGLIST_MASK 0x0000FFFF
#define HEADER_NUM_REGISTER_MASK 0x0000F000
#define HEADER_NUM_EXT_REGISTER_MASK 0x0000C000

#define WRITE_HEADER_SET_MEM(HEADER, ACCESS_TYPE)               \
    HEADER |= ((ACCESS_TYPE) << HEADER_MEM_ACCESS_OFFSET)

#define WRITE_HEADER_REGISTER_INDEX(HEADER, REGISTER, INDEX)    \
    HEADER |= (((REGISTER) << (INDEX)*4))

#define WRITE_HEADER_EXT_REGISTER_INDEX(HEADER, EXT_REGISTER, Q_REG, S_REG, INDEX) { \
        HEADER |= (Q_REG << ((INDEX*7)+6));                             \
        HEADER |= (S_REG << ((INDEX*7)+5));                             \
        HEADER |= (EXT_REGISTER << (INDEX*7));                          \
    }                                                                   \

#define WRITE_HEADER_NUM_REGISTERS(HEADER, NUM)                         \
    if ((NUM) > 3) emu_abort("ERROR: Number of registers may not exceed 3"); \
    HEADER |= ((NUM) << HEADER_NUM_REGISTER_OFFSET)

#define WRITE_HEADER_NUM_EXT_REGISTERS(HEADER, NUM)   {                 \
        if ((NUM) > 2) emu_abort("ERROR: Number of registers may not exceed 2"); \
        HEADER |= ((NUM) << HEADER_NUM_EXT_REGISTER_OFFSET);            \
    }

#define WRITE_HEADER_REGLIST(HEADER, REGLIST)   \
    HEADER |= (REGLIST)

#define WRITE_HEADER_TO_BLOCK(EMU, CTX, ADDR, HEADER, TAINT_DIRECTION, CC) { \
        HEADER |= ((TAINT_DIRECTION) << HEADER_TAINT_DIRECTION_OFFSET); \
        HEADER |= ((CC) << HEADER_CC_OFFSET);                           \
        *ADDR = HEADER;                                                 \
        ADDR++;                                                         \
        if ((CC) != EMU_ARM_CC_AL) {                                    \
            arm_copy_cpsr_to_mem(EMU, CTX, ADDR);                       \
            ADDR++;                                                     \
        }                                                               \
    }

#define READ_HEADER_CC(HEADER)                          \
    ((HEADER & HEADER_CC_MASK) >> HEADER_CC_OFFSET)

#define READ_HEADER_TAINT_DIRECTION(HEADER)                             \
    ((HEADER & HEADER_TAINT_DIRECTION_MASK) >> HEADER_TAINT_DIRECTION_OFFSET)

#define READ_HEADER_MEM_ACCESS(HEADER)                                  \
    ((HEADER & HEADER_MEM_ACCESS_MASK) >> HEADER_MEM_ACCESS_OFFSET)

#define READ_HEADER_REGLIST(HEADER)             \
    (HEADER & HEADER_REGLIST_MASK)

#define READ_HEADER_NUM_REGISTERS(HEADER)                               \
    ((HEADER & HEADER_NUM_REGISTER_MASK) >> HEADER_NUM_REGISTER_OFFSET)

#define READ_HEADER_REGISTER_INDEX(HEADER, NUM)         \
    ((HEADER & (0xF << (NUM * 4))) >> (NUM * 4))

#define READ_HEADER_NUM_EXT_REGISTERS(HEADER)   \
    ((HEADER & HEADER_NUM_EXT_REGISTER_MASK) >> HEADER_NUM_EXT_REGISTER_OFFSET)

#define READ_HEADER_EXT_REGISTER_INDEX(HEADER, EXT_REGISTER, Q_REG, S_REG, INDEX) { \
        EXT_REGISTER = (HEADER & (0x1F << (INDEX*7))) >> (INDEX*7);     \
        Q_REG = (HEADER & (0x1 << ((INDEX*7)+6))) >> ((INDEX*7)+6);     \
        S_REG = (HEADER & (0x1 << ((INDEX*7)+5))) >> ((INDEX*7)+5);     \
    }

#define FOOTER_UPDOWN_OFFSET 31
#define FOOTER_PREPOSTINDEX_OFFSET 30
#define FOOTER_SHIFT_TYPE_OFFSET 28
#define FOOTER_ADD_TAINT_OFFSET 27
#define FOOTER_RN_NUMBER_OFFSET 20
#define FOOTER_RM_NUMBER_OFFSET 16

#define FOOTER_UPDOWN_MASK 0x80000000
#define FOOTER_PREPOSTINDEX_MASK 0x40000000
#define FOOTER_SHIFT_TYPE_MASK 0x30000000
#define FOOTER_ADD_TAINT_MASK 0x08000000
#define FOOTER_IMM_VALUE_MASK 0x0000FFFF
#define FOOTER_RN_NUMBER_MASK 0x00F00000
#define FOOTER_RM_NUMBER_MASK 0x000F0000

#define WRITE_FOOTER_UPDOWN(FOOTER, UPDOWN)             \
    FOOTER |= ((UPDOWN) << FOOTER_UPDOWN_OFFSET)

#define WRITE_FOOTER_PREPOSTINDEX(FOOTER, PREPOSTINDEX)         \
    FOOTER |= ((PREPOSTINDEX) << FOOTER_PREPOSTINDEX_OFFSET)

#define WRITE_FOOTER_SHIFT_TYPE(FOOTER, SHIFT_TYPE)             \
    FOOTER |= ((SHIFT_TYPE) << FOOTER_SHIFT_TYPE_OFFSET)

#define WRITE_FOOTER_SET_ADD_TAINT(FOOTER)      \
    FOOTER |= (1 << FOOTER_ADD_TAINT_OFFSET)

#define WRITE_FOOTER_RN_NUMBER(FOOTER, RN)      \
    FOOTER |= ((RN) << FOOTER_RN_NUMBER_OFFSET)

#define WRITE_FOOTER_RM_NUMBER(FOOTER, RM)      \
    FOOTER |= ((RM) << FOOTER_RM_NUMBER_OFFSET)

#define WRITE_FOOTER_IMM_VALUE(FOOTER, IMM)     \
    FOOTER |= (IMM)

#define WRITE_FOOTER_TO_BLOCK(CTX, ADDR, FOOTER) {      \
        *ADDR = (FOOTER);                               \
        ADDR++;                                         \
    }

#define READ_FOOTER_UPDOWN(FOOTER)                              \
    ((FOOTER & FOOTER_UPDOWN_MASK) >> FOOTER_UPDOWN_OFFSET)

#define READ_FOOTER_PREPOSTINDEX(FOOTER)                                \
    ((FOOTER & FOOTER_PREPOSTINDEX_MASK) >> FOOTER_PREPOSTINDEX_OFFSET)

#define READ_FOOTER_SHIFT_TYPE(FOOTER)                                  \
    ((FOOTER & FOOTER_SHIFT_TYPE_MASK) >> FOOTER_SHIFT_TYPE_OFFSET)

#define READ_FOOTER_ADD_TAINT(FOOTER)                                   \
    ((FOOTER & FOOTER_ADD_TAINT_MASK) >> FOOTER_ADD_TAINT_OFFSET)

#define READ_FOOTER_RN_NUMBER(FOOTER)                                   \
    ((FOOTER & FOOTER_RN_NUMBER_MASK) >> FOOTER_RN_NUMBER_OFFSET)

#define READ_FOOTER_RM_NUMBER(FOOTER)                                   \
    ((FOOTER & FOOTER_RM_NUMBER_MASK) >> FOOTER_RM_NUMBER_OFFSET)

#define READ_FOOTER_IMM_VALUE(FOOTER)           \
    (FOOTER & FOOTER_IMM_VALUE_MASK)

#define EXT_CUST_REGLIST_START_REG_OFFSET 0
#define EXT_CUST_REGLIST_NUM_REGS_OFFSET 5
#define EXT_CUST_REGLIST_STRIDE_OFFSET 10
#define EXT_CUST_REGLIST_ELEM_SIZE_OFFSET 12

#define EXT_CUST_REGLIST_START_REG_MASK 0x001F
#define EXT_CUST_REGLIST_NUM_REGS_MASK 0x03E0
#define EXT_CUST_REGLIST_STRIDE_MASK 0x0C00
#define EXT_CUST_REGLIST_ELEM_SIZE_MASK 0xF000

#define WRITE_EXT_CUST_REGLIST_START_REG(CUST_REGLIST, START_REG)       \
    CUST_REGLIST |= (START_REG << EXT_CUST_REGLIST_START_REG_OFFSET)

#define WRITE_EXT_CUST_REGLIST_NUM_REGS(CUST_REGLIST, NUM_REGS)         \
    CUST_REGLIST |= (NUM_REGS << EXT_CUST_REGLIST_NUM_REGS_OFFSET)

#define WRITE_EXT_CUST_REGLIST_STRIDE(CUST_REGLIST, STRIDE)     \
    CUST_REGLIST |= (STRIDE << EXT_CUST_REGLIST_STRIDE_OFFSET)

#define WRITE_EXT_CUST_REGLIST_ELEM_SIZE(CUST_REGLIST, ELEM_SIZE)       \
    CUST_REGLIST |= (ELEM_SIZE << EXT_CUST_REGLIST_ELEM_SIZE_OFFSET)

#define READ_EXT_CUST_REGLIST_START_REG(CUST_REGLIST)                   \
    ((CUST_REGLIST & EXT_CUST_REGLIST_START_REG_MASK) >> EXT_CUST_REGLIST_START_REG_OFFSET)

#define READ_EXT_CUST_REGLIST_NUM_REGS(CUST_REGLIST)                    \
    ((CUST_REGLIST & EXT_CUST_REGLIST_NUM_REGS_MASK) >> EXT_CUST_REGLIST_NUM_REGS_OFFSET)

#define READ_EXT_CUST_REGLIST_STRIDE(CUST_REGLIST)                      \
    ((CUST_REGLIST & EXT_CUST_REGLIST_STRIDE_MASK) >> EXT_CUST_REGLIST_STRIDE_OFFSET)

#define READ_EXT_CUST_REGLIST_ELEM_SIZE(CUST_REGLIST)                   \
    ((CUST_REGLIST & EXT_CUST_REGLIST_ELEM_SIZE_MASK) >> EXT_CUST_REGLIST_ELEM_SIZE_OFFSET)


#define WRITE_AND_INCREMENT(ADDR, VAL) {        \
        *ADDR = VAL;                            \
        ADDR++;                                 \
    }

#define READ_AND_INCREMENT(ADDR, VAL) {         \
        VAL = *ADDR;                            \
        ADDR++;                                 \
    }

#define INCREMENT(ADDR)                         \
    ADDR++

// A taint block is going to have (BASIC_BLOCK_SIZE/2) words.
#define TAINT_BLOCK_SIZE (BASIC_BLOCK_SIZE/2)

// The number of words that must be available in the current taint
// block for it to be usable in another iteration. If the number of
// words currently available is less than the guard size, then a new
// taint block will be created and linked to.
#define TAINT_BLOCK_GUARD_SIZE 6

#define NUM_TAINT_CHAINS ( (CODE_CACHE_SIZE) + (TRACE_FRAGMENT_NO) )

typedef struct taint_chain {
    uint16_t num_blocks;
    uint32_t *starting_block;
} taint_chain_t;

typedef struct taint_prop_record {

    int block_id;
    bool in_thumb_mode;

    bool basic_block_started;
    bool skip_taint_call;
    bool insert_taint_call;
    bool in_ldrex_block;
    bool in_it_block;

    bool await_inline_confirmation;
    bool intercept_triggered;
    emu_intercept_func_t *intercept_func;

    uint8_t num_it_inst;

    taint_chain_t **taint_chains;
    uint32_t *taint_block_addr;
    uint32_t *taint_block_start_addr;
    uint32_t *taint_block_end_addr;

} taint_prop_record_t;



#endif // EMU_TAINT_PROP_TYPES_H
