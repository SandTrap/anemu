// Note: This file should only be included in emu_pie_decoder.c. It is
// not intended to be included elsewhere.

#define REG_PROC 0
#define IMM_PROC 1

#define LDR_IMM 0
#define LDR_REG 1

#define OFFSET_DOWN 0
#define OFFSET_UP 1

#define TRANSLATE_ARM_DATA_PROC {                                       \
        uint32_t immediate, null, rd, rn, operand2;                     \
        arm_data_proc_decode_fields(read_address, &immediate, &null, &null, &rd, &rn, &operand2); \
                                                                        \
        disassem->Rd.reg = rd;                                          \
        disassem->Rn.reg = rn;                                          \
        if (rd == 15) {                                                 \
            disassem->is_branch = true;                                 \
            disassem->format = INS_UNTRANSLATED;                        \
        }                                                               \
                                                                        \
        if (immediate == REG_PROC) {                                    \
            disassem->Rm.reg = operand2 & 0xF;                          \
            if ((operand2 & 0x10) != 0) {                               \
                disassem->Rs.reg = (operand2 & 0xF00) >> 8;             \
                disassem->format = INS_RD_RN_RM_TYPE_RS;                \
            } else {                                                    \
                disassem->format = INS_RD_RN_RM;                        \
            }                                                           \
        } else {                                                        \
            disassem->format = INS_RD_RN_CONST;                         \
        }                                                               \
    }

#define TRANSLATE_SINGLE_LOAD_STORE {                                   \
        uint32_t null, immediate, rt, rn, offset, prepostindex, updown; \
        enum shift_type shift = LSL;                                    \
        uint8_t shift_amount = 0;                                       \
                                                                        \
        arm_s_data_transfer_decode_fields(read_address, &null, &immediate, &null, &rt, &rn, &offset, &prepostindex, &updown, &null); \
                                                                        \
        if (rt == pc) {                                                 \
            disassem->is_branch = true;                                 \
            disassem->format = INS_UNTRANSLATED;                        \
            break;                                                      \
        }                                                               \
                                                                        \
        disassem->Rt.reg = rt;                                          \
        disassem->Rn.reg = rn;                                          \
                                                                        \
        if (immediate == LDR_REG) {                                     \
            disassem->Rm.reg = offset & 0xF;                            \
            disassem->Rm.shift_type = (offset & 0x60) >> 5;             \
            disassem->Rm.shift_value = (offset & 0xf80) >> 7;           \
            disassem->format = INS_RT_RN_RM_SHIFT;                      \
        } else {                                                        \
            disassem->imm = offset;                                     \
            disassem->format = INS_RT_RN_IMM;                           \
        }                                                               \
                                                                        \
        disassem->is_up = updown;                                       \
        disassem->is_pre_indexed = prepostindex;                        \
    }

#define TRANSLATE_BULK_LOAD_STORES {                                    \
        uint32_t null, loadstore, rn, registers, prepostindex, updown;  \
        arm_b_data_transfer_decode_fields(read_address, &loadstore, &rn, &registers, &prepostindex, &updown, &null, &null); \
                                                                        \
        disassem->Rn.reg = rn;                                          \
        disassem->reglist = registers;                                  \
        disassem->is_up = updown;                                       \
        disassem->is_pre_indexed = prepostindex;                        \
        disassem->format = INS_RN_REGLIST;                              \
    }

#define TRANSLATE_HALFWORD_LOAD {                                       \
        uint32_t null, immediate, rt, rn, rm, imm4h, prepostindex, updown; \
        arm_ldrh_decode_fields(read_address, &immediate, &rt, &rn, &rm, &imm4h, &prepostindex, &updown, &null); \
                                                                        \
        disassem->Rt.reg = rt;                                          \
        disassem->Rn.reg = rn;                                          \
                                                                        \
        /* In LDRH, if immediate == 1, them Rm is actuall imm4l */      \
        if (immediate == 1) {                                           \
            disassem->imm = ((imm4h << 4) + rm);                        \
            disassem->format = INS_RT_RN_IMM;                           \
        } else {                                                        \
            disassem->Rm.reg = rm;                                      \
            disassem->Rm.shift_type = 0;                                \
            disassem->Rm.shift_value = 0;                               \
            disassem->format = INS_RT_RN_RM_SHIFT;                      \
        }                                                               \
                                                                        \
        disassem->is_up = updown;                                       \
        disassem->is_pre_indexed = prepostindex;                        \
    }

#define TRANSLATE_MOV {                                                 \
        uint32_t null, immediate, rd, operand2;                         \
        arm_mov_decode_fields(read_address, &immediate, &null, &rd, &operand2); \
        if (rd == pc) {                                                 \
            disassem->is_branch = true;                                 \
            disassem->format = INS_UNTRANSLATED;                        \
            break;                                                      \
        }                                                               \
                                                                        \
        disassem->Rd.reg = rd;                                          \
        if (immediate == IMM_PROC) {                                    \
            disassem->format = INS_RD_CONST;                            \
            break;                                                      \
        }                                                               \
                                                                        \
        /* Used to identify MOV (shifted register) instructions */      \
        uint16_t op_type = (operand2 & 0xff0) >> 4;                     \
        uint8_t shift_type = (operand2 & 0x70) >> 4;                    \
                                                                        \
        if (op_type == 0 ||                                             \
            shift_type == 0b100 || shift_type == 0b000 || shift_type == 0b010 || \
            shift_type == 0b110 || shift_type == 0b110) {               \
            /* If op_type == 0, instruction is just MOV (register) */   \
            /* If shift_type == 0b100, instruction is an ASR (immediate) instruction */ \
            /* If shift_type == 0b000, instruction is an LSL (immediate) instruction */ \
            /* If shift_type == 0b010, instruction is an LSR (immediate) instruction */ \
            /* If shift_type == 0b110, instruction is an ROR (immediate) or RRX instruction */ \
            disassem->Rm.reg = operand2 & 0xF;                          \
            disassem->format = INS_RD_RM;                               \
        } else {                                                        \
            disassem->Rn.reg = operand2 & 0xF;                          \
            disassem->Rm.reg = (operand2 & 0xF00) >> 8;                 \
            /* We don't really need the shift value for MOV for taintprop */ \
            disassem->Rm.shift_type = 0;                                \
            disassem->Rm.shift_value = 0;                               \
            disassem->format = INS_RD_RN_RM_SHIFT;                      \
        }                                                               \
    }

#define TRANSLATE_B_BL {                                        \
        uint32_t branch_offset, offset, target;                 \
        arm_b_decode_fields(read_address, &offset);             \
                                                                \
        branch_offset = (offset & 0x800000) ? 0xFC000000 : 0;   \
        branch_offset |= (offset<<2);                           \
        target = (uint32_t)read_address + 8 + branch_offset;    \
                                                                \
        disassem->imm = target;                                 \
        disassem->format = INS_LABEL;                           \
    }

#define TRANSLATE_NEON_VLDX_VSTX_M {                                    \
        uint32_t opcode, size, d, vd, rn, align, rm;                    \
        arm_neon_vldx_m_decode_fields(read_address, &opcode, &size, &d, &vd, &rn, &align, &rm); \
                                                                        \
        vd = (d << 4) + vd;                                             \
        disassem->reglist = (1 << vd);                                  \
        disassem->v_elem_size = size;                                   \
                                                                        \
        switch (opcode) {                                               \
        case 0b0111:                                                    \
            /* VLD1, VST1 regs = 1 */                                   \
            disassem->vldx_vstx_num = 1;                                \
            break;                                                      \
        case 0b1010:                                                    \
            /* VLD1, VST1 regs = 2 */                                   \
            disassem->vldx_vstx_num = 1;                                \
            disassem->vldx_vstx_stride = 1;                             \
            disassem->reglist |= (1 << (vd+1));                         \
            break;                                                      \
        case 0b0110:                                                    \
            /* VLD1, VST1 regs = 3 */                                   \
            disassem->vldx_vstx_num = 1;                                \
            disassem->vldx_vstx_stride = 1;                             \
            disassem->reglist |= (1 << (vd+1));                         \
            disassem->reglist |= (1 << (vd+2));                         \
            break;                                                      \
        case 0b0010:                                                    \
            /* VLD1, VST1 regs = 4 */                                   \
            disassem->vldx_vstx_num = 1;                                \
            disassem->vldx_vstx_stride = 1;                             \
            disassem->reglist |= (1 << (vd+1));                         \
            disassem->reglist |= (1 << (vd+2));                         \
            disassem->reglist |= (1 << (vd+3));                         \
            break;                                                      \
        case 0b1000:                                                    \
            /* VLD2, VST2 regs = 1, inc = 1 */                          \
            disassem->vldx_vstx_num = 2;                                \
            disassem->vldx_vstx_stride = 1;                             \
            disassem->reglist |= (1 << (vd+1));                         \
            break;                                                      \
        case 0b1001:                                                    \
            /* VLD2, VST2 regs = 1, inc = 2 */                          \
            disassem->vldx_vstx_num = 2;                                \
            disassem->vldx_vstx_stride = 2;                             \
            disassem->reglist |= (1 << (vd+2));                         \
            break;                                                      \
        case 0b0011:                                                    \
            /* VLD2, VST2 regs = 2, inc = 2 */                          \
            disassem->vldx_vstx_num = 2;                                \
            disassem->vldx_vstx_stride = 1;                             \
            disassem->reglist |= (1 << (vd+1));                         \
            disassem->reglist |= (1 << (vd+2));                         \
            disassem->reglist |= (1 << (vd+3));                         \
            break;                                                      \
        case 0b0100:                                                    \
            /* VLD3, VST3 inc = 1 */                                    \
            disassem->vldx_vstx_num = 3;                                \
            disassem->vldx_vstx_stride = 1;                             \
            disassem->reglist |= (1 << (vd+1));                         \
            disassem->reglist |= (1 << (vd+2));                         \
            break;                                                      \
        case 0b0101:                                                    \
            /* VLD3, VST3 inc = 2 */                                    \
            disassem->vldx_vstx_num = 3;                                \
            disassem->vldx_vstx_stride = 2;                             \
            disassem->reglist |= (1 << (vd+2));                         \
            disassem->reglist |= (1 << (vd+4));                         \
            break;                                                      \
        case 0b0000:                                                    \
            /* VLD4, VST4 inc = 1 */                                    \
            disassem->vldx_vstx_num = 4;                                \
            disassem->vldx_vstx_stride = 1;                             \
            disassem->reglist |= (1 << (vd+1));                         \
            disassem->reglist |= (1 << (vd+2));                         \
            disassem->reglist |= (1 << (vd+3));                         \
            break;                                                      \
        case 0b0001:                                                    \
            /* VLD4, VST4 inc = 2 */                                    \
            disassem->vldx_vstx_num = 4;                                \
            disassem->vldx_vstx_stride = 2;                             \
            disassem->reglist |= (1 << (vd+2));                         \
            disassem->reglist |= (1 << (vd+4));                         \
            disassem->reglist |= (1 << (vd+6));                         \
            break;                                                      \
        }                                                               \
                                                                        \
        disassem->Rn.reg = rn;                                          \
        if (rm == 15 || rm == 13) {                                     \
            /* Rm is not specified */                                   \
            disassem->format = INS_V_REGLIST_RN;                        \
        } else {                                                        \
            disassem->Rm.reg = rm;                                      \
            disassem->format = INS_V_REGLIST_RN_RM;                     \
        }                                                               \
    }

#define TRANSLATE_NEON_VLDX_VSTX_S_O {                                  \
        uint32_t opcode, size, d, vd, rn, index_align, rm;              \
        arm_neon_vldx_s_o_decode_fields(read_address, &opcode, &size, &d, &vd, &rn, &index_align, &rm); \
                                                                        \
        vd = (d << 4) + vd;                                             \
        disassem->reglist = (1 << vd);                                  \
        disassem->v_elem_size = size;                                   \
                                                                        \
        if (opcode == 0) {                                              \
            /* VLD1, VST1 */                                            \
            /* Nothing else to do. */                                   \
            disassem->vldx_vstx_num = 1;                                \
        } else if (opcode == 1) {                                       \
            /* VLD2, VST2 */                                            \
            disassem->vldx_vstx_num = 2;                                \
                                                                        \
            if (size == 16 && (index_align & 0b10)) {                   \
                /* double spacing */                                    \
                disassem->reglist |= (1 << (vd+2));                     \
            } if (size == 32 && (index_align & 0b100)) {                \
                /* double spacing */                                    \
                disassem->reglist |= (1 << (vd+2));                     \
            } else {                                                    \
                /* single spacing */                                    \
                disassem->reglist |= (1 << (vd+1));                     \
            }                                                           \
        } else if (opcode == 2) {                                       \
            /* VLD3, VST3 */                                            \
            disassem->vldx_vstx_num = 3;                                \
                                                                        \
            if (size == 16 && ((index_align & 0b11) == 0b10)) {         \
                /* double spacing */                                    \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+4));                     \
            } else if (size == 32 && ((index_align & 0b111) == 0b100)) { \
                /* double spacing */                                    \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+4));                     \
            } else {                                                    \
                /* single spacing */                                    \
                disassem->reglist |= (1 << (vd+1));                     \
                disassem->reglist |= (1 << (vd+2));                     \
            }                                                           \
        } else if (opcode == 3) {                                       \
            /* VLD4, VLD4 */                                            \
            disassem->vldx_vstx_num = 4;                                \
                                                                        \
            if (size == 16 && (index_align & 0b10)) {                   \
                /* double spacing */                                    \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+4));                     \
                disassem->reglist |= (1 << (vd+6));                     \
            } if (size == 32 && (index_align & 0b100)) {                \
                /* double spacing */                                    \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+4));                     \
                disassem->reglist |= (1 << (vd+6));                     \
            } else {                                                    \
                /* single spacing */                                    \
                disassem->reglist |= (1 << (vd+1));                     \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+3));                     \
            }                                                           \
        }                                                               \
                                                                        \
        disassem->Rn.reg = rn;                                          \
        if (rm == 15 || rm == 13) {                                     \
            /* Rm is not specified */                                   \
            disassem->format = INS_V_REGLIST_RN;                        \
        } else {                                                        \
            disassem->Rm.reg = rm;                                      \
            disassem->format = INS_V_REGLIST_RN_RM;                     \
        }                                                               \
    }

#define TRANSLATE_NEON_VLDX_S_A {                                       \
        uint32_t opcode, size, d, vd, inc, rn, align, rm;               \
        arm_neon_vldx_s_a_decode_fields(read_address, &opcode, &size, &d, &vd, &inc, &rn, &align, &rm); \
                                                                        \
        vd = (d << 4) + vd;                                             \
        disassem->reglist = (1 << vd);                                  \
        disassem->v_elem_size = size;                                   \
                                                                        \
        if (opcode == 0) {                                              \
            /* VLD1 */                                                  \
            disassem->vldx_vstx_num = 1;                                \
                                                                        \
            if (inc == 1) {                                             \
                disassem->reglist |= (1 << (vd+1));                     \
            }                                                           \
        } else if (opcode == 1) {                                       \
            /* VLD2 */                                                  \
            disassem->vldx_vstx_num = 2;                                \
                                                                        \
            if (inc == 0) {                                             \
                disassem->reglist |= (1 << (vd+1));                     \
            } else if (inc == 1) {                                      \
                disassem->reglist |= (1 << (vd+2));                     \
            }                                                           \
        } else if (opcode == 2) {                                       \
            /* VLD3 */                                                  \
            disassem->vldx_vstx_num = 3;                                \
                                                                        \
            if (inc == 0) {                                             \
                disassem->reglist |= (1 << (vd+1));                     \
                disassem->reglist |= (1 << (vd+2));                     \
            } else if (inc == 1) {                                      \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+4));                     \
            }                                                           \
        } else if (opcode == 3) {                                       \
            /* VLD4 */                                                  \
            disassem->vldx_vstx_num = 4;                                \
                                                                        \
            if (inc == 0) {                                             \
                disassem->reglist |= (1 << (vd+1));                     \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+3));                     \
            } else if (inc == 1) {                                      \
                disassem->reglist |= (1 << (vd+2));                     \
                disassem->reglist |= (1 << (vd+4));                     \
                disassem->reglist |= (1 << (vd+6));                     \
            }                                                           \
        }                                                               \
                                                                        \
        disassem->Rn.reg = rn;                                          \
        if (rm == 15 || rm == 13) {                                     \
            /* Rm is not specified */                                   \
            disassem->format = INS_V_REGLIST_RN;                        \
        } else {                                                        \
            disassem->Rm.reg = rm;                                      \
            disassem->format = INS_V_REGLIST_RN_RM;                     \
        }                                                               \
    }

#define TRANSLATE_NEON_VOPS_VD_VN_VM {                                  \
        uint32_t opc1, opc2, opc3, opc4, opc5, opc6, size, q, d, vd, n, vn, m, vm; \
        arm_vops_decode_fields(read_address, &opc1, &opc2, &opc3, &opc4, &opc5, &opc6, &size, &q, &d, &vd, &n, &vn, &m, &vm); \
                                                                        \
        vd = (d << 4) + vd;                                             \
        vn = (n << 4) + vn;                                             \
        vm = (m << 4) + vm;                                             \
                                                                        \
        disassem->Vd.reg = vd;                                          \
        disassem->Vn.reg = vn;                                          \
        disassem->Vm.reg = vm;                                          \
                                                                        \
        if (opc2 == 0) {                                                \
            disassem->Vd.q_reg = q;                                     \
            disassem->Vn.q_reg = q;                                     \
            disassem->Vm.q_reg = q;                                     \
        } else if (opc2 == 1) {                                         \
            /* Registers are not the same size. This generally means */ \
            /* Vd is a quad-word register but there are exceptions */   \
            /* such as in VADDHN or VADDW. */                           \
            disassem->Vd.q_reg = 1;                                     \
        }                                                               \
                                                                        \
        disassem->format = INS_V_VD_VN_VM;                              \
    }

#define TRANSLATE_NEON_VOPS_VD_VM {                                     \
        uint32_t opc1, opc2, opc3, opc4, opc5, size, d, vd, q, m, vm;   \
        arm_v_ops_2reg_decode_fields(read_address, &opc1, &opc2, &opc3, &opc4, &opc5, &size, &d, &vd, &q, &m, &vm); \
                                                                        \
        vd = (d << 4) + vd;                                             \
        vm = (m << 4) + vm;                                             \
                                                                        \
        disassem->Vd.reg = vd;                                          \
        disassem->Vm.reg = vm;                                          \
                                                                        \
        disassem->Vd.q_reg = q;                                         \
        disassem->Vm.q_reg = q;                                         \
                                                                        \
        disassem->format = INS_V_VD_VM;                                 \
    }

#define TRANSLATE_VFP_VD_VM {                                           \
        uint32_t d, opc2, vd, sz, op, m, vm;                            \
        arm_vfp_data_proc_decode_fields(read_address, &d, &opc2, &vd, &sz, &op, &m, &vm); \
                                                                        \
        if (sz == 1) {                                                  \
            vd = (d << 4) + vd;                                         \
            vm = (m << 4) + vm;                                         \
        } else {                                                        \
            disassem->Vd.s_reg = true;                                  \
            disassem->Vm.s_reg = true;                                  \
        }                                                               \
                                                                        \
        disassem->Vd.reg = vd;                                          \
        disassem->Vm.reg = vm;                                          \
                                                                        \
        disassem->format = INS_V_VD_VM;                                 \
    }

#define TRANSLATE_VFP_VD_VN_VM {                                        \
        uint32_t opc1, d, opc2, vn, vd, opc3, sz, n, m, vm;             \
        arm_vfp_3reg_decode_fields(read_address, &opc1, &d, &opc2, &vn, &vd, &opc3, &sz, &n, &m, &vm); \
                                                                        \
        if (sz == 1) {                                                  \
            vd = (d << 4) + vd;                                         \
            vn = (n << 4) + vn;                                         \
            vm = (m << 4) + vm;                                         \
        } else {                                                        \
            disassem->Vd.s_reg = true;                                  \
            disassem->Vn.s_reg = true;                                  \
            disassem->Vm.s_reg = true;                                  \
        }                                                               \
                                                                        \
        disassem->Vd.reg = vd;                                          \
        disassem->Vn.reg = vn;                                          \
        disassem->Vm.reg = vm;                                          \
                                                                        \
        disassem->format = INS_V_VD_VN_VM;                              \
    }
