#ifndef INCLUDE_EMU_PROFILER_H
#define INCLUDE_EMU_PROFILER_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/syscall.h>

#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

double time_ms();
uint64_t getticks();

int time_ns(struct timespec *res);
int64_t diff_ns(struct timespec *start,
                struct timespec *end);
int64_t ns_to_cycles(int64_t ns);


#ifndef EMU_BENCH
#define BENCH(...) (void)(NULL)
#else
#define BENCH(line) line
#endif

#endif // INCLUDE_EMU_PROFILER_H
