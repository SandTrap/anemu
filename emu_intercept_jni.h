extern void GetVersion_trampoline();

extern void DefineClass_trampoline();
extern void FindClass_trampoline();

extern void FromReflectedMethod_trampoline();
extern void FromReflectedField_trampoline();
extern void ToReflectedMethod_trampoline();

extern void GetSuperclass_trampoline();
extern void IsAssignableFrom_trampoline();

extern void ToReflectedField_trampoline();

extern void Throw_trampoline();
extern void ThrowNew_trampoline();
extern void ExceptionOccurred_trampoline();
extern void ExceptionDescribe_trampoline();
extern void ExceptionClear_trampoline();
extern void FatalError_trampoline();

extern void PushLocalFrame_trampoline();
extern void PopLocalFrame_trampoline();

extern void NewGlobalRef_trampoline();
extern void DeleteGlobalRef_trampoline();
extern void DeleteLocalRef_trampoline();
extern void IsSameObject_trampoline();
extern void NewLocalRef_trampoline();
extern void EnsureLocalCapacity_trampoline();

extern void AllocObject_trampoline();
extern void NewObject_trampoline();
extern void NewObjectV_trampoline();
extern void NewObjectA_trampoline();

extern void GetObjectClass_trampoline();
extern void IsInstanceOf_trampoline();

extern void GetMethodID_trampoline();

extern void CallObjectMethod_trampoline();
extern void CallObjectMethodV_trampoline();
extern void CallObjectMethodA_trampoline();
extern void CallBooleanMethod_trampoline();
extern void CallBooleanMethodV_trampoline();
extern void CallBooleanMethodA_trampoline();
extern void CallByteMethod_trampoline();
extern void CallByteMethodV_trampoline();
extern void CallByteMethodA_trampoline();
extern void CallCharMethod_trampoline();
extern void CallCharMethodV_trampoline();
extern void CallCharMethodA_trampoline();
extern void CallShortMethod_trampoline();
extern void CallShortMethodV_trampoline();
extern void CallShortMethodA_trampoline();
extern void CallIntMethod_trampoline();
extern void CallIntMethodV_trampoline();
extern void CallIntMethodA_trampoline();
extern void CallLongMethod_trampoline();
extern void CallLongMethodV_trampoline();
extern void CallLongMethodA_trampoline();
extern void CallFloatMethod_trampoline();
extern void CallFloatMethodV_trampoline();
extern void CallFloatMethodA_trampoline();
extern void CallDoubleMethod_trampoline();
extern void CallDoubleMethodV_trampoline();
extern void CallDoubleMethodA_trampoline();
extern void CallVoidMethod_trampoline();
extern void CallVoidMethodV_trampoline();
extern void CallVoidMethodA_trampoline();

extern void CallNonvirtualObjectMethod_trampoline();
extern void CallNonvirtualObjectMethodV_trampoline();
extern void CallNonvirtualObjectMethodA_trampoline();
extern void CallNonvirtualBooleanMethod_trampoline();
extern void CallNonvirtualBooleanMethodV_trampoline();
extern void CallNonvirtualBooleanMethodA_trampoline();
extern void CallNonvirtualByteMethod_trampoline();
extern void CallNonvirtualByteMethodV_trampoline();
extern void CallNonvirtualByteMethodA_trampoline();
extern void CallNonvirtualCharMethod_trampoline();
extern void CallNonvirtualCharMethodV_trampoline();
extern void CallNonvirtualCharMethodA_trampoline();
extern void CallNonvirtualShortMethod_trampoline();
extern void CallNonvirtualShortMethodV_trampoline();
extern void CallNonvirtualShortMethodA_trampoline();
extern void CallNonvirtualIntMethod_trampoline();
extern void CallNonvirtualIntMethodV_trampoline();
extern void CallNonvirtualIntMethodA_trampoline();
extern void CallNonvirtualLongMethod_trampoline();
extern void CallNonvirtualLongMethodV_trampoline();
extern void CallNonvirtualLongMethodA_trampoline();
extern void CallNonvirtualFloatMethod_trampoline();
extern void CallNonvirtualFloatMethodV_trampoline();
extern void CallNonvirtualFloatMethodA_trampoline();
extern void CallNonvirtualDoubleMethod_trampoline();
extern void CallNonvirtualDoubleMethodV_trampoline();
extern void CallNonvirtualDoubleMethodA_trampoline();
extern void CallNonvirtualVoidMethod_trampoline();
extern void CallNonvirtualVoidMethodV_trampoline();
extern void CallNonvirtualVoidMethodA_trampoline();

extern void GetFieldID_trampoline();

extern void GetObjectField_trampoline();
extern void GetBooleanField_trampoline();
extern void GetByteField_trampoline();
extern void GetCharField_trampoline();
extern void GetShortField_trampoline();
extern void GetIntField_trampoline();
extern void GetLongField_trampoline();
extern void GetFloatField_trampoline();
extern void GetDoubleField_trampoline();
extern void SetObjectField_trampoline();
extern void SetBooleanField_trampoline();
extern void SetByteField_trampoline();
extern void SetCharField_trampoline();
extern void SetShortField_trampoline();
extern void SetIntField_trampoline();
extern void SetLongField_trampoline();
extern void SetFloatField_trampoline();
extern void SetDoubleField_trampoline();

extern void GetStaticMethodID_trampoline();

extern void CallStaticObjectMethod_trampoline();
extern void CallStaticObjectMethodV_trampoline();
extern void CallStaticObjectMethodA_trampoline();
extern void CallStaticBooleanMethod_trampoline();
extern void CallStaticBooleanMethodV_trampoline();
extern void CallStaticBooleanMethodA_trampoline();
extern void CallStaticByteMethod_trampoline();
extern void CallStaticByteMethodV_trampoline();
extern void CallStaticByteMethodA_trampoline();
extern void CallStaticCharMethod_trampoline();
extern void CallStaticCharMethodV_trampoline();
extern void CallStaticCharMethodA_trampoline();
extern void CallStaticShortMethod_trampoline();
extern void CallStaticShortMethodV_trampoline();
extern void CallStaticShortMethodA_trampoline();
extern void CallStaticIntMethod_trampoline();
extern void CallStaticIntMethodV_trampoline();
extern void CallStaticIntMethodA_trampoline();
extern void CallStaticLongMethod_trampoline();
extern void CallStaticLongMethodV_trampoline();
extern void CallStaticLongMethodA_trampoline();
extern void CallStaticFloatMethod_trampoline();
extern void CallStaticFloatMethodV_trampoline();
extern void CallStaticFloatMethodA_trampoline();
extern void CallStaticDoubleMethod_trampoline();
extern void CallStaticDoubleMethodV_trampoline();
extern void CallStaticDoubleMethodA_trampoline();
extern void CallStaticVoidMethod_trampoline();
extern void CallStaticVoidMethodV_trampoline();
extern void CallStaticVoidMethodA_trampoline();

extern void GetStaticFieldID_trampoline();

extern void GetStaticObjectField_trampoline();
extern void GetStaticBooleanField_trampoline();
extern void GetStaticByteField_trampoline();
extern void GetStaticCharField_trampoline();
extern void GetStaticShortField_trampoline();
extern void GetStaticIntField_trampoline();
extern void GetStaticLongField_trampoline();
extern void GetStaticFloatField_trampoline();
extern void GetStaticDoubleField_trampoline();

extern void SetStaticObjectField_trampoline();
extern void SetStaticBooleanField_trampoline();
extern void SetStaticByteField_trampoline();
extern void SetStaticCharField_trampoline();
extern void SetStaticShortField_trampoline();
extern void SetStaticIntField_trampoline();
extern void SetStaticLongField_trampoline();
extern void SetStaticFloatField_trampoline();
extern void SetStaticDoubleField_trampoline();

extern void NewString_trampoline();

extern void GetStringLength_trampoline();
extern void GetStringChars_trampoline();
extern void ReleaseStringChars_trampoline();

extern void NewStringUTF_trampoline();
extern void GetStringUTFLength_trampoline();
extern void GetStringUTFChars_trampoline();
extern void ReleaseStringUTFChars_trampoline();

extern void GetArrayLength_trampoline();
extern void NewObjectArray_trampoline();
extern void GetObjectArrayElement_trampoline();
extern void SetObjectArrayElement_trampoline();

extern void NewBooleanArray_trampoline();
extern void NewByteArray_trampoline();
extern void NewCharArray_trampoline();
extern void NewShortArray_trampoline();
extern void NewIntArray_trampoline();
extern void NewLongArray_trampoline();
extern void NewFloatArray_trampoline();
extern void NewDoubleArray_trampoline();

extern void GetBooleanArrayElements_trampoline();
extern void GetByteArrayElements_trampoline();
extern void GetCharArrayElements_trampoline();
extern void GetShortArrayElements_trampoline();
extern void GetIntArrayElements_trampoline();
extern void GetLongArrayElements_trampoline();
extern void GetFloatArrayElements_trampoline();
extern void GetDoubleArrayElements_trampoline();

extern void ReleaseBooleanArrayElements_trampoline();
extern void ReleaseByteArrayElements_trampoline();
extern void ReleaseCharArrayElements_trampoline();
extern void ReleaseShortArrayElements_trampoline();
extern void ReleaseIntArrayElements_trampoline();
extern void ReleaseLongArrayElements_trampoline();
extern void ReleaseFloatArrayElements_trampoline();
extern void ReleaseDoubleArrayElements_trampoline();

extern void GetBooleanArrayRegion_trampoline();
extern void GetByteArrayRegion_trampoline();
extern void GetCharArrayRegion_trampoline();
extern void GetShortArrayRegion_trampoline();
extern void GetIntArrayRegion_trampoline();
extern void GetLongArrayRegion_trampoline();
extern void GetFloatArrayRegion_trampoline();
extern void GetDoubleArrayRegion_trampoline();
extern void SetBooleanArrayRegion_trampoline();
extern void SetByteArrayRegion_trampoline();
extern void SetCharArrayRegion_trampoline();
extern void SetShortArrayRegion_trampoline();
extern void SetIntArrayRegion_trampoline();
extern void SetLongArrayRegion_trampoline();
extern void SetFloatArrayRegion_trampoline();
extern void SetDoubleArrayRegion_trampoline();

extern void RegisterNatives_trampoline();
extern void UnregisterNatives_trampoline();

extern void MonitorEnter_trampoline();
extern void MonitorExit_trampoline();

extern void GetJavaVM_trampoline();

extern void GetStringRegion_trampoline();
extern void GetStringUTFRegion_trampoline();

extern void GetPrimitiveArrayCritical_trampoline();
extern void ReleasePrimitiveArrayCritical_trampoline();

extern void GetStringCritical_trampoline();
extern void ReleaseStringCritical_trampoline();

extern void NewWeakGlobalRef_trampoline();
extern void DeleteWeakGlobalRef_trampoline();

extern void ExceptionCheck_trampoline();

extern void NewDirectByteBuffer_trampoline();
extern void GetDirectBufferAddress_trampoline();
extern void GetDirectBufferCapacity_trampoline();

extern void GetObjectRefType_trampoline();


#define JNI_INTERCEPT_HALT(METHOD) {                                    \
        method_sym = dlsym(lib_handle, "method_"#METHOD);               \
                                                                        \
        intercept_func = calloc(1, sizeof(emu_intercept_func_t));       \
        intercept_func->name = #METHOD;                                 \
        intercept_func->id = METHOD##_ID;                               \
        intercept_func->orig_func_addr = *(uint32_t*)method_sym;        \
        intercept_func->trampoline_read_addr = 0;                       \
                                                                        \
        HASH_ADD_INT(*intercept_tbl, orig_func_addr, intercept_func);   \
    }

#define JNI_INTERCEPT_DIRECT(METHOD) {                                  \
        method_sym = dlsym(lib_handle, "method_"#METHOD);               \
                                                                        \
        intercept_func = calloc(1, sizeof(emu_intercept_func_t));       \
        intercept_func->name = #METHOD;                                 \
        intercept_func->id = METHOD##_ID;                               \
        intercept_func->orig_func_addr = *(uint32_t*)method_sym;        \
        intercept_func->trampoline_read_addr = (uint32_t)&METHOD##_trampoline; \
        intercept_func->target_func_addr = *(uint32_t*)method_sym;      \
                                                                        \
        HASH_ADD_INT(*intercept_tbl, orig_func_addr, intercept_func);   \
    }

#define JNI_INTERCEPT_REDIRECT_TARGET(ORIGMETHOD, TARGETMETHOD, STARTSTACKARG) { \
        intercept_func = calloc(1, sizeof(emu_intercept_func_t));       \
        intercept_func->name = #ORIGMETHOD;                             \
        intercept_func->id = ORIGMETHOD##_ID;                           \
                                                                        \
        method_sym = dlsym(lib_handle, "method_"#ORIGMETHOD);           \
        intercept_func->orig_func_addr = *(uint32_t*)method_sym;        \
        intercept_func->trampoline_read_addr = (uint32_t)&ORIGMETHOD##_trampoline; \
        method_sym = dlsym(lib_handle, "method_"#TARGETMETHOD);         \
        intercept_func->target_func_addr = *(uint32_t*)method_sym;      \
                                                                        \
        intercept_func->put_args_in_stack = true;                       \
        intercept_func->start_stack_arg = STARTSTACKARG;                \
                                                                        \
        HASH_ADD_INT(*intercept_tbl, orig_func_addr, intercept_func);   \
    }

#define JNI_INTERCEPT_ALL {                                             \
        JNI_INTERCEPT_DIRECT(GetVersion);                               \
                                                                        \
        JNI_INTERCEPT_DIRECT(DefineClass);                              \
        JNI_INTERCEPT_DIRECT(FindClass);                                \
                                                                        \
        JNI_INTERCEPT_DIRECT(FromReflectedMethod);                      \
        JNI_INTERCEPT_DIRECT(FromReflectedField);                       \
        JNI_INTERCEPT_DIRECT(ToReflectedMethod);                        \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetSuperclass);                            \
        JNI_INTERCEPT_DIRECT(IsAssignableFrom);                         \
                                                                        \
        JNI_INTERCEPT_DIRECT(ToReflectedField);                         \
                                                                        \
        JNI_INTERCEPT_DIRECT(Throw);                                    \
        JNI_INTERCEPT_DIRECT(ThrowNew);                                 \
        JNI_INTERCEPT_DIRECT(ExceptionOccurred);                        \
        JNI_INTERCEPT_DIRECT(ExceptionDescribe);                        \
        JNI_INTERCEPT_DIRECT(ExceptionClear);                           \
        JNI_INTERCEPT_DIRECT(FatalError);                               \
                                                                        \
        JNI_INTERCEPT_DIRECT(PushLocalFrame);                           \
        JNI_INTERCEPT_DIRECT(PopLocalFrame);                            \
                                                                        \
        JNI_INTERCEPT_DIRECT(NewGlobalRef);                             \
        JNI_INTERCEPT_DIRECT(DeleteGlobalRef);                          \
        JNI_INTERCEPT_DIRECT(DeleteLocalRef);                           \
        JNI_INTERCEPT_DIRECT(IsSameObject);                             \
        JNI_INTERCEPT_DIRECT(NewLocalRef);                              \
        JNI_INTERCEPT_DIRECT(EnsureLocalCapacity);                      \
                                                                        \
        JNI_INTERCEPT_DIRECT(AllocObject);                              \
        JNI_INTERCEPT_REDIRECT_TARGET(NewObject, NewObjectExtraArg, 3); \
        JNI_INTERCEPT_DIRECT(NewObjectV);                               \
        JNI_INTERCEPT_HALT(NewObjectA);                                 \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetObjectClass);                           \
        JNI_INTERCEPT_HALT(IsInstanceOf);                               \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetMethodID);                              \
                                                                        \
        JNI_INTERCEPT_REDIRECT_TARGET(CallObjectMethod, CallObjectMethodExtraArg, 3); \
        JNI_INTERCEPT_DIRECT(CallObjectMethodV);                        \
        JNI_INTERCEPT_HALT(CallObjectMethodA);                          \
        JNI_INTERCEPT_HALT(CallBooleanMethod);                          \
        JNI_INTERCEPT_DIRECT(CallBooleanMethodV);                       \
        JNI_INTERCEPT_HALT(CallBooleanMethodA);                         \
        JNI_INTERCEPT_HALT(CallByteMethod);                             \
        JNI_INTERCEPT_HALT(CallByteMethodV);                            \
        JNI_INTERCEPT_HALT(CallByteMethodA);                            \
        JNI_INTERCEPT_HALT(CallCharMethod);                             \
        JNI_INTERCEPT_HALT(CallCharMethodV);                            \
        JNI_INTERCEPT_HALT(CallCharMethodA);                            \
        JNI_INTERCEPT_HALT(CallShortMethod);                            \
        JNI_INTERCEPT_HALT(CallShortMethodV);                           \
        JNI_INTERCEPT_HALT(CallShortMethodA);                           \
        JNI_INTERCEPT_HALT(CallIntMethod);                              \
        JNI_INTERCEPT_DIRECT(CallIntMethodV);                           \
        JNI_INTERCEPT_HALT(CallIntMethodA);                             \
        JNI_INTERCEPT_HALT(CallLongMethod);                             \
        JNI_INTERCEPT_DIRECT(CallLongMethodV);                          \
        JNI_INTERCEPT_HALT(CallLongMethodA);                            \
        JNI_INTERCEPT_HALT(CallFloatMethod);                            \
        JNI_INTERCEPT_HALT(CallFloatMethodV);                           \
        JNI_INTERCEPT_HALT(CallFloatMethodA);                           \
        JNI_INTERCEPT_HALT(CallDoubleMethod);                           \
        JNI_INTERCEPT_HALT(CallDoubleMethodV);                          \
        JNI_INTERCEPT_HALT(CallDoubleMethodA);                          \
        JNI_INTERCEPT_HALT(CallVoidMethod);                             \
        JNI_INTERCEPT_DIRECT(CallVoidMethodV);                          \
        JNI_INTERCEPT_HALT(CallVoidMethodA);                            \
                                                                        \
        JNI_INTERCEPT_HALT(CallNonvirtualObjectMethod);                 \
        JNI_INTERCEPT_HALT(CallNonvirtualObjectMethodV);                \
        JNI_INTERCEPT_HALT(CallNonvirtualObjectMethodA);                \
        JNI_INTERCEPT_HALT(CallNonvirtualBooleanMethod);                \
        JNI_INTERCEPT_HALT(CallNonvirtualBooleanMethodV);               \
        JNI_INTERCEPT_HALT(CallNonvirtualBooleanMethodA);               \
        JNI_INTERCEPT_HALT(CallNonvirtualByteMethod);                   \
        JNI_INTERCEPT_HALT(CallNonvirtualByteMethodV);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualByteMethodA);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualCharMethod);                   \
        JNI_INTERCEPT_HALT(CallNonvirtualCharMethodV);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualCharMethodA);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualShortMethod);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualShortMethodV);                 \
        JNI_INTERCEPT_HALT(CallNonvirtualShortMethodA);                 \
        JNI_INTERCEPT_HALT(CallNonvirtualIntMethod);                    \
        JNI_INTERCEPT_HALT(CallNonvirtualIntMethodV);                   \
        JNI_INTERCEPT_HALT(CallNonvirtualIntMethodA);                   \
        JNI_INTERCEPT_HALT(CallNonvirtualLongMethod);                   \
        JNI_INTERCEPT_HALT(CallNonvirtualLongMethodV);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualLongMethodA);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualFloatMethod);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualFloatMethodV);                 \
        JNI_INTERCEPT_HALT(CallNonvirtualFloatMethodA);                 \
        JNI_INTERCEPT_HALT(CallNonvirtualDoubleMethod);                 \
        JNI_INTERCEPT_HALT(CallNonvirtualDoubleMethodV);                \
        JNI_INTERCEPT_HALT(CallNonvirtualDoubleMethodA);                \
        JNI_INTERCEPT_HALT(CallNonvirtualVoidMethod);                   \
        JNI_INTERCEPT_HALT(CallNonvirtualVoidMethodV);                  \
        JNI_INTERCEPT_HALT(CallNonvirtualVoidMethodA);                  \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetFieldID);                               \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetObjectField);                           \
        JNI_INTERCEPT_DIRECT(GetBooleanField);                          \
        JNI_INTERCEPT_DIRECT(GetByteField);                             \
        JNI_INTERCEPT_DIRECT(GetCharField);                             \
        JNI_INTERCEPT_DIRECT(GetShortField);                            \
        JNI_INTERCEPT_DIRECT(GetIntField);                              \
        JNI_INTERCEPT_DIRECT(GetLongField);                             \
        JNI_INTERCEPT_DIRECT(GetFloatField);                            \
        JNI_INTERCEPT_DIRECT(GetDoubleField);                           \
        JNI_INTERCEPT_DIRECT(SetObjectField);                           \
        JNI_INTERCEPT_DIRECT(SetBooleanField);                          \
        JNI_INTERCEPT_DIRECT(SetByteField);                             \
        JNI_INTERCEPT_DIRECT(SetCharField);                             \
        JNI_INTERCEPT_DIRECT(SetShortField);                            \
        JNI_INTERCEPT_DIRECT(SetIntField);                              \
        JNI_INTERCEPT_DIRECT(SetLongField);                             \
        JNI_INTERCEPT_DIRECT(SetFloatField);                            \
        JNI_INTERCEPT_DIRECT(SetDoubleField);                           \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetStaticMethodID);                        \
                                                                        \
        JNI_INTERCEPT_HALT(CallStaticObjectMethod);                     \
        JNI_INTERCEPT_DIRECT(CallStaticObjectMethodV);                  \
        JNI_INTERCEPT_HALT(CallStaticObjectMethodA);                    \
        JNI_INTERCEPT_HALT(CallStaticBooleanMethod);                    \
        JNI_INTERCEPT_HALT(CallStaticBooleanMethodV);                   \
        JNI_INTERCEPT_HALT(CallStaticBooleanMethodA);                   \
        JNI_INTERCEPT_HALT(CallStaticByteMethod);                       \
        JNI_INTERCEPT_HALT(CallStaticByteMethodV);                      \
        JNI_INTERCEPT_HALT(CallStaticByteMethodA);                      \
        JNI_INTERCEPT_HALT(CallStaticCharMethod);                       \
        JNI_INTERCEPT_HALT(CallStaticCharMethodV);                      \
        JNI_INTERCEPT_HALT(CallStaticCharMethodA);                      \
        JNI_INTERCEPT_HALT(CallStaticShortMethod);                      \
        JNI_INTERCEPT_HALT(CallStaticShortMethodV);                     \
        JNI_INTERCEPT_HALT(CallStaticShortMethodA);                     \
        JNI_INTERCEPT_HALT(CallStaticIntMethod);                        \
        JNI_INTERCEPT_HALT(CallStaticIntMethodV);                       \
        JNI_INTERCEPT_HALT(CallStaticIntMethodA);                       \
        JNI_INTERCEPT_HALT(CallStaticLongMethod);                       \
        JNI_INTERCEPT_HALT(CallStaticLongMethodV);                      \
        JNI_INTERCEPT_HALT(CallStaticLongMethodA);                      \
        JNI_INTERCEPT_HALT(CallStaticFloatMethod);                      \
        JNI_INTERCEPT_HALT(CallStaticFloatMethodV);                     \
        JNI_INTERCEPT_HALT(CallStaticFloatMethodA);                     \
        JNI_INTERCEPT_HALT(CallStaticDoubleMethod);                     \
        JNI_INTERCEPT_HALT(CallStaticDoubleMethodV);                    \
        JNI_INTERCEPT_HALT(CallStaticDoubleMethodA);                    \
        JNI_INTERCEPT_HALT(CallStaticVoidMethod);                       \
        JNI_INTERCEPT_HALT(CallStaticVoidMethodV);                      \
        JNI_INTERCEPT_HALT(CallStaticVoidMethodA);                      \
                                                                        \
        JNI_INTERCEPT_HALT(GetStaticFieldID);                           \
                                                                        \
        JNI_INTERCEPT_HALT(GetStaticObjectField);                       \
        JNI_INTERCEPT_HALT(GetStaticBooleanField);                      \
        JNI_INTERCEPT_HALT(GetStaticByteField);                         \
        JNI_INTERCEPT_HALT(GetStaticCharField);                         \
        JNI_INTERCEPT_HALT(GetStaticShortField);                        \
        JNI_INTERCEPT_HALT(GetStaticIntField);                          \
        JNI_INTERCEPT_HALT(GetStaticLongField);                         \
        JNI_INTERCEPT_HALT(GetStaticFloatField);                        \
        JNI_INTERCEPT_HALT(GetStaticDoubleField);                       \
                                                                        \
        JNI_INTERCEPT_HALT(SetStaticObjectField);                       \
        JNI_INTERCEPT_HALT(SetStaticBooleanField);                      \
        JNI_INTERCEPT_HALT(SetStaticByteField);                         \
        JNI_INTERCEPT_HALT(SetStaticCharField);                         \
        JNI_INTERCEPT_HALT(SetStaticShortField);                        \
        JNI_INTERCEPT_HALT(SetStaticIntField);                          \
        JNI_INTERCEPT_HALT(SetStaticLongField);                         \
        JNI_INTERCEPT_HALT(SetStaticFloatField);                        \
        JNI_INTERCEPT_HALT(SetStaticDoubleField);                       \
                                                                        \
        JNI_INTERCEPT_DIRECT(NewString);                                \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetStringLength);                          \
        JNI_INTERCEPT_DIRECT(GetStringChars);                           \
        JNI_INTERCEPT_DIRECT(ReleaseStringChars);                       \
                                                                        \
        JNI_INTERCEPT_DIRECT(NewStringUTF);                             \
        JNI_INTERCEPT_HALT(GetStringUTFLength);                         \
        JNI_INTERCEPT_DIRECT(GetStringUTFChars);                        \
        JNI_INTERCEPT_DIRECT(ReleaseStringUTFChars);                    \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetArrayLength);                           \
        JNI_INTERCEPT_HALT(NewObjectArray);                             \
        JNI_INTERCEPT_DIRECT(GetObjectArrayElement);                    \
        JNI_INTERCEPT_HALT(SetObjectArrayElement);                      \
                                                                        \
        JNI_INTERCEPT_HALT(NewBooleanArray);                            \
        JNI_INTERCEPT_DIRECT(NewByteArray);                             \
        JNI_INTERCEPT_HALT(NewCharArray);                               \
        JNI_INTERCEPT_HALT(NewShortArray);                              \
        JNI_INTERCEPT_HALT(NewIntArray);                                \
        JNI_INTERCEPT_DIRECT(NewLongArray);                             \
        JNI_INTERCEPT_HALT(NewFloatArray);                              \
        JNI_INTERCEPT_HALT(NewDoubleArray);                             \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetBooleanArrayElements);                  \
        JNI_INTERCEPT_DIRECT(GetByteArrayElements);                     \
        JNI_INTERCEPT_DIRECT(GetCharArrayElements);                     \
        JNI_INTERCEPT_DIRECT(GetShortArrayElements);                    \
        JNI_INTERCEPT_DIRECT(GetIntArrayElements);                      \
        JNI_INTERCEPT_DIRECT(GetLongArrayElements);                     \
        JNI_INTERCEPT_DIRECT(GetFloatArrayElements);                    \
        JNI_INTERCEPT_DIRECT(GetDoubleArrayElements);                   \
                                                                        \
        JNI_INTERCEPT_DIRECT(ReleaseBooleanArrayElements);              \
        JNI_INTERCEPT_DIRECT(ReleaseByteArrayElements);                 \
        JNI_INTERCEPT_DIRECT(ReleaseCharArrayElements);                 \
        JNI_INTERCEPT_DIRECT(ReleaseShortArrayElements);                \
        JNI_INTERCEPT_DIRECT(ReleaseIntArrayElements);                  \
        JNI_INTERCEPT_DIRECT(ReleaseLongArrayElements);                 \
        JNI_INTERCEPT_DIRECT(ReleaseFloatArrayElements);                \
        JNI_INTERCEPT_DIRECT(ReleaseDoubleArrayElements);               \
                                                                        \
        JNI_INTERCEPT_HALT(GetBooleanArrayRegion);                      \
        JNI_INTERCEPT_DIRECT(GetByteArrayRegion);                       \
        JNI_INTERCEPT_HALT(GetCharArrayRegion);                         \
        JNI_INTERCEPT_HALT(GetShortArrayRegion);                        \
        JNI_INTERCEPT_HALT(GetIntArrayRegion);                          \
        JNI_INTERCEPT_HALT(GetLongArrayRegion);                         \
        JNI_INTERCEPT_HALT(GetFloatArrayRegion);                        \
        JNI_INTERCEPT_HALT(GetDoubleArrayRegion);                       \
        JNI_INTERCEPT_HALT(SetBooleanArrayRegion);                      \
        JNI_INTERCEPT_DIRECT(SetByteArrayRegion);                       \
        JNI_INTERCEPT_HALT(SetCharArrayRegion);                         \
        JNI_INTERCEPT_HALT(SetShortArrayRegion);                        \
        JNI_INTERCEPT_HALT(SetIntArrayRegion);                          \
        JNI_INTERCEPT_DIRECT(SetLongArrayRegion);                       \
        JNI_INTERCEPT_HALT(SetFloatArrayRegion);                        \
        JNI_INTERCEPT_HALT(SetDoubleArrayRegion);                       \
                                                                        \
        JNI_INTERCEPT_DIRECT(RegisterNatives);                          \
        JNI_INTERCEPT_HALT(UnregisterNatives);                          \
                                                                        \
        JNI_INTERCEPT_HALT(MonitorEnter);                               \
        JNI_INTERCEPT_HALT(MonitorExit);                                \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetJavaVM);                                \
                                                                        \
        JNI_INTERCEPT_HALT(GetStringRegion);                            \
        JNI_INTERCEPT_DIRECT(GetStringUTFRegion);                       \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetPrimitiveArrayCritical);                \
        JNI_INTERCEPT_DIRECT(ReleasePrimitiveArrayCritical);            \
                                                                        \
        JNI_INTERCEPT_DIRECT(GetStringCritical);                        \
        JNI_INTERCEPT_DIRECT(ReleaseStringCritical);                    \
                                                                        \
        JNI_INTERCEPT_DIRECT(NewWeakGlobalRef);                         \
        JNI_INTERCEPT_HALT(DeleteWeakGlobalRef);                        \
                                                                        \
        JNI_INTERCEPT_DIRECT(ExceptionCheck);                           \
                                                                        \
        JNI_INTERCEPT_DIRECT(NewDirectByteBuffer);                      \
        JNI_INTERCEPT_DIRECT(GetDirectBufferAddress);                   \
        JNI_INTERCEPT_HALT(GetDirectBufferCapacity);                    \
                                                                        \
        JNI_INTERCEPT_HALT(GetObjectRefType);                           \
    }
