#ifndef INCLUDE_EMU_EMULATE_H
#define INCLUDE_EMU_EMULATE_H

#include "emu_thread.h"

#define LSL(val, shift) (val << shift)
#define LSR(val, shift) (val >> shift)
#define ASR(val, shift) LSR((int32_t)val, shift)
#define ROR(val, rotate) (((val) >> (rotate)) | ((val) << (32 - (rotate))))

uint32_t emu_read_reg(emu_thread_t *emu, emu_arm_reg reg);
uint32_t *emu_write_reg(emu_thread_t *emu, emu_arm_reg reg);

/* read register by number */
#define RREGN(reg)  emu_read_reg(emu, reg)

/* read register by register specifier (e.g. Rd, Rm, Rn, Rt) */
#define RREG(Rx)   emu_read_reg(emu, d->Rx.reg)

bool is_cond_pass(emu_thread_t *emu, emu_arm_instr_cc cond);
void emu_go_to_address(emu_thread_t *emu, uint32_t address);

#endif // INCLUDE_EMU_EMULATE_H
