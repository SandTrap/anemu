#ifndef EMU_MAMBO_TAINT_PROP_H
#define EMU_MAMBO_TAINT_PROP_H

#include "mambo/dbm.h"
#include "mambo/plugins.h"
#include "emu_debug.h"
#include "emu_intercept.h"
#include "emu_thread.h"

void emu_taint_prop_plugin_register();
void emu_taint_prop_plugin_init(emu_thread_t *emu);
void emu_taint_prop_plugin_free(emu_thread_t *emu);
void emu_set_branch_inline(mambo_context *ctx, bool do_inline);
void emu_intercept_triggered(dbm_thread *thread_data, emu_intercept_func_t *intercept_func);

#endif // EMU_MAMBO_TAINT_PROP_H
