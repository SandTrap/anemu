#ifndef INCLUDE_EMU_MARKERS_H
#define INCLUDE_EMU_MARKERS_H

/* Lifted from $KERNEL/include/linux/stringify.h */
/* Indirect stringification.  Doing two levels allows the parameter to be a
 * macro itself.  For example, compile with -DFOO=bar, __stringify(FOO)
 * converts to "bar".
 */

#define __stringify_1(x...)	#x
#define __stringify(x...)	__stringify_1(x)

#define MARKER_START_VAL    32
#define MARKER_STOP_VAL     0xfdee /* udf 0xfdee : KGDB_BREAKINST */

/* Lifted from $KERNEL/arch/arm/kernel/ptrace.c */
/*
 * Breakpoint SWI instruction: SWI &9F0001
 */
#define BREAKINST_ARM_SWI	  0xef9f0001

/*
 * New breakpoints - use an undefined instruction.  The ARM architecture
 * reference manual guarantees that the following instruction space
 * will produce an undefined instruction exception on all CPUs:
 *
 *  ARM:   xxxx 0111 1111 xxxx xxxx xxxx 1111 xxxx
 *  Thumb: 1101 1110 xxxx xxxx
 */
#define BREAKINST_ARM	  0xe7f001f0 // udf #16 - SIGTRAP

#define GDB_BREAKINST   BREAKINST_ARM_SWI // SIGTRAP
#define KGDB_BREAKINST  0xe7ffdefe        // udf #0xfdee - SIGILL

/* #define MARKER_START  GDB_BREAKINST */
#define MARKER_START  BREAKINST_ARM
#define MARKER_STOP  KGDB_BREAKINST
#define MARKER_BYPASS GDB_BREAKINST

#define ASM(opcode)      asm volatile(".inst " __stringify(opcode))

#define EMU_MARKER_START ASM(MARKER_START) /* Generate SIGTRAP*/
#define EMU_MARKER_STOP  ASM(MARKER_STOP)  /* Generate SIGILL*/
#define EMU_MARKER_BYPASS ASM(MARKER_BYPASS) /* Generate SIGTRAP*/

#ifdef TEST_TOOLS
#define MARKER_END_TAINT_PROP 0xe7ffdeff // udf #0xfdef
#define EMU_MARKER_END_TAINT_PROP ASM(MARKER_END_TAINT_PROP)
#endif // TEST_TOOLS

/* As noted above, the undefined instruction (UDF) range for Thumb
 * starts from 0xdeXX, where XX may be substituted by any byte
 * value. This byte value also dictates the UDF number. For example
 * "0xde01" is "UDF 1". It turns out that "UDF 1" generates a SIGTRAP
 * while "UDF 0" generates a SIGILL. */
#define MARKER_START_THUMB 0xde01
#define MARKER_STOP_THUMB 0xde00
#define EMU_MARKER_START_THUMB ASM(MARKER_START_THUMB); /* Generate SIGTRAP */
#define EMU_MARKER_STOP_THUMB ASM(MARKER_STOP_THUMB); /* Generate SIGILL */

#endif // INCLUDE_EMU_MARKERS_H
