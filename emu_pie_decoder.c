#include "emu_debug.h"
#include "emu_thread.h"
#include "emu_pie_decoder.h"

#include "emu_pie_decoder_macros.h"

static char *inst_strings[] = {
  "EMU_INST_ADC",
  "EMU_INST_ADD",
  "EMU_INST_AND",
  "EMU_INST_B",
  "EMU_INST_BFC",
  "EMU_INST_BFI",
  "EMU_INST_BIC",
  "EMU_INST_BKPT",
  "EMU_INST_BL",
  "EMU_INST_BLX",
  "EMU_INST_BLXI",
  "EMU_INST_BX",
  "EMU_INST_CDP",
  "EMU_INST_CDP2",
  "EMU_INST_CLREX",
  "EMU_INST_CLZ",
  "EMU_INST_CMN",
  "EMU_INST_CMP",
  "EMU_INST_DBG",
  "EMU_INST_DMB",
  "EMU_INST_DSB",
  "EMU_INST_EOR",
  "EMU_INST_ISB",
  "EMU_INST_LDC",
  "EMU_INST_LDM",
  "EMU_INST_LDR",
  "EMU_INST_LDRB",
  "EMU_INST_LDRBT",
  "EMU_INST_LDRD",
  "EMU_INST_LDREX",
  "EMU_INST_LDREXB",
  "EMU_INST_LDREXD",
  "EMU_INST_LDREXH",
  "EMU_INST_LDRH",
  "EMU_INST_LDRHT",
  "EMU_INST_LDRSB",
  "EMU_INST_LDRSBT",
  "EMU_INST_LDRSH",
  "EMU_INST_LDRSHT",
  "EMU_INST_LDRT",
  "EMU_INST_MCR",
  "EMU_INST_MCRR",
  "EMU_INST_MLA",
  "EMU_INST_MLS",
  "EMU_INST_MOV",
  "EMU_INST_MOVT",
  "EMU_INST_MOVW",
  "EMU_INST_MRC",
  "EMU_INST_MRRC",
  "EMU_INST_MRS",
  "EMU_INST_MSR",
  "EMU_INST_MSRI",
  "EMU_INST_MUL",
  "EMU_INST_MVN",
  "EMU_INST_NOP",
  "EMU_INST_ORR",
  "EMU_INST_PKH",
  "EMU_INST_PLD",
  "EMU_INST_PLI",
  "EMU_INST_PLII",
  "EMU_INST_QADD",
  "EMU_INST_QADD16",
  "EMU_INST_QADD8",
  "EMU_INST_QASX",
  "EMU_INST_QDADD",
  "EMU_INST_QDSUB",
  "EMU_INST_QSAX",
  "EMU_INST_QSUB",
  "EMU_INST_QSUB16",
  "EMU_INST_QSUB8",
  "EMU_INST_RBIT",
  "EMU_INST_REV",
  "EMU_INST_REV16",
  "EMU_INST_REVSH",
  "EMU_INST_RRX",
  "EMU_INST_RSB",
  "EMU_INST_RSC",
  "EMU_INST_SADD16",
  "EMU_INST_SADD8",
  "EMU_INST_SASX",
  "EMU_INST_SBC",
  "EMU_INST_SBFX",
  "EMU_INST_SDIV",
  "EMU_INST_SEL",
  "EMU_INST_SETEND",
  "EMU_INST_SEV",
  "EMU_INST_SHADD16",
  "EMU_INST_SHADD8",
  "EMU_INST_SHASX",
  "EMU_INST_SHSAX",
  "EMU_INST_SHSUB16",
  "EMU_INST_SHSUB8",
  "EMU_INST_SMLABB",
  "EMU_INST_SMLABT",
  "EMU_INST_SMLATB",
  "EMU_INST_SMLATT",
  "EMU_INST_SMLAD",
  "EMU_INST_SMLAL",
  "EMU_INST_SMLALBB",
  "EMU_INST_SMLALBT",
  "EMU_INST_SMLALTB",
  "EMU_INST_SMLALTT",
  "EMU_INST_SMLALD",
  "EMU_INST_SMLALDX",
  "EMU_INST_SMLAWB",
  "EMU_INST_SMLAWT",
  "EMU_INST_SMLSD",
  "EMU_INST_SMLSDX",
  "EMU_INST_SMLSLD",
  "EMU_INST_SMLSLDX",
  "EMU_INST_SMMLA",
  "EMU_INST_SMMLAR",
  "EMU_INST_SMMLS",
  "EMU_INST_SMMLSR",
  "EMU_INST_SMMUL",
  "EMU_INST_SMMULR",
  "EMU_INST_SMUAD",
  "EMU_INST_SMUADX",
  "EMU_INST_SMULBB",
  "EMU_INST_SMULBT",
  "EMU_INST_SMULTB",
  "EMU_INST_SMULTT",
  "EMU_INST_SMULL",
  "EMU_INST_SMULWB",
  "EMU_INST_SMULWT",
  "EMU_INST_SMUSD",
  "EMU_INST_SMUSDX",
  "EMU_INST_SSAT",
  "EMU_INST_SSAT16",
  "EMU_INST_SSAX",
  "EMU_INST_SSUB16",
  "EMU_INST_SSUB8",
  "EMU_INST_STC",
  "EMU_INST_STM",
  "EMU_INST_STR",
  "EMU_INST_STRB",
  "EMU_INST_STRD",
  "EMU_INST_STREX",
  "EMU_INST_STREXB",
  "EMU_INST_STREXD",
  "EMU_INST_STREXH",
  "EMU_INST_STRH",
  "EMU_INST_STRHT",
  "EMU_INST_STRT",
  "EMU_INST_SUB",
  "EMU_INST_SVC",
  "EMU_INST_SWP",
  "EMU_INST_SWPB",
  "EMU_INST_SXTAB",
  "EMU_INST_SXTAB16",
  "EMU_INST_SXTAH",
  "EMU_INST_SXTB",
  "EMU_INST_SXTB16",
  "EMU_INST_SXTH",
  "EMU_INST_TEQ",
  "EMU_INST_TST",
  "EMU_INST_UADD16",
  "EMU_INST_UADD8",
  "EMU_INST_UASX",
  "EMU_INST_UBFX",
  "EMU_INST_UDF",
  "EMU_INST_UDIV",
  "EMU_INST_UHADD16",
  "EMU_INST_UHADD8",
  "EMU_INST_UHASX",
  "EMU_INST_UHSAX",
  "EMU_INST_UHSUB16",
  "EMU_INST_UHSUB8",
  "EMU_INST_UMAAL",
  "EMU_INST_UMLAL",
  "EMU_INST_UMULL",
  "EMU_INST_UQADD16",
  "EMU_INST_UQADD8",
  "EMU_INST_UQASX",
  "EMU_INST_UQSAX",
  "EMU_INST_UQSUB16",
  "EMU_INST_UQSUB8",
  "EMU_INST_USAD8",
  "EMU_INST_USADA8",
  "EMU_INST_USAT",
  "EMU_INST_USAT16",
  "EMU_INST_USAX",
  "EMU_INST_USUB16",
  "EMU_INST_USUB8",
  "EMU_INST_UXTAB",
  "EMU_INST_UXTAB16",
  "EMU_INST_UXTAH",
  "EMU_INST_UXTB",
  "EMU_INST_UXTB16",
  "EMU_INST_UXTH",
  "EMU_INST_WFE",
  "EMU_INST_WFI",
  "EMU_INST_YIELD",
  "EMU_INST_NEON_VABA",
  "EMU_INST_NEON_VABAL",
  "EMU_INST_NEON_VABD_I",
  "EMU_INST_NEON_VABD_F",
  "EMU_INST_NEON_VABDL",
  "EMU_INST_NEON_VABS",
  "EMU_INST_VFP_VABS",
  "EMU_INST_NEON_VACGE",
  "EMU_INST_NEON_VACGT",
  "EMU_INST_NEON_VADD_I",
  "EMU_INST_NEON_VADD_F",
  "EMU_INST_VFP_VADD",
  "EMU_INST_NEON_VADDHN",
  "EMU_INST_NEON_VADDL",
  "EMU_INST_NEON_VADDW",
  "EMU_INST_NEON_VAND",
  "EMU_INST_NEON_VBICI",
  "EMU_INST_NEON_VBIC",
  "EMU_INST_NEON_VBIF",
  "EMU_INST_NEON_VBIT",
  "EMU_INST_NEON_VBSL",
  "EMU_INST_NEON_VCEQ_I",
  "EMU_INST_NEON_VCEQ_F",
  "EMU_INST_NEON_VCEQZ",
  "EMU_INST_NEON_VCGE_I",
  "EMU_INST_NEON_VCGE_F",
  "EMU_INST_NEON_VCGEZ",
  "EMU_INST_NEON_VCGT_I",
  "EMU_INST_NEON_VCGT_F",
  "EMU_INST_NEON_VCGTZ",
  "EMU_INST_NEON_VCLEZ",
  "EMU_INST_NEON_VCLS",
  "EMU_INST_NEON_VCLTZ",
  "EMU_INST_NEON_VCLZ",
  "EMU_INST_VFP_VCMP",
  "EMU_INST_VFP_VCMPZ",
  "EMU_INST_VFP_VCMPE",
  "EMU_INST_VFP_VCMPEZ",
  "EMU_INST_NEON_VCNT",
  "EMU_INST_NEON_VCVT_F_I",
  "EMU_INST_NEON_VCVT_F_FP",
  "EMU_INST_NEON_VCVT_HP_SP",
  "EMU_INST_VFP_VCVT_F_I",
  "EMU_INST_VFP_VCVT_F_FP",
  "EMU_INST_VFP_VCVT_DP_SP",
  "EMU_INST_VFP_VCVTB",
  "EMU_INST_VFP_VCVTT",
  "EMU_INST_VFP_VDIV",
  "EMU_INST_NEON_VDUP_SCAL",
  "EMU_INST_NEON_VDUP_CORE",
  "EMU_INST_NEON_VEOR",
  "EMU_INST_NEON_VEXT",
  "EMU_INST_NEON_VFMA",
  "EMU_INST_NEON_VFMS",
  "EMU_INST_VFP_VFMA",
  "EMU_INST_VFP_VFMS",
  "EMU_INST_VFP_VFNMA",
  "EMU_INST_VFP_VFNMS",
  "EMU_INST_NEON_VHADD",
  "EMU_INST_NEON_VHSUB",
  "EMU_INST_NEON_VLDX_M",
  "EMU_INST_NEON_VLDX_S_O",
  "EMU_INST_NEON_VLDX_S_A",
  "EMU_INST_VFP_VLDM_DP",
  "EMU_INST_VFP_VLDM_SP",
  "EMU_INST_VFP_VLDR_DP",
  "EMU_INST_VFP_VLDR_SP",
  "EMU_INST_NEON_VMAX_I",
  "EMU_INST_NEON_VMIN_I",
  "EMU_INST_NEON_VMAX_F",
  "EMU_INST_NEON_VMIN_F",
  "EMU_INST_NEON_VMLA_I",
  "EMU_INST_NEON_VMLS_I",
  "EMU_INST_NEON_VMLAL_I",
  "EMU_INST_NEON_VMLSL_I",
  "EMU_INST_NEON_VMLA_F",
  "EMU_INST_NEON_VMLS_F",
  "EMU_INST_VFP_VMLA_F",
  "EMU_INST_VFP_VMLS_F",
  "EMU_INST_NEON_VMLA_SCAL",
  "EMU_INST_NEON_VMLS_SCAL",
  "EMU_INST_NEON_VMLAL_SCAL",
  "EMU_INST_NEON_VMLSL_SCAL",
  "EMU_INST_NEON_VMOVI",
  "EMU_INST_VFP_VMOVI",
  "EMU_INST_VFP_VMOV",
  "EMU_INST_VFP_VMOV_CORE_SCAL",
  "EMU_INST_VFP_VMOV_SCAL_CORE",
  "EMU_INST_VFP_VMOV_CORE_SP",
  "EMU_INST_VFP_VMOV_2CORE_2SP",
  "EMU_INST_VFP_VMOV_2CORE_DP",
  "EMU_INST_NEON_VMOVL",
  "EMU_INST_NEON_VMOVN",
  "EMU_INST_VFP_VMRS",
  "EMU_INST_VFP_VMSR",
  "EMU_INST_NEON_VMUL_I",
  "EMU_INST_NEON_VMULL_I",
  "EMU_INST_NEON_VMUL_F",
  "EMU_INST_VFP_VMUL_F",
  "EMU_INST_NEON_VMUL_SCAL",
  "EMU_INST_NEON_VMULL_SCAL",
  "EMU_INST_NEON_VMVNI",
  "EMU_INST_NEON_VMVN",
  "EMU_INST_NEON_VNEG",
  "EMU_INST_VFP_VNEG",
  "EMU_INST_VFP_VNMLA",
  "EMU_INST_VFP_VNMLS",
  "EMU_INST_VFP_VNMUL",
  "EMU_INST_NEON_VORN",
  "EMU_INST_NEON_VORRI",
  "EMU_INST_NEON_VORR",
  "EMU_INST_NEON_VPADAL",
  "EMU_INST_NEON_VPADD_I",
  "EMU_INST_NEON_VPADD_F",
  "EMU_INST_NEON_VPADDL",
  "EMU_INST_NEON_VPMAX_I",
  "EMU_INST_NEON_VPMIN_I",
  "EMU_INST_NEON_VPMAX_F",
  "EMU_INST_NEON_VPMIN_F",
  "EMU_INST_VFP_VPOP_DP",
  "EMU_INST_VFP_VPOP_SP",
  "EMU_INST_VFP_VPUSH_DP",
  "EMU_INST_VFP_VPUSH_SP",
  "EMU_INST_NEON_VQABS",
  "EMU_INST_NEON_VQADD",
  "EMU_INST_NEON_VQDMLAL_I",
  "EMU_INST_NEON_VQDMLSL_I",
  "EMU_INST_NEON_VQDMLAL_SCAL",
  "EMU_INST_NEON_VQDMLSL_SCAL",
  "EMU_INST_NEON_VQDMULH_I",
  "EMU_INST_NEON_VQDMULH_SCAL",
  "EMU_INST_NEON_VQDMULL_I",
  "EMU_INST_NEON_VQDMULL_SCAL",
  "EMU_INST_NEON_VQMOVN",
  "EMU_INST_NEON_VQMOVUN",
  "EMU_INST_NEON_VQNEG",
  "EMU_INST_NEON_VQRDMULH_I",
  "EMU_INST_NEON_VQRDMULH_SCAL",
  "EMU_INST_NEON_VQRSHL",
  "EMU_INST_NEON_VQRSHRN",
  "EMU_INST_NEON_VQRSHRUN",
  "EMU_INST_NEON_VQSHL",
  "EMU_INST_NEON_VQSHLI",
  "EMU_INST_NEON_VQSHLUI",
  "EMU_INST_NEON_VQSHRN",
  "EMU_INST_NEON_VQSHRUN",
  "EMU_INST_NEON_VQSUB",
  "EMU_INST_NEON_VRADDHN",
  "EMU_INST_NEON_VRECPE",
  "EMU_INST_NEON_VRECPS",
  "EMU_INST_NEON_VREV16",
  "EMU_INST_NEON_VREV32",
  "EMU_INST_NEON_VREV64",
  "EMU_INST_NEON_VRHADD",
  "EMU_INST_NEON_VRSHL",
  "EMU_INST_NEON_VRSHR",
  "EMU_INST_NEON_VRSHRN",
  "EMU_INST_NEON_VRSQRTE",
  "EMU_INST_NEON_VRSQRTS",
  "EMU_INST_NEON_VRSRA",
  "EMU_INST_NEON_VRSUBHN",
  "EMU_INST_NEON_VSHL",
  "EMU_INST_NEON_VSHLI",
  "EMU_INST_NEON_VSHLL",
  "EMU_INST_NEON_VSHLL2",
  "EMU_INST_NEON_VSHR",
  "EMU_INST_NEON_VSHRN",
  "EMU_INST_NEON_VSLI",
  "EMU_INST_VFP_VSQRT",
  "EMU_INST_NEON_VSRA",
  "EMU_INST_NEON_VSRI",
  "EMU_INST_NEON_VSTX_M",
  "EMU_INST_NEON_VSTX_S_O",
  "EMU_INST_VFP_VSTM_DP",
  "EMU_INST_VFP_VSTM_SP",
  "EMU_INST_VFP_VSTR_DP",
  "EMU_INST_VFP_VSTR_SP",
  "EMU_INST_NEON_VSUB_I",
  "EMU_INST_NEON_VSUB_F",
  "EMU_INST_VFP_VSUB_F",
  "EMU_INST_NEON_VSUBHN",
  "EMU_INST_NEON_VSUBL",
  "EMU_INST_NEON_VSUBW",
  "EMU_INST_NEON_VSWP",
  "EMU_INST_NEON_VTBL",
  "EMU_INST_NEON_VTBX",
  "EMU_INST_NEON_VTRN",
  "EMU_INST_NEON_VTST",
  "EMU_INST_NEON_VUZP",
  "EMU_INST_NEON_VZIP",
  "EMU_INST_INVALID",
  "EMU_INST_B_COND",
  "EMU_INST_BL32",
  "EMU_INST_BL_ARM32",
  "EMU_INST_CBNZ",
  "EMU_INST_CBZ",
  "EMU_INST_IT",
  "EMU_INST_ORN",
  "EMU_INST_TBB",
  "EMU_INST_TBH",
};

void print_emu_disassem(emu_disassem_t *disassem) {
    emu_log_always("Instruction %s", inst_strings[disassem->inst]);

    switch (disassem->format) {

    case INS_INVALID:
        emu_log_always("Format: INS_INVALID.\n");
        emu_log_always("INSTRUCTION IS NOT HANDLED AT ALL BY DECODER!!!!!!!!\n");
        break;

    case INS_UNTRANSLATED:
        emu_log_always("Format: INS_UNTRANSLATED.\n");
        emu_log_always("Instruction not translated.\n");
        break;

    case INS_LABEL:
        emu_log_always("Format: INS_LABEL.\n");
        emu_log_always("Target: 0x%08x\n", disassem->imm);
        break;

    case INS_RD_CONST:
        emu_log_always("Format: RD_CONST");
        emu_log_always("Rd: %d, Imm: <unknown>", disassem->Rd.reg);
        break;

    case INS_RD_RM:
        emu_log_always("Format: RD_RM");
        emu_log_always("Rd: %d, Rm: %d",  disassem->Rd.reg, disassem->Rm.reg);
        break;

    case INS_RD_IMM_RN:
        emu_log_always("Format: RD_IMM_RN");
        emu_log_always("Rd: %d, Imm: <unknown>, Rn: %d",  disassem->Rd.reg, disassem->Rn.reg);
        break;

    case INS_RD_RN_RM:
        emu_log_always("Format: RD_RN_RM");
        emu_log_always("Rd: %d, Rn: %d, Rm: %d",  disassem->Rd.reg, disassem->Rn.reg, disassem->Rm.reg);
        break;

    case INS_RD_RN_CONST:
        emu_log_always("Format: RD_RN_CONST");
        emu_log_always("Rd: %d, Rn: %d, Imm: %d",  disassem->Rd.reg, disassem->Rn.reg, disassem->imm);
        break;

    case INS_RD_RN_RM_TYPE_RS:
        emu_log_always("Format: RD_RN_RM_TYPE_RS");
        emu_log_always("Rd: %d, Rn: %d, Rm: %d, <shift> Rs: %d",  disassem->Rd.reg, disassem->Rn.reg, disassem->Rm.reg, disassem->Rs.reg);
        break;

    case INS_RD_RN_LSB_WIDTH:
        emu_log_always("Format: RD_RN_LSB_WIDTH");
        emu_log_always("Rd: %d, Rn: %d, Lsb: <unknown>, Width: <unknown>", disassem->Rd.reg, disassem->Rn.reg);
        break;

    case INS_RD_RN_RM_SHIFT:
        emu_log_always("Format: RD_RN_RM_SHIFT");
        emu_log_always("Rd: %d, Rn: %d, Rm: %d, Rm.shift_type: %d, Rm.shift_value: %d",
                      disassem->Rd.reg, disassem->Rn.reg, disassem->Rm.reg, disassem->Rm.shift_type, disassem->Rm.shift_value);
        break;

    case INS_RN_REGLIST:
        emu_log_always("Format: RN_REGLIST");
        emu_log_always("Rn: %d, Reglist: 0x%08x", disassem->Rn.reg, disassem->reglist);
        break;

    case INS_RD_RN_RM_RA:
        emu_log_always("Format: RD_RN_RM_RA\n");
        emu_log_always("Rd: %d, Rn: %d, Rm: %d, Ra: %d.\n",
                      disassem->Rd.reg, disassem->Rn.reg, disassem->Rm.reg, disassem->Ra.reg);
        break;

    case INS_RT_RT2_RN_IMM:
        emu_log_always("Format: RT_RT2_RN_IMM");
        emu_log_always("Rt: %d, Rt2: %d, Rn: %d, Imm: %d.\n",
                      disassem->Rt.reg, disassem->Rt2.reg, disassem->Rn.reg, disassem->imm);
        break;

    case INS_RT_RT2_RN_RM:
        emu_log_always("Format: RT_RT2_RN_RM");
        emu_log_always("Rt: %d, Rt2: %d, Rn: %d, Rm: %d.\n",
                      disassem->Rt.reg, disassem->Rt2.reg, disassem->Rn.reg, disassem->Rm.reg);
        break;

    case INS_RT_RN_IMM:
        emu_log_always("Format: RT_RN_IMM.\n");
        emu_log_always("Rt: %d, Rn: %d, Imm: %d.\n",
                      disassem->Rt.reg, disassem->Rn.reg, disassem->imm);
        break;

    case INS_RT_RN_RM_SHIFT:
        emu_log_always("Format: RT_RN_RM_SHIFT.\n");
        emu_log_always("Rt: %d, Rn: %d, Rm: %d, Rm.shift_type: %d, Rm.shift_value: %d.\n",
                      disassem->Rt.reg, disassem->Rn.reg, disassem->Rm.reg, disassem->Rm.shift_type, disassem->Rm.shift_value);
        break;

    case INS_RDLO_RDHI_RN_RM:
        emu_log_always("Format: RDLO_RDHI_RN_RM.\n");
        emu_log_always("Rdlo: %d, Rdhi: %d, Rn: %d, Rm: %d.\n",
                      disassem->Rdlo.reg, disassem->Rdhi.reg, disassem->Rn.reg, disassem->Rm.reg);
        break;

    case INS_V_REGLIST:
        emu_log_always("Format: V_REGLIST.\n");
        emu_log_always("Reglist: 0x%08x.\n", disassem->reglist);
        break;

    case INS_V_REGLIST_RN:
        emu_log_always("Format: V_REGLIST_RN.\n");
        emu_log_always("Reglist: 0x%08x, Rn: %d",
                      disassem->reglist, disassem->Rn.reg);
        break;

    case INS_V_REGLIST_RN_RM:
        emu_log_always("Format: V_REGLIST_RN.\n");
        emu_log_always("Reglist: 0x%08x, Rn: %d, Rm: %d",
                      disassem->reglist, disassem->Rn.reg, disassem->Rm.reg);
        break;

    case INS_V_RT_VN:
        emu_log_always("Format: V_RT_VN.\n");
        emu_log_always("Rt: %d Vn: %d (q: %d, s: %d).\n",
                      disassem->Rt.reg,
                      disassem->Vn.reg, disassem->Vn.q_reg, disassem->Vn.s_reg);
        break;

    case INS_V_VN_RT:
        emu_log_always("Format: V_VN_RT.\n");
        emu_log_always("Vn: %d (q: %d, s: %d). Rt: %d\n",
                      disassem->Vn.reg, disassem->Vn.q_reg, disassem->Vn.s_reg,
                      disassem->Rt.reg);
        break;

    case INS_V_VD_VN_VM:
        emu_log_always("Format: V_VD_VN_VM.\n");
        emu_log_always("Vd: %d (q: %d, s: %d), Vn: %d (q: %d, s: %d), Vm: %d (q: %d, s: %d).\n",
                      disassem->Vd.reg, disassem->Vd.q_reg, disassem->Vd.s_reg,
                      disassem->Vn.reg, disassem->Vn.q_reg, disassem->Vn.s_reg,
                      disassem->Vm.reg, disassem->Vm.q_reg, disassem->Vm.s_reg);
        break;

    case INS_V_VD_VM:
        emu_log_always("Format: V_VD_VM.\n");
        emu_log_always("Vd: %d (q: %d, s: %d), Vm: %d (q: %d, s: %d).\n",
                      disassem->Vd.reg, disassem->Vd.q_reg, disassem->Vd.s_reg,
                      disassem->Vm.reg, disassem->Vm.q_reg, disassem->Vm.s_reg);
        break;

    case INS_V_VD_VM_CONST:
        emu_log_always("Format: V_VD_VM_CONST.\n");
        emu_log_always("Vd: %d (q: %d, s: %d), Vm: %d (q: %d, s: %d), Const <unknown>.\n",
                      disassem->Vd.reg, disassem->Vd.q_reg, disassem->Vd.s_reg,
                      disassem->Vm.reg, disassem->Vm.q_reg, disassem->Vm.s_reg);
        break;

    case INS_V_VD_IMM:
        emu_log_always("Format: V_VD_IMM.\n");
        emu_log_always("Vd: %d (q: %d, s: %d), Imm: <unknown>.\n",
                      disassem->Vd.reg, disassem->Vd.q_reg, disassem->Vd.s_reg);
        break;

    case INS_V_VD_RN_IMM:
        emu_log_always("Format: V_VD_RN_IMM.\n");
        emu_log_always("Vd: %d (q: %d, s: %d), Rn: %d, Imm: 0x%08x.\n",
                      disassem->Vd.reg, disassem->Vd.q_reg, disassem->Vd.s_reg,
                      disassem->Rn.reg, disassem->imm);
        break;

    case INS_V_RN_REGLIST:
        emu_log_always("Format: V_RN_REGLIST.\n");
        emu_log_always("Rn: %d, Reglist: 0x%08x.\n",
                       disassem->Rn.reg, disassem->reglist);
        break;

    default:
        emu_abort("[print_emu_disassem]: Unhandled instruction format: %d.\n", disassem->format);
    }

    emu_log_always("cond: %d, is_branch: %d, is_up: %d, is_pre_indexed: %d,"
                  " is_reglist_single: %d, v_elem_size: %d, vldx_vstx_num: %d, vldx_vstx_stride: %d.\n",
                  disassem->cond, disassem->is_branch, disassem->is_up, disassem->is_pre_indexed,
                  disassem->is_reglist_single, disassem->v_elem_size, disassem->vldx_vstx_num, disassem->vldx_vstx_stride);
}

void arm_translate_to_disassem(arm_instruction inst, uint32_t *read_address, emu_disassem_t *disassem) {

    // Reset struct
    memset(disassem, 0, sizeof(emu_disassem_t));

    int cond = (*read_address >> 28);
    if (cond == EMU_ARM_CC_ALT) {
        cond = EMU_ARM_CC_AL;
    }
    disassem->cond = cond;
    disassem->size = 4;

    switch (inst) {

    case ARM_ADC:
        disassem->inst = EMU_INST_ADC;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_ADD:
        disassem->inst = EMU_INST_ADD;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_AND:
        disassem->inst = EMU_INST_AND;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_B:
        TRANSLATE_B_BL;
        disassem->inst = EMU_INST_B;
        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_BFC:
        disassem->inst = EMU_INST_BFC;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_BFI: {
        uint32_t null, rd, rn;
        arm_bfi_decode_fields(read_address, &rd, &rn, &null, &null);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->inst = EMU_INST_BFI;
        disassem->format = INS_RD_RN_LSB_WIDTH;
        break;
    }
    case ARM_BIC:
        TRANSLATE_ARM_DATA_PROC;
        disassem->inst = EMU_INST_BIC;
        break;
    case ARM_BKPT:
        disassem->inst = EMU_INST_BKPT;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_BL:
        TRANSLATE_B_BL;
        disassem->inst = EMU_INST_BL;
        disassem->is_branch = true;
        break;
    case ARM_BLX:
        disassem->inst = EMU_INST_BLX;
        disassem->format = INS_UNTRANSLATED;
        disassem->is_branch = true;
        break;
    case ARM_BLXI:
        disassem->inst = EMU_INST_BLXI;
        disassem->format = INS_UNTRANSLATED;
        disassem->is_branch = true;
        break;
    case ARM_BX:
        disassem->inst = EMU_INST_BX;
        disassem->format = INS_UNTRANSLATED;
        disassem->is_branch = true;
        break;
    case ARM_CDP:
        disassem->inst = EMU_INST_CDP;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_CDP2:
        disassem->inst = EMU_INST_CDP2;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_CLREX:
        disassem->inst = EMU_INST_CLREX;
        break;
    case ARM_CLZ: {
        uint32_t rd, rm;
        arm_clz_decode_fields(read_address, &rd, &rm);
        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->inst = EMU_INST_CLZ;
        disassem->format = INS_RD_RM;
        break;
    }
    case ARM_CMN:
        disassem->inst = EMU_INST_CMN;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_CMP:
        disassem->inst = EMU_INST_CMP;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_DBG:
        disassem->inst = EMU_INST_DBG;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_DMB:
        disassem->inst = EMU_INST_DMB;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_DSB:
        disassem->inst = EMU_INST_DSB;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_EOR:
        disassem->inst = EMU_INST_EOR;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_ISB:
        disassem->inst = EMU_INST_ISB;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_LDC:
        disassem->inst = EMU_INST_LDC;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_LDM:
        disassem->inst = EMU_INST_LDM;
        TRANSLATE_BULK_LOAD_STORES;
        if ((disassem->reglist & 0x8000) != 0) {
            disassem->is_branch = true;
        }
        break;
    case ARM_LDR:
        disassem->inst = EMU_INST_LDR;
        TRANSLATE_SINGLE_LOAD_STORE;
        break;
    case ARM_LDRB:
        disassem->inst = EMU_INST_LDRB;
        TRANSLATE_SINGLE_LOAD_STORE;
        break;
    case ARM_LDRBT:
        disassem->inst = EMU_INST_LDRBT;
        break;
    case ARM_LDRD: {
        uint32_t immediate, rt, rn, rm, imm4h, prepostindex, updown, null;
        arm_ldrd_decode_fields(read_address, &immediate, &rt, &rn, &rm, &imm4h, &prepostindex, &updown, &null);

        disassem->Rt.reg = rt;
        disassem->Rt2.reg = rt+1;
        disassem->Rn.reg = rn;

        // In LDRD, if immediate == 1, then it's the immediate variant
        // and Rm is really imm4l
        if (immediate == 1) {
            disassem->imm = (imm4h << 4) + rm;
            disassem->format = INS_RT_RT2_RN_IMM;
        } else {
            disassem->Rm.reg = rm;
            disassem->format = INS_RT_RT2_RN_RM;
        }

        disassem->is_up = updown;
        disassem->is_pre_indexed = prepostindex;
        disassem->inst = EMU_INST_LDRD;
        break;
    }
    case ARM_LDREX:
        disassem->inst = EMU_INST_LDREX;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_LDREXB:
        disassem->inst = EMU_INST_LDREXB;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_LDREXD:
        disassem->inst = EMU_INST_LDREXD;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_LDREXH:
        disassem->inst = EMU_INST_LDREXH;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_LDRH:
        disassem->inst = EMU_INST_LDRH;
        TRANSLATE_HALFWORD_LOAD;
        break;
    case ARM_LDRHT:
        disassem->inst = EMU_INST_LDRHT;
        break;
    case ARM_LDRSB: {
        uint32_t immediate, rt, rn, rm_imm4l, imm4h, index, updown, writeback;
        arm_ldrsb_decode_fields(read_address, &immediate, &rt, &rn, &rm_imm4l, &imm4h, &index, &updown, &writeback);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;

        if (immediate == 1) {
            disassem->imm = (imm4h << 4) + rm_imm4l;
            disassem->format = INS_RT_RN_IMM;
        } else {
            disassem->Rm.reg = rm_imm4l;
            disassem->format = INS_RT_RN_RM_SHIFT;
        }

        disassem->is_up = updown;
        disassem->is_pre_indexed = index;
        disassem->inst = EMU_INST_LDRSB;
        break;
    }
    case ARM_LDRSBT:
        disassem->inst = EMU_INST_LDRSBT;
        break;
    case ARM_LDRSH:
        disassem->inst = EMU_INST_LDRSH;
        TRANSLATE_HALFWORD_LOAD;
        break;
    case ARM_LDRSHT:
        disassem->inst = EMU_INST_LDRSHT;
        break;
    case ARM_LDRT:
        disassem->inst = EMU_INST_LDRT;
        break;
    case ARM_MCR:
        disassem->inst = EMU_INST_MCR;
        break;
    case ARM_MCRR:
        disassem->inst = EMU_INST_MCRR;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_MLA: {
        uint32_t rd, rn, rm, ra;
        arm_mla_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = ra;
        disassem->inst = EMU_INST_MLA;
        disassem->format = INS_RD_RN_RM_RA;
        break;
    }
    case ARM_MLS: {
        uint32_t rd, rn, rm, ra;
        arm_mls_decode_fields(read_address, &rd, &rn, &rm, &ra);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = ra;
        disassem->format = INS_RD_RN_RM_RA;

        disassem->inst = EMU_INST_MLS;
        break;
    }
    case ARM_MOV:
        disassem->inst = EMU_INST_MOV;
        TRANSLATE_MOV;
        break;
    case ARM_MOVT:
        disassem->inst = EMU_INST_MOVT;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_MOVW:
        disassem->inst = EMU_INST_MOVW;
        TRANSLATE_MOV;
        break;
    case ARM_MRC:
        disassem->inst = EMU_INST_MRC;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_MRRC:
        disassem->inst = EMU_INST_MRRC;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_MRS:
        disassem->inst = EMU_INST_MRS;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_MSR:
        disassem->inst = EMU_INST_MSR;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_MSRI:
        disassem->inst = EMU_INST_MSRI;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_MUL: {
        uint32_t rd, rn, rm;
        arm_mul_decode_fields(read_address, &rd, &rn, &rm);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->inst = EMU_INST_MUL;
        disassem->format = INS_RD_RN_RM;
        break;
    }
    case ARM_MVN:
        disassem->inst = EMU_INST_MVN;
        TRANSLATE_MOV;
        break;
    case ARM_NOP:
        disassem->inst = EMU_INST_NOP;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_ORR:
        disassem->inst = EMU_INST_ORR;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_PKH: {
        uint32_t rd, rn, rm, null;
        arm_pkh_decode_fields(read_address, &rd, &rn, &rm, &null, &null);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;
        disassem->inst = EMU_INST_PKH;
        break;
    }
    case ARM_PLD:
        disassem->inst = EMU_INST_PLD;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_PLI:
        disassem->inst = EMU_INST_PLI;
        break;
    case ARM_PLII:
        disassem->inst = EMU_INST_PLII;
        break;
    case ARM_QADD:
        disassem->inst = EMU_INST_QADD;
        break;
    case ARM_QADD16:
        disassem->inst = EMU_INST_QADD16;
        break;
    case ARM_QADD8:
        disassem->inst = EMU_INST_QADD8;
        break;
    case ARM_QASX:
        disassem->inst = EMU_INST_QASX;
        break;
    case ARM_QDADD:
        disassem->inst = EMU_INST_QDADD;
        break;
    case ARM_QDSUB:
        disassem->inst = EMU_INST_QDSUB;
        break;
    case ARM_QSAX:
        disassem->inst = EMU_INST_QSAX;
        break;
    case ARM_QSUB:
        disassem->inst = EMU_INST_QSUB;
        break;
    case ARM_QSUB16:
        disassem->inst = EMU_INST_QSUB16;
        break;
    case ARM_QSUB8:
        disassem->inst = EMU_INST_QSUB8;
        break;
    case ARM_RBIT: {
        uint32_t rd, rm;
        arm_rbit_decode_fields(read_address, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_RBIT;
        break;
    }
    case ARM_REV: {
        uint32_t rd, rm;
        arm_rev_decode_fields(read_address, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_REV;
        break;
    }
    case ARM_REV16: {
        uint32_t rd, rm;
        arm_rev16_decode_fields(read_address, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_REV16;
        break;
    }
    case ARM_REVSH:
        disassem->inst = EMU_INST_REVSH;
        break;
    case ARM_RRX:
        disassem->inst = EMU_INST_RRX;
        TRANSLATE_MOV;
        break;
    case ARM_RSB:
        disassem->inst = EMU_INST_RSB;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_RSC: {
        uint32_t null, rd, rn;
        arm_rsc_decode_fields(read_address, &null, &null, &rd, &rn, &null);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_RSC;
        break;
    }
    case ARM_SADD16: {
        uint32_t rd, rn, rm;
        arm_sadd16_decode_fields(read_address, &rd, &rn, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SADD16;
        break;
    }
    case ARM_SADD8:
        disassem->inst = EMU_INST_SADD8;
        break;
    case ARM_SASX:
        disassem->inst = EMU_INST_SASX;
        break;
    case ARM_SBC:
        disassem->inst = EMU_INST_SBC;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_SBFX: {
        uint32_t rd, rn, null;
        arm_sbfx_decode_fields(read_address, &rd, &rn, &null, &null);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_LSB_WIDTH;

        disassem->inst = EMU_INST_SBFX;
        break;
    }
    case ARM_SDIV:
        disassem->inst = EMU_INST_SDIV;
        break;
    case ARM_SEL:
        disassem->inst = EMU_INST_SEL;
        break;
    case ARM_SETEND:
        disassem->inst = EMU_INST_SETEND;
        break;
    case ARM_SEV:
        disassem->inst = EMU_INST_SEV;
        break;
    case ARM_SHADD16:
        disassem->inst = EMU_INST_SHADD16;
        break;
    case ARM_SHADD8:
        disassem->inst = EMU_INST_SHADD8;
        break;
    case ARM_SHASX:
        disassem->inst = EMU_INST_SHASX;
        break;
    case ARM_SHSAX:
        disassem->inst = EMU_INST_SHSAX;
        break;
    case ARM_SHSUB16:
        disassem->inst = EMU_INST_SHSUB16;
        break;
    case ARM_SHSUB8:
        disassem->inst = EMU_INST_SHSUB8;
        break;
    case ARM_SMLABB: {
	uint32_t rd, rn, rm, ra;
        arm_smlabb_decode_fields(read_address, &rd, &rn, &rm, &ra);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = ra;
        disassem->format = INS_RD_RN_RM_RA;

        disassem->inst = EMU_INST_SMLABB;
        break;
    }
    case ARM_SMLABT:
        disassem->inst = EMU_INST_SMLABT;
        break;
    case ARM_SMLATB:
        disassem->inst = EMU_INST_SMLATB;
        break;
    case ARM_SMLATT:
        disassem->inst = EMU_INST_SMLATT;
        break;
    case ARM_SMLAD:
        disassem->inst = EMU_INST_SMLAD;
        break;
    case ARM_SMLAL: {
        uint32_t set_condition, rdhi, rdlo, rm, rn;
        arm_smlal_decode_fields(read_address, &set_condition, &rdhi, &rdlo, &rm, &rn);

        disassem->Rdhi.reg = rdhi;
        disassem->Rdlo.reg = rdlo;
        disassem->Rm.reg = rm;
        disassem->Rn.reg = rn;
        disassem->format = INS_RDLO_RDHI_RN_RM;

        disassem->inst = EMU_INST_SMLAL;
        break;
    }
    case ARM_SMLALBB:
        disassem->inst = EMU_INST_SMLALBB;
        break;
    case ARM_SMLALBT:
        disassem->inst = EMU_INST_SMLALBT;
        break;
    case ARM_SMLALTB:
        disassem->inst = EMU_INST_SMLALTB;
        break;
    case ARM_SMLALTT:
        disassem->inst = EMU_INST_SMLALTT;
        break;
    case ARM_SMLALD:
        disassem->inst = EMU_INST_SMLALD;
        break;
    case ARM_SMLALDX:
        disassem->inst = EMU_INST_SMLALDX;
        break;
    case ARM_SMLAWB: {
        uint32_t rd, rn, rm, ra;
        arm_smlawb_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = ra;
        disassem->format = INS_RD_RN_RM_RA;
        disassem->inst = EMU_INST_SMLAWB;
        break;
    }
    case ARM_SMLAWT: {
        uint32_t rd, rn, rm, ra;
        arm_smlawt_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = ra;
        disassem->format = INS_RD_RN_RM_RA;
        disassem->inst = EMU_INST_SMLAWT;
        break;
    }
    case ARM_SMLSD:
        disassem->inst = EMU_INST_SMLSD;
        break;
    case ARM_SMLSDX:
        disassem->inst = EMU_INST_SMLSDX;
        break;
    case ARM_SMLSLD:
        disassem->inst = EMU_INST_SMLSLD;
        break;
    case ARM_SMLSLDX:
        disassem->inst = EMU_INST_SMLSLDX;
        break;
    case ARM_SMMLA:
        disassem->inst = EMU_INST_SMMLA;
        break;
    case ARM_SMMLAR:
        disassem->inst = EMU_INST_SMMLAR;
        break;
    case ARM_SMMLS:
        disassem->inst = EMU_INST_SMMLS;
        break;
    case ARM_SMMLSR:
        disassem->inst = EMU_INST_SMMLSR;
        break;
    case ARM_SMMUL:
        disassem->inst = EMU_INST_SMMUL;
        break;
    case ARM_SMMULR:
        disassem->inst = EMU_INST_SMMULR;
        break;
    case ARM_SMUAD:
        disassem->inst = EMU_INST_SMUAD;
        break;
    case ARM_SMUADX:
        disassem->inst = EMU_INST_SMUADX;
        break;
    case ARM_SMULBB: {
        uint32_t rd, rn, rm;
        arm_smulbb_decode_fields(read_address, &rd, &rn, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SMULBB;
        break;
    }
    case ARM_SMULBT:
        disassem->inst = EMU_INST_SMULBT;
        break;
    case ARM_SMULTB:
        disassem->inst = EMU_INST_SMULTB;
        break;
    case ARM_SMULTT:
        disassem->inst = EMU_INST_SMULTT;
        break;
    case ARM_SMULL: {
        uint32_t null, rdhi, rdlo, rm, rn;
        arm_smull_decode_fields(read_address, &null, &rdhi, &rdlo, &rm, &rn);

        disassem->Rdhi.reg = rdhi;
        disassem->Rdlo.reg = rdlo;
        disassem->Rm.reg = rm;
        disassem->Rn.reg = rn;
        disassem->format = INS_RDLO_RDHI_RN_RM;

        disassem->inst = EMU_INST_SMULL;
        break;
    }
    case ARM_SMULWB: {
        uint32_t rd, rn, rm;
        arm_smulwb_decode_fields(read_address, &rd, &rn, &rm);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;
        disassem->inst = EMU_INST_SMULWB;
        break;
    }
    case ARM_SMULWT: {
        uint32_t rd, rn, rm;
        arm_smulwt_decode_fields(read_address, &rd, &rn, &rm);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;
        disassem->inst = EMU_INST_SMULWT;
        break;
    }
    case ARM_SMUSD:
        disassem->inst = EMU_INST_SMUSD;
        break;
    case ARM_SMUSDX:
        disassem->inst = EMU_INST_SMUSDX;
        break;
    case ARM_SSAT:
        disassem->inst = EMU_INST_SSAT;
        break;
    case ARM_SSAT16:
        disassem->inst = EMU_INST_SSAT16;
        break;
    case ARM_SSAX:
        disassem->inst = EMU_INST_SSAX;
        break;
    case ARM_SSUB16: {
        uint32_t rd, rn, rm;
        arm_ssub16_decode_fields(read_address, &rd, &rn, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SSUB16;
        break;
    }
    case ARM_SSUB8:
        disassem->inst = EMU_INST_SSUB8;
        break;
    case ARM_STC:
        disassem->inst = EMU_INST_STC;
        break;
    case ARM_STM:
        disassem->inst = EMU_INST_STM;
        TRANSLATE_BULK_LOAD_STORES;
        break;
    case ARM_STR:
        disassem->inst = EMU_INST_STR;
        TRANSLATE_SINGLE_LOAD_STORE;
        break;
    case ARM_STRB:
        disassem->inst = EMU_INST_STRB;
        TRANSLATE_SINGLE_LOAD_STORE;
        break;
    case ARM_STRD: {
        uint32_t null, immediate, rt, rn, rm, imm4h, prepostindex, updown;
        arm_strd_decode_fields(read_address, &immediate, &rt, &rn, &rm, &imm4h, &prepostindex, &updown, &null);

        disassem->Rt.reg = rt;
        disassem->Rt2.reg = rt+1;
        disassem->Rn.reg = rn;

        // In STRD, if immediate == 1, then it's the immediate variant
        // and Rm is really imm4l
        if (immediate == 1) {
            disassem->imm = (imm4h << 4) + rm;
            disassem->format = INS_RT_RT2_RN_IMM;
        } else {
            disassem->Rm.reg = rm;
            disassem->format = INS_RT_RT2_RN_RM;
        }
        disassem->is_up = updown;
        disassem->is_pre_indexed = prepostindex;
        disassem->inst = EMU_INST_STRD;
        break;
    }
    case ARM_STREX:
        disassem->inst = EMU_INST_STREX;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_STREXB:
        disassem->inst = EMU_INST_STREXB;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_STREXD:
        disassem->inst = EMU_INST_STREXD;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_STREXH:
        disassem->inst = EMU_INST_STREXH;
        disassem->format = INS_UNTRANSLATED;
        break;
    case ARM_STRH: {
        uint32_t null, immediate, rt, rn, rm, imm4h, prepostindex, updown;
        arm_strh_decode_fields(read_address, &immediate, &rt, &rn, &rm, &imm4h, &prepostindex, &updown, &null);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;

        // In STRH, if immediate == 1, then it's the immediate variant
        // and Rm is really imm4l
        if (immediate == 1) {
            disassem->imm = (imm4h << 4) + rm;
            disassem->format = INS_RT_RN_IMM;
        } else {
            disassem->Rm.reg = rm;
            disassem->Rm.shift_type = 0;
            disassem->Rm.shift_value = 0;
            disassem->format = INS_RT_RN_RM_SHIFT;
        }
        disassem->is_up = updown;
        disassem->is_pre_indexed = prepostindex;
        disassem->inst = EMU_INST_STRH;
        break;
    }
    case ARM_STRHT:
        disassem->inst = EMU_INST_STRHT;
        break;
    case ARM_STRT:
        disassem->inst = EMU_INST_STRT;
        break;
    case ARM_SUB:
        disassem->inst = EMU_INST_SUB;
        TRANSLATE_ARM_DATA_PROC;
        break;
    case ARM_SVC:
        disassem->inst = EMU_INST_SVC;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_SWP:
        disassem->inst = EMU_INST_SWP;
        break;
    case ARM_SWPB:
        disassem->inst = EMU_INST_SWPB;
        break;
    case ARM_SXTAB:
        disassem->inst = EMU_INST_SXTAB;
        break;
    case ARM_SXTAB16:
        disassem->inst = EMU_INST_SXTAB16;
        break;
    case ARM_SXTAH:
        disassem->inst = EMU_INST_SXTAH;
        break;
    case ARM_SXTB: {
        uint32_t rd, rm, rotate;
        arm_sxtb_decode_fields(read_address, &rd, &rm, &rotate);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_SXTB;
        break;
    }
    case ARM_SXTB16:
        disassem->inst = EMU_INST_SXTB16;
        break;
    case ARM_SXTH: {
        uint32_t rd, rm, rotate;
        arm_sxth_decode_fields(read_address, &rd, &rm, &rotate);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_SXTH;
        break;
    }
    case ARM_TEQ:
        disassem->inst = EMU_INST_TEQ;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_TST:
        disassem->inst = EMU_INST_TST;
        disassem->format = INS_UNTRANSLATED;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_UADD16: {
        uint32_t rd, rn, rm;
        arm_uadd16_decode_fields(read_address, &rd, &rn, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_UADD16;
        break;
    }
    case ARM_UADD8:
        disassem->inst = EMU_INST_UADD8;
        break;
    case ARM_UASX:
        disassem->inst = EMU_INST_UASX;
        break;
    case ARM_UBFX: {
        uint32_t null, rd, rn;
        arm_ubfx_decode_fields(read_address, &rd, &rn, &null, &null);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_LSB_WIDTH;
        disassem->inst = EMU_INST_UBFX;
        break;
    }
    case ARM_UDF: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_UDF;
        break;
    }
    case ARM_UDIV:
        disassem->inst = EMU_INST_UDIV;
        break;
    case ARM_UHADD16:
        disassem->inst = EMU_INST_UHADD16;
        break;
    case ARM_UHADD8:
        disassem->inst = EMU_INST_UHADD8;
        break;
    case ARM_UHASX:
        disassem->inst = EMU_INST_UHASX;
        break;
    case ARM_UHSAX:
        disassem->inst = EMU_INST_UHSAX;
        break;
    case ARM_UHSUB16:
        disassem->inst = EMU_INST_UHSUB16;
        break;
    case ARM_UHSUB8:
        disassem->inst = EMU_INST_UHSUB8;
        break;
    case ARM_UMAAL:
        disassem->inst = EMU_INST_UMAAL;
        break;
    case ARM_UMLAL: {
        uint32_t null, rdhi, rdlo, rm, rn;
        arm_umlal_decode_fields(read_address, &null, &rdhi, &rdlo, &rm, &rn);

        disassem->Rdhi.reg = rdhi;
        disassem->Rdlo.reg = rdlo;
        disassem->Rm.reg = rm;
        disassem->Rn.reg = rn;
        disassem->format = INS_RDLO_RDHI_RN_RM;

        disassem->inst = EMU_INST_UMLAL;
        break;
    }
    case ARM_UMULL: {
        uint32_t null, rdhi, rdlo, rm, rn;
        arm_umull_decode_fields(read_address, &null, &rdhi, &rdlo, &rm, &rn);
        disassem->Rdhi.reg = rdhi;
        disassem->Rdlo.reg = rdlo;
        disassem->Rm.reg = rm;
        disassem->Rn.reg = rn;
        disassem->format = INS_RDLO_RDHI_RN_RM;
        disassem->inst = EMU_INST_UMULL;
        break;
    }
    case ARM_UQADD16:
        disassem->inst = EMU_INST_UQADD16;
        break;
    case ARM_UQADD8:
        disassem->inst = EMU_INST_UQADD8;
        break;
    case ARM_UQASX:
        disassem->inst = EMU_INST_UQASX;
        break;
    case ARM_UQSAX:
        disassem->inst = EMU_INST_UQSAX;
        break;
    case ARM_UQSUB16:
        disassem->inst = EMU_INST_UQSUB16;
        break;
    case ARM_UQSUB8:
        disassem->inst = EMU_INST_UQSUB8;
        break;
    case ARM_USAD8:
        disassem->inst = EMU_INST_USAD8;
        break;
    case ARM_USADA8: {
        uint32_t rd, rn, rm, ra;
        arm_usada8_decode_fields(read_address, &rd, &rn, &rm, &ra);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = ra;
        disassem->format = INS_RD_RN_RM_RA;

        disassem->inst = EMU_INST_USADA8;
        break;
    }
    case ARM_USAT:
        disassem->inst = EMU_INST_USAT;
        break;
    case ARM_USAT16: {
        uint32_t rd, rn, null;
        arm_usat16_decode_fields(read_address, &rd, &null, &rn);
        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_IMM_RN;
        disassem->inst = EMU_INST_USAT16;
        break;
    }
    case ARM_USAX:
        disassem->inst = EMU_INST_USAX;
        break;
    case ARM_USUB16:
        disassem->inst = EMU_INST_USUB16;
        break;
    case ARM_USUB8:
        disassem->inst = EMU_INST_USUB8;
        break;
    case ARM_UXTAB: {
        uint32_t rd, rn, rm, rotate;
        arm_uxtab_decode_fields(read_address, &rd, &rn, &rm, &rotate);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_UXTAB;
        break;
    }
    case ARM_UXTAB16: {
        uint32_t rd, rn, rm, rotate;
        arm_uxtab16_decode_fields(read_address, &rd, &rn, &rm, &rotate);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_UXTAB16;
        break;
    }
    case ARM_UXTAH: {
        uint32_t rd, rn, rm, rotate;
        arm_uxtah_decode_fields(read_address, &rd, &rn, &rm, &rotate);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_UXTAH;
        break;
    }
    case ARM_UXTB: {
        uint32_t rd, rm, null;
        arm_uxtb_decode_fields(read_address, &rd, &rm, &null);
        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;
        disassem->inst = EMU_INST_UXTB;
        break;
    }
    case ARM_UXTB16: {
        uint32_t rd, rm, rotate;
        arm_uxtb16_decode_fields(read_address, &rd, &rm, &rotate);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_UXTB16;
        break;
    }
    case ARM_UXTH: {
        uint32_t null, rd, rm;
        arm_uxth_decode_fields(read_address, &rd, &rm, &null);
        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;
        disassem->inst = EMU_INST_UXTH;
        break;
    }
    case ARM_WFE:
        disassem->inst = EMU_INST_WFE;
        break;
    case ARM_WFI:
        disassem->inst = EMU_INST_WFI;
        break;
    case ARM_YIELD:
        disassem->inst = EMU_INST_YIELD;
        break;
    case ARM_NEON_VABA:
        disassem->inst = EMU_INST_NEON_VABA;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VABAL:
        disassem->inst = EMU_INST_NEON_VABAL;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VABD_I:
        disassem->inst = EMU_INST_NEON_VABD_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VABD_F:
        disassem->inst = EMU_INST_NEON_VABD_F;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VABDL:
        disassem->inst = EMU_INST_NEON_VABDL;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VABS:
        disassem->inst = EMU_INST_NEON_VABS;
        TRANSLATE_NEON_VOPS_VD_VM;
        break;
    case ARM_VFP_VABS:
        disassem->inst = EMU_INST_VFP_VABS;
        TRANSLATE_VFP_VD_VM;
        break;
    case ARM_NEON_VACGE:
        disassem->inst = EMU_INST_NEON_VACGE;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VACGT:
        disassem->inst = EMU_INST_NEON_VACGT;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VADD_I:
        disassem->inst = EMU_INST_NEON_VADD_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VADD_F:
        disassem->inst = EMU_INST_NEON_VADD_F;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_VFP_VADD:
        disassem->inst = EMU_INST_VFP_VADD;
        TRANSLATE_VFP_VD_VN_VM;
        break;
    case ARM_NEON_VADDHN:
        disassem->inst = EMU_INST_NEON_VADDHN;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        disassem->Vd.q_reg = false;
        disassem->Vn.q_reg = true;
        disassem->Vm.q_reg = true;
        break;
    case ARM_NEON_VADDL:
        disassem->inst = EMU_INST_NEON_VADDL;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VADDW:
        disassem->inst = EMU_INST_NEON_VADDW;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        disassem->Vd.q_reg = true;
        disassem->Vn.q_reg = true;
        disassem->Vm.q_reg = false;
        break;
    case ARM_NEON_VAND:
        disassem->inst = EMU_INST_NEON_VAND;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VBICI:
        disassem->inst = EMU_INST_NEON_VBICI;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_NEON_VBIC:
        disassem->inst = EMU_INST_NEON_VBIC;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VBIF:
        disassem->inst = EMU_INST_NEON_VBIF;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VBIT:
        disassem->inst = EMU_INST_NEON_VBIT;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VBSL:
        disassem->inst = EMU_INST_NEON_VBSL;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VCEQ_I:
        disassem->inst = EMU_INST_NEON_VCEQ_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VCEQ_F:
        disassem->inst = EMU_INST_NEON_VCEQ_F;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VCEQZ:
        disassem->inst = EMU_INST_NEON_VCEQZ;
        TRANSLATE_NEON_VOPS_VD_VM
        break;
    case ARM_NEON_VCGE_I:
        disassem->inst = EMU_INST_NEON_VCGE_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VCGE_F:
        disassem->inst = EMU_INST_NEON_VCGE_F;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VCGEZ:
        disassem->inst = EMU_INST_NEON_VCGEZ;
        TRANSLATE_NEON_VOPS_VD_VM
        break;
    case ARM_NEON_VCGT_I:
        disassem->inst = EMU_INST_NEON_VCGT_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM
        break;
    case ARM_NEON_VCGT_F:
        disassem->inst = EMU_INST_NEON_VCGT_F;
        TRANSLATE_NEON_VOPS_VD_VN_VM
        break;
    case ARM_NEON_VCGTZ:
        disassem->inst = EMU_INST_NEON_VCGTZ;
        TRANSLATE_NEON_VOPS_VD_VM
        break;
    case ARM_NEON_VCLEZ:
        disassem->inst = EMU_INST_NEON_VCLEZ;
        TRANSLATE_NEON_VOPS_VD_VM
        break;
    case ARM_NEON_VCLS:
        disassem->inst = EMU_INST_NEON_VCLS;
        TRANSLATE_NEON_VOPS_VD_VM
        break;
    case ARM_NEON_VCLTZ:
        disassem->inst = EMU_INST_NEON_VCLTZ;
        TRANSLATE_NEON_VOPS_VD_VM
        break;
    case ARM_NEON_VCLZ:
        disassem->inst = EMU_INST_NEON_VCLZ;
        TRANSLATE_NEON_VOPS_VD_VM
        break;
    case ARM_VFP_VCMP:
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMP;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_VFP_VCMPZ:
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMPZ;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_VFP_VCMPE:
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMPE;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_VFP_VCMPEZ:
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMPEZ;
        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        break;
    case ARM_NEON_VCNT:
        disassem->inst = EMU_INST_NEON_VCNT;
        TRANSLATE_NEON_VOPS_VD_VM;
        break;
    case ARM_NEON_VCVT_F_I:
        disassem->inst = EMU_INST_NEON_VCVT_F_I;
        TRANSLATE_NEON_VOPS_VD_VM;
        break;
    case ARM_NEON_VCVT_F_FP:
        disassem->inst = EMU_INST_NEON_VCVT_F_FP;
        TRANSLATE_NEON_VOPS_VD_VM;
        break;
    case ARM_NEON_VCVT_HP_SP:
        disassem->inst = EMU_INST_NEON_VCVT_HP_SP;
        TRANSLATE_NEON_VOPS_VD_VM;
        break;
    case ARM_VFP_VCVT_F_I: {
        uint32_t op, op2, size, d, vd, m, vm;
        arm_vfp_vcvt_f_i_decode_fields(read_address, &op, &op2, &size, &d, &vd, &m, &vm);

        bool to_integer = ((op2 & 0x4) != 0);
        bool dp_operation = (size == 1);

        if (to_integer == true) {
            disassem->Vd.reg = vd;
            disassem->Vd.s_reg = true;

            if (dp_operation == true) {
                disassem->Vm.reg = (m << 4) + vm;
            } else {
                disassem->Vm.reg = vm;
                disassem->Vm.s_reg = true;
            }
        } else {
            disassem->Vm.reg = vm;
            disassem->Vm.s_reg = true;
            if (dp_operation == true) {
                disassem->Vd.reg = (d << 4) + vd;
            } else {
                disassem->Vd.reg = vd;
                disassem->Vd.s_reg = true;
            }
        }

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_VFP_VCVT_F_I;
        break;
    }
    case ARM_VFP_VCVT_F_FP:
        disassem->inst = EMU_INST_VFP_VCVT_F_FP;
        break;
    case ARM_VFP_VCVT_DP_SP: {
        uint32_t size, d, vd, m, vm;
        arm_vfp_vcvt_dp_sp_decode_fields(read_address, &size, &d, &vd, &m, &vm);

        if (size == 1) {
            // Double to single
            disassem->Vd.reg = vd;
            disassem->Vd.s_reg = true;
            disassem->Vm.reg = (m << 4) + vm;
        } else {
            // Single to double
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vm.reg = vm;
            disassem->Vm.s_reg = true;
        }
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_VFP_VCVT_DP_SP;
        break;
    }
    case ARM_VFP_VCVTB:
        disassem->inst = EMU_INST_VFP_VCVTB;
        break;
    case ARM_VFP_VCVTT:
        disassem->inst = EMU_INST_VFP_VCVTT;
        break;
    case ARM_VFP_VDIV: {
        uint32_t size, d, vd, n, vn, m, vm;
        arm_vfp_vdiv_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VDIV;
        break;
    }
    case ARM_NEON_VDUP_SCAL:
        disassem->inst = EMU_INST_NEON_VDUP_SCAL;
        TRANSLATE_NEON_VOPS_VD_VM;
        break;
    case ARM_NEON_VDUP_CORE: {
        uint32_t null, q, d, vd, rt;
        arm_neon_vdup_core_decode_fields(read_address, &null, &null, &q, &d, &vd, &rt);

        vd = (d << 4) + vd;
        disassem->Vd.reg = vd;
        disassem->Vd.q_reg = q;
        disassem->Rt.reg = rt;

        disassem->format = INS_V_VD_RT;
        disassem->inst = EMU_INST_NEON_VDUP_CORE;
        break;
    }
    case ARM_NEON_VEOR:
        disassem->inst = EMU_INST_NEON_VEOR;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VEXT:
        disassem->inst = EMU_INST_NEON_VEXT;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VFMA:
        disassem->inst = EMU_INST_NEON_VFMA;
        break;
    case ARM_NEON_VFMS:
        disassem->inst = EMU_INST_NEON_VFMS;
        break;
    case ARM_VFP_VFMA:
        disassem->inst = EMU_INST_VFP_VFMA;
        break;
    case ARM_VFP_VFMS:
        disassem->inst = EMU_INST_VFP_VFMS;
        break;
    case ARM_VFP_VFNMA:
        disassem->inst = EMU_INST_VFP_VFNMA;
        break;
    case ARM_VFP_VFNMS:
        disassem->inst = EMU_INST_VFP_VFNMS;
        break;
    case ARM_NEON_VHADD:
        disassem->inst = EMU_INST_NEON_VHADD;
        break;
    case ARM_NEON_VHSUB:
        disassem->inst = EMU_INST_NEON_VHSUB;
        break;
    case ARM_NEON_VLDX_M:
        // VLD1, VLD2, VLD3, VLD4, multiple element structures
        disassem->inst = EMU_INST_NEON_VLDX_M;
        TRANSLATE_NEON_VLDX_VSTX_M;
        break;
    case ARM_NEON_VLDX_S_O:
        // VLD1, VLD2, VLD3, VLD4, single element structures to one lanes
        disassem->inst = EMU_INST_NEON_VLDX_S_O;
        TRANSLATE_NEON_VLDX_VSTX_S_O;
        break;
    case ARM_NEON_VLDX_S_A:
        // VLD1, VLD2, VLD3, VLD4, single element structures to all lanes
        disassem->inst = EMU_INST_NEON_VLDX_S_A;
        TRANSLATE_NEON_VLDX_S_A;
        break;
    case ARM_VFP_VLDM_DP: {
        uint32_t null, upwards, d, rn, vd, imm8;

        arm_vfp_vldm_dp_decode_fields(read_address, &null, &upwards, &d, &null, &rn, &vd, &imm8);

        vd = (d << 4) + vd;

        disassem->Rn.reg = rn;
        disassem->is_up = upwards;

        uint32_t num_regs = imm8/2;
        int i = 0;
        for (i=0; i<num_regs; i++) {
            disassem->reglist |= (1 << (vd+i));
        }

        disassem->imm = (imm8 << 2);
        disassem->is_reglist_single = false;
        disassem->format = INS_V_RN_REGLIST;
        disassem->inst = EMU_INST_VFP_VLDM_DP;
        break;
    }
    case ARM_VFP_VLDM_SP:
        disassem->inst = EMU_INST_VFP_VLDM_SP;
        break;
    case ARM_VFP_VLDR_DP: {
        uint32_t upwards, d, rn, vd, imm8;
        arm_vfp_vldr_dp_decode_fields(read_address, &upwards, &d, &rn, &vd, &imm8);

        vd = (d << 4) + vd;
        disassem->Vd.reg = vd;
        disassem->Rn.reg = rn;
        disassem->is_up = upwards;
        disassem->is_pre_indexed = true;

        // According to the manual, imm32 = ZeroExtend(imm8:'00', 32);
        // Hence the bitshift left by 2.
        disassem->imm = (imm8 << 2);

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VLDR_DP;
        break;
    }
    case ARM_VFP_VLDR_SP: {
        uint32_t upwards, rn, d, vd, imm8;
        arm_vfp_vldr_sp_decode_fields(read_address, &upwards, &d, &rn, &vd, &imm8);

        disassem->Vd.reg = vd;
        disassem->Vd.s_reg = true;

        disassem->Rn.reg = rn;
        disassem->is_up = upwards;
        disassem->is_pre_indexed = true;

        // According to the manual, imm32 = ZeroExtend(imm8:'00', 32);
        // Hence the bitshift left by 2.
        disassem->imm = (imm8 << 2);

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VLDR_SP;
        break;
    }
    case ARM_NEON_VMAX_I:
        disassem->inst = EMU_INST_NEON_VMAX_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VMIN_I:
        disassem->inst = EMU_INST_NEON_VMIN_I;
        break;
    case ARM_NEON_VMAX_F:
        disassem->inst = EMU_INST_NEON_VMAX_F;
        break;
    case ARM_NEON_VMIN_F:
        disassem->inst = EMU_INST_NEON_VMIN_F;
        break;
    case ARM_NEON_VMLA_I:
        disassem->inst = EMU_INST_NEON_VMLA_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VMLS_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        arm_neon_vmls_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;

        if (q == 1) {
            disassem->Vd.q_reg = true;
            disassem->Vn.q_reg = true;
            disassem->Vm.q_reg = true;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VMLS_I;
        break;
    }
    case ARM_NEON_VMLAL_I: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        arm_neon_vmlal_i_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vd.q_reg = true;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_NEON_VMLAL_I;
        break;
    }
    case ARM_NEON_VMLSL_I: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        arm_neon_vmlsl_i_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vd.q_reg = true;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VMLSL_I;
        break;
    }
    case ARM_NEON_VMLA_F:
        disassem->inst = EMU_INST_NEON_VMLA_F;
        break;
    case ARM_NEON_VMLS_F:
        disassem->inst = EMU_INST_NEON_VMLS_F;
        break;
    case ARM_VFP_VMLA_F:
        disassem->inst = EMU_INST_VFP_VMLA_F;
        break;
    case ARM_VFP_VMLS_F:
        disassem->inst = EMU_INST_VFP_VMLS_F;
        break;
    case ARM_NEON_VMLA_SCAL:
        disassem->inst = EMU_INST_NEON_VMLA_SCAL;
        break;
    case ARM_NEON_VMLS_SCAL:
        disassem->inst = EMU_INST_NEON_VMLS_SCAL;
        break;
    case ARM_NEON_VMLAL_SCAL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        arm_neon_vmlal_scal_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vd.q_reg = true;
        disassem->Vn.reg = (n << 4) + vn;

        if (size == 0b01) {
            disassem->Vm.reg = vm & 0x7;
        } else if (size == 0b10) {
            disassem->Vm.reg = vm;
        } else {
            emu_abort("ARM_NEON_VMLAL_SCAL.size has unexpected value (%d).\n", size);
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VMLAL_SCAL;
        break;
    }
    case ARM_NEON_VMLSL_SCAL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        arm_neon_vmlsl_scal_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vd.q_reg = true;
        disassem->Vn.reg = (n << 4) + vn;

        if (size == 0b01) {
            disassem->Vm.reg = vm & 0x7;
        } else if (size == 0b10) {
            disassem->Vm.reg = vm;
        } else {
            emu_abort("ARM_NEON_VMULL_SCAL.size has unexpected value (%d).\n", size);
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VMLSL_SCAL;
        break;
    }
    case ARM_NEON_VMOVI: {
        uint32_t q, d, vd, null;
        arm_neon_vmovi_decode_fields(read_address, &q, &d, &vd, &null, &null, &null, &null, &null);
        vd = (d << 4) + vd;
        disassem->Vd.reg = vd;
        disassem->Vd.q_reg = q;
        disassem->format = INS_V_VD_IMM;
        disassem->inst = EMU_INST_NEON_VMOVI;
        break;
    }
    case ARM_VFP_VMOVI: {
        uint32_t size, d, vd, imm4h, imm4l;
        arm_vfp_vmovi_decode_fields(read_address, &size, &d, &vd, &imm4h, &imm4l);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vd.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
        }

        disassem->format = INS_V_VD_IMM;
        disassem->inst = EMU_INST_VFP_VMOVI;
        break;
    }
    case ARM_VFP_VMOV:
        disassem->inst = EMU_INST_VFP_VMOV;
        break;
    case ARM_VFP_VMOV_CORE_SCAL: {
        uint32_t d, vd, opc1, opc2, rt;
        arm_vfp_vmov_core_scal_decode_fields(read_address, &d, &vd, &opc1, &opc2, &rt);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Rt.reg = rt;
        disassem->format = INS_V_VD_RT;

        // case opc1:opc2  ('opc1' and 'opc2' are 2 bits each)
        if ((opc1 & 0xb10) != 0) {
            // when '1xxx'
            disassem->size = 8;
        } else if ((opc2 & 0b01) != 0) {
            // when '0xx1'
            disassem->size = 16;
        } else if (opc2 == 0) {
            // when '0x00'
            disassem->size = 32;
        } else {
            emu_abort("Invalid size for ARM_VFP_VMOV_CORE_SCAL. opc1: 0x%02x, opc2: 0x%02x.\n", opc1, opc2);
        }

        disassem->inst = EMU_INST_VFP_VMOV_CORE_SCAL;
        break;
    }
    case ARM_VFP_VMOV_SCAL_CORE: {
        uint32_t unsign, rt, n, vn, opc1, opc2;
        arm_vfp_vmov_scal_core_decode_fields(read_address, &unsign, &rt, &n, &vn, &opc1, &opc2);

        disassem->Vn.reg = (n << 4) + vn;
        disassem->Rt.reg = rt;
        disassem->format = INS_V_RT_VN;

        // 'u' below corresponds to the 'unsign' variable
        // case u:opc1:opc2  ('u' is 1 bit, 'opc1' and 'opc2' are 2 bits each)
        if ((opc1 & 0xb10) != 0) {
            // when 'x1xxx'
            disassem->size = 8;
        } else if ((opc2 & 0b01) != 0) {
            // when 'x0xx1'
            disassem->size = 16;
        } else if ((unsign == 0) && (opc2 == 0)) {
            // when '00x00'
            disassem->size = 32;
        } else {
            emu_abort("Invalid size for ARM_VFP_VMOV_SCAL_CORE. opc1: 0x%02x, opc2: 0x%02x.\n", opc1, opc2);
        }

        disassem->inst = EMU_INST_VFP_VMOV_SCAL_CORE;
        break;
    }
    case ARM_VFP_VMOV_CORE_SP: {
        uint32_t op, rt, n, vn;
        arm_vfp_vmov_core_sp_decode_fields(read_address, &op, &rt, &n, &vn);

        disassem->Vn.reg = vn;
        disassem->Vn.s_reg = true;
        disassem->Rt.reg = rt;

        if (op == 1) {
            disassem->format = INS_V_RT_VN;
        } else {
            disassem->format = INS_V_VN_RT;
        }

        disassem->inst = EMU_INST_VFP_VMOV_CORE_SP;
        break;
    }
    case ARM_VFP_VMOV_2CORE_2SP:
        disassem->inst = EMU_INST_VFP_VMOV_2CORE_2SP;
        break;
    case ARM_VFP_VMOV_2CORE_DP: {
        uint32_t to_arm, rt, rt2, m, vm;
        arm_vfp_vmov_2core_dp_decode_fields(read_address, &to_arm, &rt, &rt2, &m, &vm);

        disassem->Rt.reg = rt;
        disassem->Rt2.reg = rt2;
        disassem->Vm.reg = (m << 4) + vm;

        if (to_arm == 1) {
            disassem->format = INS_V_RT_RT2_VM;
        } else {
            disassem->format = INS_V_VM_RT_RT2;
        }

        disassem->inst = EMU_INST_VFP_VMOV_2CORE_DP;
        break;
    }
    case ARM_NEON_VMOVL: {
        uint32_t unsign, d, vd, m, vm, imm3;
        arm_neon_vmovl_decode_fields(read_address, &unsign, &d, &vd, &m, &vm, &imm3);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = true;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VMOVL;
        break;
    }
    case ARM_NEON_VMOVN: {
        uint32_t d, vd, m, vm, null;
        arm_neon_vmovn_decode_fields(read_address, &null, &d, &vd, &m, &vm);

        vd = (d << 4) + vd;
        vm = (m << 4) + vm;
        disassem->Vd.reg = vd;
        disassem->Vm.reg = vm;
        disassem->Vm.q_reg = true;

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_NEON_VMOVN;
        break;
    }
    case ARM_VFP_VMRS: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VMRS;
        break;
    }
    case ARM_VFP_VMSR:
        disassem->inst = EMU_INST_VFP_VMSR;
        break;
    case ARM_NEON_VMUL_I:
        disassem->inst = EMU_INST_NEON_VMUL_I;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VMULL_I: {
        uint32_t opcode, opcode4, size, d, vd, n, vn, m, vm;
        arm_neon_vmull_i_decode_fields(read_address, &opcode, &opcode4, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = true;
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_NEON_VMULL_I;
        break;
    }
    case ARM_NEON_VMUL_F:
        disassem->inst = EMU_INST_NEON_VMUL_F;
        break;
    case ARM_VFP_VMUL_F: {
	uint32_t size, d, vn, vd, n, m, vm;
        arm_vfp_vmul_f_decode_fields(read_address, &size, &d, &vn, &vd, &n, &m, &vm);

        if (size == 1) {
            // dp operation
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        } else {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VMUL_F;
        break;
    }
    case ARM_NEON_VMUL_SCAL:{
        uint32_t opcode, opcode5, size, d, vd, n, vn, m, vm;
        arm_neon_vmul_scal_decode_fields(read_address, &opcode, &opcode5, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        if (opcode == 1) {
            disassem->Vd.q_reg = true;
            disassem->Vn.q_reg = true;
        }

        if (size == 0b01) {
            // m = UInt(Vm<2:0>)
            disassem->Vm.reg = (vm & 0x7);
        } else if (size == 0b10) {
            // m = UInt(Vm)
            disassem->Vm.reg = vm;
        } else {
            emu_abort("Unsupported size in ARM_NEON_VMUL_SCAL");
        }
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_NEON_VMUL_SCAL;
        break;
    }
    case ARM_NEON_VMULL_SCAL: {
        uint32_t opcode, size, d, vd, n, vn, m, vm;
        arm_neon_vmull_scal_decode_fields(read_address, &opcode, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vd.q_reg = true;
        disassem->Vn.reg = (n << 4) + vn;

        if (size == 0b01) {
            disassem->Vm.reg = vm & 0x7;
        } else if (size == 0b10) {
            disassem->Vm.reg = vm;
        } else {
            emu_abort("ARM_NEON_VMULL_SCAL.size has unexpected value (%d).\n", size);
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VMULL_SCAL;
        break;
    }
    case ARM_NEON_VMVNI: {
        uint32_t q, d, vd, null;
        arm_neon_vmvni_decode_fields(read_address, &q, &d, &vd, &null, &null, &null, &null);
	vd = (d << 4) + vd;
        disassem->Vd.reg = vd;
        disassem->Vd.q_reg = q;
        disassem->format = INS_V_VD_IMM;
        disassem->inst = EMU_INST_NEON_VMVNI;
        break;
    }
    case ARM_NEON_VMVN: {
        uint32_t size, q, d, vd, m, vm;
        arm_neon_vmvn_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        if (q == 1) {
            disassem->Vd.q_reg = true;
            disassem->Vm.q_reg = true;
        }

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_NEON_VMVN;
        break;
    }
    case ARM_NEON_VNEG: {
        uint32_t float_p, size, q, d, vd, m, vm;
        arm_neon_vneg_decode_fields(read_address, &float_p, &size, &q, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vm.q_reg = q;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VNEG;
        break;
    }
    case ARM_VFP_VNEG:
        disassem->inst = EMU_INST_VFP_VNEG;
        break;
    case ARM_VFP_VNMLA:
        disassem->inst = EMU_INST_VFP_VNMLA;
        break;
    case ARM_VFP_VNMLS:
        disassem->inst = EMU_INST_VFP_VNMLS;
        break;
    case ARM_VFP_VNMUL:
        disassem->inst = EMU_INST_VFP_VNMUL;
        break;
    case ARM_NEON_VORN:
        disassem->inst = EMU_INST_NEON_VORN;
        break;
    case ARM_NEON_VORRI:
        disassem->inst = EMU_INST_NEON_VORRI;
        break;
    case ARM_NEON_VORR:
        disassem->inst = EMU_INST_NEON_VORR;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VPADAL: {
        uint32_t unsign, size, q, d, vd, m, vm;
        arm_neon_vpadal_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vm.q_reg = q;

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_NEON_VPADAL;
        break;
    }
    case ARM_NEON_VPADD_I: {
	uint32_t size, q, d, vd, n, vn, m, vm;
        arm_neon_vpadd_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_NEON_VPADD_I;
        break;
    }
    case ARM_NEON_VPADD_F:
        disassem->inst = EMU_INST_NEON_VPADD_F;
        break;
    case ARM_NEON_VPADDL: {
        uint32_t unsign, size, q, d, vd, m, vm;
        arm_neon_vpaddl_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vm.q_reg = q;

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_NEON_VPADDL;
        break;
    }
    case ARM_NEON_VPMAX_I: {
	uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        arm_neon_vpmax_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VPMAX_I;
        break;
    }
    case ARM_NEON_VPMIN_I:
        disassem->inst = EMU_INST_NEON_VPMIN_I;
        break;
    case ARM_NEON_VPMAX_F:
        disassem->inst = EMU_INST_NEON_VPMAX_F;
        break;
    case ARM_NEON_VPMIN_F:
        disassem->inst = EMU_INST_NEON_VPMIN_F;
        break;
    case ARM_VFP_VPOP_DP: {
        uint32_t d, vd, imm8;
        arm_vfp_vpop_dp_decode_fields(read_address, &d, &vd, &imm8);

        vd = (d << 4) + vd;
        uint32_t num_regs = imm8/2;
        int i = 0;
        for (i=0; i<num_regs; i++) {
            disassem->reglist |= (1 << (vd+i));
        }

        disassem->imm = (imm8 << 2);
        disassem->is_reglist_single = false;
        disassem->format = INS_V_REGLIST;
        disassem->inst = EMU_INST_VFP_VPOP_DP;
        break;
    }
    case ARM_VFP_VPOP_SP:
        disassem->inst = EMU_INST_VFP_VPOP_SP;
        break;
    case ARM_VFP_VPUSH_DP: {
        uint32_t d, vd, imm8;
        arm_vfp_vpush_dp_decode_fields(read_address, &d, &vd, &imm8);

        vd = (d << 4) + vd;

        uint32_t num_regs = imm8/2;
        int i = 0;
        for (i=0; i<num_regs; i++) {
            disassem->reglist |= (1 << (vd+i));
        }

        disassem->imm = (imm8 << 2);
        disassem->is_reglist_single = false;
        disassem->format = INS_V_REGLIST;
        disassem->inst = EMU_INST_VFP_VPUSH_DP;
        break;
    }
    case ARM_VFP_VPUSH_SP:
        disassem->inst = EMU_INST_VFP_VPUSH_SP;
        break;
    case ARM_NEON_VQABS:
        disassem->inst = EMU_INST_NEON_VQABS;
        break;
    case ARM_NEON_VQADD: {
	uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        arm_neon_vqadd_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;

        if (size == 1) {
            // quad word operation
            disassem->Vd.q_reg = true;
            disassem->Vn.q_reg = true;
            disassem->Vm.q_reg = true;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VQADD;
        break;
    }
    case ARM_NEON_VQDMLAL_I:
        disassem->inst = EMU_INST_NEON_VQDMLAL_I;
        break;
    case ARM_NEON_VQDMLSL_I:
        disassem->inst = EMU_INST_NEON_VQDMLSL_I;
        break;
    case ARM_NEON_VQDMLAL_SCAL:
        disassem->inst = EMU_INST_NEON_VQDMLAL_SCAL;
        break;
    case ARM_NEON_VQDMLSL_SCAL:
        disassem->inst = EMU_INST_NEON_VQDMLSL_SCAL;
        break;
    case ARM_NEON_VQDMULH_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        arm_neon_vqdmulh_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VQDMULH_I;
        break;
    }
    case ARM_NEON_VQDMULH_SCAL: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        arm_neon_vqdmulh_scal_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;

        if (size == 0b01) {
            disassem->Vm.reg = vm & 0x7;
        } else if (size == 0b10) {
            disassem->Vm.reg = vm;
        }

        if (q == 1) {
            disassem->Vd.q_reg = true;
            disassem->Vn.q_reg = true;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VQDMULH_SCAL;
        break;
    }
    case ARM_NEON_VQDMULL_I:
        disassem->inst = EMU_INST_NEON_VQDMULL_I;
        break;
    case ARM_NEON_VQDMULL_SCAL:
        disassem->inst = EMU_INST_NEON_VQDMULL_SCAL;
        break;
    case ARM_NEON_VQMOVN:
        disassem->inst = EMU_INST_NEON_VQMOVN;
        break;
    case ARM_NEON_VQMOVUN: {
        uint32_t size, d, vd, m, vm;
        arm_neon_vqmovun_decode_fields(read_address, &size, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vm.q_reg = true;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VQMOVUN;
        break;
    }
    case ARM_NEON_VQNEG:
        disassem->inst = EMU_INST_NEON_VQNEG;
        break;
    case ARM_NEON_VQRDMULH_I:
        disassem->inst = EMU_INST_NEON_VQRDMULH_I;
        break;
    case ARM_NEON_VQRDMULH_SCAL:
        disassem->inst = EMU_INST_NEON_VQRDMULH_SCAL;
        break;
    case ARM_NEON_VQRSHL:
        disassem->inst = EMU_INST_NEON_VQRSHL;
        break;
    case ARM_NEON_VQRSHRN: {
        uint32_t unsign, d, vd, m, vm, imm6;
        arm_neon_vqrshrn_decode_fields(read_address, &unsign, &d, &vd, &m, &vm, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vm.q_reg = true;
        disassem->format = INS_V_VD_VM_CONST;

        disassem->inst = EMU_INST_NEON_VQRSHRN;
        break;
    }
    case ARM_NEON_VQRSHRUN: {
        uint32_t d, vd, m, vm, imm6;
        arm_neon_vqshrun_decode_fields(read_address, &d, &vd, &m, &vm, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vm.q_reg = true;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VQRSHRUN;
        break;
    }
    case ARM_NEON_VQSHL:
        disassem->inst = EMU_INST_NEON_VQSHL;
        break;
    case ARM_NEON_VQSHLI:
        disassem->inst = EMU_INST_NEON_VQSHLI;
        break;
    case ARM_NEON_VQSHLUI:
        disassem->inst = EMU_INST_NEON_VQSHLUI;
        break;
    case ARM_NEON_VQSHRN:
        disassem->inst = EMU_INST_NEON_VQSHRN;
        break;
    case ARM_NEON_VQSHRUN:
        disassem->inst = EMU_INST_NEON_VQSHRUN;
        break;
    case ARM_NEON_VQSUB: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        arm_neon_vqsub_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;
        if (q == 1) {
            disassem->Vd.q_reg = true;
            disassem->Vn.q_reg = true;
            disassem->Vm.q_reg = true;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VQSUB;
        break;
    }
    case ARM_NEON_VRADDHN:
        disassem->inst = EMU_INST_NEON_VRADDHN;
        break;
    case ARM_NEON_VRECPE:
        disassem->inst = EMU_INST_NEON_VRECPE;
        break;
    case ARM_NEON_VRECPS:
        disassem->inst = EMU_INST_NEON_VRECPS;
        break;
    case ARM_NEON_VREV16:
        disassem->inst = EMU_INST_NEON_VREV16;
        break;
    case ARM_NEON_VREV32: {
        uint32_t size, q, d, vd, m, vm;
        arm_neon_vrev32_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vm.q_reg = q;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VREV32;
        break;
    }
    case ARM_NEON_VREV64: {
        uint32_t size, q, d, vd, m, vm;
        arm_neon_vrev64_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vm.q_reg = q;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VREV64;
        break;
    }
    case ARM_NEON_VRHADD: {
	uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        arm_neon_vrhadd_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vn.q_reg = q;
        disassem->Vm.q_reg = q;
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_NEON_VRHADD;
        break;
    }
    case ARM_NEON_VRSHL:
        disassem->inst = EMU_INST_NEON_VRSHL;
        break;
    case ARM_NEON_VRSHR: {
        uint32_t unsign, q, d, vd, m, vm, l, imm6;
        arm_neon_vrshr_decode_fields(read_address, &unsign, &q, &d, &vd, &m, &vm, &l, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        if (q == 1) {
            disassem->Vd.q_reg = true;
            disassem->Vm.q_reg = true;
        }
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VRSHR;
        break;
    }
    case ARM_NEON_VRSHRN: {

        uint32_t d, vd, m, vm, imm6;
        arm_neon_vrshrn_decode_fields(read_address, &d, &vd, &m, &vm, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vm.q_reg = true;
        disassem->format = INS_V_VD_VM_CONST;

        disassem->inst = EMU_INST_NEON_VRSHRN;
        break;
    }
    case ARM_NEON_VRSQRTE:
        disassem->inst = EMU_INST_NEON_VRSQRTE;
        break;
    case ARM_NEON_VRSQRTS:
        disassem->inst = EMU_INST_NEON_VRSQRTS;
        break;
    case ARM_NEON_VRSRA:
        disassem->inst = EMU_INST_NEON_VRSRA;
        break;
    case ARM_NEON_VRSUBHN:
        disassem->inst = EMU_INST_NEON_VRSUBHN;
        break;
    case ARM_NEON_VSHL:
        disassem->inst = EMU_INST_NEON_VSHL;
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        break;
    case ARM_NEON_VSHLI: {

        uint32_t q, d, vd, m, vm, l, imm6;
        arm_neon_vshli_decode_fields(read_address, &q, &d, &vd, &m, &vm, &l, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vm.q_reg = q;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VSHLI;
        break;
    }
    case ARM_NEON_VSHLL: {
        uint32_t unsign, d, vd, m, vm, imm6;
        arm_neon_vshll_decode_fields(read_address, &unsign, &d, &vd, &m, &vm, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = true;

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_NEON_VSHLL;
        break;
    }
    case ARM_NEON_VSHLL2:
        disassem->inst = EMU_INST_NEON_VSHLL2;
        break;
    case ARM_NEON_VSHR: {
        uint32_t unsign, q, d, vd, m, vm, l, imm6;
        arm_neon_vshr_decode_fields(read_address, &unsign, &q, &d, &vd, &m, &vm, &l, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vm.q_reg = q;
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_NEON_VSHR;
        break;
    }
    case ARM_NEON_VSHRN: {
        uint32_t d, vd, m, vm, imm6;
        arm_neon_vshrn_decode_fields(read_address, &d, &vd, &m, &vm, &imm6);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vm.q_reg = true;
        disassem->format = INS_V_VD_VM_CONST;

        disassem->inst = EMU_INST_NEON_VSHRN;
        break;
    }
    case ARM_NEON_VSLI:
        disassem->inst = EMU_INST_NEON_VSLI;
        break;
    case ARM_VFP_VSQRT:
        disassem->inst = EMU_INST_VFP_VSQRT;
        break;
    case ARM_NEON_VSRA:
        disassem->inst = EMU_INST_NEON_VSRA;
        break;
    case ARM_NEON_VSRI:
        disassem->inst = EMU_INST_NEON_VSRI;
        break;
    case ARM_NEON_VSTX_M:
        // VST1, VST2, VST3, VST4, multiple element structures
        disassem->inst = EMU_INST_NEON_VSTX_M;
        TRANSLATE_NEON_VLDX_VSTX_M;
        break;
    case ARM_NEON_VSTX_S_O:
        // VST1, VST2, VST3, VST4, single element structures to one lanes
        disassem->inst = EMU_INST_NEON_VSTX_S_O;
        TRANSLATE_NEON_VLDX_VSTX_S_O;
        break;
    case ARM_VFP_VSTM_DP: {
        uint32_t null, upwards, d, rn, vd, imm8;
        arm_vfp_vstm_dp_decode_fields(read_address, &null, &upwards, &d, &null, &rn, &vd, &imm8);

        vd = (d << 4) + vd;
        disassem->Rn.reg = rn;
        disassem->is_up = upwards;

        uint32_t num_regs = imm8/2;
        int i = 0;
        for (i=0; i<num_regs; i++) {
            disassem->reglist |= (1 << (vd+i));
        }

        disassem->imm = (imm8 << 2);
        disassem->is_reglist_single = false;
        disassem->format = INS_V_RN_REGLIST;
        disassem->inst = EMU_INST_VFP_VSTM_DP;
        break;
    }
    case ARM_VFP_VSTM_SP:
        disassem->inst = EMU_INST_VFP_VSTM_SP;
        break;
    case ARM_VFP_VSTR_DP: {
        uint32_t upwards, d, rn, vd, imm8;
        arm_vfp_vstr_dp_decode_fields(read_address, &upwards, &d, &rn, &vd, &imm8);

        vd = (d << 4) + vd;
        disassem->Vd.reg = vd;
        disassem->Rn.reg = rn;
        disassem->is_up = upwards;

        // VFP_VSTR_DP is always pre_indexed.
        disassem->is_pre_indexed = true;

        // According to the manual, imm32 = ZeroExtend(imm8:'00', 32);
        // Hence the bitshift left by 2.
        disassem->imm = (imm8 << 2);

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VSTR_DP;
        break;
    }
    case ARM_VFP_VSTR_SP: {
        uint32_t upwards, d, rn, vd, imm8;
        arm_vfp_vstr_dp_decode_fields(read_address, &upwards, &d, &rn, &vd, &imm8);

        disassem->Vd.reg = vd;
        disassem->Vd.s_reg = true;
        disassem->Rn.reg = rn;

        disassem->imm = (imm8 << 2);
        disassem->is_up = upwards;
        disassem->is_pre_indexed = true;

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VSTR_SP;
        break;
    }
    case ARM_NEON_VSUB_I:
        TRANSLATE_NEON_VOPS_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VSUB_I;
        break;
    case ARM_NEON_VSUB_F:
        disassem->inst = EMU_INST_NEON_VSUB_F;
        break;
    case ARM_VFP_VSUB_F: {
        uint32_t size, d, vd, n, vn, m, vm;
        arm_vfp_vsub_f_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_VFP_VSUB_F;
        break;
    }
    case ARM_NEON_VSUBHN:
        disassem->inst = EMU_INST_NEON_VSUBHN;
        break;
    case ARM_NEON_VSUBL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        arm_neon_vsubl_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = true;

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VSUBL;
        break;
    }
    case ARM_NEON_VSUBW:
        disassem->inst = EMU_INST_NEON_VSUBW;
        break;
    case ARM_NEON_VSWP: {
        uint32_t size, q, d, vd, m, vm;
        arm_neon_vswp_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vm.reg = (m << 4) + vm;
        if (q == 1) {
            disassem->Vd.q_reg = true;
            disassem->Vm.q_reg = true;
        }

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_NEON_VSWP;
        break;
    }
    case ARM_NEON_VTBL:
        disassem->inst = EMU_INST_NEON_VTBL;
        break;
    case ARM_NEON_VTBX:
        disassem->inst = EMU_INST_NEON_VTBX;
        break;
    case ARM_NEON_VTRN:
        TRANSLATE_NEON_VOPS_VD_VM;
        disassem->inst = EMU_INST_NEON_VTRN;
        break;
    case ARM_NEON_VTST: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        arm_neon_vtst_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Vn.reg = (n << 4) + vn;
        disassem->Vm.reg = (m << 4) + vm;
        disassem->Vd.q_reg = q;
        disassem->Vn.q_reg = q;
        disassem->Vm.q_reg = q;

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_NEON_VTST;
        break;
    }
    case ARM_NEON_VUZP:
        disassem->inst = EMU_INST_NEON_VUZP;
        break;
    case ARM_NEON_VZIP:
        disassem->inst = EMU_INST_NEON_VZIP;
        break;
    case ARM_INVALID:
        disassem->inst = EMU_INST_INVALID;
        emu_abort("INVALID INSTRUCTION!");
        break;
    }

    if (disassem->format == INS_INVALID) {
        emu_abort("Format of Instruction (%d, %s, 0x%08x,) not set during ARM translation!", disassem->inst, inst_strings[disassem->inst], *(read_address));
    }
}

void thumb_translate_to_disassem(thumb_instruction inst, uint16_t *read_address, emu_disassem_t *disassem) {

    memset(disassem, 0, sizeof(emu_disassem_t));

    // By default, we'll assume the Thumb instruction is a 4-byte
    // instruction and we'll overwrite the default value if the
    // instruction turns out to be a 2-byte instruction.
    disassem->size = 4;

    // Thumb instructions are typically unconditional. The two times
    // an unconditional instruction becomes conditional is when it
    // appears when an IT block, or when the instruction is
    // B_COND. Let's just set it as unconditional for now, change it
    // if the instruction is a B_COND, and leave IT block handling to
    // whoever needs the decoder.
    disassem->cond = EMU_ARM_CC_AL;

    switch(inst) {

    case THUMB_ADC16: {
        uint32_t rm, rdn;
        thumb_adc16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_ADC;
        disassem->size = 2;
        break;
    }
    case THUMB_ADD16: {
        uint32_t rm, rn, rd;
        thumb_add16_decode_fields(read_address, &rm, &rn, &rd);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_ADD;
        disassem->size = 2;
        break;
    }
    case THUMB_ADD_FROM_PC16: {
        uint32_t rd, imm8;
        thumb_add_from_pc16_decode_fields(read_address, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->format = INS_RD_CONST;

        disassem->inst = EMU_INST_ADD;
        disassem->size = 2;
        break;
    }
    case THUMB_ADD_FROM_SP16: {
        uint32_t rd, imm8;
        thumb_add_from_sp16_decode_fields(read_address, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = EMU_ARM_REG_SP;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_ADD;
        disassem->size = 2;
        break;
    }
    case THUMB_ADDH16: {
        uint32_t dn, rm, rdn;
        thumb_addh16_decode_fields(read_address, &dn, &rm, &rdn);

        disassem->Rd.reg = (dn << 3) + rdn;
        disassem->Rn.reg = (dn << 3) + rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_ADD;
        disassem->size = 2;
        break;
    }
    case THUMB_ADDI16: {
        uint32_t imm3, rn, rd;
        thumb_addi16_decode_fields(read_address, &imm3, &rn, &rd);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_ADD;
        disassem->size = 2;
        break;
    }
    case THUMB_ADDRI16: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_ADD;
        disassem->size = 2;
        break;
    }
    case THUMB_ADD_SP_I16: {
        // uint32_t imm7;
        // thumb_add_sp_i16_decode_fields(read_address, &imm7);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_ADD;
        disassem->size = 2;
        break;
    }
    case THUMB_AND16: {
        uint32_t rm, rdn;
        thumb_and16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_AND;
        disassem->size = 2;
        break;
    }
    case THUMB_ASR16: {
        uint32_t rm, rdn;
        thumb_asr16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_ASRI16: {
        uint32_t imm5, rm, rd;
        thumb_asri16_decode_fields(read_address, &imm5, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_B16: {
        // uint32_t imm11;
        // thumb_b16_decode_fields(read_address, &imm11);

        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_B;
        disassem->size = 2;
        break;
    }
    case THUMB_BIC16: {
        uint32_t rm, rdn;
        thumb_bic16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_BIC;
        disassem->size = 2;
        break;
    }
    case THUMB_BKPT16: {
        uint32_t imm8;
        thumb_bkpt16_decode_fields(read_address, &imm8);
        disassem->inst = EMU_INST_BKPT;
        disassem->size = 2;
        break;
    }
    case THUMB_B_COND16: {
        uint32_t condition, imm8;
        thumb_b_cond16_decode_fields(read_address, &condition, &imm8);

        disassem->cond = condition;
        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_B_COND;
        disassem->size = 2;
        break;
    }
    case THUMB_BLX16: {
        uint32_t rm;
        thumb_blx16_decode_fields(read_address, &rm);

        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_BLX;
        disassem->size = 2;
        break;
    }
    case THUMB_BX16: {
        // uint32_t rm;
        // thumb_bx16_decode_fields(read_address, &rm);

        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_BX;
        disassem->size = 2;
        break;
    }
    case THUMB_CBNZ16: {
        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CBNZ;
        disassem->size = 2;
        break;
    }
    case THUMB_CBZ16: {
        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CBZ;
        disassem->size = 2;
        break;
    }
    case THUMB_CMN16: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMN;
        disassem->size = 2;
        break;
    }
    case THUMB_CMP16: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMP;
        disassem->size = 2;
        break;
    }
    case THUMB_CMPH16: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMP;
        disassem->size = 2;
        break;
    }
    case THUMB_CMPRI16: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMP;
        disassem->size = 2;
        break;
    }
    case THUMB_EOR16: {
        uint32_t rm, rdn;
        thumb_eor16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_EOR;
        disassem->size = 2;
        break;
    }
    case THUMB_IT16: {
        uint32_t cond, mask;
        thumb_it16_decode_fields(read_address, &cond, &mask);
        disassem->cond = cond;
        disassem->imm = mask;

        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_IT;
        disassem->size = 2;
        break;
    }
    case THUMB_LDMFD16: {
        uint32_t rn, reglist;
        thumb_ldmfd16_decode_fields(read_address, &rn, &reglist);

        disassem->Rn.reg = rn;
        disassem->reglist = reglist;

        if ((reglist & 0x8000) != 0) {
            disassem->is_branch = true;
        }

        // LDM/LDMIA/LDMFD: is_up == false, is_pre_indexed == true,  start_address = R[n]
        disassem->is_up = false;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_LDM;
        disassem->size = 2;
        break;
    }
    case THUMB_LDR16: {
        uint32_t rm, rn, rt;
        thumb_ldr16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDR;
        disassem->size = 2;
        break;
    }
    case THUMB_LDRB16: {
        uint32_t rm, rn, rt;
        thumb_ldrb16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRB;
        disassem->size = 2;
        break;
    }
    case THUMB_LDRBI16: {
        uint32_t imm5, rn, rt;
        thumb_ldrbi16_decode_fields(read_address, &imm5, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm5;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRB;
        disassem->size = 2;
        break;
    }
    case THUMB_LDRH16: {
        uint32_t rm, rn, rt;
        thumb_ldrh16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRH;
        disassem->size = 2;
        break;
    }
    case THUMB_LDRHI16: {
        uint32_t imm5, rn, rt;
        thumb_ldrhi16_decode_fields(read_address, &imm5, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = (imm5 << 1);

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRH;
        disassem->size = 2;
        break;
    }
    case THUMB_LDRI16: {
        uint32_t imm5, rn, rt;
        thumb_ldri16_decode_fields(read_address, &imm5, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = (imm5 << 2);

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDR;
        disassem->size = 2;
        break;
    }
    case THUMB_LDR_PC_16: {
        uint32_t rt, imm8;
        thumb_ldr_pc_16_decode_fields(read_address, &rt, &imm8);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = EMU_ARM_REG_PC;
        disassem->imm = (imm8 << 2);
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDR;
        disassem->size = 2;
        break;
    }
    case THUMB_LDRSB16: {
        uint32_t rm, rn, rt;
        thumb_ldrsb16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRSB;
        disassem->size = 2;
        break;
    }
    case THUMB_LDRSH16: {
        uint32_t rm, rn, rt;
        thumb_ldrsh16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRSH;
        disassem->size = 2;
        break;
    }
    case THUMB_LDR_SP16: {
        uint32_t rt, imm8;
        thumb_ldr_sp16_decode_fields(read_address, &rt, &imm8);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = EMU_ARM_REG_SP;
        disassem->imm = (imm8 << 2);

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDR;
        disassem->size = 2;
        break;
    }
    case THUMB_LSL16: {
        uint32_t rm, rdn;
        thumb_lsl16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_LSLI16: {
        uint32_t imm5, rm, rd;
        thumb_lsli16_decode_fields(read_address, &imm5, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_LSR16: {
        uint32_t rm, rdn;
        thumb_lsr16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_LSRI16: {
        uint32_t imm5, rm, rd;
        thumb_lsri16_decode_fields(read_address, &imm5, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_MOVH16: {
        uint32_t dn, rm, rdn;
        thumb_movh16_decode_fields(read_address, &dn, &rm, &rdn);

        disassem->Rd.reg = (dn << 3) + rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_MOVI16: {
        uint32_t rm, rd;
        thumb_movi16_decode_fields(read_address, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_MOVRI16: {
        uint32_t rdn, imm8;
        thumb_movri16_decode_fields(read_address, &rdn, &imm8);

        disassem->Rd.reg = rdn;
        disassem->format = INS_RD_CONST;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_MUL16: {
        uint32_t rm, rdn;
        thumb_mul16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MUL;
        disassem->size = 2;
        break;
    }
    case THUMB_MVN16: {
        uint32_t rm, rd;
        thumb_mvn16_decode_fields(read_address, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MVN;
        disassem->size = 2;
        break;
    }
    case THUMB_NOP16: {
        disassem->size = 2;
        break;
    }
    case THUMB_ORR16: {
        uint32_t rm, rdn;
        thumb_orr16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_ORR;
        disassem->size = 2;
        break;
    }
    case THUMB_POP16: {
        uint32_t reg;
        thumb_pop16_decode_fields(read_address, &reg);

        // This follows the reglist calculation of encoding T1 of POP,
        // which is 'P':'0000000':register_list. The "reg" variable
        // given by PIE is the last 9 bits of the instruction. See ARM
        // DDI 0406C.b, section A8.8.131, page A8-534, for more
        // information.
        bool P = (reg & 0x100) >> 8;
        uint32_t reglist = reg & 0xff;
        reglist = (P << 15) + reglist;

        if (P == true) {
            disassem->is_branch = true;
        }

        disassem->Rn.reg = EMU_ARM_REG_SP;
        disassem->reglist = reglist;

        // This is_up and is_pre_indexed properties here follow that
        // of LDM, where
        // LDM: is_up == true , is_pre_indexed == false, start_address = R[n]
        disassem->is_up = true;
        disassem->is_pre_indexed = false;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_LDM;
        disassem->size = 2;
        break;
    }
    case THUMB_PUSH16: {
        uint32_t reg;
        thumb_push16_decode_fields(read_address, &reg);

        // This follows the reglist calculation of encoding T1 of
        // PUSH, which is '0':M:'000000':register_list. The "reg"
        // variable given by PIE is the last 9 bits of the
        // instruction. See ARM DDI 0406C.b, section A8.8.133, page
        // A8-538, for more information.
        bool M = (reg & 0x100) >> 8;
        uint32_t reglist = reg & 0xff;
        reglist = (M << 14) + reglist;

        disassem->Rn.reg = EMU_ARM_REG_SP;
        disassem->reglist = reglist;

        // The is_up and is_pre_indexed property here follows that of STMDB, where
        // STMDB: is_up == false, is_pre_indexed == true,  start_address = R[n] - 4*BitCount(registers)
        disassem->is_up = false;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_STM;
        disassem->size = 2;
        break;
    }
    case THUMB_REV16: {
        uint32_t rn, rd;
        thumb_rev16_decode_fields(read_address, &rn, &rd);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_REV;
        disassem->size = 2;
        break;
    }
    case THUMB_REV1616: {
        uint32_t rn, rd;
        thumb_rev1616_decode_fields(read_address, &rn, &rd);
        disassem->inst = EMU_INST_REV;
        disassem->size = 2;
        break;
    }
    case THUMB_REVSH16: {
        uint32_t rn, rd;
        thumb_revsh16_decode_fields(read_address, &rn, &rd);
        disassem->inst = EMU_INST_REVSH;
        disassem->size = 2;
        break;
    }
    case THUMB_ROR16: {
        uint32_t rm, rdn;
        thumb_ror16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MOV;
        disassem->size = 2;
        break;
    }
    case THUMB_RSBI16: {
        uint32_t rm, rdn;
        thumb_rsbi16_decode_fields(read_address, &rm, &rdn);

        // For this instruction, PIE's Thumb decoder table assigns the
        // label "rm" to Rn, and "rdn" to Rd. That's why this looks a
        // little weird.
        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rm;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_RSB;
        disassem->size = 2;
        break;
    }
    case THUMB_SBC16: {
        uint32_t rm, rdn;
        thumb_sbc16_decode_fields(read_address, &rm, &rdn);

        disassem->Rd.reg = rdn;
        disassem->Rn.reg = rdn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SBC;
        disassem->size = 2;
        break;
    }
    case THUMB_SETEND16: {
        uint32_t is_bigend;
        thumb_setend16_decode_fields(read_address, &is_bigend);
        disassem->inst = EMU_INST_SETEND;
        disassem->size = 2;
        break;
    }
    case THUMB_SEV16: {
        disassem->inst = EMU_INST_SEV;
        disassem->size = 2;
        break;
    }
    case THUMB_STMFD16: {
        uint32_t rn, reglist;
        thumb_stmfd16_decode_fields(read_address, &rn, &reglist);

        // XXX: This appears to be a bug in PIE. There is no 16-bit
        // Thumb encoding of STMFD and this instruction is in fact
        // STMEA16.

        disassem->Rn.reg = rn;
        disassem->reglist = reglist;

        // STM/STMIA/STMEA: is_up == true , is_pre_indexed == false, start_address = R[n]
        disassem->is_up = true;
        disassem->is_pre_indexed = false;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_STM;
        disassem->size = 2;
        break;
    }
    case THUMB_STR16: {
        uint32_t rm, rn, rt;
        thumb_str16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_STR;
        disassem->size = 2;
        break;
    }
    case THUMB_STRB16: {
        uint32_t rm, rn, rt;
        thumb_strb16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_STRB;
        disassem->size = 2;
        break;
    }
    case THUMB_STRBI16: {
        uint32_t imm5, rn, rt;
        thumb_strbi16_decode_fields(read_address, &imm5, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm5;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_STRB;
        disassem->size = 2;
        break;
    }
    case THUMB_STRH16: {
        uint32_t rm, rn, rt;
        thumb_strh16_decode_fields(read_address, &rm, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_STRH;
        disassem->size = 2;
        break;
    }
    case THUMB_STRHI16: {
        uint32_t imm5, rn, rt;
        thumb_strhi16_decode_fields(read_address, &imm5, &rn, &rt);

        // According to the ARM manual, in STRH (Immediate, Thumb),
        // encoding T1, imm32 = ZeroExtend(imm5:'0', 32). This is why
        // we're doing the bit shift left by 1. See ARM DDI 0406C.b,
        // page A8-698, section A8.8.216.
        uint32_t imm = 0;
        imm = (imm5 << 1);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_STRH;
        disassem->size = 2;
        break;
    }
    case THUMB_STRI16: {
        uint32_t imm5, rn, rt;
        thumb_stri16_decode_fields(read_address, &imm5, &rn, &rt);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = (imm5 << 2);

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_STR;
        disassem->size = 2;
        break;
    }
    case THUMB_STR_SP16: {
        uint32_t rt, imm8;
        thumb_str_sp16_decode_fields(read_address, &rt, &imm8);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = EMU_ARM_REG_SP;
        disassem->imm = (imm8 << 2);

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_STR;
        disassem->size = 2;
        break;
    }
    case THUMB_SUB16: {
        uint32_t rm, rn, rd;
        thumb_sub16_decode_fields(read_address, &rm, &rn, &rd);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SUB;
        disassem->size = 2;
        break;
    }
    case THUMB_SUBI16: {
        uint32_t imm3, rn, rd;
        thumb_subi16_decode_fields(read_address, &imm3, &rn, &rd);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_SUB;
        disassem->size = 2;
        break;
    }
    case THUMB_SUBRI16: {
        uint32_t rdn, imm8;
        thumb_subri16_decode_fields(read_address, &rdn, &imm8);

        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_SUB;
        disassem->size = 2;
        break;
    }
    case THUMB_SUB_SP_I16: {
        // uint32_t imm7;
        // thumb_sub_sp_i16_decode_fields(read_address, &imm7);

        /* No need to translate further because there is no taint
         * propagation for this instruction. */
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_SUB;
        disassem->size = 2;
        break;
    }
    case THUMB_SVC16: {
        uint32_t imm8;
        thumb_svc16_decode_fields(read_address, &imm8);
        disassem->inst = EMU_INST_SVC;
        disassem->size = 2;
        break;
    }
    case THUMB_SXTB16: {
        uint32_t rm, rd;
        thumb_sxtb16_decode_fields(read_address, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_SXTB;
        disassem->size = 2;
        break;
    }
    case THUMB_SXTH16: {
        uint32_t rm, rd;
        thumb_sxth16_decode_fields(read_address, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_SXTH;
        disassem->size = 2;
        break;
    }
    case THUMB_TST16: {
        uint32_t rm, rdn;
        thumb_tst16_decode_fields(read_address, &rm, &rdn);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_TST;
        disassem->size = 2;
        break;
    }
    case THUMB_UDF16: {
        uint32_t imm8;
        thumb_udf16_decode_fields(read_address, &imm8);
        disassem->inst = EMU_INST_UDF;
        disassem->size = 2;
        break;
    }
    case THUMB_UXTB16: {
        uint32_t rm, rd;
        thumb_uxtb16_decode_fields(read_address, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_UXTB;
        disassem->size = 2;
        break;
    }
    case THUMB_UXTH16: {
        uint32_t rm, rd;
        thumb_uxth16_decode_fields(read_address, &rm, &rd);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_UXTH;
        disassem->size = 2;
        break;
    }
    case THUMB_WFE16: {
        disassem->inst = EMU_INST_WFE;
        disassem->size = 2;
        break;
    }
    case THUMB_WFI16: {
        disassem->inst = EMU_INST_WFI;
        disassem->size = 2;
        break;
    }
    case THUMB_YIELD: {
        disassem->inst = EMU_INST_YIELD;
        disassem->size = 2;
        break;
    }
    case THUMB_ADC32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_adc32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_ADC;
        break;
    }
    case THUMB_ADCI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_adci32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_ADC;
        break;
    }
    case THUMB_ADD32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_add32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_ADD;
        break;
    }
    case THUMB_ADDI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_addi32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_ADD;
        break;
    }
    case THUMB_ADDWI32: {
        uint32_t imm1, rn, imm3, rd, imm8;
        thumb_addwi32_decode_fields(read_address, &imm1, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_ADD;
        break;
    }
    case THUMB_ADRWI_POST32: {
        uint32_t imm1, rn, imm3, rd, imm8;
        thumb_adrwi_post32_decode_fields(read_address, &imm1, &rn, &imm3, &rd, &imm8);
        disassem->inst = EMU_INST_ADD;
        break;
    }
    case THUMB_ADRWI_PRE32: {
        uint32_t imm1, rn, imm3, rd, imm8;
        thumb_adrwi_pre32_decode_fields(read_address, &imm1, &rn, &imm3, &rd, &imm8);
        disassem->inst = EMU_INST_ADD;
        break;
    }
    case THUMB_AND32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_and32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_AND;
        break;
    }
    case THUMB_ANDI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_andi32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_AND;
        break;
    }
    case THUMB_ASR32: {
        uint32_t set_flags, rn, rd, rm;
        thumb_asr32_decode_fields(read_address, &set_flags, &rn, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_ASRI32: {
        uint32_t set_flags, imm3, rd, imm2, rm;
        thumb_asri32_decode_fields(read_address, &set_flags, &imm3, &rd, &imm2, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_B32: {
        uint32_t sign_bit, offset_high, j1, j2, offset_low;
        thumb_b32_decode_fields(read_address, &sign_bit, &offset_high, &j1, &j2, &offset_low);

        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_B;
        break;
    }
    case THUMB_B_COND32: {
        uint32_t sign_bit, condition, offset_high, j1, j2, offset_low;
        thumb_b_cond32_decode_fields(read_address, &sign_bit, &condition, &offset_high, &j1, &j2, &offset_low);

        disassem->cond = condition;
        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_B_COND;
        break;
    }
    case THUMB_BFC32: {
        uint32_t imm3, rd, imm2, imm5;
        thumb_bfc32_decode_fields(read_address, &imm3, &rd, &imm2, &imm5);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_BFC;
        break;
    }
    case THUMB_BFI32: {
        uint32_t rn, imm3, rd, imm2, imm5;
        thumb_bfi32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &imm5);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_LSB_WIDTH;

        disassem->inst = EMU_INST_BFI;
        break;
    }
    case THUMB_BIC32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_bic32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_BIC;
        break;
    }
    case THUMB_BICI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_bici32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_BIC;
        break;
    }
    case THUMB_BL32: {
        // uint32_t sign_bit, offset_high, j1, j2, offset_low;
        // thumb_bl32_decode_fields(read_address, &sign_bit, &offset_high, &j1, &j2, &offset_low);

        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_BL32;
        break;
    }
    case THUMB_BL_ARM32: {
        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_BL_ARM32;
        break;
    }
    case THUMB_CDP32: {
        uint32_t coproc, opc1, crd, crn, crm, opc2;
        thumb_cdp32_decode_fields(read_address, &coproc, &opc1, &crd, &crn, &crm, &opc2);
        disassem->inst = EMU_INST_CDP;
        break;
    }
    case THUMB_CDP232: {
        uint32_t coproc, opc1, crd, crn, crm, opc2;
        thumb_cdp232_decode_fields(read_address, &coproc, &opc1, &crd, &crn, &crm, &opc2);
        disassem->inst = EMU_INST_CDP2;
        break;
    }
    case THUMB_CLREX32: {
        uint32_t option;
        thumb_clrex32_decode_fields(read_address, &option);
        disassem->inst = EMU_INST_CLREX;
        break;
    }
    case THUMB_CLZ32: {
        uint32_t rn, rd, rm;
        thumb_clz32_decode_fields(read_address, &rn, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_CLZ;
        break;
    }
    case THUMB_CMN32: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMN;
        break;
    }
    case THUMB_CMNI32: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMN;
        break;
    }
    case THUMB_CMP32: {
        uint32_t rn, imm3, imm2, shift_type, rm;
        thumb_cmp32_decode_fields(read_address, &rn, &imm3, &imm2, &shift_type, &rm);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMP;
        break;
    }
    case THUMB_CMPI32: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_CMP;
        break;
    }
    case THUMB_DBG32: {
        uint32_t option;
        thumb_dbg32_decode_fields(read_address, &option);
        disassem->inst = EMU_INST_DBG;
        break;
    }
    case THUMB_DMB32: {
        uint32_t option;
        thumb_dmb32_decode_fields(read_address, &option);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_DMB;
        break;
    }
    case THUMB_DSB32: {
        uint32_t option;
        thumb_dsb32_decode_fields(read_address, &option);
        disassem->inst = EMU_INST_DSB;
        break;
    }
    case THUMB_EOR32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_eor32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_EOR;
        break;
    }
    case THUMB_EORI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_eori32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_EOR;
        break;
    }
    case THUMB_ISB32: {
        uint32_t option;
        thumb_isb32_decode_fields(read_address, &option);
        disassem->inst = EMU_INST_ISB;
        break;
    }
    case THUMB_LDC32: {
        uint32_t coproc, d, crd, rn, pre_index, upwards, writeback, imm8;
        thumb_ldc32_decode_fields(read_address, &coproc, &d, &crd, &rn, &pre_index, &upwards, &writeback, &imm8);
        disassem->inst = EMU_INST_LDC;
        break;
    }
    case THUMB_LDC232: {
        uint32_t coproc, d, crd, rn, pre_index, upwards, writeback, imm8;
        thumb_ldc232_decode_fields(read_address, &coproc, &d, &crd, &rn, &pre_index, &upwards, &writeback, &imm8);
        disassem->inst = EMU_INST_LDC;
        break;
    }
    case THUMB_LDMEA32: {
        uint32_t writeback, rn, reglist;
        thumb_ldmea32_decode_fields(read_address, &writeback, &rn, &reglist);

        disassem->Rn.reg = rn;
        disassem->reglist = reglist;

        if ((reglist & 0x8000) != 0) {
            disassem->is_branch = true;
        }

        // LDMDB/LDMEA: is_up == false, is_pre_indexed == true,  start_address = R[n] - 4*BitCount(registers)
        disassem->is_up = false;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_LDM;
        break;
    }
    case THUMB_LDMFD32: {
        uint32_t writeback, rn, reglist;
        thumb_ldmfd32_decode_fields(read_address, &writeback, &rn, &reglist);

        disassem->Rn.reg = rn;
        disassem->reglist = reglist;

        if ((reglist & 0x8000) != 0) {
            disassem->is_branch = true;
        }

        // LDM/LDMIA/LDMFD: is_up == false, is_pre_indexed == true,  start_address = R[n]
        disassem->is_up = false;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_LDM;
        break;
    }
    case THUMB_LDR32: {
        uint32_t rn, rt, shift, rm;
        thumb_ldr32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_value = shift;
        disassem->Rm.shift_type = EMU_ARM_SFT_LSL;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDR;
        break;
    }
    case THUMB_LDRB32: {
        uint32_t rn, rt, shift, rm;
        thumb_ldrb32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_value = shift;
        disassem->Rm.shift_type = EMU_ARM_SFT_LSL;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRB;
        break;
    }
    case THUMB_LDRBI32: {
        uint32_t rn, rt, imm8, pre_index, upwards, writeback;
        thumb_ldrbi32_decode_fields(read_address, &rn, &rt, &imm8, &pre_index, &upwards, &writeback);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm8;

        disassem->is_up = upwards;
        disassem->is_pre_indexed = pre_index;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRB;
        break;
    }
    case THUMB_LDRBWI32: {
        uint32_t rn, rt, imm12;
        thumb_ldrbwi32_decode_fields(read_address, &rn, &rt, &imm12);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm12;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRB;
        break;
    }
    case THUMB_LDRBT32: {
        uint32_t rt, rn, imm8;
        thumb_ldrbt32_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_LDRBT;
        break;
    }
    case THUMB_LDRD32: {
        uint32_t pre_index, upwards, writeback, rn, rt, rt2, imm8;
        thumb_ldrd32_decode_fields(read_address, &pre_index, &upwards, &writeback, &rn, &rt, &rt2, &imm8);

        disassem->Rt.reg = rt;
        disassem->Rt2.reg = rt2;
        disassem->Rn.reg = rn;
        disassem->imm = (imm8 << 2);

        disassem->is_up = upwards;
        disassem->is_pre_indexed = pre_index;
        disassem->format = INS_RT_RT2_RN_IMM;

        disassem->inst = EMU_INST_LDRD;
        break;
    }
    case THUMB_LDREX32: {
        uint32_t rn, rt, imm8;
        thumb_ldrex32_decode_fields(read_address, &rn, &rt, &imm8);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_LDREX;
        break;
    }
    case THUMB_LDREXB32: {
        uint32_t rn, rt;
        thumb_ldrexb32_decode_fields(read_address, &rn, &rt);
        disassem->inst = EMU_INST_LDREXB;
        break;
    }
    case THUMB_LDREXD32: {
        uint32_t rn, rt, rt2;
        thumb_ldrexd32_decode_fields(read_address, &rn, &rt, &rt2);
        disassem->inst = EMU_INST_LDREXD;
        break;
    }
    case THUMB_LDREXH32: {
        uint32_t rn, rt;
        thumb_ldrexh32_decode_fields(read_address, &rn, &rt);
        disassem->inst = EMU_INST_LDREXH;
        break;
    }
    case THUMB_LDRH32: {
        uint32_t rn, rt, shift, rm;
        thumb_ldrh32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_type = EMU_ARM_SFT_LSL;
        disassem->Rm.shift_value = shift;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRH;
        break;
    }
    case THUMB_LDRHI32: {
        uint32_t rn, rt, imm8, pre_index, upwards, writeback;
        thumb_ldrhi32_decode_fields(read_address, &rn, &rt, &imm8, &pre_index, &upwards, &writeback);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm8;

        disassem->is_up = upwards;
        disassem->is_pre_indexed = pre_index;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRH;
        break;
    }
    case THUMB_LDRHWI32: {
        uint32_t rn, rt, imm12;
        thumb_ldrhwi32_decode_fields(read_address, &rn, &rt, &imm12);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm12;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRH;
        break;
    }
    case THUMB_LDRHT32: {
        uint32_t rt, rn, imm8;
        thumb_ldrht32_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_LDRHT;
        break;
    }
    case THUMB_LDRI32: {
        uint32_t rn, rt, imm8, pre_index, upwards, writeback;
        thumb_ldri32_decode_fields(read_address, &rn, &rt, &imm8, &pre_index, &upwards, &writeback);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm8;

        disassem->is_up = upwards;
        disassem->is_pre_indexed = pre_index;
        disassem->format = INS_RT_RN_IMM;

        if (disassem->Rt.reg == EMU_ARM_REG_PC) {
            disassem->is_branch = true;
        }

        disassem->inst = EMU_INST_LDR;
        break;
    }
    case THUMB_LDRL32: {
        uint32_t rt, imm12, upwards;
        thumb_ldrl32_decode_fields(read_address, &rt, &imm12, &upwards);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = EMU_ARM_REG_PC;
        disassem->imm = imm12;

        disassem->is_up = upwards;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDR;
        break;
    }
    case THUMB_LDRSB32: {
        uint32_t rn, rt, shift, rm;
        thumb_ldrsb32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_type = EMU_ARM_SFT_LSL;
        disassem->Rm.shift_value = shift;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRSB;
        break;
    }
    case THUMB_LDRSBI32: {
        uint32_t rn, rt, imm8, pre_index, upwards, writeback;
        thumb_ldrsbi32_decode_fields(read_address, &rn, &rt, &imm8, &pre_index, &upwards, &writeback);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm8;

        disassem->is_up = upwards;
        disassem->is_pre_indexed = pre_index;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRSB;
        break;
    }
    case THUMB_LDRSBWI32: {
        uint32_t rn, rt, imm12;
        thumb_ldrsbwi32_decode_fields(read_address, &rn, &rt, &imm12);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm12;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRSB;
        break;
    }
    case THUMB_LDRSBT32: {
        uint32_t rt, rn, imm8;
        thumb_ldrsbt32_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_LDRSBT;
        break;
    }
    case THUMB_LDRSH32: {
        uint32_t rn, rt, shift, rm;
        thumb_ldrsh32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_value = shift;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_LDRSH;
        break;
    }
    case THUMB_LDRSHI32: {
        uint32_t rn, rt, imm8, pre_index, upwards, writeback;
        thumb_ldrshi32_decode_fields(read_address, &rn, &rt, &imm8, &pre_index, &upwards, &writeback);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm8;

        disassem->is_up = upwards;
        disassem->is_pre_indexed = pre_index;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRSH;
        break;
    }
    case THUMB_LDRSHWI32: {
        uint32_t rn, rt, imm12;
        thumb_ldrshwi32_decode_fields(read_address, &rn, &rt, &imm12);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm12;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDRSH;
        break;
    }
    case THUMB_LDRSHT32: {
        uint32_t rt, rn, imm8;
        thumb_ldrsht32_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_LDRSHT;
        break;
    }
    case THUMB_LDRT32: {
        uint32_t rt, rn, imm8;
        thumb_ldrt32_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_LDRT;
        break;
    }
    case THUMB_LDRWI32: {
        uint32_t rn, rt, imm12;
        thumb_ldrwi32_decode_fields(read_address, &rn, &rt, &imm12);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm12;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_LDR;
        break;
    }
    case THUMB_LSL32: {
        uint32_t set_flags, rn, rd, rm;
        thumb_lsl32_decode_fields(read_address, &set_flags, &rn, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_LSLI32: {
        uint32_t set_flags, imm3, rd, imm2, rm;
        thumb_lsli32_decode_fields(read_address, &set_flags, &imm3, &rd, &imm2, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_LSR32: {
        uint32_t set_flags, rn, rd, rm;
        thumb_lsr32_decode_fields(read_address, &set_flags, &rn, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_LSRI32: {
        uint32_t set_flags, imm3, rd, imm2, rm;
        thumb_lsri32_decode_fields(read_address, &set_flags, &imm3, &rd, &imm2, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_MCR32: {
        uint32_t opc1, crn, rt, coproc, opc2, crm;
        thumb_mcr32_decode_fields(read_address, &opc1, &crn, &rt, &coproc, &opc2, &crm);
        disassem->inst = EMU_INST_MCR;
        break;
    }
    case THUMB_MCR232: {
        uint32_t opc1, crn, rt, coproc, opc2, crm;
        thumb_mcr232_decode_fields(read_address, &opc1, &crn, &rt, &coproc, &opc2, &crm);
        disassem->inst = EMU_INST_MCR;
        break;
    }
    case THUMB_MCRR32: {
        uint32_t coproc, opc1, rt, rt2, crm;
        thumb_mcrr32_decode_fields(read_address, &coproc, &opc1, &rt, &rt2, &crm);
        disassem->inst = EMU_INST_MCRR;
        break;
    }
    case THUMB_MCRR232: {
        uint32_t coproc, opc1, rt, rt2, crm;
        thumb_mcrr232_decode_fields(read_address, &coproc, &opc1, &rt, &rt2, &crm);
        disassem->inst = EMU_INST_MCRR;
        break;
    }
    case THUMB_MLA32: {
        uint32_t rn, racc, rd, rm;
        thumb_mla32_decode_fields(read_address, &rn, &racc, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = racc;
        disassem->format = INS_RD_RN_RM_RA;

        disassem->inst = EMU_INST_MLA;
        break;
    }
    case THUMB_MLS32: {
        uint32_t rn, racc, rd, rm;
        thumb_mls32_decode_fields(read_address, &rn, &racc, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = racc;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_RM_RA;

        disassem->inst = EMU_INST_MLS;
        break;
    }
    case THUMB_MOV32: {
        uint32_t set_flags, rd, rm;
        thumb_mov32_decode_fields(read_address, &set_flags, &rd, &rm);
        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_MOVI32: {
        uint32_t imm1, set_condition, imm3, rd, imm8;
        thumb_movi32_decode_fields(read_address, &imm1, &set_condition, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->format = INS_RD_CONST;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_MOVWI32: {
        uint32_t imm1, imm4, imm3, rd, imm8;
        thumb_movwi32_decode_fields(read_address, &imm1, &imm4, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->format = INS_RD_CONST;

        disassem->inst = EMU_INST_MOVW;
        break;
    }
    case THUMB_MOVTI32: {
        uint32_t imm1, imm4, imm3, rd, imm8;
        thumb_movti32_decode_fields(read_address, &imm1, &imm4, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->format = INS_RD_CONST;

        disassem->inst = EMU_INST_MOVT;
        break;
    }
    case THUMB_MRC32: {
        uint32_t opc1, crn, rt, coproc, opc2, crm;
        thumb_mrc32_decode_fields(read_address, &opc1, &crn, &rt, &coproc, &opc2, &crm);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_MRC;
        break;
    }
    case THUMB_MRC232: {
        uint32_t opc1, crn, rt, coproc, opc2, crm;
        thumb_mrc232_decode_fields(read_address, &opc1, &crn, &rt, &coproc, &opc2, &crm);
        disassem->inst = EMU_INST_MRC;
        break;
    }
    case THUMB_MRRC32: {
        uint32_t coproc, opc, rt, rt2, crm;
        thumb_mrrc32_decode_fields(read_address, &coproc, &opc, &rt, &rt2, &crm);
        disassem->inst = EMU_INST_MRRC;
        break;
    }
    case THUMB_MRRC232: {
        uint32_t coproc, opc, rt, rt2, crm;
        thumb_mrrc232_decode_fields(read_address, &coproc, &opc, &rt, &rt2, &crm);
        disassem->inst = EMU_INST_MRRC;
        break;
    }
    case THUMB_MRS32: {
        uint32_t rd;
        thumb_mrs32_decode_fields(read_address, &rd);
        disassem->inst = EMU_INST_MRS;
        break;
    }
    case THUMB_MSR32: {
        uint32_t rn, mask;
        thumb_msr32_decode_fields(read_address, &rn, &mask);
        disassem->inst = EMU_INST_MSR;
        break;
    }
    case THUMB_MUL32: {
        uint32_t rn, rd, rm;
        thumb_mul32_decode_fields(read_address, &rn, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_MUL;
        break;
    }
    case THUMB_MVN32: {
        uint32_t set_flags, imm3, rd, imm2, shift_type, rm;
        thumb_mvn32_decode_fields(read_address, &set_flags, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MVN;
        break;
    }
    case THUMB_MVNI32: {
        uint32_t imm1, set_condition, imm3, rd, imm8;
        thumb_mvni32_decode_fields(read_address, &imm1, &set_condition, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->format = INS_RD_CONST;

        disassem->inst = EMU_INST_MVN;
        break;
    }
    case THUMB_NOP32: {
        disassem->inst = EMU_INST_NOP;
        break;
    }
    case THUMB_ORN32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_orn32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);
        disassem->inst = EMU_INST_ORN;
        break;
    }
    case THUMB_ORNI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_orni32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_ORN;
        break;
    }
    case THUMB_ORR32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_orr32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_ORR;
        break;
    }
    case THUMB_ORRI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_orri32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_ORR;
        break;
    }
    case THUMB_PKH32: {
        uint32_t rn, imm3, rd, imm2, shift_type, rm;
        thumb_pkh32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &shift_type, &rm);
        disassem->inst = EMU_INST_PKH;
        break;
    }
    case THUMB_PLD32: {
        uint32_t rn, shift, rm;
        thumb_pld32_decode_fields(read_address, &rn, &shift, &rm);
        disassem->inst = EMU_INST_PLD;
        break;
    }
    case THUMB_PLDI32: {
        // uint32_t rn, imm12;
        // thumb_pldi32_decode_fields(read_address, &rn, &imm12);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_PLD;
        break;
    }
    case THUMB_PLDIM32: {
        uint32_t rn, imm8;
        thumb_pldim32_decode_fields(read_address, &rn, &imm8);
        disassem->inst = EMU_INST_PLD;
        break;
    }
    case THUMB_PLD_LIT32: {
        uint32_t upwards, imm12;
        thumb_pld_lit32_decode_fields(read_address, &upwards, &imm12);
        disassem->inst = EMU_INST_PLD;
        break;
    }
    case THUMB_PLDW32: {
        uint32_t rn, shift, rm;
        thumb_pldw32_decode_fields(read_address, &rn, &shift, &rm);
        disassem->inst = EMU_INST_PLD;
        break;
    }
    case THUMB_PLDWI32: {
        uint32_t rn, imm12;
        thumb_pldwi32_decode_fields(read_address, &rn, &imm12);
        disassem->inst = EMU_INST_PLD;
        break;
    }
    case THUMB_PLDWIM32: {
        uint32_t rn, imm8;
        thumb_pldwim32_decode_fields(read_address, &rn, &imm8);
        disassem->inst = EMU_INST_PLD;
        break;
    }
    case THUMB_PLI32: {
        uint32_t rn, rm, shift;
        thumb_pli32_decode_fields(read_address, &rn, &rm, &shift);
        disassem->inst = EMU_INST_PLI;
        break;
    }
    case THUMB_PLII32: {
        uint32_t rn, imm12;
        thumb_plii32_decode_fields(read_address, &rn, &imm12);
        disassem->inst = EMU_INST_PLI;
        break;
    }
    case THUMB_PLIIM32: {
        uint32_t rn, imm8;
        thumb_pliim32_decode_fields(read_address, &rn, &imm8);
        disassem->inst = EMU_INST_PLI;
        break;
    }
    case THUMB_PLI_LIT32: {
        uint32_t upwards, imm12;
        thumb_pli_lit32_decode_fields(read_address, &upwards, &imm12);
        disassem->inst = EMU_INST_PLI;
        break;
    }
    case THUMB_QADD32: {
        uint32_t rn, rd, rm;
        thumb_qadd32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_QADD;
        break;
    }
    case THUMB_QADD1632: {
        uint32_t rd, rn, rm;
        thumb_qadd1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_QADD16;
        break;
    }
    case THUMB_QADD832: {
        uint32_t rd, rn, rm;
        thumb_qadd832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_QADD8;
        break;
    }
    case THUMB_QASX32: {
        uint32_t rd, rn, rm;
        thumb_qasx32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_QASX;
        break;
    }
    case THUMB_QDADD32: {
        uint32_t rn, rd, rm;
        thumb_qdadd32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_QDADD;
        break;
    }
    case THUMB_QDSUB32: {
        uint32_t rn, rd, rm;
        thumb_qdsub32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_QDSUB;
        break;
    }
    case THUMB_QSAX32: {
        uint32_t rd, rn, rm;
        thumb_qsax32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_QSAX;
        break;
    }
    case THUMB_QSUB32: {
        uint32_t rn, rd, rm;
        thumb_qsub32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_QSUB;
        break;
    }
    case THUMB_QSUB1632: {
        uint32_t rd, rn, rm;
        thumb_qsub1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_QSUB16;
        break;
    }
    case THUMB_QSUB832: {
        uint32_t rd, rn, rm;
        thumb_qsub832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_QSUB8;
        break;
    }
    case THUMB_RBIT32: {
        uint32_t rn, rd, rm;
        thumb_rbit32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_RBIT;
        break;
    }
    case THUMB_REV32: {
        // This instruction encodes the same value of Rm into two
        // different positions. In the following, PIE decodes one of
        // the values into "rn" and the other one into "rm", and "rm
        // == rm" must always be true. If it is false, then it's a bad
        // instruction.
        uint32_t rn, rd, rm;
        thumb_rev32_decode_fields(read_address, &rn, &rd, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_REV;
        break;
    }
    case THUMB_REV1632: {
        uint32_t rn, rd, rm;
        thumb_rev1632_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_REV16;
        break;
    }
    case THUMB_REVSH32: {
        uint32_t rn, rd, rm;
        thumb_revsh32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_REVSH;
        break;
    }
    case THUMB_ROR32: {
        uint32_t set_flags, rn, rd, rm;
        thumb_ror32_decode_fields(read_address, &set_flags, &rn, &rd, &rm);
        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_RORI32: {
        uint32_t set_flags, imm3, rd, imm2, rm;
        thumb_rori32_decode_fields(read_address, &set_flags, &imm3, &rd, &imm2, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_MOV;
        break;
    }
    case THUMB_RRX32: {
        uint32_t set_flags, rd, rm;
        thumb_rrx32_decode_fields(read_address, &set_flags, &rd, &rm);
        disassem->inst = EMU_INST_RRX;
        break;
    }
    case THUMB_RSB32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_rsb32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_RSB;
        break;
    }
    case THUMB_RSBI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_rsbi32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_RSB;
        break;
    }
    case THUMB_SADD1632: {
        uint32_t rd, rn, rm;
        thumb_sadd1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SADD16;
        break;
    }
    case THUMB_SADD832: {
        uint32_t rd, rn, rm;
        thumb_sadd832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SADD8;
        break;
    }
    case THUMB_SASX32: {
        uint32_t rd, rn, rm;
        thumb_sasx32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SASX;
        break;
    }
    case THUMB_SBC32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_sbc32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SBC;
        break;
    }
    case THUMB_SBCI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_sbci32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_SBC;
        break;
    }
    case THUMB_SBFX32: {
        uint32_t rn, imm3, rd, imm2, imm5;
        thumb_sbfx32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &imm5);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_LSB_WIDTH;

        disassem->inst = EMU_INST_SBFX;
        break;
    }
    case THUMB_SDIV32: {
        uint32_t rn, rdhi, rm;
        thumb_sdiv32_decode_fields(read_address, &rn, &rdhi, &rm);
        disassem->inst = EMU_INST_SDIV;
        break;
    }
    case THUMB_SEL32: {
        uint32_t rn, rd, rm;
        thumb_sel32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_SEL;
        break;
    }
    case THUMB_SEV32: {
        disassem->inst = EMU_INST_SEV;
        break;
    }
    case THUMB_SHADD1632: {
        uint32_t rd, rn, rm;
        thumb_shadd1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SHADD16;
        break;
    }
    case THUMB_SHADD832: {
        uint32_t rd, rn, rm;
        thumb_shadd832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SHADD8;
        break;
    }
    case THUMB_SHASX32: {
        uint32_t rd, rn, rm;
        thumb_shasx32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SHASX;
        break;
    }
    case THUMB_SHSAX32: {
        uint32_t rd, rn, rm;
        thumb_shsax32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SHSAX;
        break;
    }
    case THUMB_SHSUB1632: {
        uint32_t rd, rn, rm;
        thumb_shsub1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SHSUB16;
        break;
    }
    case THUMB_SHSUB832: {
        uint32_t rd, rn, rm;
        thumb_shsub832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SHSUB8;
        break;
    }
    case THUMB_SIMD_ADD_SUB32: {
        uint32_t opcode, rn, rd, pref, rm;
        thumb_simd_add_sub32_decode_fields(read_address, &opcode, &rn, &rd, &pref, &rm);
        break;
    }
    case THUMB_SMLABB32: {
        uint32_t rd, rn, rm, ra;
        thumb_smlabb32_decode_fields(read_address, &rd, &rn, &rm, &ra);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Ra.reg = ra;
        disassem->format = INS_RD_RN_RM_RA;

        disassem->inst = EMU_INST_SMLABB;
        break;
    }
    case THUMB_SMLABT32: {
        uint32_t rd, rn, rm, ra;
        thumb_smlabt32_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->inst = EMU_INST_SMLABT;
        break;
    }
    case THUMB_SMLATB32: {
        uint32_t rd, rn, rm, ra;
        thumb_smlatb32_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->inst = EMU_INST_SMLATB;
        break;
    }
    case THUMB_SMLATT32: {
        uint32_t rd, rn, rm, ra;
        thumb_smlatt32_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->inst = EMU_INST_SMLATT;
        break;
    }
    case THUMB_SMLAD32: {
        uint32_t rn, racc, rd, opcode2, rm;
        thumb_smlad32_decode_fields(read_address, &rn, &racc, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_SMLAD;
        break;
    }
    case THUMB_SMLAL32: {
        uint32_t rn, rdlo, rdhi, rm;
        thumb_smlal32_decode_fields(read_address, &rn, &rdlo, &rdhi, &rm);

        disassem->Rdhi.reg = rdhi;
        disassem->Rdlo.reg = rdlo;
        disassem->Rm.reg = rm;
        disassem->Rn.reg = rn;
        disassem->format = INS_RDLO_RDHI_RN_RM;

        disassem->inst = EMU_INST_SMLAL;
        break;
    }
    case THUMB_SMLALBB32: {
        uint32_t rdlo, rdhi, rn, rm;
        thumb_smlalbb32_decode_fields(read_address, &rdlo, &rdhi, &rn, &rm);
        disassem->inst = EMU_INST_SMLALBB;
        break;
    }
    case THUMB_SMLALBT32: {
        uint32_t rdlo, rdhi, rn, rm;
        thumb_smlalbt32_decode_fields(read_address, &rdlo, &rdhi, &rn, &rm);
        disassem->inst = EMU_INST_SMLALBT;
        break;
    }
    case THUMB_SMLALTB32: {
        uint32_t rdlo, rdhi, rn, rm;
        thumb_smlaltb32_decode_fields(read_address, &rdlo, &rdhi, &rn, &rm);
        disassem->inst = EMU_INST_SMLALTB;
        break;
    }
    case THUMB_SMLALTT32: {
        uint32_t rdlo, rdhi, rn, rm;
        thumb_smlaltt32_decode_fields(read_address, &rdlo, &rdhi, &rn, &rm);
        disassem->inst = EMU_INST_SMLALTT;
        break;
    }
    case THUMB_SMLALD32: {
        uint32_t rn, rdlo, rdhi, m_swap, rm;
        thumb_smlald32_decode_fields(read_address, &rn, &rdlo, &rdhi, &m_swap, &rm);
        disassem->inst = EMU_INST_SMLALD;
        break;
    }
    case THUMB_SMLAWB32: {
        uint32_t rd, rn, rm, ra;
        thumb_smlawb32_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->inst = EMU_INST_SMLAWB;
        break;
    }
    case THUMB_SMLAWT32: {
        uint32_t rd, rn, rm, ra;
        thumb_smlawt32_decode_fields(read_address, &rd, &rn, &rm, &ra);
        disassem->inst = EMU_INST_SMLAWT;
        break;
    }
    case THUMB_SMLSD32: {
        uint32_t rn, racc, rd, opcode2, rm;
        thumb_smlsd32_decode_fields(read_address, &rn, &racc, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_SMLSD;
        break;
    }
    case THUMB_SMLSLD32: {
        uint32_t rn, rdlo, rdhi, m_swap, rm;
        thumb_smlsld32_decode_fields(read_address, &rn, &rdlo, &rdhi, &m_swap, &rm);
        disassem->inst = EMU_INST_SMLSLD;
        break;
    }
    case THUMB_SMMLA32: {
        uint32_t rn, racc, rd, opcode2, rm;
        thumb_smmla32_decode_fields(read_address, &rn, &racc, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_SMMLA;
        break;
    }
    case THUMB_SMMLS32: {
        uint32_t rn, racc, rd, opcode2, rm;
        thumb_smmls32_decode_fields(read_address, &rn, &racc, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_SMMLS;
        break;
    }
    case THUMB_SMMUL32: {
        uint32_t rn, rd, opcode2, rm;
        thumb_smmul32_decode_fields(read_address, &rn, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_SMMUL;
        break;
    }
    case THUMB_SMUAD32: {
        uint32_t rn, rd, opcode2, rm;
        thumb_smuad32_decode_fields(read_address, &rn, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_SMUAD;
        break;
    }
    case THUMB_SMULBB32: {
        uint32_t rd, rn, rm;
        thumb_smulbb32_decode_fields(read_address, &rd, &rn, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SMULBB;
        break;
    }
    case THUMB_SMULBT32: {
        uint32_t rd, rn, rm;
        thumb_smulbt32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SMULBT;
        break;
    }
    case THUMB_SMULTB32: {
        uint32_t rd, rn, rm;
        thumb_smultb32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SMULTB;
        break;
    }
    case THUMB_SMULTT32: {
        uint32_t rd, rn, rm;
        thumb_smultt32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SMULTT;
        break;
    }
    case THUMB_SMULL32: {
        uint32_t rn, rdlo, rdhi, rm;
        thumb_smull32_decode_fields(read_address, &rn, &rdlo, &rdhi, &rm);

        disassem->Rdhi.reg = rdhi;
        disassem->Rdlo.reg = rdlo;
        disassem->Rm.reg = rm;
        disassem->Rn.reg = rn;
        disassem->format = INS_RDLO_RDHI_RN_RM;

        disassem->inst = EMU_INST_SMULL;
        break;
    }
    case THUMB_SMULWB32: {
        uint32_t rn, rd, rm;
        thumb_smulwb32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_SMULWB;
        break;
    }
    case THUMB_SMULWT32: {
        uint32_t rn, rd, rm;
        thumb_smulwt32_decode_fields(read_address, &rn, &rd, &rm);
        disassem->inst = EMU_INST_SMULWT;
        break;
    }
    case THUMB_SMUSD32: {
        uint32_t rn, rd, opcode2, rm;
        thumb_smusd32_decode_fields(read_address, &rn, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_SMUSD;
        break;
    }
    case THUMB_SSAT1632: {
        uint32_t rn, rd;
        thumb_ssat1632_decode_fields(read_address, &rn, &rd);
        disassem->inst = EMU_INST_SSAT16;
        break;
    }
    case THUMB_SSAT_ASR32: {
        uint32_t rn, imm3, rd, imm2, imm5;
        thumb_ssat_asr32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &imm5);
        disassem->inst = EMU_INST_SSAT;
        break;
    }
    case THUMB_SSAT_LSL32: {
        uint32_t rn, imm3, rd, imm2, imm5;
        thumb_ssat_lsl32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &imm5);
        disassem->inst = EMU_INST_SSAT;
        break;
    }
    case THUMB_SSAX32: {
        uint32_t rd, rn, rm;
        thumb_ssax32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SSAX;
        break;
    }
    case THUMB_SSUB1632: {
        uint32_t rd, rn, rm;
        thumb_ssub1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SSUB16;
        break;
    }
    case THUMB_SSUB832: {
        uint32_t rd, rn, rm;
        thumb_ssub832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_SSUB8;
        break;
    }
    case THUMB_STC32: {
        uint32_t coproc, d, crd, rn, pre_index, upwards, writeback, imm8;
        thumb_stc32_decode_fields(read_address, &coproc, &d, &crd, &rn, &pre_index, &upwards, &writeback, &imm8);
        disassem->inst = EMU_INST_STC;
        break;
    }
    case THUMB_STC232: {
        uint32_t coproc, d, crd, rn, pre_index, upwards, writeback, imm8;
        thumb_stc232_decode_fields(read_address, &coproc, &d, &crd, &rn, &pre_index, &upwards, &writeback, &imm8);
        disassem->inst = EMU_INST_STC;
        break;
    }
    case THUMB_STMEA32: {
        uint32_t writeback, rn, reglist;
        thumb_stmea32_decode_fields(read_address, &writeback, &rn, &reglist);

        disassem->Rn.reg = rn;
        disassem->reglist = reglist;

        // STM/STMIA/STMEA: is_up == true , is_pre_indexed == false, start_address = R[n]
        disassem->is_up = true;
        disassem->is_pre_indexed = false;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_STM;
        break;
    }
    case THUMB_STMFD32: {
        uint32_t writeback, rn, reglist;
        thumb_stmfd32_decode_fields(read_address, &writeback, &rn, &reglist);

        disassem->Rn.reg = rn;
        disassem->reglist = reglist;

        // STMDB/STMFD: is_up == false, is_pre_indexed == true,  start_address = R[n] - 4*BitCount(registers)
        disassem->is_up = false;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RN_REGLIST;

        disassem->inst = EMU_INST_STM;
        break;
    }
    case THUMB_STR32: {
        uint32_t rn, rt, shift, rm;
        thumb_str32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_type = EMU_ARM_SFT_LSL;
        disassem->Rm.shift_value = shift;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_STR;
        break;
    }
    case THUMB_STRB32: {
        uint32_t rn, rt, shift, rm;
        thumb_strb32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_value = shift;
        disassem->Rm.shift_type = EMU_ARM_SFT_LSL;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_STRB;
        break;
    }
    case THUMB_STRBI32: {
        uint32_t sign_ext, upwards, rn, rt, imm12;
        thumb_strbi32_decode_fields(read_address, &sign_ext, &upwards, &rn, &rt, &imm12);

        if (sign_ext != 0) {
            emu_abort("THUMB_STRBI32.sign_ext is %d (expected 0)! See page A8-388, ARM DDI 0406B, STRB (immediate, Thumb).\n", sign_ext);
        }

        uint32_t imm;
        bool is_up, is_pre_indexed;
        // XXX: This is a workaround a bug in the PIE decoder. It does
        // not distinguish properly between encoding T2 and T3 of STRB
        // properly, so we'll have to do it ourselves. A proper fix
        // would be to fix PIE.
        if (upwards == 1) {
            // This is encoding T2
            imm = imm12;
            is_up = true;
            is_pre_indexed = true;
        } else if (upwards == 0) {
            // This is encoding T3
            imm = (imm12 & 0x0FF);
            is_up = (imm12 & 0x200) >> 9;
            is_pre_indexed = (imm12 & 0x400) >> 10;
        } else {
            emu_abort("THUMB_STRBI32.upwards is %d (expected 0 or 1)! See page A8-388, ARM DDI 0406B, STRB (immediate, Thumb).\n", upwards);
        }

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm;

        disassem->is_up = is_up;
        disassem->is_pre_indexed = is_pre_indexed;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_STRB;
        break;
    }
    case THUMB_STRBT32: {
        uint32_t rt, rn, imm8;
        thumb_strbt32_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_STRB;
        break;
    }
    case THUMB_STRD32: {
        uint32_t pre_index, upwards, writeback, rn, rt, rt2, imm8;
        thumb_strd32_decode_fields(read_address, &pre_index, &upwards, &writeback, &rn, &rt, &rt2, &imm8);

        disassem->Rt.reg = rt;
        disassem->Rt2.reg = rt2;
        disassem->Rn.reg = rn;
        disassem->imm = (imm8 << 2);

        disassem->is_pre_indexed = pre_index;
        disassem->is_up = upwards;
        disassem->format = INS_RT_RT2_RN_IMM;

        disassem->inst = EMU_INST_STRD;
        break;
    }
    case THUMB_STREX32: {
        uint32_t rn, rt, rd, imm8;
        thumb_strex32_decode_fields(read_address, &rn, &rt, &rd, &imm8);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_STREX;
        break;
    }
    case THUMB_STREXB32: {
        uint32_t rn, rt, rd;
        thumb_strexb32_decode_fields(read_address, &rn, &rt, &rd);
        disassem->inst = EMU_INST_STREXB;
        break;
    }
    case THUMB_STREXD32: {
        uint32_t rn, rt, rt2, rd;
        thumb_strexd32_decode_fields(read_address, &rn, &rt, &rt2, &rd);
        disassem->inst = EMU_INST_STREXD;
        break;
    }
    case THUMB_STREXH32: {
        uint32_t rn, rt, rd;
        thumb_strexh32_decode_fields(read_address, &rn, &rt, &rd);
        disassem->inst = EMU_INST_STREXH;
        break;
    }
    case THUMB_STRH32: {
        uint32_t rn, rt, shift, rm;
        thumb_strh32_decode_fields(read_address, &rn, &rt, &shift, &rm);

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->Rm.shift_type = EMU_ARM_SFT_LSL;
        disassem->Rm.shift_value = shift;

        disassem->is_up = true;
        disassem->is_pre_indexed = true;
        disassem->format = INS_RT_RN_RM_SHIFT;

        disassem->inst = EMU_INST_STRH;
        break;
    }
    case THUMB_STRHI32: {
        uint32_t sign_ext, upwards, rn, rt, imm12;
        thumb_strhi32_decode_fields(read_address, &sign_ext, &upwards, &rn, &rt, &imm12);

        if (sign_ext != 0) {
            // See page A8-698, ARM DDI 0406C.b, STRH (Immediate,
            // Thumb)
            emu_abort("THUMB_STRHI32.sign_ext is %d (expected 0)!\n", sign_ext);
        }

        uint32_t imm;
        bool is_up, is_pre_indexed;

        // XXX This is a workaround of a bug in the PIE decoder. It
        // does not distinguish properly between encoding T2 and T3 of
        // STRH properly, so we'll have to do it ourselves. A proper
        // fix would be to fix PIE.
        if (upwards == 1) {
            // This is encoding T2
            imm = imm12;
            is_up = true;
            is_pre_indexed = true;
        } else if (upwards == 0) {
            // This is encoding T3
            imm = (imm12 & 0x0FF);
            is_up = (imm12 & 0x200) >> 9;
            is_pre_indexed = (imm12 & 0x400) >> 10;
        } else {
            // See page A8-698, ARM DDI 0406C.b, STRH (Immediate,
            // Thumb)
            emu_abort("THUMB_STRHI32.upwards is %d (expected 0 or 1)!\n", upwards);
        }

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm;

        disassem->is_up = is_up;
        disassem->is_pre_indexed = is_pre_indexed;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_STRH;
        break;
    }
    case THUMB_STRHT: {
        uint32_t rt, rn, imm8;
        thumb_strht_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_STRHT;
        break;
    }
    case THUMB_STRI32: {
        uint32_t sign_ext, upwards, rn, rt, imm12;
        thumb_stri32_decode_fields(read_address, &sign_ext, &upwards, &rn, &rt, &imm12);

        if (sign_ext != 0) {
            emu_abort("THUMB_STRI32.sign_ext is %d (expected 0)! See page A8-672, ARM DDI 0406C.b, STR (Immediate, Thumb).\n", sign_ext);
        }

        uint32_t imm;
        bool is_up, is_pre_indexed;

        // XX This is a workaround of a bug in the PIE decoder. It
        // does not distinguish properly between encoding T3 and T4 of
        // STR properly, so we'll have to do it ourselves. A proper
        // fix would be to fix PIE.
        if (upwards == 1) {
            // This is encoding T3
            imm = imm12;
            is_up = true;
            is_pre_indexed = true;
        } else if (upwards == 0) {
            // This is encoding T4
            imm = (imm12 & 0x0FF);
            is_up = (imm12 & 0x200) >> 9;
            is_pre_indexed = (imm12 & 0x400) >> 10;
        } else {
            emu_abort("THUMB_STRI32.upwards is %d (expected 0 or 1)! See page A8-672, ARM DDI 0406C.b, STR (Immediate, Thumb).\n", upwards);
        }

        disassem->Rt.reg = rt;
        disassem->Rn.reg = rn;
        disassem->imm = imm;

        disassem->is_up = is_up;
        disassem->is_pre_indexed = is_pre_indexed;
        disassem->format = INS_RT_RN_IMM;

        disassem->inst = EMU_INST_STR;
        break;
    }
    case THUMB_STRT32: {
        uint32_t rt, rn, imm8;
        thumb_strt32_decode_fields(read_address, &rt, &rn, &imm8);
        disassem->inst = EMU_INST_STRT;
        break;
    }
    case THUMB_SUB32: {
        uint32_t set_flags, rn, imm3, rd, imm2, shift_type, rm;
        thumb_sub32_decode_fields(read_address, &set_flags, &rn, &imm3, &rd, &imm2, &shift_type, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SUB;
        break;
    }
    case THUMB_SUBI32: {
        uint32_t imm1, set_condition, rn, imm3, rd, imm8;
        thumb_subi32_decode_fields(read_address, &imm1, &set_condition, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_SUB;
        break;
    }
    case THUMB_SUBWI32: {
        uint32_t imm1, rn, imm3, rd, imm8;
        thumb_subwi32_decode_fields(read_address, &imm1, &rn, &imm3, &rd, &imm8);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_CONST;

        disassem->inst = EMU_INST_SUB;
        break;
    }
    case THUMB_SXTAB1632: {
        uint32_t rn, rd, rotate, rm;
        thumb_sxtab1632_decode_fields(read_address, &rn, &rd, &rotate, &rm);
        disassem->inst = EMU_INST_SXTAB16;
        break;
    }
    case THUMB_SXTAB32: {
        uint32_t rn, rd, rotate, rm;
        thumb_sxtab32_decode_fields(read_address, &rn, &rd, &rotate, &rm);
        disassem->inst = EMU_INST_SXTAB;
        break;
    }
    case THUMB_SXTAH32: {
        uint32_t rn, rd, rotate, rm;
        thumb_sxtah32_decode_fields(read_address, &rn, &rd, &rotate, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_SXTAH;
        break;
    }
    case THUMB_SXTB1632: {
        uint32_t rd, rotate, rm;
        thumb_sxtb1632_decode_fields(read_address, &rd, &rotate, &rm);
        disassem->inst = EMU_INST_SXTB16;
        break;
    }
    case THUMB_SXTB32: {
        uint32_t rd, rotate, rm;
        thumb_sxtb32_decode_fields(read_address, &rd, &rotate, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_SXTB;
        break;
    }
    case THUMB_SXTH32: {
        uint32_t rd, rotate, rm;
        thumb_sxth32_decode_fields(read_address, &rd, &rotate, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_SXTH;
        break;
    }
    case THUMB_TBB32: {
        uint32_t rn, rm;
        thumb_tbb32_decode_fields(read_address, &rn, &rm);

        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_TBB;
        break;
    }
    case THUMB_TBH32: {
        uint32_t rn, rm;
        thumb_tbh32_decode_fields(read_address, &rn, &rm);

        disassem->is_branch = true;
        disassem->format = INS_UNTRANSLATED;

        disassem->inst = EMU_INST_TBH;
        break;
    }
    case THUMB_TEQ32: {
        uint32_t rn, imm3, imm2, shift_type, rm;
        thumb_teq32_decode_fields(read_address, &rn, &imm3, &imm2, &shift_type, &rm);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_TEQ;
        break;
    }
    case THUMB_TEQI32: {
        // uint32_t imm1, rn, imm3, imm8;
        // thumb_teqi32_decode_fields(read_address, &imm1, &rn, &imm3, &imm8);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_TEQ;
        break;
    }
    case THUMB_TST32: {
        uint32_t rn, imm3, imm2, shift_type, rm;
        thumb_tst32_decode_fields(read_address, &rn, &imm3, &imm2, &shift_type, &rm);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_TST;
        break;
    }
    case THUMB_TSTI32: {
        uint32_t imm1, rn, imm3, imm8;
        thumb_tsti32_decode_fields(read_address, &imm1, &rn, &imm3, &imm8);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_TST;
        break;
    }
    case THUMB_UADD1632: {
        uint32_t rd, rn, rm;
        thumb_uadd1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UADD16;
        break;
    }
    case THUMB_UADD832: {
        uint32_t rd, rn, rm;
        thumb_uadd832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UADD8;
        break;
    }
    case THUMB_UASX32: {
        uint32_t rd, rn, rm;
        thumb_uasx32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UASX;
        break;
    }
    case THUMB_UBFX32: {
        uint32_t rn, imm3, rd, imm2, imm5;
        thumb_ubfx32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &imm5);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_RN_LSB_WIDTH;

        disassem->inst = EMU_INST_UBFX;
        break;
    }
    case THUMB_UDF32: {
        uint32_t imm4, imm12;
        thumb_udf32_decode_fields(read_address, &imm4, &imm12);
        disassem->inst = EMU_INST_UDF;
        break;
    }
    case THUMB_UDIV32: {
        uint32_t rn, rdhi, rm;
        thumb_udiv32_decode_fields(read_address, &rn, &rdhi, &rm);
        disassem->inst = EMU_INST_UDIV;
        break;
    }
    case THUMB_UHADD1632: {
        uint32_t rd, rn, rm;
        thumb_uhadd1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UHADD16;
        break;
    }
    case THUMB_UHADD832: {
        uint32_t rd, rn, rm;
        thumb_uhadd832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UHADD8;
        break;
    }
    case THUMB_UHASX32: {
        uint32_t rd, rn, rm;
        thumb_uhasx32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UHASX;
        break;
    }
    case THUMB_UHSAX32: {
        uint32_t rd, rn, rm;
        thumb_uhsax32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UHSAX;
        break;
    }
    case THUMB_UHSUB1632: {
        uint32_t rd, rn, rm;
        thumb_uhsub1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UHSUB16;
        break;
    }
    case THUMB_UHSUB832: {
        uint32_t rd, rn, rm;
        thumb_uhsub832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UHSUB8;
        break;
    }
    case THUMB_UMAAL32: {
        uint32_t rn, rdlo, rdhi, rm;
        thumb_umaal32_decode_fields(read_address, &rn, &rdlo, &rdhi, &rm);
        disassem->inst = EMU_INST_UMAAL;
        break;
    }
    case THUMB_UMLAL32: {
        uint32_t rn, rdlo, rdhi, rm;
        thumb_umlal32_decode_fields(read_address, &rn, &rdlo, &rdhi, &rm);
        disassem->inst = EMU_INST_UMLAL;
        break;
    }
    case THUMB_UMULL32: {
        uint32_t rn, rdlo, rdhi, rm;
        thumb_umull32_decode_fields(read_address, &rn, &rdlo, &rdhi, &rm);

        disassem->Rdhi.reg = rdhi;
        disassem->Rdlo.reg = rdlo;
        disassem->Rm.reg = rm;
        disassem->Rn.reg = rn;
        disassem->format = INS_RDLO_RDHI_RN_RM;

        disassem->inst = EMU_INST_UMULL;
        break;
    }
    case THUMB_UQADD1632: {
        uint32_t rd, rn, rm;
        thumb_uqadd1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UQADD16;
        break;
    }
    case THUMB_UQADD832: {
        uint32_t rd, rn, rm;
        thumb_uqadd832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UQADD8;
        break;
    }
    case THUMB_UQASX32: {
        uint32_t rd, rn, rm;
        thumb_uqasx32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UQASX;
        break;
    }
    case THUMB_UQSAX32: {
        uint32_t rd, rn, rm;
        thumb_uqsax32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UQSAX;
        break;
    }
    case THUMB_UQSUB1632: {
        uint32_t rd, rn, rm;
        thumb_uqsub1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UQSUB16;
        break;
    }
    case THUMB_UQSUB832: {
        uint32_t rd, rn, rm;
        thumb_uqsub832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_UQSUB8;
        break;
    }
    case THUMB_USAD832: {
        uint32_t rn, rd, opcode2, rm;
        thumb_usad832_decode_fields(read_address, &rn, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_USAD8;
        break;
    }
    case THUMB_USADA832: {
        uint32_t rn, racc, rd, opcode2, rm;
        thumb_usada832_decode_fields(read_address, &rn, &racc, &rd, &opcode2, &rm);
        disassem->inst = EMU_INST_USADA8;
        break;
    }
    case THUMB_USAT1632: {
        uint32_t rn, rd;
        thumb_usat1632_decode_fields(read_address, &rn, &rd);
        disassem->inst = EMU_INST_USAT16;
        break;
    }
    case THUMB_USAT_ASR32: {
        uint32_t rn, imm3, rd, imm2, imm5;
        thumb_usat_asr32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &imm5);
        disassem->inst = EMU_INST_USAT;
        break;
    }
    case THUMB_USAT_LSL32: {
        uint32_t rn, imm3, rd, imm2, imm5;
        thumb_usat_lsl32_decode_fields(read_address, &rn, &imm3, &rd, &imm2, &imm5);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->format = INS_RD_IMM_RN;

        disassem->inst = EMU_INST_USAT;
        break;
    }
    case THUMB_USAX32: {
        uint32_t rd, rn, rm;
        thumb_usax32_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_USAX;
        break;
    }
    case THUMB_USUB1632: {
        uint32_t rd, rn, rm;
        thumb_usub1632_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_USUB16;
        break;
    }
    case THUMB_USUB832: {
        uint32_t rd, rn, rm;
        thumb_usub832_decode_fields(read_address, &rd, &rn, &rm);
        disassem->inst = EMU_INST_USUB8;
        break;
    }
    case THUMB_UXTAB1632: {
        uint32_t rn, rd, rotate, rm;
        thumb_uxtab1632_decode_fields(read_address, &rn, &rd, &rotate, &rm);
        disassem->inst = EMU_INST_UXTAB16;
        break;
    }
    case THUMB_UXTAB32: {
        uint32_t rn, rd, rotate, rm;
        thumb_uxtab32_decode_fields(read_address, &rn, &rd, &rotate, &rm);

        disassem->Rd.reg = rd;
        disassem->Rn.reg = rn;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RN_RM;

        disassem->inst = EMU_INST_UXTAB;
        break;
    }
    case THUMB_UXTAH32: {
        uint32_t rn, rd, rotate, rm;
        thumb_uxtah32_decode_fields(read_address, &rn, &rd, &rotate, &rm);
        disassem->inst = EMU_INST_UXTAH;
        break;
    }
    case THUMB_UXTB1632: {
        uint32_t rd, rotate, rm;
        thumb_uxtb1632_decode_fields(read_address, &rd, &rotate, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;
        disassem->inst = EMU_INST_UXTB16;
        break;
    }
    case THUMB_UXTB32: {
        uint32_t rd, rotate, rm;
        thumb_uxtb32_decode_fields(read_address, &rd, &rotate, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;
        disassem->inst = EMU_INST_UXTB;
        break;
    }
    case THUMB_UXTH32: {
        uint32_t rd, rotate, rm;
        thumb_uxth32_decode_fields(read_address, &rd, &rotate, &rm);

        disassem->Rd.reg = rd;
        disassem->Rm.reg = rm;
        disassem->format = INS_RD_RM;

        disassem->inst = EMU_INST_UXTH;
        break;
    }
    case THUMB_WFE32: {
        disassem->inst = EMU_INST_WFE;
        break;
    }
    case THUMB_WFI32:{
        disassem->inst = EMU_INST_WFI;
        break;
    }
    case THUMB_YIELD32:{
        disassem->inst = EMU_INST_YIELD;
        break;
    }
    case THUMB_NEON_VABA: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vaba_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VABA;
        break;
    }
    case THUMB_NEON_VABAL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vabal_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VABAL;
        break;
    }
    case THUMB_NEON_VABD_I: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vabd_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VABD_I;
        break;
    }
    case THUMB_NEON_VABD_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vabd_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VABD_F;
        break;
    }
    case THUMB_NEON_VABDL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vabdl_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VABDL;
        break;
    }
    case THUMB_NEON_VABS: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vabs_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VABS;
        break;
    }
    case THUMB_VFP_VABS: {
        uint32_t size, d, vd, m, vm;
        thumb_vfp_vabs_decode_fields(read_address, &size, &d, &vd, &m, &vm);

        if (size == 1) {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vm.reg = (m << 4) + vm;
        } else {
            disassem->Vd.reg = vd;
            disassem->Vm.reg = vm;
            disassem->Vd.s_reg = true;
            disassem->Vm.s_reg = true;
        }
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_VFP_VABS;
        break;
    }
    case THUMB_NEON_VACGE: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vacge_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VACGE;
        break;
    }
    case THUMB_NEON_VACGT: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vacgt_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VACGT;
        break;
    }
    case THUMB_NEON_VADD_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vadd_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VADD_I;
        break;
    }
    case THUMB_NEON_VADD_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vadd_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VADD_F;
        break;
    }
    case THUMB_VFP_VADD: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vadd_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);


        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VADD;
        break;
    }
    case THUMB_NEON_VADDHN: {
        uint32_t size, d, n, vd, vn, m, vm;
        thumb_neon_vaddhn_decode_fields(read_address, &size, &d, &n, &vd, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VADDHN;
        break;
    }
    case THUMB_NEON_VADDL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vaddl_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VADDL;
        break;
    }
    case THUMB_NEON_VADDW: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vaddw_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VADDW;
        break;
    }
    case THUMB_NEON_VAND: {
        uint32_t q, d, vd, n, vn, m, vm;
        thumb_neon_vand_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VAND;
        break;
    }
    case THUMB_NEON_VBIC: {
        uint32_t q, d, vd, n, vn, m, vm;
        thumb_neon_vbic_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VBIC;
        break;
    }
    case THUMB_NEON_VBICI: {
        uint32_t q, d, vd, cmode, i, imm3, imm4;
        thumb_neon_vbici_decode_fields(read_address, &q, &d, &vd, &cmode, &i, &imm3, &imm4);
        disassem->inst = EMU_INST_NEON_VBICI;
        break;
    }
    case THUMB_NEON_VBIF: {
        uint32_t q, d, vd, n, vn, m, vm;
        thumb_neon_vbif_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VBIF;
        break;
    }
    case THUMB_NEON_VBIT: {
        uint32_t q, d, vd, n, vn, m, vm;
        thumb_neon_vbit_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VBIT;
        break;
    }
    case THUMB_NEON_VBSL: {
        uint32_t q, d, vd, n, vn, m, vm;
        thumb_neon_vbsl_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VBSL;
        break;
    }
    case THUMB_NEON_VCEQ_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vceq_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCEQ_I;
        break;
    }
    case THUMB_NEON_VCEQ_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vceq_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCEQ_F;
        break;
    }
    case THUMB_NEON_VCEQZ: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vceqz_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCEQZ;
        break;
    }
    case THUMB_NEON_VCGE_I: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vcge_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCGE_I;
        break;
    }
    case THUMB_NEON_VCGE_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vcge_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCGE_F;
        break;
    }
    case THUMB_NEON_VCGEZ: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vcgez_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCGEZ;
        break;
    }
    case THUMB_NEON_VCGT_I: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vcgt_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCGT_I;
        break;
    }
    case THUMB_NEON_VCGT_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vcgt_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCGT_F;
        break;
    }
    case THUMB_NEON_VCGTZ: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vcgtz_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCGTZ;
        break;
    }
    case THUMB_NEON_VCLEZ: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vclez_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCLEZ;
        break;
    }
    case THUMB_NEON_VCLS: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vcls_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCLS;
        break;
    }
    case THUMB_NEON_VCLTZ: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vcltz_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCLTZ;
        break;
    }
    case THUMB_NEON_VCLZ: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vclz_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCLZ;
        break;
    }
    case THUMB_VFP_VCMP: {
        uint32_t size, d, vd, m, vm;
        thumb_vfp_vcmp_decode_fields(read_address, &size, &d, &vd, &m, &vm);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMP;
        break;
    }
    case THUMB_VFP_VCMPZ: {
        uint32_t size, d, vd;
        thumb_vfp_vcmpz_decode_fields(read_address, &size, &d, &vd);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMPZ;
        break;
    }
    case THUMB_VFP_VCMPE: {
        uint32_t size, d, vd, m, vm;
        thumb_vfp_vcmpe_decode_fields(read_address, &size, &d, &vd, &m, &vm);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMPE;
        break;
    }
    case THUMB_VFP_VCMPEZ: {
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VCMPEZ;
        break;
    }
    case THUMB_NEON_VCNT: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vcnt_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCNT;
        break;
    }
    case THUMB_NEON_VCVT_F_I: {
        uint32_t op, size, q, d, vd, m, vm;
        thumb_neon_vcvt_f_i_decode_fields(read_address, &op, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCVT_F_I;
        break;
    }
    case THUMB_NEON_VCVT_F_FP: {
        uint32_t op, unsign, q, d, vm, m, imm6;
        thumb_neon_vcvt_f_fp_decode_fields(read_address, &op, &unsign, &q, &d, &vm, &m, &imm6);
        disassem->inst = EMU_INST_NEON_VCVT_F_FP;
        break;
    }
    case THUMB_NEON_VCVT_HP_SP: {
        uint32_t op, size, d, vd, m, vm;
        thumb_neon_vcvt_hp_sp_decode_fields(read_address, &op, &size, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VCVT_HP_SP;
        break;
    }
    case THUMB_VFP_VCVT_F_I: {
        uint32_t op, op2, size, d, vd, m, vm;
        thumb_vfp_vcvt_f_i_decode_fields(read_address, &op, &op2, &size, &d, &vd, &m, &vm);

        bool to_integer = ((op2 & 0x4) != 0);
        bool dp_operation = (size == 1);

        if (to_integer == true) {
            disassem->Vd.reg = vd;
            disassem->Vd.s_reg = true;

            if (dp_operation == true) {
                disassem->Vm.reg = (m << 4) + vm;
            } else {
                disassem->Vm.reg = vm;
                disassem->Vm.s_reg = true;
            }
        } else {
            disassem->Vm.reg = vm;
            disassem->Vm.s_reg = true;
            if (dp_operation == true) {
                disassem->Vd.reg = (d << 4) + vd;
            } else {
                disassem->Vd.reg = vd;
                disassem->Vd.s_reg = true;
            }
        }

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_VFP_VCVT_F_I;
        break;
    }
    case THUMB_VFP_VCVT_F_FP: {
        uint32_t op, unsign, sf, sx, d, vd, imm4, i;
        thumb_vfp_vcvt_f_fp_decode_fields(read_address, &op, &unsign, &sf, &sx, &d, &vd, &imm4, &i);
        disassem->inst = EMU_INST_VFP_VCVT_F_FP;
        break;
    }
    case THUMB_VFP_VCVT_DP_SP: {
        uint32_t size, d, vd, m, vm;
        thumb_vfp_vcvt_dp_sp_decode_fields(read_address, &size, &d, &vd, &m, &vm);

        if (size == 1) {
            // Double to single
            disassem->Vd.reg = vd;
            disassem->Vd.s_reg = true;
            disassem->Vm.reg = (m << 4) + vm;
        } else {
            // Single to double
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vm.reg = vm;
            disassem->Vm.s_reg = true;
        }
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_VFP_VCVT_DP_SP;
        break;
    }
    case THUMB_VFP_VCVTB: {
        uint32_t op, d, vd, m, vm;
        thumb_vfp_vcvtb_decode_fields(read_address, &op, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_VFP_VCVTB;
        break;
    }
    case THUMB_VFP_VCVTT: {
        uint32_t op, d, vd, m, vm;
        thumb_vfp_vcvtt_decode_fields(read_address, &op, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_VFP_VCVTT;
        break;
    }
    case THUMB_VFP_VDIV: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vdiv_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VDIV;
        break;
    }
    case THUMB_NEON_VDUP_SCAL: {
        uint32_t q, imm4, d, vd, m, vm;
        thumb_neon_vdup_scal_decode_fields(read_address, &q, &imm4, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VDUP_SCAL;
        break;
    }
    case THUMB_NEON_VDUP_CORE: {
        uint32_t b, e, q, d, vd, rt;
        thumb_neon_vdup_core_decode_fields(read_address, &b, &e, &q, &d, &vd, &rt);
        disassem->inst = EMU_INST_NEON_VDUP_CORE;
        break;
    }
    case THUMB_NEON_VEOR: {
        uint32_t q, d, vd, n, vn, m, vm;
        thumb_neon_veor_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VEOR;
        break;
    }
    case THUMB_NEON_VEXT: {
        uint32_t q, d, vd, n, vn, m, vm, imm4;
        thumb_neon_vext_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm, &imm4);
        disassem->inst = EMU_INST_NEON_VEXT;
        break;
    }
    case THUMB_NEON_VFMA: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vfma_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VFMA;
        break;
    }
    case THUMB_NEON_VFMS: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vfms_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VFMS;
        break;
    }
    case THUMB_VFP_VFMA: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vfma_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_VFP_VFMA;
        break;
    }
    case THUMB_VFP_VFMS: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vfms_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_VFP_VFMS;
        break;
    }
    case THUMB_VFP_VFNMA: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vfnma_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_VFP_VFNMA;
        break;
    }
    case THUMB_VFP_VFNMS: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vfnms_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_VFP_VFNMS;
        break;
    }
    case THUMB_NEON_VHADD: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vhadd_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VHADD;
        break;
    }
    case THUMB_NEON_VHSUB: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vhsub_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VHSUB;
        break;
    }
    case THUMB_NEON_VLDX_M: {
        uint32_t opcode, size, d, vd, rn, align, rm;
        thumb_neon_vldx_m_decode_fields(read_address, &opcode, &size, &d, &vd, &rn, &align, &rm);
        disassem->inst = EMU_INST_NEON_VLDX_M;
        break;
    }
    case THUMB_NEON_VLDX_S_O: {
        uint32_t opcode, size, d, vd, rn, index_align, rm;
        thumb_neon_vldx_s_o_decode_fields(read_address, &opcode, &size, &d, &vd, &rn, &index_align, &rm);
        disassem->inst = EMU_INST_NEON_VLDX_S_O;
        break;
    }
    case THUMB_NEON_VLDX_S_A: {
        uint32_t opcode, size, d, vd, inc, rn, align, rm;
        thumb_neon_vldx_s_a_decode_fields(read_address, &opcode, &size, &d, &vd, &inc, &rn, &align, &rm);
        disassem->inst = EMU_INST_NEON_VLDX_S_A;
        break;
    }
    case THUMB_VFP_VLDM_DP: {
        uint32_t p, upwards, writeback, rn, d, vd, imm8;
        thumb_vfp_vldm_dp_decode_fields(read_address, &p, &upwards, &writeback, &rn, &d, &vd, &imm8);
        vd = (d << 4) + vd;

        disassem->Rn.reg = rn;
        disassem->is_up = upwards;

        uint32_t num_regs = imm8/2;
        int i = 0;
        for (i=0; i<num_regs; i++) {
            disassem->reglist |= (1 << (vd+i));
        }

        disassem->imm = (imm8 << 2);
        disassem->is_reglist_single = false;
        disassem->format = INS_V_RN_REGLIST;

        disassem->inst = EMU_INST_VFP_VLDM_DP;
        break;
    }
    case THUMB_VFP_VLDM_SP: {
        uint32_t p, upwards, writeback, rn, d, vd, imm8;
        thumb_vfp_vldm_sp_decode_fields(read_address, &p, &upwards, &writeback, &rn, &d, &vd, &imm8);

        uint32_t start_reg = (vd << 1) + d;
        uint32_t num_regs = imm8;
        int i = 0;

        for (i=0; i<num_regs; i++) {
            uint32_t s_reg = start_reg + i;
            disassem->reglist |= (1 << (s_reg/2));
        }

        disassem->Rn.reg = rn;
        disassem->imm = (imm8 << 2);
        disassem->is_up = upwards;
        disassem->is_pre_indexed = p;
        disassem->is_reglist_single = true;

        disassem->format = INS_V_RN_REGLIST;
        disassem->inst = EMU_INST_VFP_VLDM_SP;
        break;
    }
    case THUMB_VFP_VLDR_DP: {
        uint32_t upwards, rn, d, vd, imm8;
        thumb_vfp_vldr_dp_decode_fields(read_address, &upwards, &rn, &d, &vd, &imm8);

        vd = (d << 4) + vd;
        disassem->Vd.reg = vd;
        disassem->Rn.reg = rn;
        disassem->is_up = upwards;
        disassem->is_pre_indexed = true;

        // According to the manual, imm32 = ZeroExtend(imm8:'00', 32);
        // Hence the bitshift left by 2.
        disassem->imm = (imm8 << 2);

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VLDR_DP;
        break;
    }
    case THUMB_VFP_VLDR_SP: {
        uint32_t upwards, rn, d, vd, imm8;
        thumb_vfp_vldr_sp_decode_fields(read_address, &upwards, &rn, &d, &vd, &imm8);

        disassem->Vd.reg = vd;
        disassem->Vd.s_reg = true;

        disassem->Rn.reg = rn;
        disassem->is_up = upwards;
        disassem->is_pre_indexed = true;

        // According to the manual, imm32 = ZeroExtend(imm8:'00', 32);
        // Hence the bitshift left by 2.
        disassem->imm = (imm8 << 2);

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VLDR_SP;
        break;
    }
    case THUMB_NEON_VMAX_I: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmax_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMAX_I;
        break;
    }
    case THUMB_NEON_VMIN_I: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmin_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMIN_I;
        break;
    }
    case THUMB_NEON_VMAX_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmax_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMAX_F;
        break;
    }
    case THUMB_NEON_VMIN_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmin_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMIN_F;
        break;
    }
    case THUMB_NEON_VMLA_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmla_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLA_I;
        break;
    }
    case THUMB_NEON_VMLS_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmls_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLS_I;
        break;
    }
    case THUMB_NEON_VMLAL_I: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vmlal_i_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLAL_I;
        break;
    }
    case THUMB_NEON_VMLSL_I: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vmlsl_i_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLSL_I;
        break;
    }
    case THUMB_NEON_VMLA_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmla_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLA_F;
        break;
    }
    case THUMB_NEON_VMLS_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmls_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLS_F;
        break;
    }
    case THUMB_VFP_VMLA_F: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vmla_f_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VMLA_F;
        break;
    }
    case THUMB_VFP_VMLS_F: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vmls_f_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        if (size == 1) {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        } else {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VMLS_F;
        break;
    }
    case THUMB_NEON_VMLA_SCAL: {
        uint32_t is_float, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmla_scal_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLA_SCAL;
        break;
    }
    case THUMB_NEON_VMLS_SCAL: {
        uint32_t is_float, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmls_scal_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLS_SCAL;
        break;
    }
    case THUMB_NEON_VMLAL_SCAL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vmlal_scal_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLAL_SCAL;
        break;
    }
    case THUMB_NEON_VMLSL_SCAL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vmlsl_scal_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMLSL_SCAL;
        break;
    }
    case THUMB_NEON_VMOVI: {
        uint32_t q, d, vd, op, cmode, i, imm3, imm4;
        thumb_neon_vmovi_decode_fields(read_address, &q, &d, &vd, &op, &cmode, &i, &imm3, &imm4);
        disassem->inst = EMU_INST_NEON_VMOVI;
        break;
    }
    case THUMB_VFP_VMOVI: {
        uint32_t size, d, vd, imm4h, imm4l;
        thumb_vfp_vmovi_decode_fields(read_address, &size, &d, &vd, &imm4h, &imm4l);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vd.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
        }

        disassem->format = INS_V_VD_IMM;
        disassem->inst = EMU_INST_VFP_VMOVI;
        break;
    }
    case THUMB_VFP_VMOV: {
        uint32_t size, d, vd, m, vm;
        thumb_vfp_vmov_decode_fields(read_address, &size, &d, &vd, &m, &vm);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vm.reg = (m << 4) + vm;
        }

        disassem->format = INS_V_VD_VM;
        disassem->inst = EMU_INST_VFP_VMOV;
        break;
    }
    case THUMB_VFP_VMOV_CORE_SCAL: {
        uint32_t d, vd, opc1, opc2, rt;
        thumb_vfp_vmov_core_scal_decode_fields(read_address, &d, &vd, &opc1, &opc2, &rt);
        disassem->inst = EMU_INST_VFP_VMOV_CORE_SCAL;
        break;
    }
    case THUMB_VFP_VMOV_SCAL_CORE: {
        uint32_t unsign, rt, n, vn, opc1, opc2;
        thumb_vfp_vmov_scal_core_decode_fields(read_address, &unsign, &rt, &n, &vn, &opc1, &opc2);
        disassem->inst = EMU_INST_VFP_VMOV_SCAL_CORE;
        break;
    }
    case THUMB_VFP_VMOV_CORE_SP: {
        uint32_t op, rt, n, vn;
        thumb_vfp_vmov_core_sp_decode_fields(read_address, &op, &rt, &n, &vn);

        bool to_arm_register = (op == 1);
        disassem->Rt.reg = rt;
        disassem->Vn.reg = vn;
        disassem->Vn.s_reg = true;

        if (to_arm_register == true) {
            disassem->format = INS_V_RT_VN;
        } else {
            disassem->format = INS_V_VN_RT;
        }

        disassem->inst = EMU_INST_VFP_VMOV_CORE_SP;
        break;
    }
    case THUMB_VFP_VMOV_2CORE_2SP: {
        uint32_t to_arm, rt, rt2, m, vm;
        thumb_vfp_vmov_2core_2sp_decode_fields(read_address, &to_arm, &rt, &rt2, &m, &vm);
        disassem->inst = EMU_INST_VFP_VMOV_2CORE_2SP;
        break;
    }
    case THUMB_VFP_VMOV_2CORE_DP: {
        uint32_t to_arm, rt, rt2, m, vm;
        thumb_vfp_vmov_2core_dp_decode_fields(read_address, &to_arm, &rt, &rt2, &m, &vm);

        disassem->Rt.reg = rt;
        disassem->Rt2.reg = rt2;
        disassem->Vm.reg = (m << 4) + vm;

        if (to_arm == 1) {
            disassem->format = INS_V_RT_RT2_VM;
        } else {
            disassem->format = INS_V_VM_RT_RT2;
        }

        disassem->inst = EMU_INST_VFP_VMOV_2CORE_DP;
        break;
    }
    case THUMB_NEON_VMOVL: {
        uint32_t op, unsign, d, vd, m, vm;
        thumb_neon_vmovl_decode_fields(read_address, &op, &unsign, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMOVL;
        break;
    }
    case THUMB_NEON_VMOVN: {
        uint32_t size, d, vd, m, vm;
        thumb_neon_vmovn_decode_fields(read_address, &size, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMOVN;
        break;
    }
    case THUMB_VFP_VMRS: {
        uint32_t rt;
        thumb_vfp_vmrs_decode_fields(read_address, &rt);
        disassem->format = INS_UNTRANSLATED;
        disassem->inst = EMU_INST_VFP_VMRS;
        break;
    }
    case THUMB_VFP_VMSR: {
        uint32_t rt;
        thumb_vfp_vmsr_decode_fields(read_address, &rt);
        disassem->inst = EMU_INST_VFP_VMSR;
        break;
    }
    case THUMB_NEON_VMUL_I: {
        uint32_t op, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmul_i_decode_fields(read_address, &op, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMUL_I;
        break;
    }
    case THUMB_NEON_VMULL_I: {
        uint32_t op, unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vmull_i_decode_fields(read_address, &op, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMULL_I;
        break;
    }
    case THUMB_NEON_VMUL_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmul_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMUL_F;
        break;
    }
    case THUMB_VFP_VMUL: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vmul_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VMUL_F;
        break;
    }
    case THUMB_NEON_VMUL_SCAL: {
        uint32_t is_float, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vmul_scal_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMUL_SCAL;
        break;
    }
    case THUMB_NEON_VMULL_SCAL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vmull_scal_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VMULL_SCAL;
        break;
    }
    case THUMB_NEON_VMVNI: {
        uint32_t q, d, vd, cmode, i, imm3, imm4;
        thumb_neon_vmvni_decode_fields(read_address, &q, &d, &vd, &cmode, &i, &imm3, &imm4);
        disassem->inst = EMU_INST_NEON_VMVNI;
        break;
    }
    case THUMB_NEON_VMVN: {
        uint32_t q, d, vd, cmode, i, imm3, imm4;
        thumb_neon_vmvni_decode_fields(read_address, &q, &d, &vd, &cmode, &i, &imm3, &imm4);
        disassem->inst = EMU_INST_NEON_VMVN;
        break;
    }
    case THUMB_NEON_VNEG: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vneg_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VNEG;
        break;
    }
    case THUMB_VFP_VNEG: {
        uint32_t size, d, vd, m, vm;
        thumb_vfp_vneg_decode_fields(read_address, &size, &d, &vd, &m, &vm);

        if (size == 1) {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vm.reg = (m << 4) + vm;
        } else {
            disassem->Vd.reg = vd;
            disassem->Vm.reg = vm;
            disassem->Vd.s_reg = true;
            disassem->Vm.s_reg = true;
        }
        disassem->format = INS_V_VD_VM;

        disassem->inst = EMU_INST_VFP_VNEG;
        break;
    }
    case THUMB_VFP_VNMLA: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vnmla_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_VFP_VNMLA;
        break;
    }
    case THUMB_VFP_VNMLS: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vnmls_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        if (size == 1) {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        } else {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;
            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        }
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_VFP_VNMLS;
        break;
    }
    case THUMB_VFP_VNMUL: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vnmul_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        if (size == 1) {
            // single regs
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }
        disassem->format = INS_V_VD_VN_VM;

        disassem->inst = EMU_INST_VFP_VNMUL;
        break;
    }
    case THUMB_NEON_VORN: {
        uint32_t q, d, vd, n, vn, m, vm;
        thumb_neon_vorn_decode_fields(read_address, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VORN;
        break;
    }
    case THUMB_NEON_VORRI: {
        uint32_t q, d, vd, cmode, i, imm3, imm4;
        thumb_neon_vorri_decode_fields(read_address, &q, &d, &vd, &cmode, &i, &imm3, &imm4);
        disassem->inst = EMU_INST_NEON_VORRI;
        break;
    }
    case THUMB_NEON_VORR: {
        uint32_t q, d, vd, cmode, i, imm3, imm4;
        thumb_neon_vorri_decode_fields(read_address, &q, &d, &vd, &cmode, &i, &imm3, &imm4);
        disassem->inst = EMU_INST_NEON_VORR;
        break;
    }
    case THUMB_NEON_VPADAL: {
        uint32_t unsign, size, q, d, vd, m, vm;
        thumb_neon_vpadal_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPADAL;
        break;
    }
    case THUMB_NEON_VPADD_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vpadd_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPADD_I;
        break;
    }
    case THUMB_NEON_VPADD_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vpadd_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPADD_F;
        break;
    }
    case THUMB_NEON_VPADDL: {
        uint32_t unsign, size, q, d, vd, m, vm;
        thumb_neon_vpaddl_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPADDL;
        break;
    }
    case THUMB_NEON_VPMAX_I: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vpmax_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPMAX_I;
        break;
    }
    case THUMB_NEON_VPMIN_I: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vpmin_i_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPMIN_I;
        break;
    }
    case THUMB_NEON_VPMAX_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vpmax_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPMAX_F;
        break;
    }
    case THUMB_NEON_VPMIN_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vpmin_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VPMIN_F;
        break;
    }
    case THUMB_VFP_VPOP: {
        uint32_t size, d, vd, regs; // regs is really 'imm8'
        thumb_vfp_vpop_decode_fields(read_address, &size, &d, &vd, &regs);

        bool single_regs = (size == 0);
        uint32_t num_regs = 0;
        uint32_t start_reg = 0;
        int i = 0;

        if (single_regs == true) {
            num_regs = regs;
            start_reg = (vd << 1) + d;

            for (i=0; i<num_regs; i++) {
                uint32_t s_reg = start_reg + i;
                disassem->reglist |= (1 << (s_reg / 2));
            }

            disassem->imm = (regs << 2);
            disassem->inst = EMU_INST_VFP_VPOP_SP;
        } else {
            num_regs = regs/2;
            start_reg = (d << 4) + vd;

            for (i=0; i<num_regs; i++) {
                disassem->reglist |= (1 << (start_reg+i));
            }

            disassem->imm = (regs << 2);
            disassem->inst = EMU_INST_VFP_VPOP_DP;
        }

        disassem->is_reglist_single = single_regs;
        disassem->format = INS_V_REGLIST;
        break;
    }
    case THUMB_VFP_VPUSH: {
        uint32_t size, d, vd, regs; // regs is really 'imm8'
        thumb_vfp_vpush_decode_fields(read_address, &size, &d, &vd, &regs);

        bool single_regs = (size == 0);
        uint32_t num_regs = 0;
        uint32_t start_reg = 0;
        int i = 0;

        if (single_regs == true) {
            num_regs = regs;
            start_reg = (vd << 1) + d;

            for (i=0; i<num_regs; i++) {
                uint32_t s_reg = start_reg + i;
                disassem->reglist |= (1 << (s_reg/2));
            }

            disassem->imm = (regs << 2);
            disassem->inst = EMU_INST_VFP_VPUSH_SP;
        } else {
            num_regs = regs/2;
            start_reg = (d << 4) + vd;

            for (i=0; i<num_regs; i++) {
                disassem->reglist |= (1 << (start_reg+i));
            }

            disassem->imm = (regs << 2);
            disassem->inst = EMU_INST_VFP_VPUSH_DP;
        }

        disassem->is_reglist_single = single_regs;
        disassem->format = INS_V_REGLIST;
        break;
    }
    case THUMB_NEON_VQABS: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vqabs_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQABS;
        break;
    }
    case THUMB_NEON_VQADD: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vqadd_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQADD;
        break;
    }
    case THUMB_NEON_VQDMLAL_I: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vqdmlal_i_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMLAL_I;
        break;
    }
    case THUMB_NEON_VQDMLSL_I: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vqdmlsl_i_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMLSL_I;
        break;
    }
    case THUMB_NEON_VQDMLAL_SCAL: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vqdmlal_scal_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMLAL_SCAL;
        break;
    }
    case THUMB_NEON_VQDMLSL_SCAL: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vqdmlsl_scal_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMLSL_SCAL;
        break;
    }
    case THUMB_NEON_VQDMULH_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vqdmulh_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMULH_I;
        break;
    }
    case THUMB_NEON_VQDMULH_SCAL: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vqdmulh_scal_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMULH_SCAL;
        break;
    }
    case THUMB_NEON_VQDMULL_I: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vqdmull_i_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMULL_I;
        break;
    }
    case THUMB_NEON_VQDMULL_SCAL: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vqdmull_scal_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQDMULL_SCAL;
        break;
    }
    case THUMB_NEON_VQMOVN: {
        uint32_t unsign, size, d, vd, m, vm;
        thumb_neon_vqmovn_decode_fields(read_address, &unsign, &size, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQMOVN;
        break;
    }
    case THUMB_NEON_VQMOVUN: {
        uint32_t size, d, vd, m, vm;
        thumb_neon_vqmovun_decode_fields(read_address, &size, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQMOVUN;
        break;
    }
    case THUMB_NEON_VQNEG: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vqneg_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQNEG;
        break;
    }
    case THUMB_NEON_VQRDMULH_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vqrdmulh_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQRDMULH_I;
        break;
    }
    case THUMB_NEON_VQRDMULH_SCAL: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vqrdmulh_scal_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQRDMULH_SCAL;
        break;
    }
    case THUMB_NEON_VQRSHL: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vqrshl_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQRSHL;
        break;
    }
    case THUMB_NEON_VQRSHRN: {
        uint32_t unsign, d, vd, m, vm, imm6;
        thumb_neon_vqrshrn_decode_fields(read_address, &unsign, &d, &vd, &m, &vm, &imm6);
        disassem->inst = EMU_INST_NEON_VQRSHRN;
        break;
    }
    case THUMB_NEON_VQRSHRUN: {
        uint32_t d, vd, m, vm, imm6;
        thumb_neon_vqrshrun_decode_fields(read_address, &d, &vd, &m, &vm, &imm6);
        disassem->inst = EMU_INST_NEON_VQRSHRUN;
        break;
    }
    case THUMB_NEON_VQSHL: {
        uint32_t unsign, size, q, d, vd, m, vm, n, vn;
        thumb_neon_vqshl_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &m, &vm, &n, &vn);
        disassem->inst = EMU_INST_NEON_VQSHL;
        break;
    }
    case THUMB_NEON_VQSHLI: {
        uint32_t unsign, q, d, vd, m, vm, l, imm6;
        thumb_neon_vqshli_decode_fields(read_address, &unsign, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VQSHLI;
        break;
    }
    case THUMB_NEON_VQSHLUI: {
        uint32_t q, d, vd, m, vm, l, imm6;
        thumb_neon_vqshlui_decode_fields(read_address, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VQSHLUI;
        break;
    }
    case THUMB_NEON_VQSHRN: {
        uint32_t unsign, d, vd, m, vm, imm6;
        thumb_neon_vqshrn_decode_fields(read_address, &unsign, &d, &vd, &m, &vm, &imm6);
        disassem->inst = EMU_INST_NEON_VQSHRN;
        break;
    }
    case THUMB_NEON_VQSHRUN: {
        uint32_t d, vd, m, vm, imm6;
        thumb_neon_vqshrun_decode_fields(read_address, &d, &vd, &m, &vm, &imm6);
        disassem->inst = EMU_INST_NEON_VQSHRUN;
        break;
    }
    case THUMB_NEON_VQSUB: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vqsub_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VQSUB;
        break;
    }
    case THUMB_NEON_VRADDHN: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vraddhn_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRADDHN;
        break;
    }
    case THUMB_NEON_VRECPE: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vrecpe_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRECPE;
        break;
    }
    case THUMB_NEON_VRECPS: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vrecps_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRECPS;
        break;
    }
    case THUMB_NEON_VREV16: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vrev16_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VREV16;
        break;
    }
    case THUMB_NEON_VREV32: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vrev32_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VREV32;
        break;
    }
    case THUMB_NEON_VREV64: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vrev64_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VREV64;
        break;
    }
    case THUMB_NEON_VRHADD: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vrhadd_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRHADD;
        break;
    }
    case THUMB_NEON_VRSHL: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vrshl_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRSHL;
        break;
    }
    case THUMB_NEON_VRSHR: {
        uint32_t unsign, q, d, vd, m, vm, l, imm6;
        thumb_neon_vrshr_decode_fields(read_address, &unsign, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VRSHR;
        break;
    }
    case THUMB_NEON_VRSHRN: {
        uint32_t d, vd, m, vm, imm6;
        thumb_neon_vrshrn_decode_fields(read_address, &d, &vd, &m, &vm, &imm6);
        disassem->inst = EMU_INST_NEON_VRSHRN;
        break;
    }
    case THUMB_NEON_VRSQRTE: {
        uint32_t is_float, size, q, d, vd, m, vm;
        thumb_neon_vrsqrte_decode_fields(read_address, &is_float, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRSQRTE;
        break;
    }
    case THUMB_NEON_VRSQRTS: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vrsqrts_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRSQRTS;
        break;
    }
    case THUMB_NEON_VRSRA: {
        uint32_t unsign, q, d, vd, m, vm, l, imm6;
        thumb_neon_vrsra_decode_fields(read_address, &unsign, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VRSRA;
        break;
    }
    case THUMB_NEON_VRSUBHN: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vrsubhn_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VRSUBHN;
        break;
    }
    case THUMB_NEON_VSHL: {
        uint32_t unsign, size, q, d, vd, n, vn, m, vm;
        thumb_neon_vshl_decode_fields(read_address, &unsign, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSHL;
        break;
    }
    case THUMB_NEON_VSHLI: {
        uint32_t q, d, vd, m, vm, l, imm6;
        thumb_neon_vshli_decode_fields(read_address, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VSHLI;
        break;
    }
    case THUMB_NEON_VSHLL: {
        uint32_t unsign, d, vd, m, vm, imm6;
        thumb_neon_vshll_decode_fields(read_address, &unsign, &d, &vd, &m, &vm, &imm6);
        disassem->inst = EMU_INST_NEON_VSHLL;
        break;
    }
    case THUMB_NEON_VSHLL2: {
        uint32_t size, d, vd, m, vm;
        thumb_neon_vshll2_decode_fields(read_address, &size, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSHLL2;
        break;
    }
    case THUMB_NEON_VSHR: {
        uint32_t unsign, q, d, vd, m, vm, l, imm6;
        thumb_neon_vshr_decode_fields(read_address, &unsign, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VSHR;
        break;
    }
    case THUMB_NEON_VSHRN: {
        uint32_t d, vd, m, vm, imm6;
        thumb_neon_vshrn_decode_fields(read_address, &d, &vd, &m, &vm, &imm6);
        disassem->inst = EMU_INST_NEON_VSHRN;
        break;
    }
    case THUMB_NEON_VSLI: {
        uint32_t q, d, vd, m, vm, l, imm6;
        thumb_neon_vsli_decode_fields(read_address, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VSLI;
        break;
    }
    case THUMB_VFP_VSQRT: {
        uint32_t size, d, vd, m, vm;
        thumb_vfp_vsqrt_decode_fields(read_address, &size, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_VFP_VSQRT;
        break;
    }
    case THUMB_NEON_VSRA: {
        uint32_t unsign, q, d, vd, m, vm, l, imm6;
        thumb_neon_vsra_decode_fields(read_address, &unsign, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VSRA;
        break;
    }
    case THUMB_NEON_VSRI: {
        uint32_t q, d, vd, m, vm, l, imm6;
        thumb_neon_vsri_decode_fields(read_address, &q, &d, &vd, &m, &vm, &l, &imm6);
        disassem->inst = EMU_INST_NEON_VSRI;
        break;
    }
    case THUMB_NEON_VSTX_M: {
        uint32_t opcode, size, d, vd, rn, align, rm;
        thumb_neon_vstx_m_decode_fields(read_address, &opcode, &size, &d, &vd, &rn, &align, &rm);
        disassem->inst = EMU_INST_NEON_VSTX_M;
        break;
    }
    case THUMB_NEON_VSTX_S_O: {
        uint32_t opcode, size, d, vd, rn, index_align, rm;
        thumb_neon_vstx_s_o_decode_fields(read_address, &opcode, &size, &d, &vd, &rn, &index_align, &rm);
        disassem->inst = EMU_INST_NEON_VSTX_S_O;
        break;
    }
    case THUMB_VFP_VSTM_DP: {
        uint32_t p, upwards, writeback, rn, d, vd, imm8;
        thumb_vfp_vstm_dp_decode_fields(read_address, &p, &upwards, &writeback, &rn, &d, &vd, &imm8);

        vd = (d << 4) + vd;
        disassem->Rn.reg = rn;
        disassem->is_up = upwards;

        uint32_t num_regs = imm8/2;
        int i = 0;
        for (i=0; i<num_regs; i++) {
            disassem->reglist |= (1 << (vd+i));
        }

        disassem->imm = (imm8 << 2);
        disassem->is_reglist_single = false;
        disassem->format = INS_V_RN_REGLIST;

        disassem->inst = EMU_INST_VFP_VSTM_DP;
        break;
    }
    case THUMB_VFP_VSTM_SP: {
        uint32_t p, upwards, writeback, rn, d, vd, imm8;
        thumb_vfp_vstm_sp_decode_fields(read_address, &p, &upwards, &writeback, &rn, &d, &vd, &imm8);

        vd = (vd << 1) + d;
        disassem->Rn.reg = rn;
        disassem->is_up = upwards;

        // The register numbers given by the PIE are in terms of the S
        // registers. However, we want to set the register list in
        // terms of the D registers.
        uint32_t num_regs = imm8;
        int i = 0;
        for (i=0; i<num_regs; i++) {
            uint32_t dn = (vd + i) / 2;
            disassem->reglist |= (1 << dn);
        }

        disassem->imm = (imm8 << 2);
        disassem->is_reglist_single = true;
        disassem->format = INS_V_RN_REGLIST;
        disassem->inst = EMU_INST_VFP_VSTM_SP;
        break;
    }
    case THUMB_VFP_VSTR_DP: {
        uint32_t upwards, rn, d, vd, imm8;
        thumb_vfp_vstr_dp_decode_fields(read_address, &upwards, &rn, &d, &vd, &imm8);

        disassem->Vd.reg = (d << 4) + vd;
        disassem->Rn.reg = rn;

        disassem->imm = (imm8 << 2);
        disassem->is_up = upwards;
        disassem->is_pre_indexed = true;

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VSTR_DP;
        break;
    }
    case THUMB_VFP_VSTR_SP: {
        uint32_t upwards, rn, d, vd, imm8;
        thumb_vfp_vstr_sp_decode_fields(read_address, &upwards, &rn, &d, &vd, &imm8);

        disassem->Vd.reg = vd;
        disassem->Vd.s_reg = true;
        disassem->Rn.reg = rn;

        // According to the manual, imm32 = ZeroExtend(imm8:'00', 32);
        // Hence the bitshift left by 2.
        disassem->imm = (imm8 << 2);
        disassem->is_up = upwards;

        // VFP_VSTR_DP is always pre_indexed.
        disassem->is_pre_indexed = true;

        disassem->format = INS_V_VD_RN_IMM;
        disassem->inst = EMU_INST_VFP_VSTR_SP;
        break;
    }
    case THUMB_NEON_VSUB_I: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vsub_i_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSUB_I;
        break;
    }
    case THUMB_NEON_VSUB_F: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vsub_f_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSUB_F;
        break;
    }
    case THUMB_VFP_VSUB: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_vfp_vsub_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);

        bool single_register = (size == 0);
        if (single_register == true) {
            disassem->Vd.reg = vd;
            disassem->Vn.reg = vn;
            disassem->Vm.reg = vm;

            disassem->Vd.s_reg = true;
            disassem->Vn.s_reg = true;
            disassem->Vm.s_reg = true;
        } else {
            disassem->Vd.reg = (d << 4) + vd;
            disassem->Vn.reg = (n << 4) + vn;
            disassem->Vm.reg = (m << 4) + vm;
        }

        disassem->format = INS_V_VD_VN_VM;
        disassem->inst = EMU_INST_VFP_VSUB_F;
        break;
    }
    case THUMB_NEON_VSUBHN: {
        uint32_t size, d, vd, n, vn, m, vm;
        thumb_neon_vsubhn_decode_fields(read_address, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSUBHN;
        break;
    }
    case THUMB_NEON_VSUBL: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vsubl_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSUBL;
        break;
    }
    case THUMB_NEON_VSUBW: {
        uint32_t unsign, size, d, vd, n, vn, m, vm;
        thumb_neon_vsubw_decode_fields(read_address, &unsign, &size, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSUBW;
        break;
    }
    case THUMB_NEON_VSWP: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vswp_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VSWP;
        break;
    }
    case THUMB_NEON_VTBL: {
        uint32_t d, vd, n, vn, len, m, vm;
        thumb_neon_vtbl_decode_fields(read_address, &d, &vd, &n, &vn, &len, &m, &vm);
        disassem->inst = EMU_INST_NEON_VTBL;
        break;
    }
    case THUMB_NEON_VTBX: {
        uint32_t d, vd, n, vn, len, m, vm;
        thumb_neon_vtbx_decode_fields(read_address, &d, &vd, &n, &vn, &len, &m, &vm);
        disassem->inst = EMU_INST_NEON_VTBX;
        break;
    }
    case THUMB_NEON_VTRN: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vtrn_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VTRN;
        break;
    }
    case THUMB_NEON_VTST: {
        uint32_t size, q, d, vd, n, vn, m, vm;
        thumb_neon_vtst_decode_fields(read_address, &size, &q, &d, &vd, &n, &vn, &m, &vm);
        disassem->inst = EMU_INST_NEON_VTST;
        break;
    }
    case THUMB_NEON_VUZP: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vuzp_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VUZP;
        break;
    }
    case THUMB_NEON_VZIP: {
        uint32_t size, q, d, vd, m, vm;
        thumb_neon_vzip_decode_fields(read_address, &size, &q, &d, &vd, &m, &vm);
        disassem->inst = EMU_INST_NEON_VZIP;
        break;
    }
    case THUMB_VFP_VSEL: {
        uint32_t size, cond, d, vd, n, vn, m, vm;
        thumb_vfp_vsel_decode_fields(read_address, &size, &cond, &d, &vd, &n, &vn, &m, &vm);
        break;
    }
    case THUMB_INVALID: {
        disassem->inst = EMU_INST_INVALID;
        break;
    }
    }

    if (disassem->format == INS_INVALID) {
        if (disassem->size == 2) {
            emu_abort("Format of Instruction (%d, %s, 0x%04x,) not set during Thumb (16-bit) translation!", inst, inst_strings[disassem->inst], *(read_address));
        } else {
            emu_abort("Format of Instruction (%d, %s, 0x%04x,0x%04x,) not set during Thumb (32-bit) translation!", inst, inst_strings[disassem->inst], *(read_address), *(read_address+1));
        }
    }

}
