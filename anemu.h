#ifndef INCLUDE_ANEMU_H
#define INCLUDE_ANEMU_H

#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

#include <stdbool.h>
#include <stdint.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include "emu_markers.h"
#include "emu_profiler.h"

/* Public API */
__BEGIN_DECLS

/* syscalls used by trampolines */
extern ssize_t __read(int, void *, size_t);
extern ssize_t __write(int, void *, size_t);

/* Hooks */
void emu_hook_thread_entry(void *arg);
void emu_hook_pthread_internal_free(void *arg);
void emu_hook_bionic_clone_entry();
void emu_hook_bionic_atfork_run_child(void *arg);
void emu_hook_exit_thread(int ret);
void emu_hook_Zygote_forkAndSpecializeCommon(void *arg);

/* Trampolines */
ssize_t emu_trampoline_read(int fd, void *buf, size_t count);
ssize_t emu_trampoline_write(int fd, void *buf, size_t count);

/* check if current pid / app is targeted for emulation */
uint32_t emu_target();
void emu_set_target(pid_t pid);

void emu_set_taint_array(uint32_t addr, uint32_t length, uint32_t tag);
uint32_t emu_get_taint_array(uint32_t addr, uint32_t length);

uint32_t emu_dump_taintmaps();
uint32_t emu_dump_taintpages();

bool emu_running();
uint8_t emu_disabled();
bool emu_initialized();
bool emu_untracked();

int emu_ignore_protections();
void emu_obey_protections();

void *emu_dlopen(const char *filename, int flags);

/**
 * Indicates whether the thread should ignore or obey sandtrap
 * protections when ending emulation.
 */
void emu_set_protections_mode_on_exit(bool ignore_protections_on_exit);

// This function must be called to terminate any ongoing
// emulation. Note that you _must_ call this with the argument "0". If
// this returns 0, it means SandTrap wasn't emulating anything. If it
// returns 1, it means SandTrap was actively emulating the thread and
// just stopped.
int emu_terminate(int ret);

void emu_set_jni_return_taint(uint32_t tag, size_t length);
char* emu_get_last_status();

#ifdef TEST_TOOLS
void emu_test_set_reg_labels(uint32_t core_reg_lbls[]);
void emu_test_get_reg_labels(uint32_t core_reg_lbls[]);
void emu_test_save_regs();
void emu_test_get_regs(uint32_t core_regs[]);
#endif // TEST_TOOLS

__END_DECLS

#endif  /* INCLUDE_ANEMU_H */
