#define HANDLE_REG_TO_MEM_AND_MEM_TO_REG_TAINT_PROP {                   \
        switch (mem_type) {                                             \
                                                                        \
        case MEM_RN: {                                                  \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            final_address = rn_val;                                     \
            is_up = READ_FOOTER_UPDOWN(footer_word);                    \
            is_pre_indexed = READ_FOOTER_PREPOSTINDEX(footer_word);     \
            add_taint = READ_FOOTER_ADD_TAINT(footer_word);             \
            rn_number = READ_FOOTER_RN_NUMBER(footer_word);             \
                                                                        \
            if (num_regs >= 4) {                                        \
                emu_abort("Unsupported amount of registers (%d) while handling MEM_RN taint prop.\n", num_regs); \
            } else if (num_regs == 3) {                                 \
                reg3 = READ_HEADER_REGISTER_INDEX(header_word, 2);      \
                reg2 = READ_HEADER_REGISTER_INDEX(header_word, 1);      \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
            } else if (num_regs == 2) {                                 \
                reg2 = READ_HEADER_REGISTER_INDEX(header_word, 1);      \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
            } else if (num_regs == 1) {                                 \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
                                                                        \
                if (taint_dir == MEM_TO_REG) {                          \
                    handle_mem_to_single_reg_taint(emu, final_address, reg1, rn_number, rm_number); \
                    continue;                                           \
                }                                                       \
            }                                                           \
                                                                        \
            if (taint_dir == MEM_TO_REG) {                              \
                handle_mem_to_reg_taint(emu, final_address, is_up, is_pre_indexed, reg1, reg2, reg3, rn_number, rm_number); \
            } else if (taint_dir == REG_TO_MEM) {                       \
                handle_reg_to_mem_taint(emu, add_taint, final_address, is_up, is_pre_indexed, reg1, reg2, reg3, rn_number, rm_number); \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_RN_REGLIST: {                                          \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t registers = READ_HEADER_REGLIST(header_word);      \
            final_address = rn_val;                                     \
            is_up = READ_FOOTER_UPDOWN(footer_word);                    \
            is_pre_indexed = READ_FOOTER_PREPOSTINDEX(footer_word);     \
            add_taint = READ_FOOTER_ADD_TAINT(footer_word);             \
            rn_number = READ_FOOTER_RN_NUMBER(footer_word);             \
                                                                        \
            if (taint_dir == MEM_TO_REG) {                              \
                handle_mem_to_reglist_taint(emu, final_address, is_up, is_pre_indexed, registers, rn_number); \
            } else if (taint_dir == REG_TO_MEM) {                       \
                handle_reglist_to_mem_taint(emu, add_taint, final_address, is_up, is_pre_indexed, registers, rn_number); \
            }                                                           \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_RN_IMM: {                                              \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            is_up = READ_FOOTER_UPDOWN(footer_word);                    \
            is_pre_indexed = READ_FOOTER_PREPOSTINDEX(footer_word);     \
            add_taint = READ_FOOTER_ADD_TAINT(footer_word);             \
            rn_number = READ_FOOTER_RN_NUMBER(footer_word);             \
            footer_imm = READ_FOOTER_IMM_VALUE(footer_word);            \
                                                                        \
            if (is_pre_indexed) {                                       \
                if (is_up) {                                            \
                    final_address = rn_val + footer_imm;                \
                } else {                                                \
                    final_address = rn_val - footer_imm;                \
                }                                                       \
            } else {                                                    \
                final_address = rn_val;                                 \
            }                                                           \
                                                                        \
            if (num_regs >= 4) {                                        \
                emu_abort("Unsupported amount of registers (%d) while handling MEM_RN_IMM taint prop.\n", num_regs); \
            } else if (num_regs == 3) {                                 \
                reg3 = READ_HEADER_REGISTER_INDEX(header_word, 2);      \
                reg2 = READ_HEADER_REGISTER_INDEX(header_word, 1);      \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
            } else if (num_regs == 2) {                                 \
                reg2 = READ_HEADER_REGISTER_INDEX(header_word, 1);      \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
            } else if (num_regs == 1) {                                 \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
                                                                        \
                if (taint_dir == MEM_TO_REG) {                          \
                    handle_mem_to_single_reg_taint(emu, final_address, reg1, rn_number, rm_number); \
                    continue;                                           \
                }                                                       \
            }                                                           \
                                                                        \
            if (taint_dir == MEM_TO_REG) {                              \
                handle_mem_to_reg_taint(emu, final_address, is_up, is_pre_indexed, reg1, reg2, reg3, rn_number, rm_number); \
            } else if (taint_dir == REG_TO_MEM) {                       \
                handle_reg_to_mem_taint(emu, add_taint, final_address, is_up, is_pre_indexed, reg1, reg2, reg3, rn_number, rm_number); \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_RN_RM_SHIFT: {                                         \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, rm_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            is_up = READ_FOOTER_UPDOWN(footer_word);                    \
            is_pre_indexed = READ_FOOTER_PREPOSTINDEX(footer_word);     \
            rn_number = READ_FOOTER_RN_NUMBER(footer_word);             \
            add_taint = READ_FOOTER_ADD_TAINT(footer_word);             \
            footer_imm = READ_FOOTER_IMM_VALUE(footer_word);            \
            shift_type = READ_FOOTER_SHIFT_TYPE(footer_word);           \
                                                                        \
            uint32_t shifted_val = rm_val;                              \
                                                                        \
            if (footer_imm != 0) {                                      \
                switch (shift_type) {                                   \
                case EMU_ARM_SFT_LSL: shifted_val = LSL(rm_val, footer_imm); break; \
                case EMU_ARM_SFT_LSR: shifted_val = LSR(rm_val, footer_imm); break; \
                case EMU_ARM_SFT_ASR: shifted_val = ASR(rm_val, footer_imm); break; \
                case EMU_ARM_SFT_ROR: shifted_val = ROR(rm_val, footer_imm); break; \
                default:                                                \
                    emu_abort("Unsupported shift type (%d) in MEM_RN_RM_SHIFT.\n", shift_type); \
                    break;                                              \
                }                                                       \
            }                                                           \
                                                                        \
            if (is_pre_indexed) {                                       \
                if (is_up) {                                            \
                    final_address = rn_val + shifted_val;               \
                } else {                                                \
                    final_address = rn_val - shifted_val;               \
                }                                                       \
                rm_number = READ_FOOTER_RM_NUMBER(footer_word);         \
            } else {                                                    \
                final_address = rn_val;                                 \
            }                                                           \
                                                                        \
            if (num_regs >= 4) {                                        \
                emu_abort("Unsupported amount of registers (%d) while handling MEM_RN_RM_SHIFT taint prop.\n", num_regs); \
            } else if (num_regs == 3) {                                 \
                reg3 = READ_HEADER_REGISTER_INDEX(header_word, 2);      \
                reg2 = READ_HEADER_REGISTER_INDEX(header_word, 1);      \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
            } else if (num_regs == 2) {                                 \
                reg2 = READ_HEADER_REGISTER_INDEX(header_word, 1);      \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
            } else if (num_regs == 1) {                                 \
                reg1 = READ_HEADER_REGISTER_INDEX(header_word, 0);      \
                                                                        \
                if (taint_dir == MEM_TO_REG) {                          \
                    handle_mem_to_single_reg_taint(emu, final_address, reg1, rn_number, rm_number); \
                    continue;                                           \
                }                                                       \
            }                                                           \
                                                                        \
            if (taint_dir == MEM_TO_REG) {                              \
                handle_mem_to_reg_taint(emu, final_address, is_up, is_pre_indexed, reg1, reg2, reg3, rn_number, rm_number); \
            } else if (taint_dir == REG_TO_MEM) {                       \
                handle_reg_to_mem_taint(emu, add_taint, final_address, is_up, is_pre_indexed, reg1, reg2, reg3, rn_number, rm_number); \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        default:                                                        \
            emu_abort("Unsupported mem type (%d) in mem-to-reg or reg-to-mem taint.\n", mem_type); \
            break;                                                      \
                                                                        \
                                                                        \
        } /* End of switch (mem_type)) */                               \
    }


#define HANDLE_EXT_REG_TO_MEM_AND_MEM_TO_EXT_REG_TAINT_PROP {           \
                                                                        \
        switch (mem_type) {                                             \
                                                                        \
        case MEM_RN_IMM: {                                              \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            is_up = READ_FOOTER_UPDOWN(footer_word);                    \
            is_pre_indexed = READ_FOOTER_PREPOSTINDEX(footer_word);     \
            add_taint = READ_FOOTER_ADD_TAINT(footer_word);             \
            rn_number = READ_FOOTER_RN_NUMBER(footer_word);             \
            footer_imm = READ_FOOTER_IMM_VALUE(footer_word);            \
                                                                        \
            if (is_pre_indexed) {                                       \
                if (is_up) {                                            \
                    final_address = rn_val + footer_imm;                \
                } else {                                                \
                    final_address = rn_val - footer_imm;                \
                }                                                       \
            } else {                                                    \
                final_address = rn_val;                                 \
            }                                                           \
                                                                        \
            uint8_t num_ext_regs = READ_HEADER_NUM_EXT_REGISTERS(header_word); \
            if (num_ext_regs != 1) {                                    \
                emu_abort("Unexpected number of extended registers in MEM_RN_IMM"); \
            }                                                           \
                                                                        \
            uint8_t ext_reg, q_reg, s_reg;                              \
            READ_HEADER_EXT_REGISTER_INDEX(header_word, ext_reg, q_reg, s_reg, 0); \
                                                                        \
            if (taint_dir == MEM_TO_EXTREG) {                           \
                handle_mem_to_single_extreg_taint(emu, final_address, ext_reg, q_reg, s_reg, rn_number); \
            } else if (taint_dir == EXTREG_TO_MEM) {                    \
                handle_single_extreg_to_mem_taint(emu, final_address, ext_reg, q_reg, s_reg, rn_number); \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VLD1_M: {                                              \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            /* In multiple single element VLD1, ``elements from memory */ \
            /* are stored into one, two, three, or four registers... */ \
            /* Every element of each register is loaded.'' As this */   \
            /* means we'll replace the value of the vector register, */ \
            /* we replace its taint tag rather than add to it. */       \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
                                                                        \
            int i = 0;                                                  \
            for (i = 0; i < num_regs; i++) {                            \
                uint32_t taint_label = 0;                               \
                uint32_t base_address = rn_val + (i * EXT_REG_SIZE_BYTES); \
                taint_label |= emu_get_taint_mem(emu, base_address);    \
                taint_label |= emu_get_taint_mem(emu, base_address+4);  \
                                                                        \
                emu->taintextreg[starting_reg] = taint_label;           \
                starting_reg += stride;                                 \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VLD3_M: {                                              \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size = READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            /* TODO: What about the taint label of register Rn? */      \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint8_t reg_1 = starting_reg;                               \
            uint8_t reg_2 = reg_1 + stride;                             \
            uint8_t reg_3 = reg_2 + stride;                             \
                                                                        \
            if (elem_size == 0b00) {                                    \
                /* Element size is 1 byte */                            \
                taint_label |= emu_get_taint_mem(emu, base_address);    \
                taint_label |= emu_get_taint_mem(emu, base_address+4);  \
                taint_label |= emu_get_taint_mem(emu, base_address+8);  \
                taint_label |= emu_get_taint_mem(emu, base_address+12); \
                taint_label |= emu_get_taint_mem(emu, base_address+16); \
                taint_label |= emu_get_taint_mem(emu, base_address+20); \
                                                                        \
                emu->taintextreg[reg_1] = taint_label;                  \
                emu->taintextreg[reg_2] = taint_label;                  \
                emu->taintextreg[reg_3] = taint_label;                  \
                                                                        \
            } else if (elem_size == 0b01) {                             \
                /* Element size is 2 bytes */                           \
                uint32_t word_lbl_1 = emu_get_taint_mem(emu, base_address); \
                uint32_t word_lbl_2 = emu_get_taint_mem(emu, base_address+4); \
                uint32_t word_lbl_3 = emu_get_taint_mem(emu, base_address+8); \
                uint32_t word_lbl_4 = emu_get_taint_mem(emu, base_address+12); \
                uint32_t word_lbl_5 = emu_get_taint_mem(emu, base_address+16); \
                uint32_t word_lbl_6 = emu_get_taint_mem(emu, base_address+20); \
                                                                        \
                taint_label = word_lbl_1 | word_lbl_2 | word_lbl_4 | word_lbl_5; \
                emu->taintextreg[reg_1] = taint_label;                  \
                                                                        \
                taint_label = word_lbl_1 | word_lbl_3 | word_lbl_4 | word_lbl_6; \
                emu->taintextreg[reg_2] = taint_label;                  \
                                                                        \
                taint_label = word_lbl_2 | word_lbl_3 | word_lbl_5 | word_lbl_6; \
                emu->taintextreg[reg_3] = taint_label;                  \
                                                                        \
            } else if (elem_size == 0b10) {                             \
                /* Element size is 4 bytes */                           \
                taint_label |= emu_get_taint_mem(emu, base_address);    \
                taint_label |= emu_get_taint_mem(emu, base_address+12); \
                emu->taintextreg[reg_1] = taint_label;                  \
                                                                        \
                taint_label = 0;                                        \
                taint_label |= emu_get_taint_mem(emu, base_address+4);  \
                taint_label |= emu_get_taint_mem(emu, base_address+16); \
                emu->taintextreg[reg_2] = taint_label;                  \
                                                                        \
                taint_label = 0;                                        \
                taint_label |= emu_get_taint_mem(emu, base_address+8);  \
                taint_label |= emu_get_taint_mem(emu, base_address+20); \
                emu->taintextreg[reg_3] = taint_label;                  \
                                                                        \
            } else if (elem_size == 0b11) {                             \
                /* Element size is 8 bytes */                           \
                /* This is invalid in VLD3 (multiple) */                \
                emu_abort("ELEM SIZE IS 0b11 in VLD3 (M)");             \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VLD4_M: {                                              \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size = READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            /* TODO: What about the taint label of register Rn? */      \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint8_t reg_1 = starting_reg;                               \
            uint8_t reg_2 = reg_1 + stride;                             \
            uint8_t reg_3 = reg_2 + stride;                             \
            uint8_t reg_4 = reg_3 + stride;                             \
                                                                        \
            if (elem_size == 0b00) {                                    \
                /* Element size is 1 byte */                            \
                taint_label |= emu_get_taint_mem(emu, base_address);    \
                taint_label |= emu_get_taint_mem(emu, base_address+4);  \
                taint_label |= emu_get_taint_mem(emu, base_address+8);  \
                taint_label |= emu_get_taint_mem(emu, base_address+12); \
                taint_label |= emu_get_taint_mem(emu, base_address+16); \
                taint_label |= emu_get_taint_mem(emu, base_address+20); \
                taint_label |= emu_get_taint_mem(emu, base_address+24); \
                taint_label |= emu_get_taint_mem(emu, base_address+28); \
                                                                        \
                emu->taintextreg[reg_1] = taint_label;                  \
                emu->taintextreg[reg_2] = taint_label;                  \
                emu->taintextreg[reg_3] = taint_label;                  \
                emu->taintextreg[reg_4] = taint_label;                  \
                                                                        \
            } else if (elem_size == 0b01) {                             \
                /* Element size is 2 bytes */                           \
                taint_label |= emu_get_taint_mem(emu, base_address);    \
                taint_label |= emu_get_taint_mem(emu, base_address+8);  \
                taint_label |= emu_get_taint_mem(emu, base_address+16); \
                taint_label |= emu_get_taint_mem(emu, base_address+24); \
                emu->taintextreg[reg_1] = taint_label;                  \
                emu->taintextreg[reg_2] = taint_label;                  \
                                                                        \
                taint_label = 0;                                        \
                taint_label |= emu_get_taint_mem(emu, base_address+4);  \
                taint_label |= emu_get_taint_mem(emu, base_address+12); \
                taint_label |= emu_get_taint_mem(emu, base_address+20); \
                taint_label |= emu_get_taint_mem(emu, base_address+28); \
                emu->taintextreg[reg_3] = taint_label;                  \
                emu->taintextreg[reg_4] = taint_label;                  \
                                                                        \
            } else if (elem_size == 0b10) {                             \
                /* Element size is 4 bytes */                           \
                taint_label |= emu_get_taint_mem(emu, base_address);    \
                taint_label |= emu_get_taint_mem(emu, base_address+16); \
                emu->taintextreg[reg_1] = taint_label;                  \
                                                                        \
                taint_label = 0;                                        \
                taint_label |= emu_get_taint_mem(emu, base_address+4);  \
                taint_label |= emu_get_taint_mem(emu, base_address+20); \
                emu->taintextreg[reg_2] = taint_label;                  \
                                                                        \
                taint_label = 0;                                        \
                taint_label |= emu_get_taint_mem(emu, base_address+8);  \
                taint_label |= emu_get_taint_mem(emu, base_address+24); \
                emu->taintextreg[reg_3] = taint_label;                  \
                                                                        \
                taint_label = 0;                                        \
                taint_label |= emu_get_taint_mem(emu, base_address+12); \
                taint_label |= emu_get_taint_mem(emu, base_address+28); \
                emu->taintextreg[reg_4] = taint_label;                  \
                                                                        \
            } else if (elem_size == 0b11) {                             \
                /* Element size is 8 bytes */                           \
                /* This is invalid in VLD4 (multiple */                 \
                emu_abort("ELEM SIZE IS 0b11");                         \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VLD1_SO: {                                             \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            taint_label |= emu->taintextreg[starting_reg];              \
            taint_label |= emu_get_taint_mem(emu, base_address);        \
            emu->taintextreg[starting_reg] = taint_label;               \
                                                                        \
            continue;                                                   \
                                                                        \
        }                                                               \
                                                                        \
        case MEM_VLD3_SO: {                                             \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t elem_size =  READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
            uint8_t stride =  READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint8_t reg_1 = starting_reg;                               \
            uint8_t reg_2 = reg_1 + stride;                             \
            uint8_t reg_3 = reg_2 + stride;                             \
                                                                        \
            uint32_t tag_1 = emu->taintextreg[reg_1];                   \
            uint32_t tag_2 = emu->taintextreg[reg_2];                   \
            uint32_t tag_3 = emu->taintextreg[reg_3];                   \
                                                                        \
            uint32_t mem_tag = 0;                                       \
            if (elem_size == 0b00) {                                    \
                /* 8 bit element */                                     \
                mem_tag = emu_get_taint_mem(emu, base_address);         \
                tag_1 |= mem_tag;                                       \
                tag_2 |= mem_tag;                                       \
                tag_3 |= mem_tag;                                       \
                                                                        \
            } else if (elem_size == 0b01) {                             \
                /* 16 bit element */                                    \
                mem_tag = emu_get_taint_mem(emu, base_address);         \
                tag_1 |= mem_tag;                                       \
                tag_2 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+4);       \
                tag_1 |= mem_tag;                                       \
                tag_3 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+8);       \
                tag_2 |= mem_tag;                                       \
                tag_3 |= mem_tag;                                       \
                                                                        \
            } else if (elem_size == 0b10) {                             \
                /* 32 bit element */                                    \
                mem_tag = emu_get_taint_mem(emu, base_address);         \
                tag_1 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+4);       \
                tag_2 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+8);       \
                tag_3 |= mem_tag;                                       \
                                                                        \
            }                                                           \
                                                                        \
            emu->taintextreg[reg_1] = tag_1;                            \
            emu->taintextreg[reg_2] = tag_2;                            \
            emu->taintextreg[reg_3] = tag_3;                            \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VLD4_SO: {                                             \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t elem_size =  READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
            uint8_t stride =  READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint8_t reg_1 = starting_reg;                               \
            uint8_t reg_2 = reg_1 + stride;                             \
            uint8_t reg_3 = reg_2 + stride;                             \
            uint8_t reg_4 = reg_3 + stride;                             \
                                                                        \
            uint32_t tag_1 = emu->taintextreg[reg_1];                   \
            uint32_t tag_2 = emu->taintextreg[reg_2];                   \
            uint32_t tag_3 = emu->taintextreg[reg_3];                   \
            uint32_t tag_4 = emu->taintextreg[reg_4];                   \
                                                                        \
            uint32_t mem_tag = 0;                                       \
            if (elem_size == 0b00) {                                    \
                /* 8 bit element */                                     \
                mem_tag = emu_get_taint_mem(emu, base_address);         \
                tag_1 |= mem_tag;                                       \
                tag_2 |= mem_tag;                                       \
                tag_3 |= mem_tag;                                       \
                tag_4 |= mem_tag;                                       \
            } else if (elem_size == 0b01) {                             \
                /* 16 bit element */                                    \
                mem_tag = emu_get_taint_mem(emu, base_address);         \
                tag_1 |= mem_tag;                                       \
                tag_2 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+4);       \
                tag_3 |= mem_tag;                                       \
                tag_4 |= mem_tag;                                       \
            } else if (elem_size == 0b10) {                             \
                /* 32 bit element */                                    \
                mem_tag = emu_get_taint_mem(emu, base_address);         \
                tag_1 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+4);       \
                tag_2 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+8);       \
                tag_3 |= mem_tag;                                       \
                                                                        \
                mem_tag = emu_get_taint_mem(emu, base_address+16);      \
                tag_4 |= mem_tag;                                       \
            }                                                           \
                                                                        \
            emu->taintextreg[reg_1] = tag_1;                            \
            emu->taintextreg[reg_2] = tag_2;                            \
            emu->taintextreg[reg_3] = tag_3;                            \
            emu->taintextreg[reg_4] = tag_4;                            \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VST1_M: {                                              \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs =  READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride =  READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size =  READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
            bool word_aligned = false;                                  \
            uint8_t ext_reg = starting_reg;                             \
                                                                        \
            word_aligned = ((base_address & 0x3) == 0);                 \
            if (num_regs >= 1) {                                        \
                if (word_aligned == true) {                             \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                    emu_set_taint_mem(emu, base_address + 4, taint_label); \
                } else {                                                \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address); \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address+4, taint_label); \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address + 8); \
                    emu_set_taint_mem(emu, base_address + 8, taint_label); \
                }                                                       \
            }                                                           \
                                                                        \
            if (num_regs >= 2) {                                        \
                base_address = rn_val + EXT_REG_SIZE_BYTES;             \
                word_aligned = ((base_address & 0x3) == 0);             \
                ext_reg += stride;                                      \
                                                                        \
                if (word_aligned == true) {                             \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                    emu_set_taint_mem(emu, base_address + 4, taint_label); \
                } else {                                                \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address); \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address + 4, taint_label); \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address + 8); \
                    emu_set_taint_mem(emu, base_address + 8, taint_label); \
                }                                                       \
            }                                                           \
                                                                        \
            if (num_regs >= 3) {                                        \
                base_address = rn_val + (EXT_REG_SIZE_BYTES * 2);       \
                word_aligned = ((base_address & 0x3) == 0);             \
                ext_reg += stride;                                      \
                                                                        \
                if (word_aligned == true) {                             \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                    emu_set_taint_mem(emu, base_address + 4, taint_label); \
                } else {                                                \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address); \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address + 4, taint_label); \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address + 8); \
                    emu_set_taint_mem(emu, base_address + 8, taint_label); \
                }                                                       \
            }                                                           \
                                                                        \
            if (num_regs >= 4) {                                        \
                base_address = rn_val + (EXT_REG_SIZE_BYTES * 3);       \
                word_aligned = ((base_address & 0x3) == 0);             \
                ext_reg += stride;                                      \
                                                                        \
                if (word_aligned == true) {                             \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                    emu_set_taint_mem(emu, base_address + 4, taint_label); \
                } else {                                                \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address); \
                    emu_set_taint_mem(emu, base_address, taint_label);  \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    emu_set_taint_mem(emu, base_address + 4, taint_label); \
                                                                        \
                    taint_label = emu->taintextreg[ext_reg];            \
                    taint_label |= emu_get_taint_mem(emu, base_address + 8); \
                    emu_set_taint_mem(emu, base_address + 8, taint_label); \
                }                                                       \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VST3_M: {                                              \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg =  READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs =  READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size =  READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint8_t reg_1 = starting_reg;                               \
            uint8_t reg_2 = reg_1 + stride;                             \
            uint8_t reg_3 = reg_2 + stride;                             \
                                                                        \
            if (elem_size == 0b00) {                                    \
                /* Element size is 1 byte */                            \
                taint_label = emu->taintextreg[reg_1];                  \
                taint_label |= emu->taintextreg[reg_2];                 \
                taint_label |= emu->taintextreg[reg_3];                 \
                                                                        \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                emu_set_taint_mem(emu, base_address+4, taint_label);    \
                emu_set_taint_mem(emu, base_address+8, taint_label);    \
                emu_set_taint_mem(emu, base_address+12, taint_label);   \
                emu_set_taint_mem(emu, base_address+16, taint_label);   \
                emu_set_taint_mem(emu, base_address+20, taint_label);   \
                                                                        \
            } else if (elem_size == 0b01) {                             \
                /* Element size is 2 bytes */                           \
                uint32_t reg_lbl_1 = emu->taintextreg[reg_1];           \
                uint32_t reg_lbl_2 = emu->taintextreg[reg_2];           \
                uint32_t reg_lbl_3 = emu->taintextreg[reg_3];           \
                                                                        \
                taint_label = reg_lbl_1 | reg_lbl_2;                    \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                                                                        \
                taint_label = reg_lbl_3 | reg_lbl_1;                    \
                emu_set_taint_mem(emu, base_address+4, taint_label);    \
                                                                        \
                taint_label = reg_lbl_2 | reg_lbl_3;                    \
                emu_set_taint_mem(emu, base_address+8, taint_label);    \
                                                                        \
                taint_label = reg_lbl_1 | reg_lbl_2;                    \
                emu_set_taint_mem(emu, base_address+12, taint_label);   \
                                                                        \
                taint_label = reg_lbl_3 | reg_lbl_1;                    \
                emu_set_taint_mem(emu, base_address+16, taint_label);   \
                                                                        \
                taint_label = reg_lbl_2 | reg_lbl_3;                    \
                emu_set_taint_mem(emu, base_address+20, taint_label);   \
                                                                        \
            } else if (elem_size == 0b10) {                             \
                /* Element size is 4 bytes */                           \
                taint_label = emu->taintextreg[reg_1];                  \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                emu_set_taint_mem(emu, base_address+12, taint_label);   \
                                                                        \
                taint_label = emu->taintextreg[reg_2];                  \
                emu_set_taint_mem(emu, base_address+4, taint_label);    \
                emu_set_taint_mem(emu, base_address+16, taint_label);   \
                                                                        \
                taint_label = emu->taintextreg[reg_3];                  \
                emu_set_taint_mem(emu, base_address+8, taint_label);    \
                emu_set_taint_mem(emu, base_address+20, taint_label);   \
                                                                        \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
                                                                        \
        case MEM_VST4_M: {                                              \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg =  READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs =  READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size =  READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint8_t reg_1 = starting_reg;                               \
            uint8_t reg_2 = reg_1 + stride;                             \
            uint8_t reg_3 = reg_2 + stride;                             \
            uint8_t reg_4 = reg_3 + stride;                             \
                                                                        \
                                                                        \
            if (elem_size == 0b00) {                                    \
                /* Element size is 1 byte */                            \
                taint_label = emu->taintextreg[reg_1];                  \
                taint_label |= emu->taintextreg[reg_2];                 \
                taint_label |= emu->taintextreg[reg_3];                 \
                taint_label |= emu->taintextreg[reg_4];                 \
                                                                        \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                emu_set_taint_mem(emu, base_address + 4, taint_label);  \
                emu_set_taint_mem(emu, base_address + 8, taint_label);  \
                emu_set_taint_mem(emu, base_address + 12, taint_label); \
                emu_set_taint_mem(emu, base_address + 16, taint_label); \
                emu_set_taint_mem(emu, base_address + 20, taint_label); \
                emu_set_taint_mem(emu, base_address + 24, taint_label); \
                emu_set_taint_mem(emu, base_address + 28, taint_label); \
                                                                        \
            } else if (elem_size == 0b01) {                             \
                /* Element size is 2 bytes */                           \
                taint_label = emu->taintextreg[reg_1];                  \
                taint_label |= emu->taintextreg[reg_2];                 \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                emu_set_taint_mem(emu, base_address + 8, taint_label);  \
                emu_set_taint_mem(emu, base_address + 16, taint_label); \
                emu_set_taint_mem(emu, base_address + 24, taint_label); \
                                                                        \
                taint_label = emu->taintextreg[reg_3];                  \
                taint_label |= emu->taintextreg[reg_4];                 \
                emu_set_taint_mem(emu, base_address + 4, taint_label);  \
                emu_set_taint_mem(emu, base_address + 12, taint_label); \
                emu_set_taint_mem(emu, base_address + 20, taint_label); \
                emu_set_taint_mem(emu, base_address + 28, taint_label); \
                                                                        \
            } else if (elem_size == 0b10) {                             \
                /* Element size is 4 bytes */                           \
                taint_label = emu->taintextreg[reg_1];                  \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                emu_set_taint_mem(emu, base_address + 16, taint_label); \
                                                                        \
                taint_label = emu->taintextreg[reg_2];                  \
                emu_set_taint_mem(emu, base_address + 4, taint_label);  \
                emu_set_taint_mem(emu, base_address + 20, taint_label); \
                                                                        \
                taint_label = emu->taintextreg[reg_3];                  \
                emu_set_taint_mem(emu, base_address + 8, taint_label);  \
                emu_set_taint_mem(emu, base_address + 24, taint_label); \
                                                                        \
                taint_label = emu->taintextreg[reg_4];                  \
                emu_set_taint_mem(emu, base_address + 12, taint_label); \
                emu_set_taint_mem(emu, base_address + 28, taint_label); \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VST1_SO: {                                             \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size = READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            uint32_t taint_label = emu->taintextreg[starting_reg];      \
            uint32_t base_address = rn_val;                             \
                                                                        \
            if (elem_size != 0b10) {                                    \
                taint_label |= emu_get_taint_mem(emu, base_address);    \
            }                                                           \
            emu_set_taint_mem(emu, base_address, taint_label);          \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VST3_SO: {                                             \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size = READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint32_t reg_lbl_1 = emu->taintextreg[starting_reg];        \
            uint32_t reg_lbl_2 = emu->taintextreg[starting_reg + stride]; \
            uint32_t reg_lbl_3 = emu->taintextreg[starting_reg + stride + stride]; \
                                                                        \
            if (elem_size == 0b00) {                                    \
                /* Element size is 1 byte */                            \
                taint_label = emu_get_taint_mem(emu, base_address);     \
                taint_label |= (reg_lbl_1 | reg_lbl_2 | reg_lbl_3);     \
                emu_set_taint_mem(emu, base_address, taint_label);      \
            } else if (elem_size == 0b01) {                             \
                /* Element size is 2 bytes */                           \
                taint_label = (reg_lbl_1 | reg_lbl_2);                  \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                                                                        \
                taint_label = emu_get_taint_mem(emu, base_address+4);   \
                taint_label |= reg_lbl_3;                               \
                emu_set_taint_mem(emu, base_address+4, taint_label);    \
            } else if (elem_size == 0b10) {                             \
                /* Element size is 4 bytes */                           \
                emu_set_taint_mem(emu, base_address, reg_lbl_1);        \
                emu_set_taint_mem(emu, base_address+4, reg_lbl_2);      \
                emu_set_taint_mem(emu, base_address+8, reg_lbl_3);      \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VST4_SO: {                                             \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint8_t stride = READ_EXT_CUST_REGLIST_STRIDE(cust_reglist); \
            uint8_t elem_size = READ_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist); \
                                                                        \
            uint32_t taint_label = 0;                                   \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint32_t reg_lbl_1 = emu->taintextreg[starting_reg];        \
            uint32_t reg_lbl_2 = emu->taintextreg[starting_reg + stride]; \
            uint32_t reg_lbl_3 = emu->taintextreg[starting_reg + stride + stride]; \
            uint32_t reg_lbl_4 = emu->taintextreg[starting_reg + stride + stride + stride ]; \
                                                                        \
            if (elem_size == 0b00) {                                    \
                /* Element size is 1 byte */                            \
                taint_label = (reg_lbl_1 | reg_lbl_2 | reg_lbl_3 | reg_lbl_4); \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                                                                        \
            } else if (elem_size == 0b01) {                             \
                /* Element size is 2 bytes */                           \
                taint_label = (reg_lbl_1 | reg_lbl_2);                  \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                                                                        \
                taint_label = (reg_lbl_3 | reg_lbl_4);                  \
                emu_set_taint_mem(emu, base_address+4, taint_label);    \
                                                                        \
            } else if (elem_size == 0b10) {                             \
                /* Element size is 4 bytes */                           \
                emu_set_taint_mem(emu, base_address, reg_lbl_1);        \
                emu_set_taint_mem(emu, base_address+4, reg_lbl_2);      \
                emu_set_taint_mem(emu, base_address+8, reg_lbl_3);      \
                emu_set_taint_mem(emu, base_address+12, reg_lbl_4);     \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VPUSH_DP: {                                            \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            footer_imm = READ_FOOTER_IMM_VALUE(footer_word);            \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint32_t base_address = rn_val - footer_imm;                \
                                                                        \
            uint8_t ext_reg = starting_reg;                             \
            uint32_t taint_label = 0;                                   \
                                                                        \
            int i = 0;                                                  \
            for (i = 0; i < num_regs; i++) {                            \
                taint_label = emu->taintextreg[ext_reg];                \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                emu_set_taint_mem(emu, base_address + 4, taint_label);  \
                                                                        \
                base_address += 8;                                      \
                ext_reg++;                                              \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VPOP_DP: {                                             \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs = READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint32_t base_address = rn_val;                             \
                                                                        \
            uint8_t ext_reg = starting_reg;                             \
            uint32_t taint_label = 0;                                   \
                                                                        \
            int i = 0;                                                  \
            for (i = 0; i < num_regs; i++) {                            \
                taint_label = emu_get_taint_mem(emu, base_address);     \
                taint_label |= emu_get_taint_mem(emu, base_address + 4); \
                emu->taintextreg[ext_reg] = taint_label;                \
                                                                        \
                base_address += 8;                                      \
                ext_reg++;                                              \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VLDM_DP: {                                             \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            is_up = READ_FOOTER_UPDOWN(footer_word);                    \
            footer_imm = READ_FOOTER_IMM_VALUE(footer_word);            \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg =  READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs =  READ_EXT_CUST_REGLIST_NUM_REGS(cust_reglist); \
            uint32_t base_address = 0;                                  \
                                                                        \
            if (is_up) {                                                \
                base_address = rn_val;                                  \
            } else {                                                    \
                base_address = rn_val - footer_imm;                     \
            }                                                           \
                                                                        \
            uint8_t ext_reg = starting_reg;                             \
            uint32_t taint_label = 0;                                   \
                                                                        \
            int i = 0;                                                  \
            for (i = 0; i < num_regs; i++) {                            \
                taint_label = emu_get_taint_mem(emu, base_address);     \
                taint_label |= emu_get_taint_mem(emu, base_address + 4); \
                                                                        \
                emu->taintextreg[ext_reg] = taint_label;                \
                                                                        \
                base_address += 8;                                      \
                ext_reg++;                                              \
            }                                                           \
                                                                        \
            continue;                                                   \
        }                                                               \
                                                                        \
        case MEM_VSTM_DP:                                               \
        case MEM_VSTM_SP:{                                              \
                                                                        \
            READ_AND_INCREMENT(taint_info_addr, rn_val);                \
            READ_AND_INCREMENT(taint_info_addr, footer_word);           \
                                                                        \
            if (!perform_tp) {                                          \
                continue;                                               \
            }                                                           \
                                                                        \
            is_up = READ_FOOTER_UPDOWN(footer_word);                    \
            footer_imm = READ_FOOTER_IMM_VALUE(footer_word);            \
                                                                        \
            uint16_t cust_reglist = READ_HEADER_REGLIST(header_word);   \
            uint8_t starting_reg = READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint8_t num_regs =  READ_EXT_CUST_REGLIST_START_REG(cust_reglist); \
            uint32_t base_address = 0;                                  \
                                                                        \
            if (is_up) {                                                \
                base_address = rn_val;                                  \
            } else {                                                    \
                base_address = rn_val - footer_imm;                     \
            }                                                           \
                                                                        \
            uint8_t ext_reg = starting_reg;                             \
            uint32_t taint_label = 0;                                   \
                                                                        \
            int i = 0;                                                  \
            for (i = 0; i < num_regs; i++) {                            \
                taint_label = emu->taintextreg[ext_reg];                \
                emu_set_taint_mem(emu, base_address, taint_label);      \
                emu_set_taint_mem(emu, base_address + 4, taint_label);  \
                                                                        \
                base_address += 8;                                      \
                ext_reg++;                                              \
            }                                                           \
            continue;                                                   \
        }                                                               \
                                                                        \
        default:                                                        \
            emu_abort("Unsupported mem type (%d) in mem-to-extreg or extreg-to-mem taint.\n", mem_type); \
            break;                                                      \
        } /* End of switch (mem_type) */                                \
    }
