# Useful adb line options:
# adb shell setprop log.redirect-stdio true
# adb shell setprop dalvik.vm.jniopts logThirdPartyJni

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE            := libanemu
LOCAL_MODULE_TAGS       := optional
LOCAL_SHARED_LIBRARIES  := libdl
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := anemu.c  emu_mem_mgmt.c emu_taint_mgmt.c emu_mambo_taint_prop.c emu_pie_decoder.c
LOCAL_SRC_FILES	        += emu_emulate.c emu_thread.c emu_profiler.c emu_debug.c emu_intercept.c emu_intercept_gl.c intercept_trampolines.S

# Atomics need ANDROID_SMP=1
LOCAL_CFLAGS            += -DANDROID_SMP=1
LOCAL_C_INCLUDES        += bionic/libc/private

# mambo integration
LOCAL_CFLAGS           += -DDBM_LINK_UNCOND_IMM  -DDBM_INLINE_UNCOND_IMM -DDBM_LINK_COND_IMM -DPLUGINS_NEW
LOCAL_CFLAGS           += -DLINK_BX_ALT -DDBM_LINK_CBZ -DDBM_INLINE_HASH -DDBM_D_INLINE_HASH -DDBM_TB_DIRECT -DDBM_TRACES
LOCAL_CFLAGS           += -std=gnu99

LOCAL_SRC_FILES        += mambo/_dispatcher.S mambo/scanner_thumb.c mambo/scanner_arm.c mambo/common.c mambo/dbm.c mambo/traces.c mambo/syscalls.c mambo/dispatcher.c
LOCAL_SRC_FILES        += mambo/api/emit_arm.c mambo/api/emit_thumb.c mambo/api/helpers.c mambo/api/plugin_support.c mambo/util.S

LOCAL_C_INCLUDES       += bionic/

LOCAL_WHOLE_STATIC_LIBRARIES += pie_arm pie_thumb
# end of mambo integration

LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9 -mfpu=neon
LOCAL_CFLAGS            += -O3

LOCAL_CFLAGS            += -g3
LOCAL_CFLAGS            += -fno-omit-frame-pointer
LOCAL_CFLAGS            += -nodefaultlibs -nostdlib
LOCAL_CFLAGS            += -fno-strict-aliasing
LOCAL_LDFLAGS           := -Wl,--exclude-libs=libgcc.a

# explicitly out implicit libs like libdl and libc
LOCAL_SYSTEM_SHARED_LIBRARIES :=
LOCAL_STATIC_LIBRARIES += libc_nomalloc

# Flag to control whether benchmarking (profiling) of TaintTrap should
# be done.
#LOCAL_CFLAGS		+= -DEMU_BENCH
#LOCAL_CFLAGS		+= -UEMU_BENCH

# Flag to control thread isolation. It should be set to false by
# default so that tracked and untracked threads run in parallel. If
# set to true, ensure that the kernel compiled with the
# CONFIG_SANDTRAP_SCHEDULER_ISOLATION flag enabled has been flashed to
# the device.
# LOCAL_CFLAGS            += -DENABLE_THREAD_ISOLATION

# Create a non-debug build by definining the NO_DEBUG flag and create a
# debug build by commenting out the flag.
LOCAL_CFLAGS            += -DNO_DEBUG

# Have a separate flag to control whether asserts are on because
# sometimes, we want to run things in non-debug mode, for performance
# reasons, but still catch errors due to assert failures
LOCAL_CLFAGS		+= -DASSERTS_ON

# These flags are designed to used in conjunction with the taint prop
# tester utility. Be sure to comment out both if you want to use the
# taint prop tester.
# LOCAL_CFLAGS += -DTEST_TOOLS
# TEST_TOOLS = true

ifeq ($(ARCH_ARM_HAVE_VFP),true)
LOCAL_CFLAGS += -DWITH_VFP
endif
ifeq ($(ARCH_ARM_HAVE_VFP_D32),true)
LOCAL_CFLAGS += -DWITH_VFP_D32
endif

# include $(BUILD_SHARED_LIBRARY)
include $(BUILD_STATIC_LIBRARY)

ifeq ($(TEST_TOOLS),true)

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-test-disassem
LOCAL_MODULE_TAGS       := optional
LOCAL_REQUIRED_MODULES  := libanemu #libdarm
LOCAL_SHARED_LIBRARIES  := libc libdl
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/test-disassem.c
LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9
LOCAL_C_INCLUDES        += bionic/

# Capstone
LOCAL_STATIC_LIBRARIES  := capst
LOCAL_WHOLE_STATIC_LIBRARIES += capst
LOCAL_REQUIRED_MODULES += capst
LOCAL_C_INCLUDES += bionic/capstone/include
# /Capstone

## NOTE: If the NO_DEBUG flag is defined above, it should be defined
## here too. Otherwise, it should be undefined. It needs to match with
## the above so that struct definitions are consistent across the
## tester and libanemu. Even though the NO_DEBUG is enabled by default,
## it is strongly recommended that it is disabled while this tool is
## actively being used. That will ensure that the log output of the
## tool becomes very useful.
LOCAL_CFLAGS            += -DNO_DEBUG -DTEST_TOOLS
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-test-disassem-thumb
LOCAL_MODULE_TAGS       := optional
LOCAL_REQUIRED_MODULES  := libanemu #libdarm
LOCAL_SHARED_LIBRARIES  := libc libdl
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/test-disassem.c
LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9 -mthumb
LOCAL_C_INCLUDES        += bionic/

# Capstone
LOCAL_STATIC_LIBRARIES := capst
LOCAL_WHOLE_STATIC_LIBRARIES += capst
LOCAL_REQUIRED_MODULES += capst
LOCAL_C_INCLUDES += bionic/capstone/include
# /Capstone

## NOTE: If the NO_DEBUG flag is defined above, it should be defined
## here too. Otherwise, it should be undefined. It needs to match with
## the above so that struct definitions are consistent across the
## tester and libanemu. Even though the NO_DEBUG is enabled by default,
## it is strongly recommended that it is disabled while this tool is
## actively being used. That will ensure that the log output of the
## tool becomes very useful.
LOCAL_CFLAGS            += -DNO_DEBUG -DTEST_TOOLS
LOCAL_CFLAGS            += -DTEST_THUMB
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-test-taintprop
LOCAL_MODULE_TAGS       := optional
LOCAL_REQUIRED_MODULES  := libanemu #libdarm
LOCAL_SHARED_LIBRARIES  := libc libdl
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/test-taintprop.c
LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9
LOCAL_C_INCLUDES        += bionic/

## NOTE: If the NO_DEBUG flag is defined above, it should be defined
## here too. Otherwise, it should be undefined. It needs to match with
## the above so that struct definitions are consistent across the
## tester and libanemu. Even though the NO_DEBUG is enabled by default,
## it is strongly recommended that it is disabled while this tool is
## actively being used. That will ensure that the log output of the
## tool becomes very useful.
LOCAL_CFLAGS            += -DNO_DEBUG -DTEST_TOOLS
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)

endif ## end of "ifeq ($(TEST_TOOLS),true)"

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-matrix
LOCAL_MODULE_TAGS       := optional
LOCAL_REQUIRED_MODULES  := libanemu #libdarm
LOCAL_SHARED_LIBRARIES  := libc libdl
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/matrix.c
LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9 -mfloat-abi=soft
LOCAL_CFLAGS            += -O3
# LOCAL_CFLAGS            += -O0
# DEBUG: keep macros + debug symbols
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-matrix-thumb
LOCAL_MODULE_TAGS       := optional
LOCAL_REQUIRED_MODULES  := libanemu #libdarm
LOCAL_SHARED_LIBRARIES  := libc libdl
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/matrix-thumb.c
LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9 -mfloat-abi=soft -mthumb
LOCAL_CFLAGS            += -O3
# LOCAL_CFLAGS            += -O0
# DEBUG: keep macros + debug symbols
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-jpeg
LOCAL_MODULE_TAGS       := optional
LOCAL_REQUIRED_MODULES  := libdarm
LOCAL_SHARED_LIBRARIES  := libexif libdl libjpeg
LOCAL_C_INCLUDES        := external/jhead
LOCAL_C_INCLUDES        += external/jpeg
# LOCAL_WHOLE_STATIC_LIBRARIES  += libanemu
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/jpeg.c
LOCAL_CFLAGS            += -O0 -Wall -march=armv7-a -mcpu=cortex-a9 -mfloat-abi=soft
# DEBUG: keep macros + debug symbols
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-jpeg-thumb
LOCAL_MODULE_TAGS       := optional
LOCAL_REQUIRED_MODULES  := libdarm
LOCAL_SHARED_LIBRARIES  := libexif libdl libjpeg
LOCAL_C_INCLUDES        := external/jhead
LOCAL_C_INCLUDES        += external/jpeg
# LOCAL_WHOLE_STATIC_LIBRARIES  += libanemu
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/jpeg-thumb.c
LOCAL_CFLAGS            += -O0 -Wall -march=armv7-a -mcpu=cortex-a9 -mfloat-abi=soft -mthumb -mthumb-interwork
# DEBUG: keep macros + debug symbols
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE            := emu-pass
LOCAL_MODULE_TAGS       := optional
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_SHARED_LIBRARIES  += libdl
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/pass.c
LOCAL_CFLAGS            += -O3 -Wall -march=armv7-a -mcpu=cortex-a9
LOCAL_CFLAGS            += -DANDROID_SMP=1
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_MODULE            := emu-pass-thumb
LOCAL_MODULE_TAGS       := optional
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_SHARED_LIBRARIES  += libdl
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/pass-thumb.c
LOCAL_CFLAGS            += -O3 -Wall -march=armv7-a -mcpu=cortex-a9 -mthumb
LOCAL_CFLAGS            += -DANDROID_SMP=1
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_MODULE            := emu-print
LOCAL_MODULE_TAGS       := optional
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_SHARED_LIBRARIES  += libdl
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/print.c
LOCAL_CFLAGS            += -O3 -Wall -march=armv7-a -mcpu=cortex-a9
LOCAL_CFLAGS            += -DANDROID_SMP=1
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)



include $(CLEAR_VARS)
LOCAL_MODULE            := emu-print-thumb
LOCAL_MODULE_TAGS       := optional
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_SHARED_LIBRARIES  += libdl
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/print-thumb.c
LOCAL_CFLAGS            += -O3 -Wall -march=armv7-a -mcpu=cortex-a9 -mthumb
LOCAL_CFLAGS            += -DANDROID_SMP=1
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)



include $(CLEAR_VARS)
LOCAL_MODULE            := emu-domain
LOCAL_MODULE_TAGS       := optional
LOCAL_STATIC_LIBRARIES  += libanemu
LOCAL_SHARED_LIBRARIES  += libdl
LOCAL_ARM_MODE          := arm
LOCAL_SRC_FILES         := tests/domain.c
LOCAL_CFLAGS            += -O3 -Wall -march=armv7-a -mcpu=cortex-a9
LOCAL_CFLAGS            += -DANDROID_SMP=1
LOCAL_CFLAGS            += -g3
include $(BUILD_EXECUTABLE)
