#include <unistd.h>
#include <fcntl.h>
#include <dlfcn.h>              /* dladdr */
#include "emu_debug.h"
#include "emu_thread.h"

// wait for gdb to attach to process
// when attached, to continue: set var c = 1
// ignored if called again when already attached
void
gdb_wait() {
    static int attached = 0;
    if (attached) {
        emu_log_debug("already attached!\n");
        return;
    }
    emu_log_always(LOG_BANNER_SIG);
    emu_log_always("waiting for gdb to attach pid: %d tid: %d\n", getpid(), gettid());
    volatile int c = 0;
    while(!attached && c == 0) {
        *(int volatile *)&c;
    }
    attached = 1;
}

void
dbg_print_method_info(uint32_t pc)
{
    Dl_info info;
    ptrdiff_t function_offset = 0;
    uint32_t instruction_offset = 0;

    if (dladdr((void *)pc, &info)) {

        instruction_offset = pc - (uint32_t)info.dli_fbase;
        if (info.dli_saddr == NULL) {
            emu_log_always("At PC 0x%08x: symbol not found, library: %s"
                          " instruction offset: 0x%08x, value at PC: 0x%08x\n",
                          pc, info.dli_fname,
                          instruction_offset, *(uint32_t*)pc);
        } else {

            function_offset = (uintptr_t)info.dli_saddr - (uintptr_t)info.dli_fbase;
            emu_log_always("At PC 0x%08x: symbol: %s, library: %s"
                          " function offset: 0x%08x, instruction offset: 0x%08x"
                          " value at PC: 0x%08x\n",
                          pc, info.dli_sname, info.dli_fname,
                          function_offset, instruction_offset,
                          *(uint32_t*)pc);
        }
    } else {
        emu_log_always("At PC 0x%08x: symbol and library not found, value at PC: 0x%08x\n",
                      pc, *(uint32_t*)pc);
    }
}

void dbg_print_dumb_backtrace(uint32_t *sp, int crawl_amount) {
    int i = 0;

    uint32_t val = 0;
    for (i=0; i<crawl_amount; i++) {
        val = *sp;
        dbg_print_method_info(val);
        sp++;
    }
}


void
dbg_print_stack(uint32_t *sp, int crawl_amount)
{
    int i = 0;
    uint32_t *addr = 0;
    for (i = -crawl_amount; i < crawl_amount; i++) {
        addr = sp + i;
        emu_log_always("SP:0x%08x, val:0x%08x\n", (uint32_t)addr, *addr);
    }
}
