#ifndef EMU_COMMON_H
#define EMU_COMMON_H

typedef enum {
    EMU_ARM_REG_R0 = 0,
    EMU_ARM_REG_R1 = 1,
    EMU_ARM_REG_R2 = 2,
    EMU_ARM_REG_R3 = 3,
    EMU_ARM_REG_R4 = 4,
    EMU_ARM_REG_R5 = 5,
    EMU_ARM_REG_R6 = 6,
    EMU_ARM_REG_R7 = 7,
    EMU_ARM_REG_R8 = 8,
    EMU_ARM_REG_R9 = 9,
    EMU_ARM_REG_R10 = 10,
    EMU_ARM_REG_R11 = 11,
    EMU_ARM_REG_R12 = 12,
    EMU_ARM_REG_R13 = 13,
    EMU_ARM_REG_R14 = 14,
    EMU_ARM_REG_R15 = 15,
    EMU_ARM_REG_R16 = 16,

    EMU_ARM_REG_FP = EMU_ARM_REG_R11,
    EMU_ARM_REG_IP = EMU_ARM_REG_R12,
    EMU_ARM_REG_SP = EMU_ARM_REG_R13,
    EMU_ARM_REG_LR = EMU_ARM_REG_R14,
    EMU_ARM_REG_PC = EMU_ARM_REG_R15,
    EMU_ARM_REG_CPSR = EMU_ARM_REG_R16,

    EMU_ARM_REG_D0 = 0,
    EMU_ARM_REG_D1,
    EMU_ARM_REG_D2,
    EMU_ARM_REG_D3,
    EMU_ARM_REG_D4,
    EMU_ARM_REG_D5,
    EMU_ARM_REG_D6,
    EMU_ARM_REG_D7,
    EMU_ARM_REG_D8,
    EMU_ARM_REG_D9,
    EMU_ARM_REG_D10,
    EMU_ARM_REG_D11,
    EMU_ARM_REG_D12,
    EMU_ARM_REG_D13,
    EMU_ARM_REG_D14,
    EMU_ARM_REG_D15,
    EMU_ARM_REG_D16,
    EMU_ARM_REG_D17,
    EMU_ARM_REG_D18,
    EMU_ARM_REG_D19,
    EMU_ARM_REG_D20,
    EMU_ARM_REG_D21,
    EMU_ARM_REG_D22,
    EMU_ARM_REG_D23,
    EMU_ARM_REG_D24,
    EMU_ARM_REG_D25,
    EMU_ARM_REG_D26,
    EMU_ARM_REG_D27,
    EMU_ARM_REG_D28,
    EMU_ARM_REG_D29,
    EMU_ARM_REG_D30,
    EMU_ARM_REG_D31,

    EMU_ARM_REG_S0,
    EMU_ARM_REG_S1,
    EMU_ARM_REG_S2,
    EMU_ARM_REG_S3,
    EMU_ARM_REG_S4,
    EMU_ARM_REG_S5,
    EMU_ARM_REG_S6,
    EMU_ARM_REG_S7,
    EMU_ARM_REG_S8,
    EMU_ARM_REG_S9,
    EMU_ARM_REG_S10,
    EMU_ARM_REG_S11,
    EMU_ARM_REG_S12,
    EMU_ARM_REG_S13,
    EMU_ARM_REG_S14,
    EMU_ARM_REG_S15,
    EMU_ARM_REG_S16,
    EMU_ARM_REG_S17,
    EMU_ARM_REG_S18,
    EMU_ARM_REG_S19,
    EMU_ARM_REG_S20,
    EMU_ARM_REG_S21,
    EMU_ARM_REG_S22,
    EMU_ARM_REG_S23,
    EMU_ARM_REG_S24,
    EMU_ARM_REG_S25,
    EMU_ARM_REG_S26,
    EMU_ARM_REG_S27,
    EMU_ARM_REG_S28,
    EMU_ARM_REG_S29,
    EMU_ARM_REG_S30,
    EMU_ARM_REG_S31,

    EMU_ARM_REG_Q0,
    EMU_ARM_REG_Q1,
    EMU_ARM_REG_Q2,
    EMU_ARM_REG_Q3,
    EMU_ARM_REG_Q4,
    EMU_ARM_REG_Q5,
    EMU_ARM_REG_Q6,
    EMU_ARM_REG_Q7,
    EMU_ARM_REG_Q8,
    EMU_ARM_REG_Q9,
    EMU_ARM_REG_Q10,
    EMU_ARM_REG_Q11,
    EMU_ARM_REG_Q12,
    EMU_ARM_REG_Q13,
    EMU_ARM_REG_Q14,
    EMU_ARM_REG_Q15,

} emu_arm_reg;

typedef enum emu_arm_shift_type {
    EMU_ARM_SFT_LSL = 0,
    EMU_ARM_SFT_LSR = 1,
    EMU_ARM_SFT_ASR = 2,
    EMU_ARM_SFT_ROR = 3
} emu_arm_shift_type;

typedef uint16_t taintcount_t;

typedef struct taintmap {
    uint32_t 		*data;
    uint32_t 		start;
    uint32_t 		end;
    uint32_t 		bytes;
    taintcount_t 	*taint_count; /* the number of tainted words in each page*/
    uint16_t 		page_count;
} __attribute__((aligned(4096))) taintmap_t; // 4096 is PAGE_SIZE

#define EXT_REG_SIZE_BYTES 8

#endif // EMU_COMMON_H
