#ifndef INCLUDE_OPENGL_INTERCEPT_H
#define INCLUDE_OPENGL_INTERCEPT_H

/*
 * About OpenGL, SandTrap, intercepts and wrappers:
 *
 * The relationship between SandTrap, OpenGL, intercepts and wrappers
 * are unintentionally complicated. The high-level goal is this: when
 * a third-party native function (in an Android app) calls OpenGL
 * functions, we want our OpenGL wrappers to run in place of the
 * original OpenGl functions. We need this to happen regardless of
 * whether the third-party native function is being emulated at the
 * moment or not.
 *
 * To this end, we modified libc's linker and Dalvik. When Dalvik
 * loads an app-provided native library, it will do so using
 * "emu_dlopen(...)". As the linker is resolving the jump table in the
 * library, it will call our callback, which allows us to control the
 * address placed in the jump table.
 *
 * If we detect that the linker is resolving an entry for an OpenGL
 * function in the jump table, we return the address of our
 * wrapper. Hence, when an app-provided native function calls OpenGL
 * functions, our wrappers will run regardless of whether the native
 * function is being emulated.
 *
 */

#include <GLES2/gl2.h>

// Declarations of our wrappers. The return value and parameters of
// each wrapper must match its original OpenGL function.
void glDeleteTextures_wrapper(GLsizei n, const GLuint* textures);
void glGetIntegerv_wrapper(GLenum pname, GLint* params);
void glGenTextures_wrapper(GLsizei n, GLuint* textures);
void glBindTexture_wrapper(GLenum target, GLuint texture);
void glTexParameteri_wrapper(GLenum target, GLenum pname, GLint param);
void glTexImage2D_wrapper(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* pixels);
GLenum glGetError_wrapper(void);
GLuint glCreateShader_wrapper(GLenum type);
void glShaderSource_wrapper(GLuint shader, GLsizei count, const GLchar** string, const GLint* length);
void glCompileShader_wrapper(GLuint shader);
void glGetShaderiv_wrapper(GLuint shader, GLenum pname, GLint* params);
void glGetShaderInfoLog_wrapper(GLuint shader, GLsizei bufsize, GLsizei* length, GLchar* infolog);
void glDeleteShader_wrapper(GLuint shader);
GLuint glCreateProgram_wrapper(void);
void glAttachShader_wrapper(GLuint program, GLuint shader);
void glBindAttribLocation_wrapper(GLuint program, GLuint index, const GLchar* name);
void glLinkProgram_wrapper(GLuint program);
void glGetProgramiv_wrapper(GLuint program, GLenum pname, GLint* params);
void glGetProgramInfoLog_wrapper(GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog);
void glDeleteProgram_wrapper(GLuint program);
void glReadPixels_wrapper(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid* pixels);
void glActiveTexture_wrapper(GLenum texture);
void glBindBuffer_wrapper(GLenum target, GLuint buffer);
void glBindFramebuffer_wrapper(GLenum target, GLuint framebuffer);
void glBindRenderbuffer_wrapper(GLenum target, GLuint renderbuffer);
void glBufferData_wrapper(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
void glClear_wrapper(GLbitfield mask);
void glClearColor_wrapper(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
void glDeleteBuffers_wrapper(GLsizei n, const GLuint* buffers);
void glDeleteFramebuffers_wrapper(GLsizei n, const GLuint* framebuffers);
void glDeleteRenderbuffers_wrapper(GLsizei n, const GLuint* renderbuffers);
void glDepthMask_wrapper(GLboolean flag);
void glDetachShader_wrapper(GLuint program, GLuint shader);
void glDisable_wrapper(GLenum cap);
void glDrawArrays_wrapper(GLenum mode, GLint first, GLsizei count);
void glDrawElements_wrapper(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices);
void glEnableVertexAttribArray_wrapper(GLuint index);
void glFramebufferRenderbuffer_wrapper(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
void glFramebufferTexture2D_wrapper(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
void glGenBuffers_wrapper(GLsizei n, GLuint* buffers);
void glGenFramebuffers_wrapper(GLsizei n, GLuint* framebuffers);
void glGenRenderbuffers_wrapper(GLsizei n, GLuint* renderbuffers);
void glPixelStorei_wrapper(GLenum pname, GLint param);
void glRenderbufferStorage_wrapper(GLenum target, GLenum internalformat, GLsizei width, GLsizei height);
void glUniform1f_wrapper(GLint location, GLfloat x);
void glUniform1fv_wrapper(GLint location, GLsizei count, const GLfloat* v);
void glUniform1i_wrapper(GLint location, GLint x);
void glUniform1iv_wrapper(GLint location, GLsizei count, const GLint* v);
void glUniform2f_wrapper(GLint location, GLfloat x, GLfloat y);
void glUniform2fv_wrapper(GLint location, GLsizei count, const GLfloat* v);
void glUniform3f_wrapper(GLint location, GLfloat x, GLfloat y, GLfloat z);
void glUniform3fv_wrapper(GLint location, GLsizei count, const GLfloat* v);
void glUniform4f_wrapper(GLint location, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
void glUniform4fv_wrapper(GLint location, GLsizei count, const GLfloat* v);
void glUniformMatrix2fv_wrapper(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
void glUniformMatrix3fv_wrapper(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
void glUseProgram_wrapper(GLuint program);
void glVertexAttribPointer_wrapper(GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr);
void glViewport_wrapper(GLint x, GLint y, GLsizei width, GLsizei height);
int glGetAttribLocation_wrapper(GLuint program, const GLchar* name);
int glGetUniformLocation_wrapper(GLuint program, const GLchar* name);
const GLubyte* glGetString_wrapper(GLenum name);



// Function pointers to the original OpenGL functions.
void (*glDeleteTextures_orig_ptr)(GLsizei n, const GLuint* textures);
void (*glGetIntegerv_orig_ptr)(GLenum pname, GLint* params);
void (*glGenTextures_orig_ptr)(GLsizei n, GLuint* textures);
void (*glBindTexture_orig_ptr)(GLenum target, GLuint texture);
void (*glTexParameteri_orig_ptr)(GLenum target, GLenum pname, GLint param);
void (*glTexImage2D_orig_ptr)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* pixels);
GLenum (*glGetError_orig_ptr)(void);
GLuint (*glCreateShader_orig_ptr)(GLenum type);
void (*glShaderSource_orig_ptr)(GLuint shader, GLsizei count, const GLchar** string, const GLint* length);
void (*glCompileShader_orig_ptr)(GLuint shader);
void (*glGetShaderiv_orig_ptr)(GLuint shader, GLenum pname, GLint* params);
void (*glGetShaderInfoLog_orig_ptr)(GLuint shader, GLsizei bufsize, GLsizei* length, GLchar* infolog);
void (*glDeleteShader_orig_ptr)(GLuint shader);
GLuint (*glCreateProgram_orig_ptr)(void);
void (*glAttachShader_orig_ptr)(GLuint program, GLuint shader);
void (*glBindAttribLocation_orig_ptr)(GLuint program, GLuint index, const GLchar* name);
void (*glLinkProgram_orig_ptr)(GLuint program);
void (*glGetProgramiv_orig_ptr)(GLuint program, GLenum pname, GLint* params);
void (*glGetProgramInfoLog_orig_ptr)(GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog);
void (*glDeleteProgram_orig_ptr)(GLuint program);
void (*glReadPixels_orig_ptr)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid* pixels);
void (*glActiveTexture_orig_ptr)(GLenum texture);
void (*glBindBuffer_orig_ptr)(GLenum target, GLuint buffer);
void (*glBindFramebuffer_orig_ptr)(GLenum target, GLuint framebuffer);
void (*glBindRenderbuffer_orig_ptr)(GLenum target, GLuint renderbuffer);
void (*glBufferData_orig_ptr)(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
void (*glClear_orig_ptr)(GLbitfield mask);
void (*glClearColor_orig_ptr)(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
void (*glDeleteBuffers_orig_ptr)(GLsizei n, const GLuint* buffers);
void (*glDeleteFramebuffers_orig_ptr)(GLsizei n, const GLuint* framebuffers);
void (*glDeleteRenderbuffers_orig_ptr)(GLsizei n, const GLuint* renderbuffers);
void (*glDepthMask_orig_ptr)(GLboolean flag);
void (*glDetachShader_orig_ptr)(GLuint program, GLuint shader);
void (*glDisable_orig_ptr)(GLenum cap);
void (*glDrawArrays_orig_ptr)(GLenum mode, GLint first, GLsizei count);
void (*glDrawElements_orig_ptr)(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices);
void (*glEnableVertexAttribArray_orig_ptr)(GLuint index);
void (*glFramebufferRenderbuffer_orig_ptr)(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
void (*glFramebufferTexture2D_orig_ptr)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
void (*glGenBuffers_orig_ptr)(GLsizei n, GLuint* buffers);
void (*glGenFramebuffers_orig_ptr)(GLsizei n, GLuint* framebuffers);
void (*glGenRenderbuffers_orig_ptr)(GLsizei n, GLuint* renderbuffers);
void (*glPixelStorei_orig_ptr)(GLenum pname, GLint param);
void (*glRenderbufferStorage_orig_ptr)(GLenum target, GLenum internalformat, GLsizei width, GLsizei height);
void (*glUniform1f_orig_ptr)(GLint location, GLfloat x);
void (*glUniform1fv_orig_ptr)(GLint location, GLsizei count, const GLfloat* v);
void (*glUniform1i_orig_ptr)(GLint location, GLint x);
void (*glUniform1iv_orig_ptr)(GLint location, GLsizei count, const GLint* v);
void (*glUniform2f_orig_ptr)(GLint location, GLfloat x, GLfloat y);
void (*glUniform2fv_orig_ptr)(GLint location, GLsizei count, const GLfloat* v);
void (*glUniform3f_orig_ptr)(GLint location, GLfloat x, GLfloat y, GLfloat z);
void (*glUniform3fv_orig_ptr)(GLint location, GLsizei count, const GLfloat* v);
void (*glUniform4f_orig_ptr)(GLint location, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
void (*glUniform4fv_orig_ptr)(GLint location, GLsizei count, const GLfloat* v);
void (*glUniformMatrix2fv_orig_ptr)(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
void (*glUniformMatrix3fv_orig_ptr)(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
void (*glUseProgram_orig_ptr)(GLuint program);
void (*glVertexAttribPointer_orig_ptr)(GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr);
void (*glViewport_orig_ptr)(GLint x, GLint y, GLsizei width, GLsizei height);
int (*glGetAttribLocation_orig_ptr)(GLuint program, const GLchar* name);
int (*glGetUniformLocation_orig_ptr)(GLuint program, const GLchar* name);
const GLubyte* (*glGetString_orig_ptr)(GLenum name);



#define CHECK_AND_RETURN(METHOD)                                        \
    if (sym_addr == (unsigned long)METHOD##_orig_ptr) return (unsigned long)&METHOD##_wrapper

// This macro will be used to get the address of the original OpenGL
// function.
#define OPENGL_GET_ORIG_PTR(METHOD) {                   \
        method_sym = dlsym(lib_handle, #METHOD);        \
        METHOD##_orig_ptr = method_sym;                 \
    }

// This macro is required to intercept, during emulation, any calls to
// our OpenGL wrappers so that the wrapper is executed directly.
#define OPENGL_INTERCEPT_WRAPPER(METHOD) {                              \
        intercept_func = calloc(1, sizeof(emu_intercept_func_t));       \
        intercept_func->name = "WRAP_"#METHOD;                            \
        intercept_func->id = METHOD##_ID;                               \
        intercept_func->orig_func_addr = (uint32_t)&METHOD##_wrapper;   \
        intercept_func->trampoline_read_addr = (uint32_t)&METHOD##_trampoline; \
        intercept_func->target_func_addr = (uint32_t)&METHOD##_wrapper; \
        HASH_ADD_INT(*intercept_tbl, orig_func_addr, intercept_func);   \
    }

#define OPENGL_INTERCEPT_ALL {                                  \
                                                                \
        OPENGL_GET_ORIG_PTR(glDeleteTextures);                  \
        OPENGL_GET_ORIG_PTR(glGetIntegerv);                     \
        OPENGL_GET_ORIG_PTR(glGenTextures);                     \
        OPENGL_GET_ORIG_PTR(glBindTexture);                     \
        OPENGL_GET_ORIG_PTR(glTexParameteri);                   \
        OPENGL_GET_ORIG_PTR(glTexImage2D);                      \
        OPENGL_GET_ORIG_PTR(glGetError);                        \
        OPENGL_GET_ORIG_PTR(glCreateShader);                    \
        OPENGL_GET_ORIG_PTR(glShaderSource);                    \
        OPENGL_GET_ORIG_PTR(glCompileShader);                   \
        OPENGL_GET_ORIG_PTR(glGetShaderiv);                     \
        OPENGL_GET_ORIG_PTR(glGetShaderInfoLog);                \
        OPENGL_GET_ORIG_PTR(glDeleteShader);                    \
        OPENGL_GET_ORIG_PTR(glCreateProgram);                   \
        OPENGL_GET_ORIG_PTR(glAttachShader);                    \
        OPENGL_GET_ORIG_PTR(glBindAttribLocation);              \
        OPENGL_GET_ORIG_PTR(glLinkProgram);                     \
        OPENGL_GET_ORIG_PTR(glGetProgramiv);                    \
        OPENGL_GET_ORIG_PTR(glGetProgramInfoLog);               \
        OPENGL_GET_ORIG_PTR(glDeleteProgram);                   \
        OPENGL_GET_ORIG_PTR(glReadPixels);                      \
        OPENGL_GET_ORIG_PTR(glActiveTexture);                   \
        OPENGL_GET_ORIG_PTR(glBindBuffer);                      \
        OPENGL_GET_ORIG_PTR(glBindFramebuffer);                 \
        OPENGL_GET_ORIG_PTR(glBindRenderbuffer);                \
        OPENGL_GET_ORIG_PTR(glBufferData);                      \
        OPENGL_GET_ORIG_PTR(glClear);                           \
        OPENGL_GET_ORIG_PTR(glClearColor);                      \
        OPENGL_GET_ORIG_PTR(glDeleteBuffers);                   \
        OPENGL_GET_ORIG_PTR(glDeleteFramebuffers);              \
        OPENGL_GET_ORIG_PTR(glDeleteRenderbuffers);             \
        OPENGL_GET_ORIG_PTR(glDepthMask);                       \
        OPENGL_GET_ORIG_PTR(glDetachShader);                    \
        OPENGL_GET_ORIG_PTR(glDisable);                         \
        OPENGL_GET_ORIG_PTR(glDrawArrays);                      \
        OPENGL_GET_ORIG_PTR(glDrawElements);                    \
        OPENGL_GET_ORIG_PTR(glEnableVertexAttribArray);         \
        OPENGL_GET_ORIG_PTR(glFramebufferRenderbuffer);         \
        OPENGL_GET_ORIG_PTR(glFramebufferTexture2D);            \
        OPENGL_GET_ORIG_PTR(glGenBuffers);                      \
        OPENGL_GET_ORIG_PTR(glGenFramebuffers);                 \
        OPENGL_GET_ORIG_PTR(glGenRenderbuffers);                \
        OPENGL_GET_ORIG_PTR(glPixelStorei);                     \
        OPENGL_GET_ORIG_PTR(glRenderbufferStorage);             \
        OPENGL_GET_ORIG_PTR(glUniform1f);                       \
        OPENGL_GET_ORIG_PTR(glUniform1fv);                      \
        OPENGL_GET_ORIG_PTR(glUniform1i);                       \
        OPENGL_GET_ORIG_PTR(glUniform1iv);                      \
        OPENGL_GET_ORIG_PTR(glUniform2f);                       \
        OPENGL_GET_ORIG_PTR(glUniform2fv);                      \
        OPENGL_GET_ORIG_PTR(glUniform3f);                       \
        OPENGL_GET_ORIG_PTR(glUniform3fv);                      \
        OPENGL_GET_ORIG_PTR(glUniform4f);                       \
        OPENGL_GET_ORIG_PTR(glUniform4fv);                      \
        OPENGL_GET_ORIG_PTR(glUniformMatrix2fv);                \
        OPENGL_GET_ORIG_PTR(glUniformMatrix3fv);                \
        OPENGL_GET_ORIG_PTR(glUseProgram);                      \
        OPENGL_GET_ORIG_PTR(glVertexAttribPointer);             \
        OPENGL_GET_ORIG_PTR(glViewport);                        \
        OPENGL_GET_ORIG_PTR(glGetAttribLocation);               \
        OPENGL_GET_ORIG_PTR(glGetUniformLocation);              \
        OPENGL_GET_ORIG_PTR(glGetString);                       \
                                                                \
        OPENGL_INTERCEPT_WRAPPER(glDeleteTextures);             \
        OPENGL_INTERCEPT_WRAPPER(glGetIntegerv);                \
        OPENGL_INTERCEPT_WRAPPER(glGenTextures);                \
        OPENGL_INTERCEPT_WRAPPER(glBindTexture);                \
        OPENGL_INTERCEPT_WRAPPER(glTexParameteri);              \
        OPENGL_INTERCEPT_WRAPPER(glTexImage2D);                 \
        OPENGL_INTERCEPT_WRAPPER(glGetError);                   \
        OPENGL_INTERCEPT_WRAPPER(glCreateShader);               \
        OPENGL_INTERCEPT_WRAPPER(glShaderSource);               \
        OPENGL_INTERCEPT_WRAPPER(glCompileShader);              \
        OPENGL_INTERCEPT_WRAPPER(glGetShaderiv);                \
        OPENGL_INTERCEPT_WRAPPER(glGetShaderInfoLog);           \
        OPENGL_INTERCEPT_WRAPPER(glDeleteShader);               \
        OPENGL_INTERCEPT_WRAPPER(glCreateProgram);              \
        OPENGL_INTERCEPT_WRAPPER(glAttachShader);               \
        OPENGL_INTERCEPT_WRAPPER(glBindAttribLocation);         \
        OPENGL_INTERCEPT_WRAPPER(glLinkProgram);                \
        OPENGL_INTERCEPT_WRAPPER(glGetProgramiv);               \
        OPENGL_INTERCEPT_WRAPPER(glGetProgramInfoLog);          \
        OPENGL_INTERCEPT_WRAPPER(glDeleteProgram);              \
        OPENGL_INTERCEPT_WRAPPER(glReadPixels);                 \
        OPENGL_INTERCEPT_WRAPPER(glActiveTexture);              \
        OPENGL_INTERCEPT_WRAPPER(glBindBuffer);                 \
        OPENGL_INTERCEPT_WRAPPER(glBindFramebuffer);            \
        OPENGL_INTERCEPT_WRAPPER(glBindRenderbuffer);           \
        OPENGL_INTERCEPT_WRAPPER(glBufferData);                 \
        OPENGL_INTERCEPT_WRAPPER(glClear);                      \
        OPENGL_INTERCEPT_WRAPPER(glClearColor);                 \
        OPENGL_INTERCEPT_WRAPPER(glDeleteBuffers);              \
        OPENGL_INTERCEPT_WRAPPER(glDeleteFramebuffers);         \
        OPENGL_INTERCEPT_WRAPPER(glDeleteRenderbuffers);        \
        OPENGL_INTERCEPT_WRAPPER(glDepthMask);                  \
        OPENGL_INTERCEPT_WRAPPER(glDetachShader);               \
        OPENGL_INTERCEPT_WRAPPER(glDisable);                    \
        OPENGL_INTERCEPT_WRAPPER(glDrawArrays);                 \
        OPENGL_INTERCEPT_WRAPPER(glDrawElements);               \
        OPENGL_INTERCEPT_WRAPPER(glEnableVertexAttribArray);    \
        OPENGL_INTERCEPT_WRAPPER(glFramebufferRenderbuffer);    \
        OPENGL_INTERCEPT_WRAPPER(glFramebufferTexture2D);       \
        OPENGL_INTERCEPT_WRAPPER(glGenBuffers);                 \
        OPENGL_INTERCEPT_WRAPPER(glGenFramebuffers);            \
        OPENGL_INTERCEPT_WRAPPER(glGenRenderbuffers);           \
        OPENGL_INTERCEPT_WRAPPER(glPixelStorei);                \
        OPENGL_INTERCEPT_WRAPPER(glRenderbufferStorage);        \
        OPENGL_INTERCEPT_WRAPPER(glUniform1f);                  \
        OPENGL_INTERCEPT_WRAPPER(glUniform1fv);                 \
        OPENGL_INTERCEPT_WRAPPER(glUniform1i);                  \
        OPENGL_INTERCEPT_WRAPPER(glUniform1iv);                 \
        OPENGL_INTERCEPT_WRAPPER(glUniform2f);                  \
        OPENGL_INTERCEPT_WRAPPER(glUniform2fv);                 \
        OPENGL_INTERCEPT_WRAPPER(glUniform3f);                  \
        OPENGL_INTERCEPT_WRAPPER(glUniform3fv);                 \
        OPENGL_INTERCEPT_WRAPPER(glUniform4f);                  \
        OPENGL_INTERCEPT_WRAPPER(glUniform4fv);                 \
        OPENGL_INTERCEPT_WRAPPER(glUniformMatrix2fv);           \
        OPENGL_INTERCEPT_WRAPPER(glUniformMatrix3fv);           \
        OPENGL_INTERCEPT_WRAPPER(glUseProgram);                 \
        OPENGL_INTERCEPT_WRAPPER(glVertexAttribPointer);        \
        OPENGL_INTERCEPT_WRAPPER(glViewport);                   \
        OPENGL_INTERCEPT_WRAPPER(glGetAttribLocation);          \
        OPENGL_INTERCEPT_WRAPPER(glGetUniformLocation);         \
        OPENGL_INTERCEPT_WRAPPER(glGetString);                  \
                                                                \
    }

#endif // INCLUDE_OPENGL_INTERCEPT_H
