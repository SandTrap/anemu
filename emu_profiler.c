#include <unistd.h>
#include <linux/perf_event.h>
#include <sys/cdefs.h>
#include "emu_profiler.h"
#include "emu_thread.h"
#include "emu_debug.h"

__BEGIN_DECLS
extern ssize_t __read(int, void *, size_t);

#define CLOCK_MONOTONIC_RAW 4
#define NANOS 1e9

double _time_ms() {
    struct timespec res;
    clock_gettime(CLOCK_MONOTONIC_RAW, &res);
    return 1000.0 * res.tv_sec + (double) res.tv_nsec / 1e6;
}


int64_t _time_ns() {
    struct timespec res;
    clock_gettime(CLOCK_MONOTONIC_RAW, &res);
    return res.tv_sec * NANOS + res.tv_nsec;
}


int64_t diff_ns(struct timespec *start,
                struct timespec *end) {
    int64_t sec, nsec;
    sec  = end->tv_sec  - start->tv_sec;
    nsec = end->tv_nsec - start->tv_nsec;
    return sec * NANOS + nsec;
}


int64_t ns_to_cycles(int64_t ns) {
    // TODO: dynamically read scaling
    // /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
    static const double scaling = 1.2; /* GHz */
    return ns / scaling;
}


inline
int time_ns(struct timespec *res) {
    return clock_gettime(CLOCK_MONOTONIC_RAW, res);
}


uint64_t getticks(void)
{
    static int fd,init = 0;
    static struct perf_event_attr attr;
    static uint64_t buffer;

    if(!init) {
        attr.type = PERF_TYPE_HARDWARE;
        attr.config = PERF_COUNT_HW_CPU_CYCLES;
        fd = syscall(__NR_perf_event_open, &attr, 0, -1, -1, 0);
        if(fd < 0) {
            fprintf(stderr,"ERROR - Cannot open perf event file descriptor:\n");
            if(errno == -EPERM || errno == -EACCES)
                fprintf(stderr,"  Permission denied.\n");
            else if(errno == ENOENT)
                fprintf(stderr,"  PERF_COUNT_HW_CPU_CYCLES event is not supported.\n");
            else
                fprintf(stderr,"  Attempting to open the file descriptor returned %d (%s).\n",errno, strerror(errno));
            exit(-1);
        }
        init = 1;
    }
    __read(fd,&buffer,sizeof(uint64_t));
    return buffer;
}
