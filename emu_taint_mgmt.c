#include <inttypes.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include "emu_taint_mgmt.h"
#include "emu_taint_mgmt_macros.h"
#include "emu_mem_mgmt.h"
#include "anemu-private.h"
#include "emu_emulate.h"
#include "emu_intercept.h"

int
emu_init_taintmaps() {

    // This function initializes taintmaps, the data structures used
    // to store taint labels. There are two taintmaps: TAINTMAP_STACK,
    // used to store labels of tainted data in the process's main
    // stack, and TAINTMAP_LIB, used to store labels of tainted data
    // in the process's heap. The taint labels will be located in
    // fixed, well-known addresses.
    //
    // In the following, we illustrate the expected memory layout of a
    // typical process after the taint labels are initialized:
    //
    // 10000000 - 18000000 : Taint Tags (TAINTMAP_STACK)
    // 20000000 - 40000000 : Text + Data + BSS
    // 40000000 - 70000000 : Heap
    // 70000000 - be800000 : Taint Tags (TAINTMAP_LIB)
    // be800000 - bf000000 : Stack (RLIMIT_STACK = 8192 KB)
    // ffff0000 - ffff1000 : Vectors
    //
    // The starting and ending address of TAINTMAP_STACK is chosen by
    // us and fixed. The starting address of TAINTMAP_LIB (0x70000000)
    // is also chosen by us and fixed and its ending address is
    // computed during runtime; it ends where the stack begins. This
    // scheme essentially forces the heap to occupy the region of
    // 0x40000000 - 0x70000000.
    //
    // Note that the heap always starts from 0x40000000. This
    // corresponds to TASK_UNMAPPED_BASE in
    // kernel_omap/arch/arm/include/asm/memory.h. However, due to
    // ASLR, we don't know where the stack will start and stop. This
    // is why we parse /proc/self/maps in emu_parse_maps() to find out
    // where the stack ends. We can subtract RLIMIT_STACK from it to
    // find the starting address.

    int ret;
    uint32_t start, end;
    size_t bytes, num_taint_count;
    struct rlimit lim;
    void *addr, *data;
    taintcount_t *taint_count;
    taintmap_t *tm;

    emu_log_always("Initializing taintmaps.\n");

    // First, we initialize TAINTMAP_STACK. We expect the maximum
    // stack size to be 8 MB (8192 KB), and we're going to store the
    // labels in the fixed address range of 0x1000000 - 0x18000000.

    // Get the maximum size of the stack
    lim.rlim_cur = 0;
    lim.rlim_max = 0;
    ret = getrlimit(RLIMIT_STACK, &lim);
    if (ret != 0) {
        emu_log_always("[emu_init_taintmaps] Could not get maximum stack size.\n");
        return 1;
    } else if (lim.rlim_cur != 0x00800000) {
        emu_abort("[emu_init_taintmaps] Maximum stack size is %lx, not 8~MB.\n", lim.rlim_cur);
    }
    emu_global->stack_max = (int)lim.rlim_cur;

    // Allocate the space to store the taint labels
    bytes = emu_global->stack_max;
    addr = (void *)0x10000000;
    data = mmap(addr, bytes, PROT_READ | PROT_WRITE,
                MAP_FIXED | MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE,
                -1, 0);
    if (data == MAP_FAILED) {
        emu_abort("Could not allocate label storage space for TAINTMAP_STACK. Starting address: %p, bytes: %u\n",
                       addr, bytes);
    }

    num_taint_count = num_pages(bytes) * sizeof(taintcount_t);
    taint_count = emu_alloc(num_taint_count);
    if (taint_count == NULL) {
        emu_log_always("[Taint Prop] Allocation for stack taintmap "
                      "count failed\n");
        return 1;
    }

    // Record the details of the stack and the label storage space in
    // TAINTMAP_STACK.
    end = emu_global->main_thread_stack.vm_end;
    start = end - bytes;
    emu_global->stack_base = end - emu_global->stack_max;

    tm = &emu_global->taintmaps[TAINTMAP_STACK];
    tm->data = data;
    tm->start = start;
    tm->end = end;
    tm->bytes = bytes;
    tm->taint_count = taint_count;
    emu_log_always("TAINTMAP_STACK range: %x - %x length: %x\n", start, end, (end-start));
    emu_log_always("TAINTMAP_STACK label storage address: 0x%08x - 0x%08x length: %x\n", (uint32_t)addr, (uint32_t)addr+bytes, bytes);

    // Now, we initialize TAINTMAP_LIB. We're going to store the
    // labels in the address range of 0x70000000 - <start of the
    // stack>
    addr = (void *)0x70000000;
    bytes = emu_global->taintmaps[TAINTMAP_STACK].start - (uint32_t)addr;

    // Allocate the space to store the taint labels.
    data = mmap(addr, bytes, PROT_READ | PROT_WRITE,
                MAP_FIXED | MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE,
                -1, 0);

    if (data == MAP_FAILED) {
        emu_abort("Could not allocate label storage space for TAINTMAP_LIB. Starting address: %p, bytes: %u\n",
                      addr, bytes);
    }

    num_taint_count = num_pages(bytes) * sizeof(taintcount_t);
    taint_count = emu_alloc(num_taint_count);
    if (taint_count == NULL) {
        emu_log_always("[Taint Prop] Allocation for lib taintmap count failed\n");
        return 1;
    }

    // Record the details of the heap and the label storage area in
    // TAINTMAP_LIB. Note that we expect the app to only use the
    // address range of 0x40000000 - 0x70000000 as its heap.
    start = 0x40000000;
    end = 0x70000000;
    tm = &emu_global->taintmaps[TAINTMAP_LIB];
    tm->data = data;
    tm->start = start;
    tm->end = end;
    tm->bytes = bytes;
    tm->taint_count = taint_count;

    if (bytes < (end - start)) {
        emu_abort("Not enough space to hold taint labels in TAINTMAP_LIB. Start: 0x%08x, End: 0x%08x, Bytes: 0x%08x.", start, end, bytes);
    }

    emu_log_always("TAINTMAP_LIB range: %x - %x length: %x\n", start, end, (end-start));
    emu_log_always("TAINTMAP_LIB label storage address: 0x%08x - 0x%08x length: %x\n", (uint32_t)addr, end+bytes, bytes);

    emu_log_debug("taintmaps initialized\n");
    return 0;
}

uint32_t emu_get_taint_file(int fd) {
    int xbuf;
    uint32_t xtag = TAINT_CLEAR;
    int xret;

    xret = fgetxattr(fd, TAINT_XATTR_NAME, &xbuf, sizeof(xbuf));

    if (xret < 0) {
        if (errno == ENOATTR) {
            // emu_log_always("fgetxattr(%s): no taint tag\n", result);
        } else if (errno == ERANGE) {
            emu_log_debug("TaintLog: fgetxattr(%d) contents to large\n", fd);
        } else if (errno == ENOTSUP) {
            /* XATTRs are not supported. No need to spam the logs */
        } else if (errno == EPERM) {
            /* Strange interaction with /dev/log/main. Suppress the log */
        } else if (errno == EACCES) {
            /* Permission Denied on things like Sockets. Supress the log */
        } else {
            emu_log_always("TaintLog: fgetxattr(%d): unknown error code %d\n", fd, errno);
        }
    } else {
        xtag = (uint32_t) xbuf;
        emu_log_debug("%s : fd %d taint tag: 0x%x\n", __func__, fd, xtag);
    }

    return xtag;
}

uint32_t emu_set_taint_file(int fd, uint32_t tag)
{
    int32_t ret;

    emu_log_debug("In emu_set_taint_file, tag is: %u", tag);
    ret = fsetxattr(fd, TAINT_XATTR_NAME, &tag, sizeof(tag), 0);

    if (ret < 0) {
        if (errno == ENOSPC || errno == EDQUOT) {
            emu_log_always("TaintLog: fsetxattr(%d): not enough room to set xattr", fd);
        } else if (errno == ENOTSUP) {
            /* XATTRs are not supported. No need to spam the logs */
        } else if (errno == EPERM) {
            /* Strange interaction with /dev/log/main. Suppress the log */
        } else {
            emu_log_always("TaintLog: fsetxattr(%d): unknown error code %d", fd, errno);
        }
    } else {
        emu_log_debug("%s : fd %d taint tag: 0x%x\n", __func__, fd, tag);
    }

    return (uint32_t) ret;
}

/**
 * Taint a specific register (reg) with taint (tag)
 * If the specified register is PC or LR, no taint is propagated.
 */
void
emu_set_taint_reg(emu_thread_t *emu, emu_arm_reg reg, uint32_t tag) {
    emu_assert(reg <= EMU_ARM_REG_R15);

    // never taint special regs (overtaint)
    // Should we prevent tainting of SP?
    if (reg == EMU_ARM_REG_LR || reg == EMU_ARM_REG_PC) return;

    if (emu->taintreg[reg] != TAINT_CLEAR && tag == TAINT_CLEAR) {
        emu_log_debug("taint: un-tainting r%d\n", reg);
    } else if (emu->taintreg[reg] == TAINT_CLEAR && tag != TAINT_CLEAR) {
        emu_log_debug("taint: tainting r%d tag: %x\n", reg, tag);
    }

    emu->taintreg[reg] = tag;
}


uint32_t
emu_get_taint_reg(emu_thread_t *emu, emu_arm_reg reg) {
    emu_assert(reg <= EMU_ARM_REG_R15);
    uint32_t taint_tag = emu->taintreg[reg];
    return taint_tag;
}

void
emu_set_taint_ext_reg(emu_thread_t *emu, emu_arm_reg reg, uint32_t tag) {

    emu_assert(reg >= EMU_ARM_REG_D0 && reg <= EMU_ARM_REG_Q15);

    switch(reg) {

    case EMU_ARM_REG_D0:
    case EMU_ARM_REG_D1:
    case EMU_ARM_REG_D2:
    case EMU_ARM_REG_D3:
    case EMU_ARM_REG_D4:
    case EMU_ARM_REG_D5:
    case EMU_ARM_REG_D6:
    case EMU_ARM_REG_D7:
    case EMU_ARM_REG_D8:
    case EMU_ARM_REG_D9:
    case EMU_ARM_REG_D10:
    case EMU_ARM_REG_D11:
    case EMU_ARM_REG_D12:
    case EMU_ARM_REG_D13:
    case EMU_ARM_REG_D14:
    case EMU_ARM_REG_D15:
    case EMU_ARM_REG_D16:
    case EMU_ARM_REG_D17:
    case EMU_ARM_REG_D18:
    case EMU_ARM_REG_D19:
    case EMU_ARM_REG_D20:
    case EMU_ARM_REG_D21:
    case EMU_ARM_REG_D22:
    case EMU_ARM_REG_D23:
    case EMU_ARM_REG_D24:
    case EMU_ARM_REG_D25:
    case EMU_ARM_REG_D26:
    case EMU_ARM_REG_D27:
    case EMU_ARM_REG_D28:
    case EMU_ARM_REG_D29:
    case EMU_ARM_REG_D30:
    case EMU_ARM_REG_D31:
        emu->taintextreg[reg] = tag;
        break;

    case EMU_ARM_REG_S0:
    case EMU_ARM_REG_S1:
    case EMU_ARM_REG_S2:
    case EMU_ARM_REG_S3:
    case EMU_ARM_REG_S4:
    case EMU_ARM_REG_S5:
    case EMU_ARM_REG_S6:
    case EMU_ARM_REG_S7:
    case EMU_ARM_REG_S8:
    case EMU_ARM_REG_S9:
    case EMU_ARM_REG_S10:
    case EMU_ARM_REG_S11:
    case EMU_ARM_REG_S12:
    case EMU_ARM_REG_S13:
    case EMU_ARM_REG_S14:
    case EMU_ARM_REG_S15:
    case EMU_ARM_REG_S16:
    case EMU_ARM_REG_S17:
    case EMU_ARM_REG_S18:
    case EMU_ARM_REG_S19:
    case EMU_ARM_REG_S20:
    case EMU_ARM_REG_S21:
    case EMU_ARM_REG_S22:
    case EMU_ARM_REG_S23:
    case EMU_ARM_REG_S24:
    case EMU_ARM_REG_S25:
    case EMU_ARM_REG_S26:
    case EMU_ARM_REG_S27:
    case EMU_ARM_REG_S28:
    case EMU_ARM_REG_S29:
    case EMU_ARM_REG_S30:
    case EMU_ARM_REG_S31:
        emu->taintextreg[(reg - 32) / 2] = tag;
        break;

    case EMU_ARM_REG_Q0:
    case EMU_ARM_REG_Q1:
    case EMU_ARM_REG_Q2:
    case EMU_ARM_REG_Q3:
    case EMU_ARM_REG_Q4:
    case EMU_ARM_REG_Q5:
    case EMU_ARM_REG_Q6:
    case EMU_ARM_REG_Q7:
    case EMU_ARM_REG_Q8:
    case EMU_ARM_REG_Q9:
    case EMU_ARM_REG_Q10:
    case EMU_ARM_REG_Q11:
    case EMU_ARM_REG_Q12:
    case EMU_ARM_REG_Q13:
    case EMU_ARM_REG_Q14:
    case EMU_ARM_REG_Q15: ;
        int base_reg = (reg - 32 - 32) * 2;
        emu->taintextreg[base_reg] = tag;
        emu->taintextreg[base_reg + 1] = tag;
        break;
    };
}

uint32_t
emu_get_taint_ext_reg(emu_thread_t *emu, emu_arm_reg reg) {
    emu_assert(reg >= EMU_ARM_REG_D0 && reg <= EMU_ARM_REG_Q15);
    uint32_t tag = 0;
    switch(reg) {

    case EMU_ARM_REG_D0:
    case EMU_ARM_REG_D1:
    case EMU_ARM_REG_D2:
    case EMU_ARM_REG_D3:
    case EMU_ARM_REG_D4:
    case EMU_ARM_REG_D5:
    case EMU_ARM_REG_D6:
    case EMU_ARM_REG_D7:
    case EMU_ARM_REG_D8:
    case EMU_ARM_REG_D9:
    case EMU_ARM_REG_D10:
    case EMU_ARM_REG_D11:
    case EMU_ARM_REG_D12:
    case EMU_ARM_REG_D13:
    case EMU_ARM_REG_D14:
    case EMU_ARM_REG_D15:
    case EMU_ARM_REG_D16:
    case EMU_ARM_REG_D17:
    case EMU_ARM_REG_D18:
    case EMU_ARM_REG_D19:
    case EMU_ARM_REG_D20:
    case EMU_ARM_REG_D21:
    case EMU_ARM_REG_D22:
    case EMU_ARM_REG_D23:
    case EMU_ARM_REG_D24:
    case EMU_ARM_REG_D25:
    case EMU_ARM_REG_D26:
    case EMU_ARM_REG_D27:
    case EMU_ARM_REG_D28:
    case EMU_ARM_REG_D29:
    case EMU_ARM_REG_D30:
    case EMU_ARM_REG_D31:
        tag = emu->taintextreg[reg];
        return tag;

    case EMU_ARM_REG_S0:
    case EMU_ARM_REG_S1:
    case EMU_ARM_REG_S2:
    case EMU_ARM_REG_S3:
    case EMU_ARM_REG_S4:
    case EMU_ARM_REG_S5:
    case EMU_ARM_REG_S6:
    case EMU_ARM_REG_S7:
    case EMU_ARM_REG_S8:
    case EMU_ARM_REG_S9:
    case EMU_ARM_REG_S10:
    case EMU_ARM_REG_S11:
    case EMU_ARM_REG_S12:
    case EMU_ARM_REG_S13:
    case EMU_ARM_REG_S14:
    case EMU_ARM_REG_S15:
    case EMU_ARM_REG_S16:
    case EMU_ARM_REG_S17:
    case EMU_ARM_REG_S18:
    case EMU_ARM_REG_S19:
    case EMU_ARM_REG_S20:
    case EMU_ARM_REG_S21:
    case EMU_ARM_REG_S22:
    case EMU_ARM_REG_S23:
    case EMU_ARM_REG_S24:
    case EMU_ARM_REG_S25:
    case EMU_ARM_REG_S26:
    case EMU_ARM_REG_S27:
    case EMU_ARM_REG_S28:
    case EMU_ARM_REG_S29:
    case EMU_ARM_REG_S30:
    case EMU_ARM_REG_S31:
        tag = emu->taintextreg[(reg-32)/2];
        return tag;

    case EMU_ARM_REG_Q0:
    case EMU_ARM_REG_Q1:
    case EMU_ARM_REG_Q2:
    case EMU_ARM_REG_Q3:
    case EMU_ARM_REG_Q4:
    case EMU_ARM_REG_Q5:
    case EMU_ARM_REG_Q6:
    case EMU_ARM_REG_Q7:
    case EMU_ARM_REG_Q8:
    case EMU_ARM_REG_Q9:
    case EMU_ARM_REG_Q10:
    case EMU_ARM_REG_Q11:
    case EMU_ARM_REG_Q12:
    case EMU_ARM_REG_Q13:
    case EMU_ARM_REG_Q14:
    case EMU_ARM_REG_Q15: ;
        int base_reg = (reg - 32 - 32) * 2;
        tag = emu->taintextreg[base_reg] | emu->taintextreg[base_reg + 1];
        return tag;

    };

    emu_abort("UNKNOWN REGISTER TYPE IN emu_get_taint_ext_reg\n");
    return -1;
}

uint8_t
emu_regs_tainted(emu_thread_t *emu) {
    int i, tainted;
    tainted = N_REGS;
    for (i = 0; i < N_REGS; i++) {
        if (emu_get_taint_reg(emu, i) != TAINT_CLEAR) {
            if (i == EMU_ARM_REG_SP ||
                i == EMU_ARM_REG_LR ||
                i == EMU_ARM_REG_PC) {
                emu_abort("taint: WARNING special r%d tainted!\n", i);
            }
            emu_log_debug("taint: r%d val: %x tag: %x\n", i, emu_get_taint_reg(emu, i), emu_get_taint_reg(emu, i));
        } else {
            tainted--;
        }
    }
    return tainted;
}


/**
 * @brief clear the taintreg[] array of this emu_thread_t instance
 */
void
emu_clear_taintregs(emu_thread_t *emu)
{
    int i;
    for (i = 0; i < N_REGS; i++) {
        emu->taintreg[i] = TAINT_CLEAR;
    }

    for (i = 0; i < N_EXT_REGS; i++) {
        emu->taintextreg[i] = TAINT_CLEAR;
    }

}


uint32_t
emu_get_taint_mem(emu_thread_t* emu, uint32_t addr) {
    addr = Align(addr, 4);      /* word align */

    taintmap_t *taintmap = emu->heap_tm;
    if (addr > emu->stack_base) {
        taintmap = emu->stack_tm;
    }

    if (addr < taintmap->start || addr > taintmap->end) {
        emu_log_always("[pid: %d, tid: %d] emu_get_taint_mem failed."
                      " Address 0x%08x out of the range 0x%08x - 0x%08x.\n",
                      getpid(), gettid(),
                      addr, taintmap->start, taintmap->end);
        dbg_print_method_info(CPU(curr_app_ctx, pc));
        dbg_print_dumb_backtrace((uint32_t *)CPU(curr_app_ctx, sp), 500);
        emu_abort("pid: %d, tid: %d] emu_get_taint_mem failed.\n",
                  getpid(), gettid());
    }
    uint32_t    offset   = (addr - taintmap->start) >> 2;
    uint32_t    tag      = taintmap->data[offset]; /* word (32-bit) based tag storage */

    return tag;
}


// assuming lock is already held when called from emu_set_taint_array
void emu_set_taint_mem(emu_thread_t *emu, uint32_t addr, uint32_t tag) {
    emu_assert(addr > 0);

    addr = Align(addr, 4);      /* word align */

    taintmap_t *taintmap = emu->heap_tm;
    if (addr > emu->stack_base) {
        taintmap = emu->stack_tm;
    }

    if (addr < taintmap->start || addr > taintmap->end) {
        emu_abort("out of bounds addr %x\n", addr);
    }

    // sanity check offset is valid
    uint32_t    offset     = (addr - taintmap->start) >> 2;
    int8_t increment = 0;
    if (tag == TAINT_CLEAR && taintmap->data[offset] != TAINT_CLEAR) {
        emu_log_debug("taint: un-tainting mem: %x\n", addr);
        emu_log_debug("taint: un-tainting mem: %x\n", addr);
        increment = -1;
    } else if (tag != TAINT_CLEAR && taintmap->data[offset] == TAINT_CLEAR) {
        emu_log_debug("taint: tainting mem: %x tag: %x\n", addr, tag);
        emu_log_debug("taint: tainting mem: %x tag: %x\n", addr, tag);
        increment = +1;
    }
    if (increment) { // if taint toggled
        // emu_log_debug("taint: updating off: %8x tag: %x\n", offset, tag);
        taintmap->data[offset] = tag;  /* word (32-bit) based tag storage */
        emu_update_taintpage(addr, increment);
    }
}


uint32_t emu_get_opengl_taint() {
    uint32_t tag = 0;

    pthread_mutex_lock(&emu_global->mutex);
    tag = emu_global->opengl_taint;
    pthread_mutex_unlock(&emu_global->mutex);

    return tag;
}

void emu_set_opengl_taint(uint32_t tag) {
    pthread_mutex_lock(&emu_global->mutex);
    emu_global->opengl_taint = tag;
    pthread_mutex_unlock(&emu_global->mutex);
}

/**
 * Check if an address is in the stack region or not
 *
 * emu_global->stack_base is determined from the process memory maps
 * in /proc/self/maps. stack_base is the lowest address of the stack
 * region. It is calculated as the end of the [stack] region - rlimit
 * of the stack.
 * Deciding if an address is on the stack is important because taints
 * for variables that are on the stack are saved in a different taintmap
 *
 * @return bool Any address greater than the stack_base is in the stack
 *              region. Any address smaller than the stack_base is
 *              presumably in the library region.
 */
inline bool
stack_addr(uint32_t addr) {
    return (addr > emu_global->stack_base);
}


/**
 * Given an address in the process's AS, return the taintmap_t instance
 *
 * There are 2 instances of taintmap_t in emu_global->taintmaps[]:
 * TAINTMAP_LIB and TAINTMAP_STACK. Based on stack_addr(addr), this
 * function returns the pointer to the taintmap_t in emu_global->taintmaps.
 *
 * @param addr 32-bit memory address
 *
 * @return taintmap_t *
 */
inline taintmap_t *
emu_get_taintmap(uint32_t addr) {

    emu_assert(addr > 0);

    uint8_t idx = stack_addr(addr) ? TAINTMAP_STACK : TAINTMAP_LIB;
    taintmap_t *tm = &emu_global->taintmaps[idx];

    if (tm->data == NULL || tm->start == 0) {
        emu_abort("taintmap uninitialized\n");
    }

    emu_assert(addr >= tm->start && addr < tm->end);

    return tm;
}


/**
 * Get how many taints there are on the page that contains addr
 *
 */
inline taintcount_t *
emu_get_taintcount_quick(uint32_t addr) {

    taintmap_t *tm = emu_get_taintmap(addr);

    uint32_t idx = (addr - tm->start) >> PAGE_SHIFT;

    uint32_t page_header = align_page(addr);
    emu_assert(idx == (page_header - tm->start) >> PAGE_SHIFT);

    return &tm->taint_count[idx];

}

// assuming taintmap lock is held by caller so no atomic needed
void
emu_update_taintpage(uint32_t page, int16_t increment) {
    if (increment == 0) return;
    taintcount_t *tp = emu_get_taintcount_quick(page);

    emu_assert(!(*tp == TAINTPAGE_SIZE && increment > 0));
    if (*tp == 0 && increment) {
        emu_assert(increment > 0);
        emu_log_debug("Placing SandTrap protections on page: %08x\n", page);
        syscall(__NR_sandtrapsetmemprot, (unsigned long) align_page(page), ENABLE_SANDTRAP_PROTECTION);
    } else if (*tp + increment == 0) {
        emu_log_debug("Removing SandTrap protections from page: %08x\n", page);
        syscall(__NR_sandtrapsetmemprot, (unsigned long) align_page(page), DISABLE_SANDTRAP_PROTECTION);
    }

    emu_assert(!(*tp == 0 && increment == -1));
    // TODO: atomic update global taintpages (track 0->1 and 1->0 page counts)
    *tp += increment;
    emu_assert(*tp <= TAINTPAGE_SIZE);
}


// taintmaps are dumped in ranges for compact output
uint32_t
emu_dump_taintmaps() {
    // validation only

    emu_log_debug("dumping taintmaps...\n");
    uint32_t idx, offset;
    taintmap_t *tm;
    uint32_t ranges = 0;
    for (idx = TAINTMAP_LIB; idx < MAX_TAINTMAPS; idx++) {
        tm = &emu_global->taintmaps[idx];

        uint8_t  range_inside = 0;
        uint32_t range_start  = 0;
        uint32_t range_end    = 0;
        uint32_t range_tag    = TAINT_CLEAR;
        // FIXME: assuming ranges have the same tags but we don't currently enforce it
        // ultimately we want ranges to be split by tag

        emu_assert(tm->start == align_page(tm->start));

        uint32_t page_idx;
        // for each taint page
        for (page_idx = 0; page_idx < (tm->bytes >> PAGE_SHIFT); page_idx++) {
            taintcount_t count = tm->taint_count[page_idx];
            if (count) { // tainted page
                emu_assert(count <= TAINTPAGE_SIZE);
                uint32_t page_addr = (page_idx * PAGE_SIZE) >> 2; /* divide by taint granularity (word) */
                // for each taint word (1024) on taint page
                for (offset = page_addr; offset < page_addr + TAINTPAGE_SIZE; offset++) {
                    uint32_t tag = tm->data[offset];
                    if (tag != TAINT_CLEAR) {
                        if (!range_inside) {
                            range_inside = 1;
                            range_start  = offset;
                            range_tag    = tag;
                        }
                    } else { // if (tag == TAINT_CLEAR)
                        // end range if we were inside a range
                        if (range_inside) {
                            emu_assert(offset > 0);
                            range_end = offset;
                            ranges++;

                            // convert ranges offset to original word addresses
                            range_start = tm->start + range_start * sizeof(uint32_t);
                            range_end   = tm->start + range_end   * sizeof(uint32_t);
                            LOGI("taint range %2d: %s start: %x end: %x length: %5d tag: %x\n",
                                 ranges,
                                 (idx == TAINTMAP_LIB) ? "lib  " : "stack",
                                 range_start, range_end, range_end - range_start, range_tag
                                );

                            range_tag = TAINT_CLEAR;
                            range_inside = range_start = range_end = 0;
                        }
                    }
                }
            }
        }
        // BUG: when only a single page is tainted, the range_inside check above is
        // never performed and hence not unset, which will trigger the emu_assert
        // emu_assert(range_inside == 0);
    }
    // summary of taint pages
    emu_dump_taintpages();
    return ranges;
}


uint32_t
emu_dump_taintpages() {
    emu_log_debug("dumping taintpages...\n");
    uint32_t pages = 0;
    uint32_t idx;
    taintmap_t *tm;
    for (idx = TAINTMAP_LIB; idx < MAX_TAINTMAPS; idx++) {
        tm = &emu_global->taintmaps[idx];
        emu_assert(tm->start == (tm->start & PAGE_MASK));

        uint32_t page_idx;
        // for each taint page
        for (page_idx = 0; page_idx < (tm->bytes >> PAGE_SHIFT); page_idx++) {
            taintcount_t count = tm->taint_count[page_idx];
            if (count) { // tainted page
                emu_assert(count <= TAINTPAGE_SIZE);
                uint32_t page_addr = tm->start + page_idx * PAGE_SIZE;
                LOGI("taint page %x count: %4d words", page_addr, count);
                pages++;
            }
        }
    }
    return pages;
}

void handle_func_taint(emu_thread_t *emu, int func_id) {
    switch(func_id) {

    case GetVersion_ID:
    case DefineClass_ID:
    case FindClass_ID:
    case FromReflectedMethod_ID:
    case FromReflectedField_ID:
    case ToReflectedMethod_ID:
    case GetSuperclass_ID:
    case IsAssignableFrom_ID:
    case ToReflectedField_ID:
    case Throw_ID:
    case ThrowNew_ID:
    case ExceptionOccurred_ID:
    case ExceptionDescribe_ID:
    case ExceptionClear_ID:
    case FatalError_ID:
    case PushLocalFrame_ID:
    case PopLocalFrame_ID:
    case NewGlobalRef_ID:
    case DeleteGlobalRef_ID:
    case DeleteLocalRef_ID:
    case IsSameObject_ID:
    case NewLocalRef_ID:
    case EnsureLocalCapacity_ID:
    case AllocObject_ID:
    case NewObject_ID:
    case NewObjectV_ID:
    case NewObjectA_ID:
    case GetObjectClass_ID:
    case IsInstanceOf_ID:
    case GetMethodID_ID:
        break;

    case GetStringChars_ID:
        break;

    case NewStringUTF_ID:
    case GetStringUTFLength_ID:
    case GetStringUTFChars_ID:
    case ReleaseStringUTFChars_ID:
        break;

    case GetArrayLength_ID:
    case NewObjectArray_ID:
    case GetObjectArrayElement_ID:
    case SetObjectArrayElement_ID:
        break;

    case GetBooleanArrayElements_ID:
    case GetByteArrayElements_ID:
    case GetCharArrayElements_ID:
    case GetShortArrayElements_ID:
    case GetIntArrayElements_ID:
    case GetLongArrayElements_ID:
    case GetFloatArrayElements_ID:
    case GetDoubleArrayElements_ID:
        break;

    case ReleaseBooleanArrayElements_ID:
    case ReleaseByteArrayElements_ID:
    case ReleaseCharArrayElements_ID:
    case ReleaseShortArrayElements_ID:
    case ReleaseIntArrayElements_ID:
    case ReleaseLongArrayElements_ID:
    case ReleaseFloatArrayElements_ID:
    case ReleaseDoubleArrayElements_ID:
        break;

    case CallObjectMethod_ID:
    case CallObjectMethodV_ID:
    case CallObjectMethodA_ID:
    case CallBooleanMethod_ID:
    case CallBooleanMethodV_ID:
    case CallBooleanMethodA_ID:
    case CallByteMethod_ID:
    case CallByteMethodV_ID:
    case CallByteMethodA_ID:
    case CallCharMethod_ID:
    case CallCharMethodV_ID:
    case CallCharMethodA_ID:
    case CallShortMethod_ID:
    case CallShortMethodV_ID:
    case CallShortMethodA_ID:
    case CallIntMethod_ID:
    case CallIntMethodV_ID:
    case CallIntMethodA_ID:
    case CallLongMethod_ID:
    case CallLongMethodV_ID:
    case CallLongMethodA_ID:
    case CallFloatMethod_ID:
    case CallFloatMethodV_ID:
    case CallFloatMethodA_ID:
    case CallDoubleMethod_ID:
    case CallDoubleMethodV_ID:
    case CallDoubleMethodA_ID:
        if (emu->jni_return_taint_set == true) {
            if (emu->jni_return_taint_length == 8) {
                emu->taintreg[EMU_ARM_REG_R1] = emu->jni_return_taint_tag;
            }
            emu->taintreg[EMU_ARM_REG_R0] = emu->jni_return_taint_tag;
            emu->jni_return_taint_set = false;
        }
        break;

    case GetObjectField_ID:
    case GetBooleanField_ID:
    case GetByteField_ID:
    case GetCharField_ID:
    case GetShortField_ID:
    case GetIntField_ID:
    case GetLongField_ID:
    case GetFloatField_ID:
    case GetDoubleField_ID:
        if (emu->jni_return_taint_set == true) {
            if (emu->jni_return_taint_length == 8) {
                emu->taintreg[EMU_ARM_REG_R1] = emu->jni_return_taint_tag;
            }
            emu->taintreg[EMU_ARM_REG_R0] = emu->jni_return_taint_tag;
            emu->jni_return_taint_set = false;
        }
        break;

    case GetPrimitiveArrayCritical_ID:
    case ReleasePrimitiveArrayCritical_ID:
        break;

    case calloc_ID:
    case malloc_ID:
    case realloc_ID: // TODO: Investigate if realloc needs taint tracking
    case free_ID:
    case emu_trampoline_read_ID:
    case emu_trampoline_write_ID:
    case emu_get_taint_array_ID:
    case emu_set_taint_array_ID:
    case glAttachShader_ID:
    case glBindAttribLocation_ID:
    case glBindTexture_ID:
    case glCompileShader_ID:
    case glCreateProgram_ID:
    case glCreateShader_ID:
    case glGenTextures_ID:
    case glGetError_ID:
    case glGetProgramiv_ID:
    case glGetShaderiv_ID:
    case glLinkProgram_ID:
    case glReadPixels_ID:
    case glShaderSource_ID:
    case glTexImage2D_ID:
    case glTexParameteri_ID:
        break;

    default:
        emu_log_always("Function-level tainting not implemented for intercepted function with ID %d.\n", func_id);
        break;
    }
}

void handle_reg_to_reg_taint(emu_thread_t *emu, int8_t source1, int8_t source2, int8_t source3, int8_t dest1, int8_t dest2, int8_t dest3) {
    uint32_t taint_label = 0;

    if (source3 != -1) {
        taint_label |= emu->taintreg[source3];
        taint_label |= emu->taintreg[source2];
        taint_label |= emu->taintreg[source1];
    } else if (source2 != -1) {
        taint_label |= emu->taintreg[source2];
        taint_label |= emu->taintreg[source1];
    } else if (source1 != -1) {
        taint_label |= emu->taintreg[source1];
    }

    if (dest3 != -1) {
        emu->taintreg[dest3] = taint_label;
        emu->taintreg[dest2] = taint_label;
        emu->taintreg[dest1] = taint_label;
    } else if (dest2 != -1) {
        emu->taintreg[dest2] = taint_label;
        emu->taintreg[dest1] = taint_label;
    } else if (dest1 != -1) {
        emu->taintreg[dest1] = taint_label;
    }

}

void handle_extreg_to_extreg_taint(emu_thread_t *emu, int8_t ext_reg_dest, int8_t q_reg_dest, int8_t ext_reg_src1, int8_t q_reg_src1, int8_t ext_reg_src2, int8_t q_reg_src2) {

    uint32_t taint_label = 0;

    if (ext_reg_src2 != -1) {
        taint_label |= emu->taintextreg[ext_reg_src2];
        if (q_reg_src2 == 1) {
            taint_label |= emu->taintextreg[ext_reg_src2 + 1];
        }

        taint_label |= emu->taintextreg[ext_reg_src1];
        if (q_reg_src1 == 1) {
            taint_label |= emu->taintextreg[ext_reg_src1 + 1];
        }

    } else if (ext_reg_src1 != -1) {
        taint_label |= emu->taintextreg[ext_reg_src1];
        if (q_reg_src1 == 1) {
            taint_label |= emu->taintextreg[ext_reg_src1 + 1];
        }
    }

    emu->taintextreg[ext_reg_dest] = taint_label;
    if (q_reg_dest == 1) {
        emu->taintextreg[ext_reg_dest+1] = taint_label;
    }
}

void handle_reg_to_extreg_taint(emu_thread_t *emu, int8_t ext_reg_dest, int8_t q_reg_dest, int8_t reg_src, bool add_taint) {

    uint32_t taint_label = 0;

    if (add_taint) {
        taint_label = emu->taintextreg[ext_reg_dest];
    }

    if (reg_src != -1) {
        taint_label |= emu->taintreg[reg_src];
    }

    emu->taintextreg[ext_reg_dest] = taint_label;
    if (q_reg_dest == 1) {
        emu->taintextreg[ext_reg_dest+1] = taint_label;
    }
}

void handle_two_reg_to_extreg_taint(emu_thread_t *emu, int8_t ext_reg_dest, int8_t q_reg_dest, int8_t reg_src1, int8_t reg_src2) {

    uint32_t taint_label = 0;

    if (reg_src1 != -1) {
        taint_label |= emu->taintreg[reg_src1];
    }

    if (reg_src2 != -1) {
        taint_label |= emu->taintreg[reg_src2];
    }

    emu->taintextreg[ext_reg_dest] = taint_label;
    if (q_reg_dest == 1) {
        emu->taintextreg[ext_reg_dest+1] = taint_label;
    }
}


void handle_extreg_to_reg_taint(emu_thread_t *emu, int8_t reg_dest, int8_t ext_reg_src, int8_t q_reg_src) {

    uint32_t taint_label = 0;

    if (ext_reg_src != -1) {
        taint_label |= emu->taintextreg[ext_reg_src];
        if (q_reg_src == 1) {
            taint_label |= emu->taintextreg[ext_reg_src + 1];
        }
    }

    emu->taintreg[reg_dest] = taint_label;
}

void handle_extreg_to_two_reg_taint(emu_thread_t *emu, int8_t reg_dest1, int8_t reg_dest2, int8_t ext_reg_src, int8_t q_reg_src) {

    uint32_t taint_label = 0;

    if (ext_reg_src != -1) {
        taint_label |= emu->taintextreg[ext_reg_src];
        if (q_reg_src == 1) {
            taint_label |= emu->taintextreg[ext_reg_src + 1];
        }
    }

    emu->taintreg[reg_dest1] = taint_label;
    emu->taintreg[reg_dest2] = taint_label;
}


void handle_mem_to_reg_taint(emu_thread_t *emu, uint32_t address, bool is_up, bool is_pre_indexed, int8_t dest1, int8_t dest2, int8_t dest3, int8_t rn, int8_t rm) {

    uint32_t index_reg_label = 0;
    uint32_t taint_label = 0;
    uint32_t taint_address = address;
    int8_t offset = is_up ? 4 : -4;

    index_reg_label |= emu->taintreg[rn];
    if (rm != -1) index_reg_label |= emu->taintreg[rm];

    if (dest3 != -1) {
        taint_label = emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu->taintreg[dest3] = taint_label;
        taint_address += offset;

        taint_label = emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu->taintreg[dest2] = taint_label;
        taint_address += offset;

        taint_label = emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu->taintreg[dest1] = taint_label;

    } else if (dest2 != -1) {

        taint_label = emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu->taintreg[dest2] = taint_label;
        taint_address += offset;

        taint_label = emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu->taintreg[dest1] = taint_label;

    } else {

        taint_label = emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu->taintreg[dest1] = taint_label;

    }
}


void handle_mem_to_single_reg_taint(emu_thread_t *emu, uint32_t address, int8_t dest1, int8_t rn, int8_t rm) {

    uint32_t index_reg_label = 0;
    uint32_t taint_label = 0;
    uint32_t taint_address = address;

    index_reg_label |= emu->taintreg[rn];
    if (rm != -1) index_reg_label |= emu->taintreg[rm];

    taint_label = emu_get_taint_mem(emu, taint_address);
    taint_label |= index_reg_label;
    emu->taintreg[dest1] = taint_label;
}

void handle_mem_to_single_extreg_taint(emu_thread_t *emu, uint32_t address, uint8_t ext_reg_dest, uint8_t q_reg, uint8_t s_reg, int8_t rn) {

    uint32_t taint_label = 0;
    taint_label = emu_get_taint_mem(emu, address);
    taint_label |= emu->taintreg[rn];

    emu->taintextreg[ext_reg_dest] = taint_label;
    if (q_reg == 1) {
        emu->taintextreg[ext_reg_dest + 1] = taint_label;
    }
}

void handle_single_extreg_to_mem_taint(emu_thread_t *emu, uint32_t address, uint8_t ext_reg_dest, uint8_t q_reg, uint8_t s_reg, int8_t rn) {
    uint32_t taint_label = 0;
    taint_label |= emu->taintextreg[ext_reg_dest];
    taint_label |= emu->taintreg[rn];
    if (q_reg == 1) {
        taint_label |= emu->taintextreg[ext_reg_dest + 1];
    }

    emu_set_taint_mem(emu, address, taint_label);
}

void handle_mem_to_reglist_taint(emu_thread_t *emu, uint32_t address, bool is_up, bool is_pre_indexed, uint16_t reglist, int8_t rn) {

    uint32_t index_reg_label = emu->taintreg[rn];
    uint32_t taint_label = 0;
    uint32_t taint_address = address;

    // LDM:   is_up == true , is_pre_indexed == false, start_address = R[n]
    // LDMDA: is_up == false, is_pre_indexed == false, start_address = R[n] - 4*BitCount(registers) + 4
    // LDMDB: is_up == false, is_pre_indexed == true,  start_address = R[n] - 4*BitCount(registers)
    // LDMIB: is_up == true , is_pre_indexed == true,  start_address = R[n] + 4

    int i = 0;
    if (is_up == true) {

        if (is_pre_indexed == true) {
            taint_address += 4;
        }

        for (i=0; i<15; i++) {
            if ((reglist & (1 << i)) == 0) {
                continue;
            }

            taint_label = emu_get_taint_mem(emu, taint_address);
            taint_label |= index_reg_label;
            emu->taintreg[i] = taint_label;
            taint_address += 4;
        }
    } else {

        if (is_pre_indexed == false) {
            taint_address += 4;
        }

        for (i=15; i>= 0; i--) {
            if ((reglist & (1 << i)) == 0) {
                continue;
            }
            taint_address -= 4;

            if (i == 15) {
                // Not going to taint PC!
                continue;
            }

            taint_label = emu_get_taint_mem(emu, taint_address);
            taint_label |= index_reg_label;
            emu->taintreg[i] = taint_label;
        }
    }
}

void handle_reg_to_mem_taint(emu_thread_t *emu, bool add_taint, uint32_t address, bool is_up, bool is_pre_indexed, int8_t src1, int8_t src2, int8_t src3, int8_t rn, int8_t rm) {

    uint32_t index_reg_label = 0;
    uint32_t taint_label = 0;
    uint32_t taint_address = address;
    int8_t offset = is_up ? 4 : -4;

    index_reg_label |= emu->taintreg[rn];
    if (rm != -1) index_reg_label |= emu->taintreg[rm];

    if (src3 != -1) {
        taint_label = emu->taintreg[src3];
        if (add_taint) taint_label |= emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu_set_taint_mem(emu, taint_address, taint_label);
        taint_address += offset;

        taint_label = emu->taintreg[src2];
        if (add_taint) taint_label |= emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu_set_taint_mem(emu, taint_address, taint_label);
        taint_address += offset;

        taint_label = emu->taintreg[src1];
        if (add_taint) taint_label |= emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu_set_taint_mem(emu, taint_address, taint_label);

    } else if (src2 != -1) {

        taint_label = emu->taintreg[src2];
        if (add_taint) taint_label |= emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu_set_taint_mem(emu, taint_address, taint_label);
        taint_address += offset;

        taint_label = emu->taintreg[src1];
        if (add_taint) taint_label |= emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu_set_taint_mem(emu, taint_address, taint_label);

    } else if (src1 != -1) {
        taint_label = emu->taintreg[src1];
        if (add_taint) taint_label |= emu_get_taint_mem(emu, taint_address);
        taint_label |= index_reg_label;
        emu_set_taint_mem(emu, taint_address, taint_label);
    }

}

void handle_reglist_to_mem_taint(emu_thread_t *emu, bool add_taint, uint32_t address, bool is_up, bool is_pre_indexed, uint16_t reglist, int8_t rn) {

    uint32_t index_reg_label = emu->taintreg[rn];
    uint32_t taint_label = 0;
    uint32_t taint_address = address;

    // STM:   is_up == true , is_pre_indexed == false, start_address = R[n]
    // STMDA: is_up == false, is_pre_indexed == false, start_address = R[n] - 4*BitCount(registers) + 4
    // STMDB: is_up == false, is_pre_indexed == true,  start_address = R[n] - 4*BitCount(registers)
    // STMIB: is_up == true , is_pre_indexed == true,  start_address = R[n] + 4

    int i = 0;
    if (is_up == true) {

        if (is_pre_indexed == true) {
            taint_address += 4;
        }

        for (i=0; i<15; i++) {
            if ((reglist & (1 << i)) == 0) {
                continue;
            }

            taint_label = emu->taintreg[i];
            taint_label |= index_reg_label;
            emu_set_taint_mem(emu, taint_address, taint_label);
            taint_address += 4;
        }
    } else {

        if (is_pre_indexed == false) {
            taint_address += 4;
        }

        for (i=15; i>= 0; i--) {
            if ((reglist & (1 << i)) == 0) {
                continue;
            }
            taint_address -= 4;

            taint_label = emu->taintreg[i];
            taint_label |= index_reg_label;
            emu_set_taint_mem(emu, taint_address, taint_label);
        }
    }
}

void handle_taint_prop(emu_thread_t *emu, int block_id) {

    if (emu->taint_prop_off == true || emu->taint_handler_off == true) {
        return;
    }

    taint_prop_record_t *tprop_record = &emu->tprop_record;
    taint_chain_t *taint_chain = tprop_record->taint_chains[block_id];
    uint32_t *taint_info_addr = taint_chain->starting_block;

    uint32_t header_word = 0;
    uint32_t footer_word = 0;
    bool done = false;

    while (!done) {
        READ_AND_INCREMENT(taint_info_addr, header_word);
        if (header_word == 0) {
            // This happens when the basic block has instructions with
            // no taint propagation information, or when we reach the
            // end.
            done = true;
            continue;
        }

        taint_direction_t taint_dir = READ_HEADER_TAINT_DIRECTION(header_word);

        if (taint_dir == LINK_NEW_BLOCK) {
            uint32_t new_block_addr = 0;
            READ_AND_INCREMENT(taint_info_addr, new_block_addr);
            taint_info_addr = (uint32_t*)new_block_addr;
            continue;
        } else if (taint_dir == FUNC_LEVEL_TAINT) {
            uint32_t func_id = 0;
            func_id = (header_word & 0xffff);
            handle_func_taint(emu, func_id);
            continue;
        }

        emu_arm_instr_cc cc = READ_HEADER_CC(header_word);
        mem_access_type_t mem_type = READ_HEADER_MEM_ACCESS(header_word);

        uint32_t cpsr_word = 0;
        bool perform_tp = true;

        if (cc != EMU_ARM_CC_AL) {
            READ_AND_INCREMENT(taint_info_addr, cpsr_word);
            CPU(curr_app_ctx, cpsr) = cpsr_word;
            perform_tp = is_cond_pass(emu, cc);
        }

        if (taint_dir == REG_TO_REG) {
            uint32_t source_registers = 0;
            READ_AND_INCREMENT(taint_info_addr, source_registers);

            if (!perform_tp) {
                continue;
            }

            uint8_t num_source_regs = 0;
            int8_t source1 = -1, source2 = -1, source3 = -1;
            num_source_regs = (source_registers & 0xF000) >> 12;

            if (num_source_regs >= 4) {
                emu_abort("Unsupported amount of source registers (%d) while handling reg-to-reg taint prop.\n", num_source_regs);
            } else if (num_source_regs == 3) {
                source3 = (source_registers & 0xF00) >> 8;
                source2 = (source_registers & 0xF0) >> 4;
                source1 = (source_registers & 0xF);
            } else if (num_source_regs == 2) {
                source2 = (source_registers & 0xF0) >> 4;
                source1 = (source_registers & 0xF);
            } else if (num_source_regs == 1) {
                source1 = (source_registers & 0xF);
            }

            uint8_t num_dest_regs = READ_HEADER_NUM_REGISTERS(header_word);
            int8_t dest1 = -1, dest2 = -1, dest3 = -1;

            if (num_dest_regs >= 4) {
                emu_abort("Unsupported amount of dest registers (%d) while handling reg-to-reg taint prop.\n", num_dest_regs);
            } else if (num_dest_regs == 3) {
                dest3 = READ_HEADER_REGISTER_INDEX(header_word, 2);
                dest2 = READ_HEADER_REGISTER_INDEX(header_word, 1);
                dest1 = READ_HEADER_REGISTER_INDEX(header_word, 0);
            } else if (num_dest_regs == 2) {
                dest2 = READ_HEADER_REGISTER_INDEX(header_word, 1);
                dest1 = READ_HEADER_REGISTER_INDEX(header_word, 0);
            } else if (num_dest_regs == 1) {
                dest1 = READ_HEADER_REGISTER_INDEX(header_word, 0);
            }

            handle_reg_to_reg_taint(emu, source1, source2, source3, dest1, dest2, dest3);
            continue;
        } else if (taint_dir == EXTREG_TO_EXTREG) {
            uint32_t source_ext_regs_word = 0;
            READ_AND_INCREMENT(taint_info_addr, source_ext_regs_word);

            uint8_t num_dest_ext_regs = 0;
            int8_t ext_reg_dest = 0 , q_reg_dest = 0, s_reg_dest = 0;
            num_dest_ext_regs = READ_HEADER_NUM_EXT_REGISTERS(header_word);
            READ_HEADER_EXT_REGISTER_INDEX(header_word, ext_reg_dest, q_reg_dest, s_reg_dest, 0);

            if (num_dest_ext_regs != 1) {
                emu_abort("Unexpected number of destination extended registers (%d) when handling extreg-to-extreg taint prop.\n",
                          num_dest_ext_regs);
            }

            uint8_t num_source_ext_regs = 0;
            int8_t ext_reg_src1 = -1, q_reg_src1 = 0, s_reg_src1 = 0;
            int8_t ext_reg_src2 = -1, q_reg_src2 = 0, s_reg_src2 = 0;
            num_source_ext_regs = (source_ext_regs_word & 0xc000) >> 14;

            if (num_source_ext_regs > 2) {
                emu_abort("Unexpected number of source extended registers (%d) when handling extreg-to-extreg taint prop.\n",
                          num_source_ext_regs);
            }

            if (num_source_ext_regs == 1) {
                ext_reg_src1 = (source_ext_regs_word & 0x1F);
                q_reg_src1 = (source_ext_regs_word & 0x40) >> 6;
            } else if (num_source_ext_regs == 2) {

                ext_reg_src1 = (source_ext_regs_word & 0x1F);
                q_reg_src1 = (source_ext_regs_word & 0x40) >> 6;

                ext_reg_src2 = (source_ext_regs_word & 0xF80) >> 7;
                q_reg_src2 = (source_ext_regs_word & 0x2000) >> 13;
            }
            // We are not passing in s_reg value because we are
            // maintaing 32-bit taint labels for each 64-bit extended
            // register. If we maintain two 32-bit labels for each
            // 64-bit register, then we certainly need to take into
            // account the s_reg value.
            handle_extreg_to_extreg_taint(emu, ext_reg_dest, q_reg_dest, ext_reg_src1, q_reg_src1, ext_reg_src2, q_reg_src2);

            continue;
        } else if (taint_dir == REG_TO_EXTREG) {
            uint32_t source_regs_word = 0;
            READ_AND_INCREMENT(taint_info_addr, source_regs_word);

            uint8_t num_dest_ext_regs = 0;
            int8_t ext_reg_dest = 0 , q_reg_dest = 0, s_reg_dest = 0;
            num_dest_ext_regs = READ_HEADER_NUM_EXT_REGISTERS(header_word);
            READ_HEADER_EXT_REGISTER_INDEX(header_word, ext_reg_dest, q_reg_dest, s_reg_dest, 0);

            if (num_dest_ext_regs != 1) {
                emu_abort("Unexpected number of destination extended registers (%d) when handling reg-to-extreg taint prop.\n",
                          num_dest_ext_regs);
            }

            uint8_t num_source_regs = 0;
            int8_t reg_src1 = -1;
            num_source_regs = ((source_regs_word & 0x3000) >> 12);
            if (num_source_regs > 2) {
                emu_abort("Unexpected number of source registers (%d) when handling reg-to-extreg taint prop.\n",
                          num_source_regs);
            }

            // XXX: In some REG_TO_EXTREG instructions, we're only
            // replacing part of the extended register. Hence, we'll
            // have to add to its taint label. See the taint rule
            // generation comments for EMU_INST_VFP_VMOV_CORE_SCAL,
            // for example.
            bool add_taint = false;
            if ((source_regs_word & 0x80000000) != 0) {
                add_taint = true;
            }

            reg_src1 = (source_regs_word & 0xF);
            if (num_source_regs == 1) {
                handle_reg_to_extreg_taint(emu, ext_reg_dest, q_reg_dest, reg_src1, add_taint);
            } else {
                int8_t reg_src2 = ((source_regs_word & 0xF0) >> 4);
                handle_two_reg_to_extreg_taint(emu, ext_reg_dest, q_reg_dest, reg_src1, reg_src2);
            }

            continue;
        } else if (taint_dir == EXTREG_TO_REG) {
            uint32_t dest_regs_word = 0;
            READ_AND_INCREMENT(taint_info_addr, dest_regs_word);

            uint8_t num_source_ext_regs = 0;
            int8_t ext_reg_src = -1, q_reg_src = 0, s_reg_src = 0;
            num_source_ext_regs = READ_HEADER_NUM_EXT_REGISTERS(header_word);
            if (num_source_ext_regs != 1) {
                emu_abort("Unexpected number of source extended registers (%d) when handling extreg-to-reg taint prop.\n",
                          num_source_ext_regs);
            }
            READ_HEADER_EXT_REGISTER_INDEX(header_word, ext_reg_src, q_reg_src, s_reg_src, 0);

            uint8_t num_dest_regs = 0;
            int8_t reg_dest1 = -1;

            num_dest_regs = ((dest_regs_word & 0x3000) >> 12);
            if (num_dest_regs > 2) {
                emu_abort("Unexpected number of dest registers (%d) when handling extreg-to-reg taint prop.\n",
                          num_dest_regs);
            }
            reg_dest1 = (dest_regs_word & 0xF);
            if (num_dest_regs == 1) {
                handle_extreg_to_reg_taint(emu, reg_dest1, ext_reg_src, q_reg_src);
            } else {
                int8_t reg_dest2 = ((dest_regs_word & 0xF0) >> 4);
                handle_extreg_to_two_reg_taint(emu, reg_dest1, reg_dest2, ext_reg_src, q_reg_src);
            }
            continue;
        }

        if (taint_dir != REG_TO_MEM && taint_dir != MEM_TO_REG && taint_dir != MEM_TO_EXTREG && taint_dir != EXTREG_TO_MEM) {
            emu_abort("Unsupported taint direction (%d) in block %d.\n", taint_dir, block_id);
        }

        uint32_t rn_val = 0;
        uint32_t rm_val = 0;
        uint32_t final_address = 0;

        int8_t rn_number = -1;
        int8_t rm_number = -1;

        bool add_taint = false;
        uint16_t footer_imm = 0;
        bool is_up = false;
        bool is_pre_indexed = false;
        emu_arm_shift_type shift_type = 0;
        uint32_t source_taint_label = 0;

        uint8_t num_regs = READ_HEADER_NUM_REGISTERS(header_word);
        int8_t reg1 = -1, reg2 = -1, reg3 = -1;

        if (taint_dir == REG_TO_MEM || taint_dir == MEM_TO_REG) {
            HANDLE_REG_TO_MEM_AND_MEM_TO_REG_TAINT_PROP;
        } else if (taint_dir == MEM_TO_EXTREG || taint_dir == EXTREG_TO_MEM) {
            HANDLE_EXT_REG_TO_MEM_AND_MEM_TO_EXT_REG_TAINT_PROP;
        }
    }

#ifdef TEST_TOOLS
    if (emu->end_taint_prop == true) {
        emu->taint_handler_off = true;
    }
#endif // TEST_TOOLS
}

#ifdef TEST_TOOLS

void emu_test_set_reg_labels(uint32_t core_reg_lbls[]) {
    emu_thread_t *emu = emu_tls_get();
    emu->taintreg[EMU_ARM_REG_R0] = core_reg_lbls[EMU_ARM_REG_R0];
    emu->taintreg[EMU_ARM_REG_R1] = core_reg_lbls[EMU_ARM_REG_R1];
    emu->taintreg[EMU_ARM_REG_R2] = core_reg_lbls[EMU_ARM_REG_R2];
    emu->taintreg[EMU_ARM_REG_R3] = core_reg_lbls[EMU_ARM_REG_R3];
    emu->taintreg[EMU_ARM_REG_R4] = core_reg_lbls[EMU_ARM_REG_R4];
    emu->taintreg[EMU_ARM_REG_R5] = core_reg_lbls[EMU_ARM_REG_R5];
    emu->taintreg[EMU_ARM_REG_R6] = core_reg_lbls[EMU_ARM_REG_R6];
    emu->taintreg[EMU_ARM_REG_R7] = core_reg_lbls[EMU_ARM_REG_R7];
    emu->taintreg[EMU_ARM_REG_R8] = core_reg_lbls[EMU_ARM_REG_R8];
    emu->taintreg[EMU_ARM_REG_R9] = core_reg_lbls[EMU_ARM_REG_R9];
    emu->taintreg[EMU_ARM_REG_R10] = core_reg_lbls[EMU_ARM_REG_R10];
    emu->taintreg[EMU_ARM_REG_R11] = core_reg_lbls[EMU_ARM_REG_R11];
}

void emu_test_get_reg_labels(uint32_t core_reg_lbls[]) {
    emu_thread_t *emu = emu_tls_get();
    core_reg_lbls[EMU_ARM_REG_R0] = emu->taintreg[EMU_ARM_REG_R0];
    core_reg_lbls[EMU_ARM_REG_R1] = emu->taintreg[EMU_ARM_REG_R1];
    core_reg_lbls[EMU_ARM_REG_R2] = emu->taintreg[EMU_ARM_REG_R2];
    core_reg_lbls[EMU_ARM_REG_R3] = emu->taintreg[EMU_ARM_REG_R3];
    core_reg_lbls[EMU_ARM_REG_R4] = emu->taintreg[EMU_ARM_REG_R4];
    core_reg_lbls[EMU_ARM_REG_R5] = emu->taintreg[EMU_ARM_REG_R5];
    core_reg_lbls[EMU_ARM_REG_R6] = emu->taintreg[EMU_ARM_REG_R6];
    core_reg_lbls[EMU_ARM_REG_R7] = emu->taintreg[EMU_ARM_REG_R7];
    core_reg_lbls[EMU_ARM_REG_R8] = emu->taintreg[EMU_ARM_REG_R8];
    core_reg_lbls[EMU_ARM_REG_R9] = emu->taintreg[EMU_ARM_REG_R9];
    core_reg_lbls[EMU_ARM_REG_R10] = emu->taintreg[EMU_ARM_REG_R10];
    core_reg_lbls[EMU_ARM_REG_R11] = emu->taintreg[EMU_ARM_REG_R11];
}

#endif // TEST_TOOLS
