#ifndef INCLUDE_EMU_THREAD_H
#define INCLUDE_EMU_THREAD_H

#include <inttypes.h>
#include <sys/ptrace.h>         /* PSR bit macros */
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>

#include "mambo/dbm.h"

#include "emu_common.h"
#include "emu_pie_decoder.h"
#include "emu_taint_prop_types.h"

#define N_REGS 18
#define N_EXT_REGS 32
#define EXT_REG_SIZE_BYTES 8 /* Size of each extended register in bytes */

// NOTE: 4-byte aligned 0x1006 will produce 0x1004 and not 0x1008
// this is be design: we want the word that contains input value
// resulting in 4 bytes [0x1004:0x1007]
#define Align(x,a)            __ALIGN_MASK(x,(typeof(x))(a)-1)
#define __ALIGN_MASK(x,mask)  ((x)&~(mask))

#define CPU(ctx, reg) (emu->ctx.uc_mcontext.arm_##reg)

/*
CPSR bits
N: Negative result
Z: Zero result
C: Carry from operation
V: oVerflowed operation

I: IRQ
T: Thumb mode
*/
#define CPSR_N ((CPU(curr_app_ctx, cpsr) & PSR_N_BIT) >> 31)
#define CPSR_Z ((CPU(curr_app_ctx, cpsr) & PSR_Z_BIT) >> 30)
#define CPSR_C ((CPU(curr_app_ctx, cpsr) & PSR_C_BIT) >> 29)
#define CPSR_V ((CPU(curr_app_ctx, cpsr) & PSR_V_BIT) >> 28)
#define CPSR_I ((CPU(curr_app_ctx, cpsr) & PSR_I_BIT) >>  7)
#define CPSR_T ((CPU(curr_app_ctx, cpsr) & PSR_T_BIT) >>  5)

#define UCONTEXT_MCONTEXT 20
#define UCONTEXT_SIGMASK  (UCONTEXT_MCONTEXT + 84)
#define UCONTEXT_REGSPACE (UCONTEXT_SIGMASK  +  4 + 31*4)
// note the +8 is to skip over the magic and size fields
// within vfp_sigframe to get directly to vfp_sigframe.user_vfp.fpregs[32]
#define UCONTEXT_FPREGS   (UCONTEXT_REGSPACE +  8)

#define MCONTEXT_ARM_R0   (UCONTEXT_MCONTEXT +  3*4)
#define MCONTEXT_ARM_R1   (MCONTEXT_ARM_R0   +  1*4)
#define MCONTEXT_ARM_R4   (MCONTEXT_ARM_R0   +  4*4)
#define MCONTEXT_ARM_SP   (MCONTEXT_ARM_R0   + 13*4)
#define MCONTEXT_ARM_LR   (MCONTEXT_ARM_R0   + 14*4)
#define MCONTEXT_ARM_PC   (MCONTEXT_ARM_R0   + 15*4)
#define MCONTEXT_ARM_CPSR (MCONTEXT_ARM_R0   + 16*4)


// #include <linux/user.h>         /* user_vfp, user_vfp_exc */
#include "vfp.h"                /* copy of $KERNEL/arch/arm/include/asm/vfp.h */

#ifdef WITH_VFP_D32
#define NUM_VFP_REGS 32         /* VFPv3 ARMv7-A Cortex-A9 */
#else
#define NUM_VFP_REGS 16
#endif

// value from $KERNEL/arch/arm/include/asm/ucontext.h
#define VFP_MAGIC	0x56465001

/*
 * 8 byte for magic and size, 264 byte for ufp, 12 bytes for ufp_exc,
 * 4 bytes padding.
 */
#define VFP_STORAGE_SIZE sizeof(struct vfp_sigframe)

/*
 * User specific VFP registers. If only VFPv2 is present, registers 16 to 31
 * are ignored by the ptrace system call and the signal handler.
 */
struct user_vfp {
	unsigned long long fpregs[32];
	unsigned long fpscr;
};

/*
 * VFP exception registers exposed to user space during signal delivery.
 * Fields not relavant to the current VFP architecture are ignored.
 */
struct user_vfp_exc {
	unsigned long	fpexc;
	unsigned long	fpinst;
	unsigned long	fpinst2;
};

struct vfp_sigframe
{
	unsigned long		magic;
	unsigned long		size;
	struct user_vfp		ufp;
	struct user_vfp_exc	ufp_exc;
} __attribute__((__aligned__(8)));


/*
 * Auxiliary signal frame.  This saves stuff like FP state.
 * The layout of this structure is not part of the user ABI,
 * because the config options aren't.  uc_regspace is really
 * one of these.
 */
struct aux_sigframe {
	struct vfp_sigframe	vfp;
	/* Something that isn't a valid magic number for any coprocessor.  */
	unsigned long		end_magic;
} __attribute__((__aligned__(8)));


typedef struct emu_thread {
    ucontext_t  curr_app_ctx;            /* the context of the thread being emulated */
    pid_t       tid;                     /* the id of the thread being emulated */
    uint32_t   *regs;                    /* easy access to ucontext regs */
    uint64_t   *ext_regs;                /* easy access to extended regs */
    uint32_t    taintreg[N_REGS];        /* taint storage for regs */
    uint32_t    taintextreg[N_EXT_REGS]; /* taint storage for extended regs (used by NEON/VFPv3) */

#ifdef TEST_TOOLS
    uint32_t    saved_regs[N_REGS];
    bool        end_taint_prop;
#endif // TEST_TOOLS

    emu_disassem_t disassem;

    bool        taint_prop_off;          /* indicates if taint propagation is turned off for this thread */
    bool        taint_handler_off;       /* indicates if the taint handler should just return immediately */
    bool        ignore_protections_on_exit;
    bool        running;                 /* set to true if emulator is currently running */

    bool        jni_return_taint_set;
    uint32_t    jni_return_taint_tag;
    uint8_t     jni_return_taint_length; /* Size of the tainted return value in bytes */

    taintmap_t  *heap_tm;
    taintmap_t  *stack_tm;
    uint32_t    stack_base;

    dbm_thread  *dbm_thread_data;
    taint_prop_record_t tprop_record;
    uint32_t    scratch_lr;

    char        last_status[512];

} __attribute__((aligned(4096))) emu_thread_t;

/* stolen from bionic/libc/private/bionic_tls.h */
#define __get_tls() \
    ({ register unsigned int __val asm("r0");              \
       asm ("mrc p15, 0, r0, c13, c0, 3" : "=r"(__val) );  \
       (volatile void*)__val; })


/* use the next free slot in bionic_tls to stash emu state*/
#define TLS_SLOT_EMU_THREAD 5
emu_thread_t *emu_tls_get();
void emu_tls_set(emu_thread_t *emu);

void dbg_dump_ucontext(ucontext_t *, emu_thread_t *emu);
void dbg_dump_ucontext_vfp(ucontext_t *);
void emu_dump_cpsr(emu_thread_t *emu);

uint8_t emu_thumb_mode(emu_thread_t *);

#endif // INCLUDE_EMU_THREAD_H
