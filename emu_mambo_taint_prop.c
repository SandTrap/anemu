#include <sys/mman.h>

#include "emu_mambo_taint_prop.h"

#ifdef TEST_TOOLS
#include "emu_markers.h"
#endif // TEST_TOOLS

extern void handle_taint_prop(emu_thread_t *emu, int block_id);

#define COPY_SP_TO_MEM(EMU, CTX, ADDR) {                                \
        if (in_thumb_mode == false) {                                   \
            arm_copy_sp_to_mem(EMU, CTX, ADDR);                         \
        } else {                                                        \
            thumb_copy_sp_to_mem(EMU, CTX, ADDR);                       \
        }                                                               \
    }

#define COPY_REG_TO_MEM(EMU, CTX, ADDR, REG) {                          \
        if (in_thumb_mode == false) {                                   \
            arm_copy_reg_to_mem(EMU, CTX, ADDR, REG);                   \
        } else {                                                        \
            thumb_copy_reg_to_mem(EMU, CTX, ADDR, REG);                 \
        }                                                               \
    }

void arm_copy_sp_to_mem(emu_thread_t *emu, mambo_context *ctx, uint32_t *addr) {

    emit_arm_push(ctx, 0x0003);
    emit_arm_copy_to_reg_32bit(ctx, r0, (uint32_t)addr);
    emit_arm_mov(ctx, REG_PROC, 0, r1, sp);
    emit_arm_add(ctx, 1, 0, r1, r1, 8); // Add 8 because we pushed R0 and R1, and modified SP in the process.
    emit_arm_str(ctx, 0, r1, r0, 0, 0, 0, 0);
    emit_arm_pop(ctx, 0x0003);
}

void thumb_copy_sp_to_mem(emu_thread_t *emu, mambo_context *ctx, uint32_t *addr) {

    emit_thumb_push(ctx, 0x0003);
    emit_thumb_copy_to_reg_32bit(ctx, r0, (uint32_t)addr);

    // Note that what we really want to do is "R1 = SP + 8", and we do
    // the "+ 8" because we pushed R0 and R1 on the stack, thereby
    // modifying SP in the process. The instruction below does "Rd =
    // SP + imm32", where "imm32 = ZeroExtend(imm8:'00')". Since imm8
    // is shifted left two times when imm32 is calculated, when we
    // specify imm8 below, we have to shift it right by two times.
    emit_thumb_add_sp_pc_16(ctx, 1, r1, (8 >> 2));
    emit_thumb_stri16(ctx, 0, r0, r1);
    emit_thumb_pop(ctx, 0x0003);
}

void arm_copy_reg_to_mem(emu_thread_t *emu, mambo_context *ctx, uint32_t *addr, emu_arm_reg reg) {
    if (reg == EMU_ARM_REG_SP) {
        emu_abort("[arm_copy_reg_to_mem]: Do not try to copy SP from arm_copy_reg_to_mem. Use arm_copy_sp_to_mem instead.\n");
    } else if (reg == EMU_ARM_REG_PC) {
        emu_abort("[arm_copy_reg_to_mem]: Do not try to copy PC from arm_copy_reg_to_mem. Determine what PC is during scanning time and then just copy it to the taint block directly.\n");
    }

    emu_arm_reg scratch_reg = r0;
    if (reg == r0) {
        scratch_reg = r1;
    }

    emit_arm_push(ctx, (1 << scratch_reg));
    emit_arm_copy_to_reg_32bit(ctx, scratch_reg, (uint32_t)addr);
    emit_arm_str(ctx, 0, reg, scratch_reg, 0, 0, 0, 0);
    emit_arm_pop(ctx, (1 << scratch_reg));
}

void thumb_copy_reg_to_mem(emu_thread_t *emu, mambo_context *ctx, uint32_t *addr, emu_arm_reg reg) {

    if (reg == EMU_ARM_REG_SP) {
        emu_abort("[%s]: Do not try to copy SP from arm_copy_reg_to_mem. Use arm_copy_sp_to_mem instead.\n", __func__);
    } else if (reg == EMU_ARM_REG_PC) {
        emu_abort("[%s]: Do not try to copy PC from arm_copy_reg_to_mem. Determine what PC is during scanning time and then just copy it to the taint block directly.\n", __func__);
    }

    emu_arm_reg scratch_reg = r0;
    if (reg == r0) {
        scratch_reg = r1;
    }

    emit_thumb_push(ctx, (1 << scratch_reg));
    emit_thumb_copy_to_reg_32bit(ctx, scratch_reg, (uint32_t)addr);
    emit_thumb_stri32(ctx, 0, 1, scratch_reg, reg, 0);
    emit_thumb_pop(ctx, (1 << scratch_reg));

    return;
}

void arm_copy_cpsr_to_mem(emu_thread_t *emu, mambo_context *ctx, uint32_t *addr) {

    emit_arm_push(ctx, 0x0003);
    emit_arm_copy_to_reg_32bit(ctx, r0, (uint32_t)addr);
    emit_arm_mrs(ctx, r1);
    emit_arm_str(ctx, 0, r1, r0, 0, 0, 0, 0);
    emit_arm_pop(ctx, 0x0003);
}

void emu_write_arm_taint_call(emu_thread_t *emu, mambo_context *ctx) {

    int block_id = mambo_get_fragment_id(ctx);

    emit_arm_push(ctx, 0x520f);
    emit_arm_push_cpsr(ctx, r0);

    emit_arm_copy_to_reg_32bit(ctx, r0, (uint32_t)emu);
    emit_arm_copy_to_reg_32bit(ctx, r1, (uint32_t)block_id);

    emit_arm_fcall(ctx, &handle_taint_prop);

    emit_arm_pop_cpsr(ctx, r0);
    emit_arm_pop(ctx, 0x520f);
}

void emu_write_thumb_taint_call(emu_thread_t *emu, mambo_context *ctx) {

    int block_id = mambo_get_fragment_id(ctx);

    emit_thumb_push(ctx, 0x520f);
    emit_thumb_push_cpsr(ctx, r0);

    emit_thumb_copy_to_reg_32bit(ctx, r0, (uint32_t)emu);
    emit_thumb_copy_to_reg_32bit(ctx, r1, (uint32_t)block_id);

    emit_thumb_fcall(ctx, &handle_taint_prop);

    emit_thumb_pop_cpsr(ctx, r0);
    emit_thumb_pop(ctx, 0x520f);
}

void emu_compute_taint_prop(emu_thread_t *emu, mambo_context *ctx, emu_disassem_t *d) {
    int block_id = mambo_get_fragment_id(ctx);
    taint_prop_record_t *tprop_record = &emu->tprop_record;
    bool in_thumb_mode = tprop_record->in_thumb_mode;

    if (in_thumb_mode) {
        // Because IT blocks are special and can be a little
        // complicated to deal with, we have to watch out for them
        // first here before doing anything else. At the moment, we
        // are not going to do any taint propagation for instructions
        // in the IT block.

        if (tprop_record->in_it_block == true) {

            if (tprop_record->num_it_inst == 0) {
                tprop_record->in_it_block = false;
            } else {
                tprop_record->num_it_inst -= 1;

                if (d->inst == EMU_INST_IT) {
                    emu_abort("Encountered IT instruction while in IT block. Something is wrong.\n");
                }

                return;
            }
        } else {
            if (d->inst == EMU_INST_IT) {

                // The decoder places the mask of the IT instruction in
                // the imm field.
                uint8_t mask_3 = (d->imm & 0x8) >> 3;
                uint8_t mask_2 = (d->imm & 0x4) >> 2;
                uint8_t mask_1 = (d->imm & 0x2) >> 1;
                uint8_t mask_0 = (d->imm & 0x1);
                if (mask_0) {
                    tprop_record->num_it_inst = 4;
                } else if (mask_1) {
                    tprop_record->num_it_inst = 3;
                } else if (mask_2) {
                    tprop_record->num_it_inst = 2;
                } else if (mask_3) {
                    tprop_record->num_it_inst = 1;
                }

                tprop_record->in_it_block = true;

                return;
            }
        }
    }

    if (d->is_branch == true) {
        // A branch will not perform any taint propagation unless it's
        // a "pop" that includes the PC.
        if (d->inst != EMU_INST_LDM) {
            return;
        }
    }

    if (tprop_record->in_ldrex_block) {
        // TODO: LDREX-STREX blocks are tricky. Let's ignore them for
        // now.
        if (d->inst == EMU_INST_STREX) {
            // If we see an STREX, we've reached the end of the ldrex
            // block.
            tprop_record->in_ldrex_block = false;
        }
        return;
    } else if (d->inst == EMU_INST_LDREX) {
        tprop_record->in_ldrex_block = true;
        return;
    }

    // If the instruction format is deliberately set to
    // INS_UNTRANSLATED, it means no taint propagation are needed for
    // these instructions.
    if (d->format == INS_UNTRANSLATED) {

#ifdef TEST_TOOLS
        if (d->inst == EMU_INST_UDF) {
            uint32_t *read_address = (uint32_t*)mambo_get_source_addr(ctx);
            if (*read_address == MARKER_END_TAINT_PROP) {
                mambo_replace_inst(ctx);
                emu->end_taint_prop = true;
            }
        }
#endif // TEST_TOOLS

        if (d->inst != EMU_INST_BKPT) {
            // BKPTs should only be encountered because MAMBO is
            // scanning our intercept function trampoline and since we
            // have function-level taint propagation now, we will
            // handle BKPT separately below.
            return;
        }
    }

    uint32_t *taint_block_addr = tprop_record->taint_block_addr;
    uint32_t header_word = 0;

    switch (d->inst) {

    case EMU_INST_BKPT: {

            // It's bad if we find a BKPT but it's not because we're
            // intercepting a function call.
            if (tprop_record->intercept_triggered == false) {
                emu_abort("BKPT encountered but intercept not triggered!");
            }

            // Function level taints will use a custom header word
            header_word = 0;
            header_word |= (FUNC_LEVEL_TAINT << HEADER_TAINT_DIRECTION_OFFSET);
            header_word |= tprop_record->intercept_func->id;
            WRITE_AND_INCREMENT(taint_block_addr, header_word);

            break;
    }

    case EMU_INST_LDR:
    case EMU_INST_LDRB:
    case EMU_INST_LDRD:
    case EMU_INST_LDRH:
    case EMU_INST_LDRSB:
    case EMU_INST_LDRSH: {


        if (d->Rt.reg == EMU_ARM_REG_PC) {
            print_emu_disassem(d);
            int pie_inst = mambo_get_inst(ctx);
            emu_abort("LDR with PC found, but we should not be doing taint prop for it! (in_thumb_mode: %d, pie_inst: %d)",
                      in_thumb_mode, pie_inst);
        }

        uint32_t footer_word = 0;

        WRITE_HEADER_REGISTER_INDEX(header_word, d->Rt.reg, 0);

        if (d->inst == EMU_INST_LDRD) {
            WRITE_HEADER_REGISTER_INDEX(header_word, d->Rt2.reg, 1);
            WRITE_HEADER_NUM_REGISTERS(header_word, 2);
        } else {
            WRITE_HEADER_NUM_REGISTERS(header_word, 1);
        }

        if (d->format == INS_RT_RN_IMM ||
            d->format == INS_RT_RT2_RN_IMM) {

            WRITE_HEADER_SET_MEM(header_word, MEM_RN_IMM);
            WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_REG, d->cond);

            if (d->Rn.reg == EMU_ARM_REG_PC) {
                uint32_t pc = (uint32_t)mambo_get_source_addr(ctx);
                if (in_thumb_mode == false) {
                    pc += 8;
                } else {
                    pc += 4;
                }
                *taint_block_addr = pc;
            } else if (d->Rn.reg == EMU_ARM_REG_SP) {
                COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
            } else {
                COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
            }
            INCREMENT(taint_block_addr);

            if (d->imm > 0xFFFF) {
                emu_abort("LDR. immediate value (%d) greater than 0xFFFF.\n", d->imm);
            }

            WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
            WRITE_FOOTER_PREPOSTINDEX(footer_word, d->is_pre_indexed);
            WRITE_FOOTER_IMM_VALUE(footer_word, d->imm);
            WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
            WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

            break;
        } else if (d->format == INS_RT_RN_RM_SHIFT ||
                   d->format == INS_RT_RT2_RN_RM) {

            WRITE_HEADER_SET_MEM(header_word, MEM_RN_RM_SHIFT);
            WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_REG, d->cond);

            if (d->Rn.reg == EMU_ARM_REG_PC) {
                uint32_t pc = (uint32_t)mambo_get_source_addr(ctx);
                if (in_thumb_mode == false) {
                    pc += 8;
                } else {
                    pc += 4;
                }
                *taint_block_addr = pc;
            } else if (d->Rn.reg == EMU_ARM_REG_SP) {
                emu_abort("LDR, LDRB. Did not implement copying SP yet for Rn.\n");
            } else {
                COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
            }
            INCREMENT(taint_block_addr);

            if (d->Rm.reg == EMU_ARM_REG_PC) {
                uint32_t pc = (uint32_t)mambo_get_source_addr(ctx);
                if (in_thumb_mode == false) {
                    pc += 8;
                } else {
                    pc += 4;
                }
                *taint_block_addr = pc;
            } else if (d->Rm.reg == EMU_ARM_REG_SP) {
                emu_abort("LDR, LDRB. Did not implement copying SP yet for Rm.\n");
            } else {
                COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rm.reg);
            }
            INCREMENT(taint_block_addr);

            WRITE_FOOTER_SHIFT_TYPE(footer_word, d->Rm.shift_type);
            WRITE_FOOTER_IMM_VALUE(footer_word, d->Rm.shift_value);
            WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
            WRITE_FOOTER_PREPOSTINDEX(footer_word, d->is_pre_indexed);
            WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
            WRITE_FOOTER_RM_NUMBER(footer_word, d->Rm.reg);
            WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);
            break;
        }

        emu_abort("Instruction (%d) in LDR class has an unsupported format (%d).\n.",
                  d->inst, d->format);
    }

    case EMU_INST_STR:
    case EMU_INST_STRB:
    case EMU_INST_STRD:
    case EMU_INST_STRH: {

        uint32_t footer_word = 0;
        WRITE_HEADER_REGISTER_INDEX(header_word, d->Rt.reg, 0);

        if (d->inst == EMU_INST_STRD) {
            WRITE_HEADER_REGISTER_INDEX(header_word, d->Rt2.reg, 1);
            WRITE_HEADER_NUM_REGISTERS(header_word, 2);
        } else {
            WRITE_HEADER_NUM_REGISTERS(header_word, 1);
        }

        if (d->format == INS_RT_RN_IMM ||
            d->format == INS_RT_RT2_RN_IMM) {

            WRITE_HEADER_SET_MEM(header_word, MEM_RN_IMM);
            WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, REG_TO_MEM, d->cond);

            if (d->Rn.reg == EMU_ARM_REG_PC) {
                emu_abort("strb type unimplemented\n");
            } else if (d->Rn.reg == EMU_ARM_REG_SP) {
                COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
            } else {
                COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
            }
            INCREMENT(taint_block_addr);

            if (d->inst == EMU_INST_STRB) {
                WRITE_FOOTER_SET_ADD_TAINT(footer_word);
            }

            WRITE_FOOTER_IMM_VALUE(footer_word, d->imm);
            WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
            WRITE_FOOTER_PREPOSTINDEX(footer_word, d->is_pre_indexed);
            WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
            WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

            break;
        } else if (d->format == INS_RT_RN_RM_SHIFT ||
                   d->format == INS_RT_RT2_RN_RM) {
            WRITE_HEADER_SET_MEM(header_word, MEM_RN_RM_SHIFT);
            WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, REG_TO_MEM, d->cond);

            if (d->Rn.reg == EMU_ARM_REG_PC) {
                uint32_t pc = (uint32_t)mambo_get_source_addr(ctx);
                if (in_thumb_mode == false) {
                    pc += 8;
                } else {
                    pc += 4;
                }
                *taint_block_addr = pc;
            } else if (d->Rn.reg == EMU_ARM_REG_SP) {
                COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
            } else {
                COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
            }
            INCREMENT(taint_block_addr);

            if (d->Rm.reg == EMU_ARM_REG_PC) {
                uint32_t pc = (uint32_t)mambo_get_source_addr(ctx);
                if (in_thumb_mode == false) {
                    pc += 8;
                } else {
                    pc += 4;
                }
                *taint_block_addr = pc;
            } else if (d->Rm.reg == EMU_ARM_REG_SP) {
                emu_abort("STR, STRB, STRBH. Did not implement copying SP yet for Rm.\n");
            } else {
                COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rm.reg);
            }
            INCREMENT(taint_block_addr);

            if (d->inst == EMU_INST_STRB) {
                WRITE_FOOTER_SET_ADD_TAINT(footer_word);
            }

            WRITE_FOOTER_SHIFT_TYPE(footer_word, d->Rm.shift_type);
            WRITE_FOOTER_IMM_VALUE(footer_word, d->Rm.shift_value);
            WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
            WRITE_FOOTER_PREPOSTINDEX(footer_word, d->is_pre_indexed);
            WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
            WRITE_FOOTER_RM_NUMBER(footer_word, d->Rm.reg);
            WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

            break;
        }

        emu_abort("Instruction (%d) in STR class has an unsupported format (%d).\n.",
                  d->inst, d->format);
    }

    case EMU_INST_LDM:
    case EMU_INST_STM: {

        uint32_t footer_word = 0;

        WRITE_HEADER_SET_MEM(header_word, MEM_RN_REGLIST);
        WRITE_HEADER_REGLIST(header_word, d->reglist);

        if (d->inst == EMU_INST_LDM) {
            WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_REG, d->cond);
        } else if (d->inst == EMU_INST_STM) {
            WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, REG_TO_MEM, d->cond);
        }

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("LDM with pc unimplemented.");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }

        INCREMENT(taint_block_addr);

        WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
        WRITE_FOOTER_PREPOSTINDEX(footer_word, d->is_pre_indexed);
        WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
        WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

        break;
    }

    case EMU_INST_ADC:
    case EMU_INST_ADD:
    case EMU_INST_AND:
    case EMU_INST_BFI:
    case EMU_INST_BIC:
    case EMU_INST_CLZ:
    case EMU_INST_EOR:
    case EMU_INST_MLA:
    case EMU_INST_MLS:
    case EMU_INST_MOV:
    case EMU_INST_MOVT:
    case EMU_INST_MOVW:
    case EMU_INST_MUL:
    case EMU_INST_MVN:
    case EMU_INST_ORR:
    case EMU_INST_PKH:
    case EMU_INST_RBIT:
    case EMU_INST_REV:
    case EMU_INST_REV16:
    case EMU_INST_RRX:
    case EMU_INST_RSB:
    case EMU_INST_RSC:
    case EMU_INST_SBC:
    case EMU_INST_SBFX:
    case EMU_INST_SMLABB:
    case EMU_INST_SMLAWB:
    case EMU_INST_SMLAWT:
    case EMU_INST_SMULBB:
    case EMU_INST_SMULWB:
    case EMU_INST_SMULWT:
    case EMU_INST_SUB:
    case EMU_INST_SXTAH:
    case EMU_INST_SXTB:
    case EMU_INST_SXTH:
    case EMU_INST_UBFX:
    case EMU_INST_USAT:
    case EMU_INST_USAT16:
    case EMU_INST_UXTAB:
    case EMU_INST_UXTAH:
    case EMU_INST_UXTB:
    case EMU_INST_UXTH: {

        WRITE_HEADER_REGISTER_INDEX(header_word, d->Rd.reg, 0);
        WRITE_HEADER_NUM_REGISTERS(header_word, 1);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, REG_TO_REG, d->cond);

        uint32_t source_word = 0;
        uint8_t num_source_regs = 0;
        uint8_t source1 = 0;
        uint8_t source2 = 0;
        uint8_t source3 = 0;

        switch (d->format) {

        case INS_RD_CONST:
            // Leave the source word blank. It'll mean that the
            // destination registers will have their taint cleared.
            break;

        case INS_RD_RM:
            source1 = d->Rm.reg;
            num_source_regs = 1;
            break;

        case INS_RD_RN_RM:
        case INS_RD_RN_RM_SHIFT:
            source1 = d->Rm.reg;
            source2 = d->Rn.reg;
            num_source_regs = 2;
            break;

        case INS_RD_RN_CONST:
        case INS_RD_RN_LSB_WIDTH:
        case INS_RD_IMM_RN:
            source1 = d->Rn.reg;
            num_source_regs = 1;
            break;

        case INS_RD_RN_RM_TYPE_RS:
            source1 = d->Rn.reg;
            source2 = d->Rm.reg;
            source3 = d->Rs.reg;
            num_source_regs = 3;
            break;

        case INS_RD_RN_RM_RA:
            source1 = d->Rn.reg;
            source2 = d->Rm.reg;
            source3 = d->Ra.reg;
            num_source_regs = 3;
            break;

        default:
            emu_abort("Instruction (%d) in reg-to-reg class has an unsupported format (%d).\n.",
                      d->inst, d->format);
        }


        source_word = (num_source_regs << 12);
        switch (num_source_regs) {
        case 3:
            source_word |= (source3 << 8);
        case 2:
            source_word |= (source2 << 4);
        case 1:
            source_word |= source1;
        case 0:
            break;
        default:
            emu_abort("Instruction (%d, format: %d) in reg-to-reg class has unsupported amount of source registers (%d).\n",
                      d->inst, d->format, num_source_regs);
        }

        WRITE_AND_INCREMENT(taint_block_addr, source_word);
        break;
    }

    case EMU_INST_SMLAL:
    case EMU_INST_SMULL:
    case EMU_INST_UMLAL:
    case EMU_INST_UMULL: {

        if (d->format != INS_RDLO_RDHI_RN_RM) {
            emu_abort("[emu_compute_taint_prop] Instruction (%d, format: %d) does not have format of INS_RDLO_RDHI_RN_RM.\n",
                      d->inst, d->format);
        }

        WRITE_HEADER_REGISTER_INDEX(header_word, d->Rdlo.reg, 0);
        WRITE_HEADER_REGISTER_INDEX(header_word, d->Rdhi.reg, 1);
        WRITE_HEADER_NUM_REGISTERS(header_word, 2);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, REG_TO_REG, d->cond);

        uint8_t num_source_regs = 2;
        uint32_t source_word = 0;

        source_word = (num_source_regs << 12);
        source_word |= (d->Rn.reg << 4);
        source_word |= d->Rm.reg;

        WRITE_AND_INCREMENT(taint_block_addr, source_word);

        break;
    }

    case EMU_INST_NEON_VABAL:
    case EMU_INST_NEON_VABDL:
    case EMU_INST_NEON_VABS:
    case EMU_INST_NEON_VADDL:
    case EMU_INST_NEON_VADDW:
    case EMU_INST_NEON_VADD_I:
    case EMU_INST_NEON_VAND:
    case EMU_INST_NEON_VCEQZ:
    case EMU_INST_NEON_VCEQ_I:
    case EMU_INST_NEON_VCGT_I:
    case EMU_INST_NEON_VCLTZ:
    case EMU_INST_NEON_VCLZ:
    case EMU_INST_NEON_VDUP_SCAL:
    case EMU_INST_NEON_VEOR:
    case EMU_INST_NEON_VEXT:
    case EMU_INST_NEON_VMAX_I:
    case EMU_INST_NEON_VMLAL_I:
    case EMU_INST_NEON_VMLAL_SCAL:
    case EMU_INST_NEON_VMLA_I:
    case EMU_INST_NEON_VMLSL_SCAL:
    case EMU_INST_NEON_VMOVI:
    case EMU_INST_NEON_VMOVL:
    case EMU_INST_NEON_VMOVN:
    case EMU_INST_NEON_VMULL_I:
    case EMU_INST_NEON_VMULL_SCAL:
    case EMU_INST_NEON_VMUL_I:
    case EMU_INST_NEON_VMUL_SCAL:
    case EMU_INST_NEON_VMVNI:
    case EMU_INST_NEON_VNEG:
    case EMU_INST_NEON_VORR:
    case EMU_INST_NEON_VPADAL:
    case EMU_INST_NEON_VPADDL:
    case EMU_INST_NEON_VPADD_I:
    case EMU_INST_NEON_VPMAX_I:
    case EMU_INST_NEON_VQDMULH_I:
    case EMU_INST_NEON_VQMOVUN:
    case EMU_INST_NEON_VQRSHRN:
    case EMU_INST_NEON_VREV32:
    case EMU_INST_NEON_VREV64:
    case EMU_INST_NEON_VRHADD:
    case EMU_INST_NEON_VRSHRN:
    case EMU_INST_NEON_VSHL:
    case EMU_INST_NEON_VSHLI:
    case EMU_INST_NEON_VSHLL:
    case EMU_INST_NEON_VSHR:
    case EMU_INST_NEON_VSHRN:
    case EMU_INST_NEON_VSUBL:
    case EMU_INST_NEON_VSUB_I:
    case EMU_INST_NEON_VTRN:
    case EMU_INST_NEON_VTST:
    case EMU_INST_VFP_VABS:
    case EMU_INST_VFP_VADD:
    case EMU_INST_VFP_VCVT_DP_SP:
    case EMU_INST_VFP_VCVT_F_I:
    case EMU_INST_VFP_VDIV:
    case EMU_INST_VFP_VMLA_F:
    case EMU_INST_VFP_VMLS_F:
    case EMU_INST_VFP_VMOV:
    case EMU_INST_VFP_VMOVI:
    case EMU_INST_VFP_VMUL_F:
    case EMU_INST_VFP_VNEG:
    case EMU_INST_VFP_VNMLS:
    case EMU_INST_VFP_VSUB_F: {


        WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vd.reg, d->Vd.q_reg, d->Vd.s_reg, 0);
        WRITE_HEADER_NUM_EXT_REGISTERS(header_word, 1);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, EXTREG_TO_EXTREG, d->cond);

        uint32_t source_word = 0;

        uint8_t num_source_regs = 0;
        uint8_t source1 = 0;
        uint8_t source2 = 0;

        switch (d->format) {
        case INS_V_VD_IMM:
            // Leave the source word blank. It'll mean that the
            // desination registers will have their taint cleared.
            break;

        case INS_V_VD_VM:
        case INS_V_VD_VM_CONST:
            source1 = d->Vm.reg;
            source1 |= (d->Vm.q_reg << 6);
            source1 |= (d->Vm.s_reg << 5);

            num_source_regs = 1;
            break;

        case INS_V_VD_VN_VM:
            source1 = d->Vn.reg;
            source1 |= (d->Vn.q_reg << 6);
            source1 |= (d->Vn.s_reg << 5);

            source2 = d->Vm.reg;
            source2 |= (d->Vm.q_reg << 6);
            source2 |= (d->Vm.s_reg << 5);

            num_source_regs = 2;
            break;
        default:
            emu_abort("Instruction (%d) in extreg-to-extreg class has an unsupported format (%d).\n.",
                      d->inst, d->format);
        }

        source_word = (num_source_regs << 14);
        switch (num_source_regs) {
        case 2:
            source_word |= (source2 << 7);
        case 1:
            source_word |= source1;
        case 0:
            break;
        default:
            emu_abort("Instruction (%d, format: %d) in extreg-to-extreg class has unsupported amount of source registers (%d).\n",
                      d->inst, d->format, num_source_regs);
        }

        WRITE_AND_INCREMENT(taint_block_addr, source_word);
        break;
    }

    case EMU_INST_NEON_VDUP_CORE:
    case EMU_INST_VFP_VMOV_2CORE_DP:
    case EMU_INST_VFP_VMOV_CORE_SCAL:
    case EMU_INST_VFP_VMOV_CORE_SP:
    case EMU_INST_VFP_VMOV_SCAL_CORE: {

        taint_direction_t taint_dir = -1;
        uint8_t num_regs = 1;
        uint32_t regs_word = 0;
        regs_word = d->Rt.reg;

        switch (d->format) {

        case INS_V_VN_RT:
            taint_dir = REG_TO_EXTREG;
            WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vn.reg, d->Vn.q_reg, d->Vn.s_reg, 0);
            break;

        case INS_V_VD_RT:
            taint_dir = REG_TO_EXTREG;
            WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vd.reg, d->Vd.q_reg, d->Vd.s_reg, 0);
            break;

        case INS_V_RT_VN:
            taint_dir = EXTREG_TO_REG;
            WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vn.reg, d->Vn.q_reg, d->Vn.s_reg, 0);
            break;

        case INS_V_RT_RT2_VM:
            taint_dir = EXTREG_TO_REG;
            WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vm.reg, d->Vm.q_reg, d->Vm.s_reg, 0);

            num_regs = 2;
            regs_word |= (d->Rt2.reg << 4);
            break;

        case INS_V_VM_RT_RT2:
            taint_dir = REG_TO_EXTREG;
            WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vm.reg, d->Vm.q_reg, d->Vm.s_reg, 0);

            num_regs = 2;
            regs_word |= (d->Rt2.reg << 4);
            break;

        default:
            print_emu_disassem(d);
            emu_abort("Unexpected format (%d) in reg-to-extreg class.\n", d->format);
        }

        WRITE_HEADER_NUM_EXT_REGISTERS(header_word, 1);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, taint_dir, d->cond);

        regs_word |= (num_regs << 12);

        if ((d->inst == EMU_INST_VFP_VMOV_CORE_SCAL) ||
            (d->inst == EMU_INST_VFP_VMOV_CORE_SP)) {
            // XXX: This is an ugly hack. In these instruction, we
            // will always only change part of the destination
            // extended register. Hence, we'll have to add to the
            // taint label. Let's just flip up the MSB and read it
            // when handling the taint rule.
            regs_word |= 0x80000000;
        }

        WRITE_AND_INCREMENT(taint_block_addr, regs_word)
        break;
    }

    case EMU_INST_NEON_VLDX_M: {

        mem_access_type_t access_type = MEM_VLD1_M;
        if (d->vldx_vstx_num == 2) {
            access_type = MEM_VLD2_M;
        } else if (d->vldx_vstx_num == 3) {
            access_type = MEM_VLD3_M;
        } else if (d->vldx_vstx_num == 4) {
            access_type = MEM_VLD4_M;
        }

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);
        WRITE_EXT_CUST_REGLIST_STRIDE(cust_reglist, d->vldx_vstx_stride);
        WRITE_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist, d->v_elem_size);

        WRITE_HEADER_SET_MEM(header_word, access_type);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);

        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_EXTREG, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("EMU_INST_NEON_VLDX_M. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            emu_abort("EMU_INST_NEON_VLDX_M. Did not implement copying SP yet for Rn.\n");
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }

        INCREMENT(taint_block_addr);

        break;
    }

    case EMU_INST_NEON_VLDX_S_O: {

        mem_access_type_t access_type = MEM_VLD1_SO;
        if (d->vldx_vstx_num == 2) {
            access_type = MEM_VLD2_SO;
        } else if (d->vldx_vstx_num == 3) {
            access_type = MEM_VLD3_SO;
        } else if (d->vldx_vstx_num == 4) {
            access_type = MEM_VLD4_SO;
        }

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);
        WRITE_EXT_CUST_REGLIST_STRIDE(cust_reglist, d->vldx_vstx_stride);
        WRITE_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist, d->v_elem_size);

        WRITE_HEADER_SET_MEM(header_word, access_type);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_EXTREG, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("EMU_INST_NEON_VLDX_S_O. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            emu_abort("EMU_INST_NEON_VLDX_S_O. Did not implement copying SP yet for Rn.\n");
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }

        INCREMENT(taint_block_addr);
        break;
    }

    case EMU_INST_NEON_VLDX_S_A: {

        mem_access_type_t access_type = MEM_VLD1_SA;
        if (d->vldx_vstx_num == 2) {
            access_type = MEM_VLD2_SA;
        } else if (d->vldx_vstx_num == 3) {
            access_type = MEM_VLD3_SA;
        } else if (d->vldx_vstx_num == 4) {
            access_type = MEM_VLD4_SA;
        }

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);
        WRITE_EXT_CUST_REGLIST_STRIDE(cust_reglist, d->vldx_vstx_stride);
        WRITE_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist, d->v_elem_size);

        WRITE_HEADER_SET_MEM(header_word, access_type);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_EXTREG, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("EMU_INST_NEON_VLDX_S_A. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            emu_abort("EMU_INST_NEON_VLDX_S_A. Did not implement copying SP yet for Rn.\n");
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }

        if (d->format == INS_V_REGLIST_RN_RM) {
            print_emu_disassem(d);
            emu_abort("Unsupported format in EMU_INST_NEON_VLDX_S_A.\n");
        }

        INCREMENT(taint_block_addr);
        break;
    }

    case EMU_INST_NEON_VSTX_S_O: {

        mem_access_type_t access_type = MEM_VST1_SO;
        if (d->vldx_vstx_num == 2) {
            access_type = MEM_VST2_SO;
        } else if (d->vldx_vstx_num == 3) {
            access_type = MEM_VST3_SO;
        } else if (d->vldx_vstx_num == 4) {
            access_type = MEM_VST4_SO;
        }

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);
        WRITE_EXT_CUST_REGLIST_STRIDE(cust_reglist, d->vldx_vstx_stride);
        WRITE_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist, d->v_elem_size);

        WRITE_HEADER_SET_MEM(header_word, access_type);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, EXTREG_TO_MEM, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("EMU_INST_NEON_VSTX_S_O. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            emu_abort("EMU_INST_NEON_VSTX_S_O. Did not implement copying SP yet for Rn.\n");
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }

        INCREMENT(taint_block_addr);
        break;
    }

    case EMU_INST_NEON_VSTX_M: {

        mem_access_type_t access_type = MEM_VST1_M;
        if (d->vldx_vstx_num == 2) {
            access_type = MEM_VST2_M;
        } else if (d->vldx_vstx_num == 3) {
            access_type = MEM_VST3_M;
        } else if (d->vldx_vstx_num == 4) {
            access_type = MEM_VST4_M;
        }

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);
        WRITE_EXT_CUST_REGLIST_STRIDE(cust_reglist, d->vldx_vstx_stride);
        WRITE_EXT_CUST_REGLIST_ELEM_SIZE(cust_reglist, d->v_elem_size);

        WRITE_HEADER_SET_MEM(header_word, access_type);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, EXTREG_TO_MEM, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("EMU_INST_NEON_VSTX_M. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            emu_abort("EMU_INST_NEON_VSTX_M. Did not implement copying SP yet for Rn.\n");
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }

        INCREMENT(taint_block_addr);

        break;
    }

    case EMU_INST_VFP_VSTR_DP:
    case EMU_INST_VFP_VSTR_SP: {

        WRITE_HEADER_SET_MEM(header_word, MEM_RN_IMM);
        WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vd.reg, d->Vd.q_reg, d->Vd.s_reg, 0);
        WRITE_HEADER_NUM_EXT_REGISTERS(header_word, 1);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, EXTREG_TO_MEM, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("ARM_VFP_VSTR_DP. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }
        INCREMENT(taint_block_addr);

        uint32_t footer_word = 0;
        WRITE_FOOTER_IMM_VALUE(footer_word, d->imm);
        WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
        WRITE_FOOTER_PREPOSTINDEX(footer_word, d->is_pre_indexed);
        WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
        WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

        break;
    }

    case EMU_INST_VFP_VLDR_DP:
    case EMU_INST_VFP_VLDR_SP: {

        WRITE_HEADER_SET_MEM(header_word, MEM_RN_IMM);
        WRITE_HEADER_EXT_REGISTER_INDEX(header_word, d->Vd.reg, d->Vd.q_reg, d->Vd.s_reg, 0);
        WRITE_HEADER_NUM_EXT_REGISTERS(header_word, 1);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_EXTREG, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            uint32_t pc = (uint32_t)mambo_get_source_addr(ctx);
            if (in_thumb_mode == false) {
                pc += 8;
            } else {
                pc += 4;
            }
            *taint_block_addr = pc;
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }
        INCREMENT(taint_block_addr);

        uint32_t footer_word = 0;
        WRITE_FOOTER_IMM_VALUE(footer_word, d->imm);
        WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
        WRITE_FOOTER_PREPOSTINDEX(footer_word, d->is_pre_indexed);
        WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
        WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

        break;
    }

    case EMU_INST_VFP_VLDM_DP: {

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);


        WRITE_HEADER_SET_MEM(header_word, MEM_VLDM_DP);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_EXTREG, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("ARM_VFP_VLDM_DP. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }
        INCREMENT(taint_block_addr);

        uint32_t footer_word = 0;
        WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
        WRITE_FOOTER_IMM_VALUE(footer_word, d->imm);
        WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
        WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

        break;
    }

    case EMU_INST_VFP_VSTM_DP:
    case EMU_INST_VFP_VSTM_SP: {

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);

        if (d->inst == EMU_INST_VFP_VSTM_SP) {
            WRITE_HEADER_SET_MEM(header_word, MEM_VSTM_SP);
        } else {
            WRITE_HEADER_SET_MEM(header_word, MEM_VSTM_DP);
        }
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, EXTREG_TO_MEM, d->cond);

        if (d->Rn.reg == EMU_ARM_REG_PC) {
            emu_abort("ARM_VFP_VSTM_DP. Did not implement copying PC yet for Rn.\n");
        } else if (d->Rn.reg == EMU_ARM_REG_SP) {
            COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
        } else {
            COPY_REG_TO_MEM(emu, ctx, taint_block_addr, d->Rn.reg);
        }
        INCREMENT(taint_block_addr);

        uint32_t footer_word = 0;
        WRITE_FOOTER_UPDOWN(footer_word, d->is_up);
        WRITE_FOOTER_IMM_VALUE(footer_word, d->imm);
        WRITE_FOOTER_RN_NUMBER(footer_word, d->Rn.reg);
        WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

        break;
    }

    case EMU_INST_VFP_VPUSH_DP: {

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);

        WRITE_HEADER_SET_MEM(header_word, MEM_VPUSH_DP);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, EXTREG_TO_MEM, d->cond);

        COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
        INCREMENT(taint_block_addr);

        uint32_t footer_word = 0;
        WRITE_FOOTER_IMM_VALUE(footer_word, d->imm);
        WRITE_FOOTER_TO_BLOCK(ctx, taint_block_addr, footer_word);

        break;
    }

    case EMU_INST_VFP_VPOP_DP: {

        /* Use a custom encoded reglist for this instruction. */
        int8_t starting_reg = -1;
        uint8_t num_regs = 0;
        int i=0;
        for (i=0; i<32; i++) {
            if ((d->reglist & (1 << i)) != 0) {
                num_regs++;
                if (starting_reg == -1) {
                    starting_reg = i;
                }
            }
        }

        uint16_t cust_reglist = 0;
        WRITE_EXT_CUST_REGLIST_START_REG(cust_reglist, starting_reg);
        WRITE_EXT_CUST_REGLIST_NUM_REGS(cust_reglist, num_regs);

        WRITE_HEADER_SET_MEM(header_word, MEM_VPOP_DP);
        WRITE_HEADER_REGLIST(header_word, cust_reglist);
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, header_word, MEM_TO_EXTREG, d->cond);

        COPY_SP_TO_MEM(emu, ctx, taint_block_addr);
        INCREMENT(taint_block_addr);

        break;
    }

    default:
        print_emu_disassem(d);
        emu_abort("[%s] Unhandled instruction %d, taint_info-addr is %p.\n", __func__, d->inst, tprop_record->taint_block_addr);
    }

    tprop_record->taint_block_addr = taint_block_addr;

    // Ensure we have space in this current taint block to hold more
    // data. If not, we have to create a new block and link to it.
    uint32_t num_remaining = 0;
    num_remaining = (uint32_t)(tprop_record->taint_block_end_addr - tprop_record->taint_block_addr);
    if (num_remaining <= TAINT_BLOCK_GUARD_SIZE) {

        if (num_remaining < 2) {
            emu_abort("[%s] Insufficient space in current taint block to link to new block. (basic block id: %d)",
                      __func__, block_id);
        }

        taint_chain_t *taint_chain = tprop_record->taint_chains[block_id];
        uint32_t *new_taint_block = calloc(TAINT_BLOCK_SIZE, sizeof(uint32_t));

        // Write a LINK_NEW_BLOCK header and the address of the new
        // taint block into the current taint block. This will be used
        // by the taint handler to jump into the new taint block.
        uint32_t link_header = 0;
        WRITE_HEADER_TO_BLOCK(emu, ctx, taint_block_addr, link_header, LINK_NEW_BLOCK, EMU_ARM_CC_AL);
        *taint_block_addr = (uint32_t) new_taint_block;

        // Let's also write the address of the new taint block in the
        // last element of the current taint block. This is for
        // convenience.
        tprop_record->taint_block_start_addr[TAINT_BLOCK_SIZE-1] = (uint32_t) new_taint_block;

        // Okay, from now on, we'll only use the new block
        tprop_record->taint_block_addr = new_taint_block;
        tprop_record->taint_block_start_addr = new_taint_block;
        tprop_record->taint_block_end_addr = new_taint_block + TAINT_BLOCK_SIZE;
        taint_chain->num_blocks++;
    }
}

void emu_check_taint_call_point(emu_thread_t *emu, mambo_context *ctx, emu_disassem_t *d) {

    taint_prop_record_t *tprop_record = &emu->tprop_record;
    bool in_thumb_mode = tprop_record->in_thumb_mode;
    emu_intercept_func_t *intercept_func = NULL;

    if (d->inst == EMU_INST_BKPT) {

        intercept_func = tprop_record->intercept_func;

        // If we find a BKPT, it must be because we're intercepting
        // something while in ARM mode. If we find a BKPT in other
        // situations, either there's a bug (which is bad) or the
        // native function in the Android app is using bkpt (which is
        // probably bad).
        if (in_thumb_mode == true || intercept_func == NULL) {
            emu_log_always("[%s] Unexpected BKPT found in block %d (in_thumb_mode: %d, intercept null: %d).\n",
                          __func__, tprop_record->block_id, in_thumb_mode, (intercept_func == NULL));
        }

        // First copy LR into emu->scratch_lr. This is thread-safe
        // because each thread has its own instance of
        // emu_thread_t. In this sequence of instructions, we're going
        // to 1) push r0 into the stack, 2) copy the address of
        // emu->scratch_lr into r0, 3) store LR into the address
        // specified by r0, 4) restore r0.
        emit_arm_push(ctx, 0x0001);
        emit_arm_copy_to_reg_32bit(ctx, r0, (uint32_t)&emu->scratch_lr);
        emit_arm_str(ctx, 0, lr, r0, 0, 0, 0, 0);
        emit_arm_pop(ctx, 0x0001);

        if (intercept_func->put_args_in_stack == true) {
            if (intercept_func->start_stack_arg != 3) {
                // At the moment, we only expect register 3 to be
                // pushed onto the stack.
                emu_abort("Unexpected start stack arg (%d) for intercept function %s.\n",
                          intercept_func->start_stack_arg, intercept_func->name);
            }
            emit_arm_push(ctx, (1 << r3));
        }

        // Now let's call the target function directly. At this point,
        // the register and stack state should be pristine and we'll
        // be calling the target function transparently. We will muck
        // about with LR in emit_arm_fcall but that's okay.
        emit_arm_fcall(ctx, intercept_func->target_func_addr);

        if (intercept_func->put_args_in_stack == true) {
            // At the moment, we only expect register 3 to be pushed
            // onto the stack, so we only need to pop once (and we can
            // pop it into r3).
            emit_arm_pop(ctx, (1 << r3));
        }

        // Now let's restore the original value of LR from
        // emu=>scratch_lr.
        emit_arm_push(ctx, 0x0001);
        emit_arm_copy_to_reg_32bit(ctx, r0, (uint32_t)&emu->scratch_lr);
        emit_arm_ldr(ctx, 0, lr, r0, 0, 0, 0, 0);
        emit_arm_pop(ctx, 0x0001);

        // Let mambo know we've replaced the BKPT instruction.
        mambo_replace_inst(ctx);
        return;
    }

    if (d->is_branch == true) {
        if (tprop_record->in_ldrex_block) {
            tprop_record->skip_taint_call = true;
            emu_log_always("Basic block (%d) has branch in a LDREX/STREX block! Skipping taint handler ...\n", tprop_record->block_id);
        }

        if (tprop_record->in_it_block) {
            tprop_record->skip_taint_call = true;
            emu_log_always("Basic block (%d) has branch in an IT block! Skipping taint handler ...\n", tprop_record->block_id);
        }

#ifdef DBM_INLINE_UNCOND_IMM
        if (d->inst == EMU_INST_B || d->inst == EMU_INST_BL || d->inst == EMU_INST_BL32) {

            // With EMU_INST_B or EMU_INST_BL, Mambo might inline the
            // branch and continue the black scanning from the new
            // address. The logic in the Thumb scanner that determines
            // if the branch will be inlined is not
            // straightforward. Hence, we'll just set the
            // "await_inline_confirmation" flag here, and wait for the
            // scanner to call "emu_set_branch_inline" to let us know
            // if the branch will be inlined.

            tprop_record->await_inline_confirmation = true;
            return;
        }
#endif
        tprop_record->insert_taint_call = true;
    }
}

void free_taint_chain(taint_prop_record_t *tprop_record, int block_id) {
    taint_chain_t *taint_chain = tprop_record->taint_chains[block_id];
    if (taint_chain == NULL) {
        return;
    }

    // We need to go through all of the taint blocks in a taint chain
    // and free them all up. For convenience, the address of a taint
    // block B that comes after A is stored in the last element of
    // A. We can use that to traverse through all the blocks allocated
    // and free them all up.
    int j = 0;
    uint32_t *addr_to_free = NULL;
    uint32_t *next_addr_to_free = taint_chain->starting_block;
    for (j = 0; j < taint_chain->num_blocks; j++) {
        addr_to_free = next_addr_to_free;
        next_addr_to_free = (uint32_t)addr_to_free[TAINT_BLOCK_SIZE-1];
        free(addr_to_free);
    }

    free(tprop_record->taint_chains[block_id]);
    tprop_record->taint_chains[block_id] = NULL;
}

int pre_block_handler(mambo_context *ctx) {
    dbm_thread *thread_data = mambo_get_thread_data(ctx);
    emu_thread_t *emu = (emu_thread_t*)thread_data->sandtrap_handle;
    cc_type frag_type = mambo_get_fragment_type(ctx);
    taint_prop_record_t *tprop_record = &emu->tprop_record;
    int block_id = mambo_get_fragment_id(ctx);
    bool in_thumb_mode = (mambo_get_inst_type(ctx) == THUMB_INST);

    emu_assert(tprop_record->basic_block_started != true);
    tprop_record->block_id = block_id;
    tprop_record->in_thumb_mode = in_thumb_mode;
    tprop_record->await_inline_confirmation = false;

    tprop_record->basic_block_started = true;
    tprop_record->skip_taint_call = false;
    tprop_record->insert_taint_call = false;
    tprop_record->in_ldrex_block = false;
    tprop_record->in_it_block = false;
    tprop_record->num_it_inst = 0;

    if (emu->taint_prop_off == false) {

        // We expect the block ID returned by mambo to be valid indices
        // into the taint_info array because Mambo uses the IDs as indices
        // into its internal code cache array.
        if (block_id >= NUM_TAINT_CHAINS) {
            emu_abort("[%s]: block ID (%d) exceeds NUM_TAINT_CHAINS (%d) (CODE_CACHE_SIZE (%d) + TRACE_FRAGMENT_NO (%d)).\n",
                      __func__, block_id, NUM_TAINT_CHAINS, CODE_CACHE_SIZE, TRACE_FRAGMENT_NO);
        }

        if (tprop_record->taint_chains[block_id] != NULL) {
            // If we encounter a basic block for which the taint chain
            // was already allocated, it means MAMBO flushed its code
            // cache and reset the block id. Hence, we should free the
            // previously allocated taint chain. We'll create a new
            // one below for this new basic block.
            free_taint_chain(tprop_record, block_id);
        }


        taint_chain_t *taint_chain = calloc(1, sizeof(taint_chain_t));
        taint_chain->num_blocks = 1;
        taint_chain->starting_block = calloc(TAINT_BLOCK_SIZE, sizeof(uint32_t));

        tprop_record->taint_chains[block_id] = taint_chain;
        tprop_record->taint_block_addr = taint_chain->starting_block;
        tprop_record->taint_block_start_addr = taint_chain->starting_block;
        tprop_record->taint_block_end_addr = taint_chain->starting_block + TAINT_BLOCK_SIZE;
    }

    return 0;
}

int pre_instr_handler(mambo_context *ctx) {

    dbm_thread *thread_data = mambo_get_thread_data(ctx);
    emu_thread_t *emu = (emu_thread_t*)thread_data->sandtrap_handle;
    taint_prop_record_t *tprop_record = &emu->tprop_record;
    emu_assert(emu->tprop_record.basic_block_started == true);

    bool is_thumb_inst = tprop_record->in_thumb_mode;
    if (is_thumb_inst) {
        return thumb_instr_handler(emu, ctx);
    } else {
        return arm_instr_handler(emu, ctx);
    }
}

int arm_instr_handler(emu_thread_t *emu, mambo_context *ctx) {

    arm_instruction inst = mambo_get_inst(ctx);
    uint32_t *read_address = (uint32_t*)mambo_get_source_addr(ctx);
    emu_disassem_t *d = &emu->disassem;
    int block_id = mambo_get_fragment_id(ctx);
    arm_translate_to_disassem(inst, read_address, d);

    if (emu->tprop_record.insert_taint_call == true) {
        emu_abort("[%s] Scanning for new instructions in block %d even after inserting a taint call.\n", __func__, block_id);
    }

    if (emu->taint_prop_off == false) {
        emu_compute_taint_prop(emu, ctx, d);
    }

    emu_check_taint_call_point(emu, ctx, d);

    if (emu->taint_prop_off == false) {
        if ((emu->tprop_record.insert_taint_call == true) &&
            (emu->tprop_record.skip_taint_call == false)) {
            emu_write_arm_taint_call(emu, ctx);
        }
    }

    return 0;
}

int thumb_instr_handler(emu_thread_t *emu, mambo_context *ctx) {

    thumb_instruction inst = mambo_get_inst(ctx);
    uint16_t *read_address = (uint16_t*)mambo_get_source_addr(ctx);
    emu_disassem_t *d = &emu->disassem;
    int block_id = mambo_get_fragment_id(ctx);
    thumb_translate_to_disassem(inst, read_address, d);

    if (emu->tprop_record.insert_taint_call == true) {
        emu_abort("[%s] Scanning for new instructions in block %d even after inserting a taint call.\n", __func__, block_id);
    }

    if (emu->taint_prop_off == false) {
        emu_compute_taint_prop(emu, ctx, d);
    }

    emu_check_taint_call_point(emu, ctx, d);

    if (emu->taint_prop_off == false) {
        if ((emu->tprop_record.insert_taint_call == true) &&
            (emu->tprop_record.skip_taint_call == false)) {
            emu_write_thumb_taint_call(emu, ctx);
        }
    }

    return 0;
}

int post_block_handler(mambo_context *ctx) {
    dbm_thread *thread_data = mambo_get_thread_data(ctx);
    emu_thread_t *emu = (emu_thread_t*)thread_data->sandtrap_handle;

    int block_id = mambo_get_fragment_id(ctx);
    taint_prop_record_t *tprop_record = &emu->tprop_record;

    emu_assert(tprop_record->basic_block_started == true);

    if (emu->taint_prop_off == false) {
        if (tprop_record->insert_taint_call == false) {
            emu_abort("[%s] Leaving block %d (await_inline_confirmation: %d) without writing taint call.\n",
                      __func__, block_id, tprop_record->await_inline_confirmation);
        }
    }

    tprop_record->basic_block_started = false;
    tprop_record->insert_taint_call = false;
    tprop_record->intercept_triggered = false;
    tprop_record->intercept_func = NULL;
    return 0;
}

void emu_taint_prop_plugin_register() {
    mambo_context *ctx = mambo_register_plugin();
    if (ctx == NULL) {
        emu_abort("Mambo context is null while registering taint prop plugin!\n");
    }

    mambo_register_pre_fragment_cb(ctx, &pre_block_handler);
    mambo_register_pre_inst_cb(ctx, &pre_instr_handler);
    mambo_register_post_fragment_cb(ctx, &post_block_handler);
}

void emu_taint_prop_plugin_init(emu_thread_t *emu) {

    if (emu->taint_prop_off == true) {
        return;
    }

    taint_prop_record_t *tprop_record = &emu->tprop_record;
    if (tprop_record->taint_chains != NULL) {
        emu_abort("[%s] taint chains already allocated, re-allocating will cause a mem leak!", __func__);
    }
    tprop_record->taint_chains = calloc(NUM_TAINT_CHAINS, sizeof(taint_chain_t*));
}

void emu_taint_prop_plugin_free(emu_thread_t *emu) {

    if (emu->taint_prop_off == true) {
        return;
    }

    taint_prop_record_t *tprop_record = &emu->tprop_record;
    int i = 0;

    if (tprop_record->taint_chains != NULL) {

        for (i = 0; i < NUM_TAINT_CHAINS; i++) {
            free_taint_chain(tprop_record, i);
        }
        free(tprop_record->taint_chains);
        tprop_record->taint_chains = NULL;
    }
}

void emu_set_branch_inline(mambo_context *ctx, bool do_inline) {
    dbm_thread *thread_data = mambo_get_thread_data(ctx);
    emu_thread_t *emu = (emu_thread_t*)thread_data->sandtrap_handle;
    taint_prop_record_t *tprop_record = &emu->tprop_record;


    if (emu->taint_prop_off == true) {
        return;
    }

    if (tprop_record->in_thumb_mode == true) {
        thumb_instruction inst = mambo_get_inst(ctx);
        if (inst == THUMB_BL_ARM32) {

            // If we're in thumb mode, we might receive a branch
            // inline confirmation (with do_inline set to false) for
            // some branch instructions. We can ignore the inline
            // confirmation because we would have written a taint call
            // for them.

            if (do_inline == true) {
                emu_abort("[%s] do_inline is true in thumb mode after inserting taint call"
                          " (block ID: %d).\n",
                          __func__, tprop_record->block_id);
            }

            if (tprop_record->await_inline_confirmation == true) {
                emu_abort("[%s] we are awaiting inline confirmation for instructions that should not do so "
                          " (block ID: %d).\n",
                          __func__, tprop_record->block_id);
            }

            return;
        }
    }

    if (tprop_record->await_inline_confirmation == false) {
        emu_abort("[%s] branch is being inlined (do_inline: %d, block_id: %d, thumb_mode: %d)"
                  " even though we are not awaiting inline confirmation.\n",
                  __func__, do_inline, tprop_record->block_id, tprop_record->in_thumb_mode);
    }

    if (do_inline == true) {
        // If the branch is indeed going to be inlined, there is
        // nothing else left to do.
        if (tprop_record->skip_taint_call == true) {
            emu_abort("Inlining branch while skip_taint_call is set. This may not work right!\n");
        }
    } else {

        if (tprop_record->insert_taint_call == true) {
            emu_abort("[%s] a taint call has been inserted but we are receiving an inline confirmation"
                      " (Block ID: %d, Thumb mode: %d).\n",
                      __func__, tprop_record->block_id, tprop_record->in_thumb_mode);
        }

        // We are not going to inline the branch, so we should just
        // write the taint call point.
        tprop_record->insert_taint_call = true;

        if (tprop_record->in_thumb_mode == true) {
            emu_write_thumb_taint_call(emu, ctx);
        } else {
            emu_write_arm_taint_call(emu, ctx);
        }
    }

    tprop_record->await_inline_confirmation = false;
}

void emu_intercept_triggered(dbm_thread *thread_data, emu_intercept_func_t *intercept_func) {
    emu_thread_t *emu = (emu_thread_t*)thread_data->sandtrap_handle;
    taint_prop_record_t *tprop_record = &emu->tprop_record;
    tprop_record->intercept_triggered = true;
    tprop_record->intercept_func = intercept_func;
}
