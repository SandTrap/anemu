#include "emu_intercept_gl.h"
#include "emu_debug.h"
#include "anemu-private.h"

#include <stdbool.h>

extern uint32_t emu_get_opengl_taint();
extern void emu_set_opengl_taint(uint32_t tag);

extern int emu_ignore_protections();
extern void emu_obey_protections();

void glDeleteTextures_wrapper(GLsizei n, const GLuint* textures) {
    bool was_obeying_protections = emu_ignore_protections();

    glDeleteTextures_orig_ptr(n, textures);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGetIntegerv_wrapper(GLenum pname, GLint* params) {
    bool was_obeying_protections = emu_ignore_protections();

    glGetIntegerv_orig_ptr(pname, params);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGenTextures_wrapper(GLsizei n, GLuint* textures) {
    bool was_obeying_protections = emu_ignore_protections();

    glGenTextures_orig_ptr(n, textures);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glBindTexture_wrapper(GLenum target, GLuint texture) {
    bool was_obeying_protections = emu_ignore_protections();

    glBindTexture_orig_ptr(target, texture);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glTexParameteri_wrapper(GLenum target, GLenum pname, GLint param) {
    bool was_obeying_protections = emu_ignore_protections();

    glTexParameteri_orig_ptr(target, pname, param);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glTexImage2D_wrapper(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* pixels) {

    bool was_obeying_protections = emu_ignore_protections();
    uint32_t tag = 0;

    if (emu_global->taint_prop_off == false) {
        // The assumption here is that every 32-bit word in data has the
        // same taint label. So we don't need to read the labels of every
        // word in the array to get the taint tag, we only need the taint
        // label of the first word
        tag = emu_get_taint_array((uint32_t)pixels, 1);
        emu_set_opengl_taint(tag);
    }

    glTexImage2D_orig_ptr(target, level, internalformat, width, height, border, format, type, pixels);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

GLenum glGetError_wrapper(void) {
    bool was_obeying_protections = emu_ignore_protections();

    GLenum ret = glGetError_orig_ptr();

    if (was_obeying_protections) {
        emu_obey_protections();
    }

    return ret;
}

GLuint glCreateShader_wrapper(GLenum type) {
    bool was_obeying_protections = emu_ignore_protections();

    GLuint ret = glCreateShader_orig_ptr(type);

    if (was_obeying_protections) {
        emu_obey_protections();
    }

    return ret;
}

void glShaderSource_wrapper(GLuint shader, GLsizei count, const GLchar** string, const GLint* length) {
    bool was_obeying_protections = emu_ignore_protections();

    glShaderSource_orig_ptr(shader, count, string, length);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glCompileShader_wrapper(GLuint shader) {
    bool was_obeying_protections = emu_ignore_protections();

    glCompileShader_orig_ptr(shader);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGetShaderiv_wrapper(GLuint shader, GLenum pname, GLint* params) {
    bool was_obeying_protections = emu_ignore_protections();

    glGetShaderiv_orig_ptr(shader, pname, params);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGetShaderInfoLog_wrapper(GLuint shader, GLsizei bufsize, GLsizei* length, GLchar* infolog) {
    bool was_obeying_protections = emu_ignore_protections();

    glGetShaderInfoLog_orig_ptr(shader, bufsize, length, infolog);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDeleteShader_wrapper(GLuint shader) {
    bool was_obeying_protections = emu_ignore_protections();

    glDeleteShader_orig_ptr(shader);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

GLuint glCreateProgram_wrapper(void) {
    bool was_obeying_protections = emu_ignore_protections();

    GLuint ret = glCreateProgram_orig_ptr();

    if (was_obeying_protections) {
        emu_obey_protections();
    }

    return ret;
}

void glAttachShader_wrapper(GLuint program, GLuint shader) {
    bool was_obeying_protections = emu_ignore_protections();

    glAttachShader_orig_ptr(program, shader);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glBindAttribLocation_wrapper(GLuint program, GLuint index, const GLchar* name) {
    bool was_obeying_protections = emu_ignore_protections();

    glBindAttribLocation_orig_ptr(program, index, name);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glLinkProgram_wrapper(GLuint program) {
    bool was_obeying_protections = emu_ignore_protections();

    glLinkProgram_orig_ptr(program);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGetProgramiv_wrapper(GLuint program, GLenum pname, GLint* params) {
    bool was_obeying_protections = emu_ignore_protections();

    glGetProgramiv_orig_ptr(program, pname, params);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGetProgramInfoLog_wrapper(GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog) {
    bool was_obeying_protections = emu_ignore_protections();

    glGetProgramInfoLog_orig_ptr(program, bufsize, length, infolog);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDeleteProgram_wrapper(GLuint program) {
    bool was_obeying_protections = emu_ignore_protections();

    glDeleteProgram_orig_ptr(program);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glReadPixels_wrapper(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid* pixels) {
    bool was_obeying_protections = emu_ignore_protections();
    uint32_t tag = 0;

    glReadPixels_orig_ptr(x, y, width, height, format, type, pixels);

    if (emu_global->taint_prop_off == false) {
        tag = emu_get_opengl_taint();
        // TODO: We should also factor in the size of each pixel when
        // setting the taint array, and that probably depends on
        // "GLEnum type".
        emu_set_taint_array((uint32_t)pixels, (width * height), tag);
    }

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glActiveTexture_wrapper(GLenum texture) {
    bool was_obeying_protections = emu_ignore_protections();

    glActiveTexture_orig_ptr(texture);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glBindBuffer_wrapper(GLenum target, GLuint buffer) {
    bool was_obeying_protections = emu_ignore_protections();

    glBindBuffer_orig_ptr(target, buffer);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glBindFramebuffer_wrapper(GLenum target, GLuint framebuffer) {
    bool was_obeying_protections = emu_ignore_protections();

    glBindFramebuffer_orig_ptr(target, framebuffer);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glBindRenderbuffer_wrapper(GLenum target, GLuint renderbuffer) {
    bool was_obeying_protections = emu_ignore_protections();

    glBindRenderbuffer_orig_ptr(target, renderbuffer);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glBufferData_wrapper(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage) {
    bool was_obeying_protections = emu_ignore_protections();

    glBufferData_orig_ptr(target, size, data, usage);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glClear_wrapper(GLbitfield mask) {
    bool was_obeying_protections = emu_ignore_protections();

    glClear_orig_ptr(mask);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glClearColor_wrapper(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha) {
    bool was_obeying_protections = emu_ignore_protections();

    glClearColor_orig_ptr(red, green, blue, alpha);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDeleteBuffers_wrapper(GLsizei n, const GLuint* buffers) {
    bool was_obeying_protections = emu_ignore_protections();

    glDeleteBuffers_orig_ptr(n, buffers);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDeleteFramebuffers_wrapper(GLsizei n, const GLuint* framebuffers) {
    bool was_obeying_protections = emu_ignore_protections();

    glDeleteFramebuffers_orig_ptr(n, framebuffers);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDeleteRenderbuffers_wrapper(GLsizei n, const GLuint* renderbuffers) {
    bool was_obeying_protections = emu_ignore_protections();

    glDeleteRenderbuffers_orig_ptr(n, renderbuffers);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDepthMask_wrapper(GLboolean flag) {
    bool was_obeying_protections = emu_ignore_protections();

    glDepthMask_orig_ptr(flag);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDetachShader_wrapper(GLuint program, GLuint shader) {
    bool was_obeying_protections = emu_ignore_protections();

    glDetachShader_orig_ptr(program, shader);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDisable_wrapper(GLenum cap) {
    bool was_obeying_protections = emu_ignore_protections();

    glDisable_orig_ptr(cap);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDrawArrays_wrapper(GLenum mode, GLint first, GLsizei count) {
    bool was_obeying_protections = emu_ignore_protections();

    glDrawArrays_orig_ptr(mode, first, count);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glDrawElements_wrapper(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices) {
    bool was_obeying_protections = emu_ignore_protections();

    glDrawElements_orig_ptr(mode, count, type, indices);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glEnableVertexAttribArray_wrapper(GLuint index) {
    bool was_obeying_protections = emu_ignore_protections();

    glEnableVertexAttribArray_orig_ptr(index);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glFramebufferRenderbuffer_wrapper(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer) {

    bool was_obeying_protections = emu_ignore_protections();

    glFramebufferRenderbuffer_orig_ptr(target, attachment, renderbuffertarget, renderbuffer);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glFramebufferTexture2D_wrapper(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) {
    bool was_obeying_protections = emu_ignore_protections();

    glFramebufferTexture2D_orig_ptr(target, attachment, textarget, texture, level);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGenBuffers_wrapper(GLsizei n, GLuint* buffers) {
    bool was_obeying_protections = emu_ignore_protections();

    glGenBuffers_orig_ptr(n, buffers);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGenFramebuffers_wrapper(GLsizei n, GLuint* framebuffers) {
    bool was_obeying_protections = emu_ignore_protections();

    glGenFramebuffers_orig_ptr(n, framebuffers);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glGenRenderbuffers_wrapper(GLsizei n, GLuint* renderbuffers) {
    bool was_obeying_protections = emu_ignore_protections();

    glGenRenderbuffers_orig_ptr(n, renderbuffers);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glPixelStorei_wrapper(GLenum pname, GLint param) {
    bool was_obeying_protections = emu_ignore_protections();

    glPixelStorei_orig_ptr(pname, param);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glRenderbufferStorage_wrapper(GLenum target, GLenum internalformat, GLsizei width, GLsizei height) {
    bool was_obeying_protections = emu_ignore_protections();

    glRenderbufferStorage_orig_ptr(target, internalformat, width, height);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform1f_wrapper(GLint location, GLfloat x) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform1f_orig_ptr(location, x);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform1fv_wrapper(GLint location, GLsizei count, const GLfloat* v) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform1fv_orig_ptr(location, count, v);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform1i_wrapper(GLint location, GLint x) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform1i_orig_ptr(location, x);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform1iv_wrapper(GLint location, GLsizei count, const GLint* v) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform1iv_orig_ptr(location, count, v);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform2f_wrapper(GLint location, GLfloat x, GLfloat y) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform2f_orig_ptr(location, x, y);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform2fv_wrapper(GLint location, GLsizei count, const GLfloat* v) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform2fv_orig_ptr(location, count, v);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform3f_wrapper(GLint location, GLfloat x, GLfloat y, GLfloat z) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform3f_orig_ptr(location, x, y, z);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform3fv_wrapper(GLint location, GLsizei count, const GLfloat* v) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform3fv_orig_ptr(location, count, v);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform4f_wrapper(GLint location, GLfloat x, GLfloat y, GLfloat z, GLfloat w) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform4f_orig_ptr(location, x, y, z, w);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniform4fv_wrapper(GLint location, GLsizei count, const GLfloat* v) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniform4fv_orig_ptr(location, count, v);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniformMatrix2fv_wrapper(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniformMatrix2fv_orig_ptr(location, count, transpose, value);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUniformMatrix3fv_wrapper(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value) {
    bool was_obeying_protections = emu_ignore_protections();

    glUniformMatrix3fv_orig_ptr(location, count, transpose, value);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glUseProgram_wrapper(GLuint program) {
    bool was_obeying_protections = emu_ignore_protections();

    glUseProgram_orig_ptr(program);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glVertexAttribPointer_wrapper(GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr) {
    bool was_obeying_protections = emu_ignore_protections();

    glVertexAttribPointer_orig_ptr(indx, size, type, normalized, stride, ptr);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

void glViewport_wrapper(GLint x, GLint y, GLsizei width, GLsizei height) {
    bool was_obeying_protections = emu_ignore_protections();

    glViewport_orig_ptr(x, y, width, height);

    if (was_obeying_protections) {
        emu_obey_protections();
    }
}

int glGetAttribLocation_wrapper(GLuint program, const GLchar* name) {
    bool was_obeying_protections = emu_ignore_protections();

    int ret = glGetAttribLocation_orig_ptr(program, name);

    if (was_obeying_protections) {
        emu_obey_protections();
    }

    return ret;
}

int glGetUniformLocation_wrapper(GLuint program, const GLchar* name) {
    bool was_obeying_protections = emu_ignore_protections();

    int ret = glGetUniformLocation_orig_ptr(program, name);

    if (was_obeying_protections) {
        emu_obey_protections();
    }

    return ret;
}

const GLubyte* glGetString_wrapper(GLenum name) {
    bool was_obeying_protections = emu_ignore_protections();

    const GLubyte* ret = glGetString_orig_ptr(name);

    if (was_obeying_protections) {
        emu_obey_protections();
    }

    return ret;
}
