#ifndef INCLUDE_ANEMU_PRIVATE_H
#define INCLUDE_ANEMU_PRIVATE_H

#include "anemu.h"

#include <dlfcn.h>              /* dladdr */
#include <errno.h>
#include <fcntl.h>              /* open, close */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>             /* memset */
#include <sys/atomics.h>
#include <sys/prctl.h>          /* thread name */
#include <sys/ptrace.h>         /* PSR bit macros */
#include <sys/resource.h>       /* [set,get]rlimit */
#include <sys/stat.h>           /* fstat */
#include <sys/syscall.h>
#include <sys/system_properties.h>
#include <sys/uio.h>            /* writev */
#include <unistd.h>

#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

// xattr.h from $AOSP/dalvik/libattr/attr/xattr.h
#include "xattr.h"              /* fsetxattr, fgetxattr */

#include "emu_common.h"
#include "emu_debug.h"
#include "emu_emulate.h"
#include "emu_intercept.h"
#include "emu_mambo_taint_prop.h"
#include "emu_mem_mgmt.h"
#include "emu_profiler.h"
#include "emu_taint_mgmt.h"
#include "emu_thread.h"

#define atomic_inc __atomic_inc
#define atomic_dec __atomic_dec
#define property_get __system_property_get

/* This should make sure the struct instances start at the beginning
 * of a page.
 */
#define ALIGN_PAGE __attribute__((aligned(PAGE_SIZE)))
#define INLINE     __attribute__((always_inline))

#ifdef ENABLE_THREAD_ISOLATION
#define __NR_sandtrapsettrackedmode 378
#define THREAD_TRACKED 1
#define THREAD_UNTRACKED 2
#endif // ENABLE_THREAD_ISOLATION

typedef struct {
    uint32_t vm_start;
    uint32_t vm_end;
    uint64_t pgoff;
    uint32_t major, minor;
    char r, w, x, s;
    uint32_t ino;
    char name[128];
} map_t;


/* Process global SandTrap state */
typedef struct emu_global {
    pthread_mutex_t mutex;                      /* Used to serialize modifications to this struct */
    bool       initialized;                     /* Indicates if this struct has been initialized */

    bool       taint_prop_off;                  /* Indicates if taint propagation is disabled */
    bool       taint_handler_off;               /* Indicates if the taint handler should just return immediately */
    uint32_t   target;                          /* PID of process targeted for emulation */
    int32_t    thread_count;                    /* Number of threads hooked in current process */

    map_t      main_thread_stack;               /* Map of the main thread's stack */
    uint32_t   stack_max;                       /* RLIMIT_STACK */
    uint32_t   stack_base;                      /* stack top - RLIMIT_STACK */

    uint32_t   opengl_taint;                    /* The taint label to apply to the result of OpenGL's glReadPixels */

    taintmap_t taintmaps[MAX_TAINTMAPS];        /* Storage space for the taint tags */

    emu_intercept_func_t *intercept_func_table; /* A hash table used to lookup functions to intercept. */

} ALIGN_PAGE emu_global_t;

/* Synchronization */
// mutex options: PTHREAD_MUTEX_INITIALIZER or PTHREAD_RECURSIVE_MUTEX_INITIALIZER

extern emu_global_t *emu_global;

// WARNING: this must match pthread state from pthread_internal.h
typedef struct pthread_internal_t
{
    struct pthread_internal_t*  next;
    struct pthread_internal_t** pref;
    pthread_attr_t              attr;
    pid_t                       kernel_id;
    pthread_cond_t              join_cond;
    int                         join_count;
    void*                       return_value;
    int                         intern;
    void*                       altstack;
    size_t                      altstack_size;       /* includes guard size */
    size_t                      altstack_guard_size;
    int                         target;              /* emulation target */
    int                         obeying_protections;
    __pthread_cleanup_t*        cleanup_stack;
    void**                      tls;         /* thread-local storage area */
} pthread_internal_t;

/* API */

void
emu_init_handler(int sig,
                 void (*handler)(int, siginfo_t *, void *),
                 void *stack,
                 size_t stack_size);

void emu_start(emu_thread_t *emu);
void emu_stop(emu_thread_t *emu);

ssize_t check_read(int fd, void *buf, size_t count);
ssize_t check_write(int fd, void *buf, size_t count);

ssize_t emu_taint_read(int fd, void *buf, size_t count);
ssize_t emu_taint_write(int fd, void *buf, size_t count);

/* Debugging / Internal only */

void emu_parse_maps(emu_global_t *emu_global);
void emu_parse_cmdline(char *cmdline, size_t size);
char* emu_parse_threadname();

#endif  /* INCLUDE_ANEMU_PRIVATE_H */
