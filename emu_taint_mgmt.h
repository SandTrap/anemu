#ifndef _INCLUDE_TAINT_MGMT_H
#define _INCLUDE_TAINT_MGMT_H

#include "emu_pie_decoder.h"
#include "emu_thread.h"

/** We do not have to define PAGE_SIZE because it is defined in
 * bionic/libc/kernel/arch-arm/asm/page.h
 */
// #define PAGE_SIZE 4096

#define TAINT_CLEAR 0x0

#define MAX_TAINTMAPS 2
#define MAX_TAINTPAGES 256
#define TAINTMAP_LIB 0
#define TAINTMAP_STACK 1

#define TAINTPAGE_SIZE (PAGE_SIZE >> 2)

// These are new syscalls created for SandTrap. The syscall numbers
// must correspond to those in arch/arm/include/asm/unistd.h.
#define __NR_sandtrapsetprotmode 376
#define __NR_sandtrapsetmemprot 377

#define IGNORE_SANDTRAP_PROTECTIONS 1
#define OBEY_SANDTRAP_PROTECTIONS 2

#define ENABLE_SANDTRAP_PROTECTION 1
#define DISABLE_SANDTRAP_PROTECTION 2

#define TAINT_XATTR_NAME "user.taint"

int emu_init_taintmaps();

uint32_t emu_get_taint_file(int fd);
uint32_t emu_set_taint_file(int fd, uint32_t tag);

void emu_set_taint_reg(emu_thread_t *emu, emu_arm_reg reg, uint32_t tag);
uint32_t emu_get_taint_reg(emu_thread_t *emu, emu_arm_reg reg);
uint8_t emu_regs_tainted(emu_thread_t *emu);
void emu_clear_taintregs(emu_thread_t *emu);

void emu_set_taint_ext_reg(emu_thread_t *emu, emu_arm_reg reg, uint32_t tag);
uint32_t emu_get_taint_ext_reg(emu_thread_t *emu, emu_arm_reg reg);

uint32_t emu_get_taint_mem(emu_thread_t *emu, uint32_t addr);
void emu_set_taint_mem(emu_thread_t *emu, uint32_t addr, uint32_t tag);

bool stack_addr(uint32_t addr);
taintmap_t *emu_get_taintmap(uint32_t addr);
taintcount_t *emu_get_taintcount_quick(uint32_t addr);
void emu_update_taintpage(uint32_t page, int16_t increment);

uint32_t emu_dump_taintmaps();
uint32_t emu_dump_taintpages();

#endif // _INCLUDE_TAINT_MGMT_H
