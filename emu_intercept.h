#ifndef INCLUDE_INTERCEPT_H
#define INCLUDE_INTERCEPT_H

#include <stdbool.h>
#include "uthash/uthash.h"

// These are definitions of IDs of all intercepted functions. They are
// placed here, in one place, so that we may easily track them and
// ensure there are no duplicates.

#define Invalid_ID                        0

#define GetVersion_ID                     1

#define DefineClass_ID                    2
#define FindClass_ID                      3

#define FromReflectedMethod_ID            4
#define FromReflectedField_ID             5
#define ToReflectedMethod_ID              6

#define GetSuperclass_ID                  7
#define IsAssignableFrom_ID               8

#define ToReflectedField_ID               9

#define Throw_ID                          10
#define ThrowNew_ID                       11
#define ExceptionOccurred_ID              12
#define ExceptionDescribe_ID              13
#define ExceptionClear_ID                 14
#define FatalError_ID                     15

#define PushLocalFrame_ID                 16
#define PopLocalFrame_ID                  17

#define NewGlobalRef_ID                   18
#define DeleteGlobalRef_ID                19
#define DeleteLocalRef_ID                 20
#define IsSameObject_ID                   21
#define NewLocalRef_ID                    22
#define EnsureLocalCapacity_ID            23

#define AllocObject_ID                    24
#define NewObject_ID                      25
#define NewObjectV_ID                     26
#define NewObjectA_ID                     27

#define GetObjectClass_ID                 28
#define IsInstanceOf_ID                   29

#define GetMethodID_ID                    30

#define CallObjectMethod_ID               31
#define CallObjectMethodV_ID              32
#define CallObjectMethodA_ID              33
#define CallBooleanMethod_ID              34
#define CallBooleanMethodV_ID             35
#define CallBooleanMethodA_ID             36
#define CallByteMethod_ID                 37
#define CallByteMethodV_ID                38
#define CallByteMethodA_ID                39
#define CallCharMethod_ID                 40
#define CallCharMethodV_ID                41
#define CallCharMethodA_ID                42
#define CallShortMethod_ID                43
#define CallShortMethodV_ID               44
#define CallShortMethodA_ID               45
#define CallIntMethod_ID                  46
#define CallIntMethodV_ID                 47
#define CallIntMethodA_ID                 48
#define CallLongMethod_ID                 49
#define CallLongMethodV_ID                50
#define CallLongMethodA_ID                51
#define CallFloatMethod_ID                52
#define CallFloatMethodV_ID               53
#define CallFloatMethodA_ID               54
#define CallDoubleMethod_ID               55
#define CallDoubleMethodV_ID              56
#define CallDoubleMethodA_ID              57
#define CallVoidMethod_ID                 58
#define CallVoidMethodV_ID                59
#define CallVoidMethodA_ID                60

#define CallNonvirtualObjectMethod_ID     61
#define CallNonvirtualObjectMethodV_ID    62
#define CallNonvirtualObjectMethodA_ID    63
#define CallNonvirtualBooleanMethod_ID    64
#define CallNonvirtualBooleanMethodV_ID   65
#define CallNonvirtualBooleanMethodA_ID   66
#define CallNonvirtualByteMethod_ID       67
#define CallNonvirtualByteMethodV_ID      68
#define CallNonvirtualByteMethodA_ID      69
#define CallNonvirtualCharMethod_ID       70
#define CallNonvirtualCharMethodV_ID      71
#define CallNonvirtualCharMethodA_ID      72
#define CallNonvirtualShortMethod_ID      73
#define CallNonvirtualShortMethodV_ID     74
#define CallNonvirtualShortMethodA_ID     75
#define CallNonvirtualIntMethod_ID        76
#define CallNonvirtualIntMethodV_ID       77
#define CallNonvirtualIntMethodA_ID       78
#define CallNonvirtualLongMethod_ID       79
#define CallNonvirtualLongMethodV_ID      80
#define CallNonvirtualLongMethodA_ID      81
#define CallNonvirtualFloatMethod_ID      82
#define CallNonvirtualFloatMethodV_ID     83
#define CallNonvirtualFloatMethodA_ID     84
#define CallNonvirtualDoubleMethod_ID     85
#define CallNonvirtualDoubleMethodV_ID    86
#define CallNonvirtualDoubleMethodA_ID    87
#define CallNonvirtualVoidMethod_ID       88
#define CallNonvirtualVoidMethodV_ID      89
#define CallNonvirtualVoidMethodA_ID      90

#define GetFieldID_ID                     91

#define GetObjectField_ID                 92
#define GetBooleanField_ID                93
#define GetByteField_ID                   94
#define GetCharField_ID                   95
#define GetShortField_ID                  96
#define GetIntField_ID                    97
#define GetLongField_ID                   98
#define GetFloatField_ID                  99
#define GetDoubleField_ID                 100
#define SetObjectField_ID                 101
#define SetBooleanField_ID                102
#define SetByteField_ID                   103
#define SetCharField_ID                   104
#define SetShortField_ID                  105
#define SetIntField_ID                    106
#define SetLongField_ID                   107
#define SetFloatField_ID                  108
#define SetDoubleField_ID                 109

#define GetStaticMethodID_ID              110

#define CallStaticObjectMethod_ID         111
#define CallStaticObjectMethodV_ID        112
#define CallStaticObjectMethodA_ID        113
#define CallStaticBooleanMethod_ID        114
#define CallStaticBooleanMethodV_ID       115
#define CallStaticBooleanMethodA_ID       116
#define CallStaticByteMethod_ID           117
#define CallStaticByteMethodV_ID          118
#define CallStaticByteMethodA_ID          119
#define CallStaticCharMethod_ID           120
#define CallStaticCharMethodV_ID          121
#define CallStaticCharMethodA_ID          122
#define CallStaticShortMethod_ID          123
#define CallStaticShortMethodV_ID         124
#define CallStaticShortMethodA_ID         125
#define CallStaticIntMethod_ID            126
#define CallStaticIntMethodV_ID           127
#define CallStaticIntMethodA_ID           128
#define CallStaticLongMethod_ID           129
#define CallStaticLongMethodV_ID          130
#define CallStaticLongMethodA_ID          131
#define CallStaticFloatMethod_ID          132
#define CallStaticFloatMethodV_ID         133
#define CallStaticFloatMethodA_ID         134
#define CallStaticDoubleMethod_ID         135
#define CallStaticDoubleMethodV_ID        136
#define CallStaticDoubleMethodA_ID        137
#define CallStaticVoidMethod_ID           138
#define CallStaticVoidMethodV_ID          139
#define CallStaticVoidMethodA_ID          140

#define GetStaticFieldID_ID               141

#define GetStaticObjectField_ID           142
#define GetStaticBooleanField_ID          143
#define GetStaticByteField_ID             144
#define GetStaticCharField_ID             145
#define GetStaticShortField_ID            146
#define GetStaticIntField_ID              147
#define GetStaticLongField_ID             148
#define GetStaticFloatField_ID            149
#define GetStaticDoubleField_ID           150

#define SetStaticObjectField_ID           151
#define SetStaticBooleanField_ID          152
#define SetStaticByteField_ID             153
#define SetStaticCharField_ID             154
#define SetStaticShortField_ID            155
#define SetStaticIntField_ID              156
#define SetStaticLongField_ID             157
#define SetStaticFloatField_ID            158
#define SetStaticDoubleField_ID           159

#define NewString_ID                      160

#define GetStringLength_ID                161
#define GetStringChars_ID                 162
#define ReleaseStringChars_ID             163

#define NewStringUTF_ID                   164
#define GetStringUTFLength_ID             165
#define GetStringUTFChars_ID              166
#define ReleaseStringUTFChars_ID          167

#define GetArrayLength_ID                 168
#define NewObjectArray_ID                 169
#define GetObjectArrayElement_ID          170
#define SetObjectArrayElement_ID          171

#define NewBooleanArray_ID                172
#define NewByteArray_ID                   173
#define NewCharArray_ID                   174
#define NewShortArray_ID                  175
#define NewIntArray_ID                    176
#define NewLongArray_ID                   177
#define NewFloatArray_ID                  178
#define NewDoubleArray_ID                 179

#define GetBooleanArrayElements_ID        180
#define GetByteArrayElements_ID           181
#define GetCharArrayElements_ID           182
#define GetShortArrayElements_ID          183
#define GetIntArrayElements_ID            184
#define GetLongArrayElements_ID           185
#define GetFloatArrayElements_ID          186
#define GetDoubleArrayElements_ID         187

#define ReleaseBooleanArrayElements_ID    188
#define ReleaseByteArrayElements_ID       189
#define ReleaseCharArrayElements_ID       190
#define ReleaseShortArrayElements_ID      191
#define ReleaseIntArrayElements_ID        192
#define ReleaseLongArrayElements_ID       193
#define ReleaseFloatArrayElements_ID      194
#define ReleaseDoubleArrayElements_ID     195

#define GetBooleanArrayRegion_ID          196
#define GetByteArrayRegion_ID             197
#define GetCharArrayRegion_ID             198
#define GetShortArrayRegion_ID            199
#define GetIntArrayRegion_ID              200
#define GetLongArrayRegion_ID             201
#define GetFloatArrayRegion_ID            202
#define GetDoubleArrayRegion_ID           203
#define SetBooleanArrayRegion_ID          204
#define SetByteArrayRegion_ID             205
#define SetCharArrayRegion_ID             206
#define SetShortArrayRegion_ID            207
#define SetIntArrayRegion_ID              208
#define SetLongArrayRegion_ID             209
#define SetFloatArrayRegion_ID            210
#define SetDoubleArrayRegion_ID           211

#define RegisterNatives_ID                212
#define UnregisterNatives_ID              213

#define MonitorEnter_ID                   214
#define MonitorExit_ID                    215

#define GetJavaVM_ID                      216

#define GetStringRegion_ID                217
#define GetStringUTFRegion_ID             218

#define GetPrimitiveArrayCritical_ID      219
#define ReleasePrimitiveArrayCritical_ID  220

#define GetStringCritical_ID              221
#define ReleaseStringCritical_ID          222

#define NewWeakGlobalRef_ID               223
#define DeleteWeakGlobalRef_ID            224

#define ExceptionCheck_ID                 225

#define NewDirectByteBuffer_ID            226
#define GetDirectBufferAddress_ID         227
#define GetDirectBufferCapacity_ID        228

#define GetObjectRefType_ID               229

#define calloc_ID                         300
#define malloc_ID                         301
#define realloc_ID                        302
#define free_ID                           303
#define emu_trampoline_read_ID            304
#define emu_trampoline_write_ID           305
#define emu_get_taint_array_ID            306
#define emu_set_taint_array_ID            307
#define send_ID                           308
#define pthread_create_ID                 309
#define pthread_join_ID                   310
#define emu_test_save_regs_ID             311

#define glDeleteTextures_ID               500
#define glGetIntegerv_ID                  501
#define glGenTextures_ID                  502
#define glBindTexture_ID                  503
#define glTexParameteri_ID                504
#define glTexImage2D_ID                   505
#define glGetError_ID                     506
#define glCreateShader_ID                 507
#define glShaderSource_ID                 508
#define glCompileShader_ID                509
#define glGetShaderiv_ID                  510
#define glGetShaderInfoLog_ID             511
#define glDeleteShader_ID                 512
#define glCreateProgram_ID                513
#define glAttachShader_ID                 514
#define glBindAttribLocation_ID           515
#define glLinkProgram_ID                  516
#define glGetProgramiv_ID                 517
#define glGetProgramInfoLog_ID            518
#define glDeleteProgram_ID                519
#define glReadPixels_ID                   520
#define glActiveTexture_ID                521
#define glBindBuffer_ID                   522
#define glBindFramebuffer_ID              523
#define glBindRenderbuffer_ID             524
#define glBufferData_ID                   525
#define glClear_ID                        526
#define glClearColor_ID                   527
#define glDeleteBuffers_ID                528
#define glDeleteFramebuffers_ID           529
#define glDeleteRenderbuffers_ID          530
#define glDepthMask_ID                    531
#define glDetachShader_ID                 532
#define glDisable_ID                      533
#define glDrawArrays_ID                   534
#define glDrawElements_ID                 535
#define glEnableVertexAttribArray_ID      536
#define glFramebufferRenderbuffer_ID      537
#define glFramebufferTexture2D_ID         538
#define glGenBuffers_ID                   539
#define glGenFramebuffers_ID              540
#define glGenRenderbuffers_ID             541
#define glPixelStorei_ID                  542
#define glRenderbufferStorage_ID          543
#define glUniform1f_ID                    544
#define glUniform1fv_ID                   545
#define glUniform1i_ID                    546
#define glUniform1iv_ID                   547
#define glUniform2f_ID                    548
#define glUniform2fv_ID                   549
#define glUniform3f_ID                    550
#define glUniform3fv_ID                   551
#define glUniform4f_ID                    552
#define glUniform4fv_ID                   553
#define glUniformMatrix2fv_ID             554
#define glUniformMatrix3fv_ID             555
#define glUseProgram_ID                   556
#define glVertexAttribPointer_ID          557
#define glViewport_ID                     558
#define glGetAttribLocation_ID            559
#define glGetUniformLocation_ID           560
#define glGetString_ID                    561


typedef struct emu_intercept_func {
    char* name;

    // A unique ID of this function. This helps distinguish an
    // intercepted function easily. The ID has to start from 1 (0 is
    // considered a bad ID). At the time of writing, there is no
    // safety check to ensure IDs are not duplicated.
    uint16_t id;

    // Indicates that the register arguments to the function should be
    // placed in the stack before calling the target function.
    bool put_args_in_stack;

    // The starting argument register number to place in the stack.
    uint8_t start_stack_arg;

    // The address of the function we want to intercept.
    uint32_t orig_func_addr;

    // The address of the trampoline.
    uint32_t trampoline_read_addr;

    // The address of the function that should be executed in place of
    // the original function.
    uint32_t target_func_addr;

    UT_hash_handle hh;

} emu_intercept_func_t;

void emu_intercept_init(emu_intercept_func_t **intercept_tbl);
emu_intercept_func_t* emu_intercept_lookup(uint32_t address);
emu_intercept_func_t* emu_intercept_lookup_impl(emu_intercept_func_t **intercept_tbl, uint32_t address);

#endif // INCLUDE_INTERCEPT_H
