#include "anemu.h"
#include "emu_common.h"
#include "emu_debug.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#define TEST_CASE_PASSED 0
#define TEST_CASE_FAILED -1

#define MEM_ADDR 0x50000000
#define MEM_LEN 4096

typedef struct test_case {
    char* name;
    void (*init)(uint32_t core_regs[], uint32_t core_reg_lbls[]);
    void (*run)(void);
    int (*check_post_cond)(uint32_t core_regs[], uint32_t core_reg_lbls[]);
} test_case_t;

#define DECLARE_TEST_CASE(NAME, INIT_REGS, PRE_RUN, RUN, POST_COND)     \
    void NAME##_INIT(uint32_t core_regs[], uint32_t core_reg_lbls[]) {  \
        INIT_REGS;                                                      \
    }                                                                   \
                                                                        \
    __attribute__((naked)) void NAME##_RUN() {                          \
        PRE_RUN;                                                        \
        EMU_MARKER_START;                                               \
        RUN;                                                            \
        EMU_MARKER_END_TAINT_PROP;                                      \
        asm volatile("bx lr\n\t");                                      \
    }                                                                   \
                                                                        \
    int NAME##_CHECK_POST_COND(uint32_t core_regs[], uint32_t core_reg_lbls[]) { \
        POST_COND;                                                      \
        return TEST_CASE_PASSED;                                        \
    }                                                                   \
                                                                        \
    test_case_t TEST_CASE_##NAME = {                                    \
        .name = #NAME,                                                  \
        .init = &NAME##_INIT,                                           \
        .run = &NAME##_RUN,                                             \
        .check_post_cond = &NAME##_CHECK_POST_COND,                     \
    };

#define CHECK_CORE_REG_LABEL_EQUAL(REG, LBL_VAL)                        \
    {                                                                   \
        if (core_reg_lbls[REG] != (LBL_VAL)) {                          \
            emu_log_always("Condition failed: taint label of register " #REG " is %d (expected %d)", \
                           core_reg_lbls[REG], (LBL_VAL));              \
            return TEST_CASE_FAILED;                                    \
        }                                                               \
    }


// Declare all the test cases
#include "test-taintprop-cases.h"
#undef DECLARE_TEST_CASE

#define DECLARE_TEST_CASE(NAME, ...) \
    &TEST_CASE_##NAME,

test_case_t *all_test_cases[] = {
    #include "test-taintprop-cases.h"
};

#pragma GCC diagnostic pop
