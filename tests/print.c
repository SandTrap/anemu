#include <anemu.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

int main(int argc, char ** argv) {

    emu_set_target(getpid());
    /* Allocate memory for alternate stack for the thread identified
     * by pthread_self(). Update the altstack field for the thread.
     * Allocate memory for an emu_thread_t instance and save that instance
     * in the TLS of the thread. register the SIGSEGV signal handler.
     */
    emu_hook_thread_entry((void *)pthread_self());

    EMU_MARKER_START;

    printf("\nHello World\n");

    EMU_MARKER_STOP;

    return 0;
}
