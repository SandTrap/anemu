// #define EMU_BENCH
#include <anemu.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define MAX_LEN 64

#define TAINT_PASSWORD 0x00010000

void hide(char *password) {
    char stash[MAX_LEN];
    int c = 0;
    while(password[c] != '\0') {
        // obfuscate/hash password
        stash[c] = password[c] - 32;
        c++;
    }

    // write stashed password to disk or network socket
    //int fd = open("/data/cache.tmp", O_CREAT | O_RDWR);
    FILE *ff = fopen("/data/cache.tmp", "w");

    //if (fd) {
    if (ff) {
        //write(fd, stash, c);
        //close(fd);

        fprintf(ff, "The int is %d", stash[0]);
        fclose(ff);
    } else {
        printf("Cannot write to file. \n");
    }
}

bool login(char *password) {
    return true;
}

bool native_authenticate(char *password) {
    hide(password);
    return login(password);
}

// call args: <emu on/off> <runs>
int main(int argc, char ** argv) {
    if (argc != 3) {
        printf("Usage: %s [emu: 0/1] [runs]\n", argv[0]);
        return -1;
    }

    printf("pid = %d\n", getpid());
    int emu = atoi(argv[1]);
    int runs = atoi(argv[2]);
    int i;

    char *pass = "s";

    /* Does this guaranteed to be page aligned? */
    char *buf = mmap(NULL, PAGE_SIZE,
                     PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS,
                     -1, 0);

    printf("pass: %p buf: %p\n", pass, buf);

    memcpy(buf, pass, strlen(pass));

    struct timespec start, end;

    if (emu) {
        emu_set_target(getpid());
        /* Allocate memory for alternate stack for the thread identified
         * by pthread_self(). Update the altstack field for the thread.
         * Allocate memory for an emu_thread_t instance and save that instance
         * in the TLS of the thread. register the SIGSEGV signal handler.
         */
        emu_hook_thread_entry((void *)pthread_self());
        emu_set_taint_array((uintptr_t)buf, strlen(pass), TAINT_PASSWORD);
        emu_obey_protections();
        printf("Initialization Finished\n");
        time_ns(&start);
        EMU_MARKER_START_THUMB;
    }

    for (i = 0; i < runs; i++) {
        native_authenticate(buf);
    }

    if (emu) {
        emu_terminate(0);
        time_ns(&end);
        printf("Undoing setup\n");
        printf("timens = %"PRId64"\n", diff_ns(&start, &end));
        printf("ticks = %lld\n", ns_to_cycles(diff_ns(&start, &end)) / runs);
        emu_hook_pthread_internal_free((void *)pthread_self());
    }

    return 0;
}
