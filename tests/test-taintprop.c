#include <pthread.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>

#include "test-taintprop.h"
#include "anemu.h"

#define N_REGS 16

int run_test_case(test_case_t *tc) {

    uint32_t core_regs[N_REGS];
    uint32_t core_reg_lbls[N_REGS];
    int ret = 0;

    memset(core_regs, 0, sizeof(uint32_t)*15);
    memset(core_reg_lbls, 0, sizeof(uint32_t)*15);

    void *mem_addr = (void*)MEM_ADDR;
    memset(mem_addr, 0, MEM_LEN);
    emu_set_taint_array(MEM_ADDR, MEM_LEN, 0);

    (*tc->init)(core_regs, core_reg_lbls);
    emu_test_set_reg_labels(core_reg_lbls);

    uint32_t save_regs_addr = (uint32_t)&emu_test_save_regs;

    asm volatile("push {r0-r11}\n\t"
                 "mov r12, %[func_save_regs]\n\t"
                 "push {r12}\n\t"
                 "mov r12, %[func_test_run]\n\t"
                 "push {r12}\n\t"
                 "mov r0, %[addr_core_reg_vals]\n\t"
                 "ldm r0, {r0-r11}\n\t"
                 "pop {r12}\n\t"
                 "blx r12\n\t"
                 "pop {r12}\n\t"
                 "blx r12\n\t"
                 "pop {r0-r11}\n\t"
                 :
                 : [func_save_regs] "r" (save_regs_addr),
                   [func_test_run] "r" (tc->run),
                   [addr_core_reg_vals] "r" (core_regs)
        );

    // End emulation
    ret = emu_terminate(0);
    if (ret == 0) {
        emu_log_always("WARNING: Test case '%s' was not emulated.\n", tc->name);
    }

    // Get saved regs and labels
    emu_test_get_regs(core_regs);
    emu_test_get_reg_labels(core_reg_lbls);

   return  (*tc->check_post_cond)(core_regs, core_reg_lbls);
}

int main(int argc, char **argv) {

    test_case_t *tc = NULL;
    int i = 0;
    int num_test_cases = 0;
    int ret = 0;

    emu_set_target(getpid());
    emu_hook_thread_entry((void *)pthread_self());

    // Allocate memory to play with
    ret = mmap(MEM_ADDR, MEM_LEN, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, 0, 0);
    if (ret != MEM_ADDR) {
        emu_log_always("Could not allocate memory @ address 0x%08x (return value: %d), aborting tests ...\n", MEM_ADDR, ret);
        return -1;
    }

    num_test_cases = sizeof(all_test_cases) / sizeof(all_test_cases[0]);
    for (i=0; i<num_test_cases; i++) {
        tc = all_test_cases[i];
        if (tc == NULL) {
            emu_log_always("Test case %d/%d is NULL, skipping ...\n", (i+1), num_test_cases);
            continue;
        }

        emu_log_always("Running test case %d/%d: '%s'", (i+1), num_test_cases, tc->name);
        ret = run_test_case(tc);
        if (ret != TEST_CASE_PASSED) {
            emu_log_always("Failed.");
            break;
        }

        emu_log_always("Passed!");
    }

    return 0;
}
