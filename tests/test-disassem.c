#include "emu_debug.h"
#include "emu_thread.h"
#include "mambo/dbm.h"
#include <sys/mman.h>
#include <capstone.h>

uint16_t thumb_inst_arr[] =
{
/*
  0xf1ba,
  0x0f00,
  0xdd2a,
  0x2500,
  0x4f43,
  0x44f8,
  0x447f,
  0x44fb,
  0x44f9,
  0x7833,
  0x4632,
  0xb13b,
  0x2b00,
  0xd1f7,
  0x4640,
  0x4639,
  0x4604,
  0x2800,
  0xd01e,
  0x4649,
  0x4620,
  0x3501,
  0x4555,
  0xd1e0,
  0x9b03,
  0x2b00,
  0xd11f,
  0xf8df,
  0x810c,
  0xf7ff,
  0xef20,
  0xf7ff,
  0xef1e,
  0xf7ff,
  0xef1c,
  0xf89d,
  0x2038,
  0xf7ff,
  0xef1e,
  0xf1a3,
  0x0420,
  0xf812,
  0x3f01,
  0xf801,
  0x4f01,
  0xf8df,
  0xb10c,
  0xf8df,
  0x910c,
  0xf10d,
  0x0137,
*/
/*
  0xf855,0x5008,
  0x682f,
  0xf7ff,0xfe63,
  0xe92d,0x0ff0,
  0xb08c,
  0x6812,
  0x6814,
  0x9208,
  0xf340,0x80a1,
  0x6812,
  0x9306,
  0x682d,
  0x930a,
  0x6800,
  0x6816,
  0x6829,
  0x6803,
  0x960b,
  0x9109,
  0x9304,
  0x9102,
  0x5998,
  0x9007,
  0x59f5,
  0x9503,
  0x0087,
  0xeb02,0x0c07,
  0x19ef,
  0x58fd,
  0xf85c,0x2003,
  0x586d,
  0xfb05,0x6602,
  0x58fd,
  0xf85c,0x2003,
  0x586d,
  0xfb05,0x6602,
  0x5066,
  0x6804,
  0x1c5a,
  0x9202,
  0x1c51,
  0x910a,
  0x9506,
  0xf73f,0xaf71,
  0x9102,
  0x5998,
  0x9007,
  0xb00c,
  0xe8bd,0x0ff0,
  0x4770,
  0x682b,
  0xaf06,
  0x6812,
  0xf040,0x80f1,
  0xe006,
  0xf04f,0x0300,
  0x9302,
  0xe00a,
  0xf04f,0x0101,
  0x9302,
  0x681b,
  0x607b,

  0x5cf0,
  0x54d0,
  0xbd70,

  0x1e48,
  0x434b,
  0x4359,
  0x435a,
  0x8159,
  0x816b,
  0x8199,
  0x81ab,
  0xcb0c,
  0xe8df,0xf003,
  0xecbd,0x8b10,
  0xed2d,0x8b10,
  0xed83,0xaa0c,
  0xed9f,0x9b68,
  0xed9f,0xbb66,
  0xed9f,0xdb65,
  0xed9f,0xeb68,
  0xed9f,0xfb67,
  0xee05,0x1a90,
  0xee05,0x2a90,
  0xee07,0x9b07,
  0xee27,0x7b0d,
  0xee28,0x8b0b,
  0xee29,0x9b0b,
  0xee2a,0xab0c,
  0xee38,0x8b07,
  0xee3a,0xab0c,
  0xee3e,0xab48,
  0xee87,0x7b06,
  0xee8f,0xab08,
  0xeeb0,0x8b49,
  0xeeb4,0x8bcd,
  0xeeb6,0xcb00,
  0xeeb8,0x6be5,
  0xeeb8,0x7be5,
  0xeebd,0xabca,
  0xeef1,0xfa10,


  0xf245,0x0234,
  0xf806,0x0008,
  0xf80d,0x2003,
  0xf80e,0x5003,
  0xf816,0x1008,
  0xf816,0x200b,
  0xf816,0x700b,
  0xf819,0x3005,
  0xfb06,0xf205,


    0x4082,
    0x40a1,
    0x4313,
    0x52d0,
    0x5ac8,
    0xb2d0,
    0xb2db,
    0xe9c4,0x230e,
    0xea29,0x7be9,
    0xea4f,0x1121,
    0xea4f,0x1122,
    0xea4f,0x1320,
    0xea4f,0x1525,
    0xea4f,0x1829,
    0xea4f,0x1b2b,
    0xea4f,0x1c2c,
    0xebb3,0x0f42,
    0xebb3,0x0f82,
    0xebb3,0x0fc2,
    0xebb4,0x1fc1,
    0xebb9,0x0f42,
    0xebba,0x0f48,
    0xebbb,0x0f43,
    0xf018,0x0f10,
    0xf042,0x0210,
    0xf06f,0x0103,
    0xf06f,0x4040,
    0xf20d,0x5d34,
    0xf2ad,0x5d34,
    0xf829,0x1012,
    0xf8a0,0x1124,
    0xf8a0,0x1126,
    0xf8a4,0x2124,
    0xf8a4,0x2126,
    0xf8b4,0x2124,
    0xf8b4,0x2126,
    0xfa00,0xf202,
    0xfa00,0xf30c,
    0xfa01,0xfc0c,
    0xfa03,0xf202,
    0xfa09,0xf900,
    0xfa46,0xf102,
    0xfa46,0xf204,
    0xfa46,0xf302,
    0xfa46,0xf303,
    0xfa5f,0xfc82,
    0xfb12,0xf303,
    0xfb13,0x4101,
    0xfb13,0x550c,
    0xfb13,0xaa0b,

*/

    0x414b,
    0x424b,
    0x899b,
    0xb280,
    0xb29b,
    0xb2a4,
    0xe9dd,0x230c,
    0xf836,0x9013,
    0xf838,0x0009,
    0xf933,0x1c06,
    0xf933,0x1c0e,
    0xf933,0x2c02,
    0xf933,0x2c04,
    0xf933,0x4c08,
    0xf933,0x4c0c,
    0xf933,0xcc0a,
    0xf9b0,0x3070,
    0xf9b0,0x4000,
    0xf9b0,0x405e,
    0xf9b0,0x500e,
    0xf9b0,0x504e,
    0xf9b0,0x601e,
    0xf9b0,0x603e,
    0xf9b0,0x902e,
    0xf9b2,0x2000,
    0xf9b3,0x9000,
    0xf9b5,0x8000,
    0xf9b5,0xa002,
    0xf9b5,0xa004,
    0xf9b5,0xa006,
    0xf9b5,0xa008,
    0xf9b5,0xa00a,
    0xf9b5,0xa00c,
    0xf9b5,0xa00e,
    0xf9b5,0xa010,
    0xf9b5,0xa012,
    0xf9b5,0xa014,
    0xf9b5,0xa016,
    0xf9b5,0xa018,
    0xf9b5,0xa01a,
    0xf9b5,0xa01c,
    0xf9b5,0xa01e,
    0xf9b5,0xa020,
    0xf9b5,0xa022,
    0xf9b5,0xa024,
    0xf9b5,0xa026,
    0xf9b5,0xa028,
    0xf9b5,0xa02a,
    0xf9b5,0xa02c,
    0xf9b5,0xa02e,
    0xf9b5,0xa030,
    0xf9b5,0xa032,
    0xf9b5,0xa034,
    0xf9b5,0xa036,
    0xf9b5,0xa038,
    0xf9b5,0xa03a,
    0xf9b5,0xa03c,
    0xf9b5,0xa03e,
    0xf9b5,0xa040,
    0xf9b5,0xa042,
    0xf9b5,0xa044,
    0xf9b5,0xa046,
    0xf9b5,0xa048,
    0xf9b5,0xa04a,
    0xf9b5,0xa04c,
    0xf9b5,0xa04e,
    0xf9b5,0xa050,
    0xf9b5,0xa052,
    0xf9b5,0xa054,
    0xf9b5,0xa056,
    0xf9b5,0xa058,
    0xf9b5,0xa05a,
    0xf9b5,0xa05c,
    0xf9b5,0xa05e,
    0xf9b5,0xa060,
    0xf9b5,0xa062,
    0xf9b5,0xa064,
    0xf9b5,0xa066,
    0xf9b5,0xa068,
    0xf9b5,0xa06a,
    0xf9b5,0xa06c,
    0xf9b5,0xa06e,
    0xf9b5,0xa070,
    0xf9b5,0xa072,
    0xf9b5,0xa074,
    0xf9b5,0xa076,
    0xf9b5,0xa078,
    0xf9b5,0xa07a,
    0xf9b5,0xa07c,
    0xf9b5,0xa07e,
    0xfa21,0xf000,
    0xfa21,0xf202,
    0xfa21,0xf302,
    0xfa21,0xf309,
    0xfa21,0xf808,
    0xfa21,0xfc0c,
    0xfa25,0xf002,
    0xfa28,0xf101,
    0xfa28,0xf302,
    0xfa29,0xf101,
    0xfa29,0xf302,
    0xfa53,0xf380,
    0xfbc3,0x4507,



};


uint32_t arm_inst_arr[] =
{
/*
    0xf29062a8,
    0xf29286c0,
    0xf292cae9,
    0xf292e2c1,
    0xf292e2c2,
    0xf2944ac8,
    0xf294ca6a,
    0xf294cae1,
    0xf294ea69,
    0xf295c269,
    0xf295e242,
    0xf296a6e0,
    0xf296c2e2,
    0xf296c2ea,
    0xf296eae9,
    0xf29a86c0,
    0xf29aa2c9,
    0xf29ac6e0,
    0xf29acae9,
    0xf29ae2c1,
    0xf29ae2c2,
    0xf29c42ca,
    0xf29c4ac8,
    0xf29cc2c8,
    0xf29ccae1,
    0xf29d6a30,
    0xf29d6a38,
    0xf29daa30,
    0xf29daa38,
    0xf29e82e8,
    0xf29ea6e0,
    0xf29ec2e2,
    0xf29ec2ea,
    0xf29ee6c0,
    0xf29eeae9,
    0xf2ad6556,
    0xf2ada55a,
    0xf2ce0970,
    0xf2ce1972,
    0xf2ce2974,
    0xf2ce3976,
    0xf2ce4978,
    0xf2ce597a,
    0xf2ce697c,
    0xf2ce797e,
    0xf2d0081c,
    0xf2d01816,
    0xf2d02812,
    0xf2d03816,
    0xf2d04812,
    0xf2d05812,
    0xf2d0681a,
    0xf2d07814,
    0xf2d0881c,
    0xf2d09816,
    0xf2d0a812,
    0xf2d0b816,
    0xf2d0c812,
    0xf2d0d812,
    0xf2d0e81a,
    0xf2d0f814,
    0xf2d14874,
    0xf2d15876,
    0xf2d21531,
    0xf2d28878,
    0xf2d2987a,
    0xf2d2c87c,
    0xf2d2d87e,
    0xf2d5085c,
    0xf2d52852,
    0xf2d54852,
    0xf2d5685a,
    0xf2d58856,
    0xf2d5a856,
    0xf2d5c852,
    0xf2d5e854,
    0xf2d64a49,
    0xf2d6ca69,
    0xf2d76a49,
    0xf2d7ea69,
    0xf2d84261,
    0xf2d88a41,
    0xf2d96261,
    0xf2d9aa41,
    0xf322284c,
    0xf3226848,
    0xf324484e,
    0xf3266844,
    0xf326684a,
    0xf32a284c,
    0xf3b2a268,
    0xf3b2b264,
    0xf3b2c26c,
    0xf3f200a1,
    0xf3f220a3,
    0xf3f240a5,
    0xf3f260a7,
    0xf3f600a2,
    0xf3f600e2,
    0xf3f640a6,
    0xf3f640e6,
    0xf3f680aa,
    0xf3f6c0ae,
    0xf3fa00a4,
    0xf3fa20a6,
    0xf3fa80ac,
    0xf3faa0ae,
*/

    0xf2914050,
    0xf2916052,
    0xf3884a1a,
    0xf3886a1b,
    0xf3888a1c,
    0xf3d84ca0,
    0xf3d96ca1,
    0xf3da0ca2,
    0xf3db2ca3,
    0xf3f583e8,
    0xf3f5a3ea,
    0xf3f82042,
    0xf3f8a042,
    0xf3f8c042,
    0xf3f8e042,

};

int main(int argc, char** argv) {

    /* This if-statement is a hack. It's there simply to ensure that
     * the compiler doesn't omit the "INSTRUCTIONS" block in the
     * compiled output. */
    if (argc > 10) {
        goto INSTRUCTIONS;
    }

    uint32_t instr_addr = (uint32_t)&&INSTRUCTIONS;
    uint32_t final_addr = (uint32_t)&&END_OF_INSTRUCTIONS;

    // In case instructions are defined in encoded form in an array
#ifdef TEST_THUMB
    instr_addr = (uint32_t)thumb_inst_arr;
    final_addr = instr_addr + sizeof(thumb_inst_arr);
#else
    instr_addr = (uint32_t)arm_inst_arr;
    final_addr = instr_addr + sizeof(arm_inst_arr);
#endif // TEST_THUMB

    int inst = 0;
    int ret = 0;
    uint32_t *read_address = NULL;
    emu_disassem_t *disassem = NULL;

    csh cs_arm_handle;
    cs_insn *cs_inst_arm;

    csh cs_thumb_handle;
    cs_insn *cs_inst_thumb;

    // Set up capstone [only used to print instruction]
    cs_open(CS_ARCH_ARM, CS_MODE_ARM, &cs_arm_handle);
    cs_option(cs_arm_handle, CS_OPT_DETAIL, CS_OPT_ON);
    cs_inst_arm = cs_malloc(cs_arm_handle);

    cs_open(CS_ARCH_ARM, CS_MODE_THUMB, &cs_thumb_handle);
    cs_option(cs_thumb_handle, CS_OPT_DETAIL, CS_OPT_ON);

    // Set up the struct used to store decoded instructions
    disassem = calloc(1, sizeof(emu_disassem_t));

    // Set up some final things
    read_address = (uint32_t*)instr_addr;

    emu_log_always("\n\nStarting tester\n\n");

    while (instr_addr < final_addr) {

        // Capstone related
        const unsigned char *cs_code = (const unsigned char *)read_address;
        uint64_t cs_addr = (uint64_t) read_address;
        size_t cs_code_size = sizeof(cs_code);
        bool cs_success = false;

        // First decode the instruction with capstone and print it out
#ifndef TEST_THUMB
        cs_success = cs_disasm_iter(cs_arm_handle, &cs_code, &cs_code_size, &cs_addr, cs_inst_arm);
        if (cs_success) {
            emu_log_always("[Capstone]: %s\t%s.\n", cs_inst_arm->mnemonic, cs_inst_arm->op_str);
        } else {
            emu_log_always("Capstone could not decode ARM instruction.\n");
        }
#else
        cs_success = cs_disasm_iter(cs_thumb_handle, &cs_code, &cs_code_size, &cs_addr, cs_inst_thumb);
        if (cs_success) {
            emu_log_always("[Capstone]: %s\t%s.\n", cs_inst_thumb->mnemonic, cs_inst_thumb->op_str);
        } else {
            emu_log_always("Capstone could not decode Thumb instruction.\n");
        }
#endif

        // Now, decode the instruction with SandTrap and PIE
#ifndef TEST_THUMB
        inst = arm_decode(read_address);
        arm_translate_to_disassem(inst, read_address, disassem);
#else  // TEST_THUMB
        inst = thumb_decode(read_address);
        thumb_translate_to_disassem(inst, read_address, disassem);
#endif // TEST_THUMB

        // Print the instruction with the SandTrap instruction printer
        if (disassem->size == 4) {
            emu_log_always("Encoded instruction: 0x%08x", *read_address);
        } else {
            emu_log_always("Encoded instruction: 0x%04x", *((uint16_t*)read_address));
        }
        print_emu_disassem(disassem);

        // Go to next instruction
        instr_addr += disassem->size;
        read_address = (uint32_t*)instr_addr;
        emu_log_always("\n\n");
    }

    emu_log_always("\n\nTester complete.");

    return 0;

#ifndef TEST_THUMB
INSTRUCTIONS:


    asm volatile("adc r1, r2, #4");
    asm volatile("adc r10, r11, r12");
    asm volatile("adc r5, r6, r7, LSL r8");

    asm volatile("add r1, r2, r3\n\t");
    asm volatile("add r5, r6, #4\n\t");

    asm volatile("and r1, r2, #5");
    asm volatile("and r4, r5, r6");

    asm volatile("bic r4, r5, #4");
    asm volatile("bic r6, r7, r8");
    asm volatile("bic r6, r7, r8, asr r10");

    asm volatile("eor r1, r2, #4");
    asm volatile("eor r3, r4, r5");
    asm volatile("eor r6, r7, r8, lsl r9");

    asm volatile("sbc r8, r9, #4");
    asm volatile("sbc r5, r6, r7");
    asm volatile("sbc r1, r2, r3, lsl r4");

    asm volatile("rsb r8, r9, #4");
    asm volatile("rsb r5, r6, r7");
    asm volatile("rsb r1, r2, r3, lsl r4");

    asm volatile("orr r8, r9, #4");
    asm volatile("orr r5, r6, r7");
    asm volatile("orr r1, r2, r3, lsl r4");

    asm volatile("sub r1, r2, #500");
    asm volatile("sub r2, r2, r3");
    asm volatile("sub r3, r2, r3, LSL r4");
    asm volatile("sub r4, sp, #12");
    asm volatile("sub r5, sp, r2");

    asm volatile("mul r10, r12, r13");

    asm volatile("bfi r1, r2, #3, #3");

    asm volatile("clz r4, r5");

    asm volatile("ldr r1, [R2, #44]");
    asm volatile("ldr r1, [R2, #-44]");

    asm volatile("ldr r1, [R2], #44");
    asm volatile("ldr r1, [R2], #-44");

    asm volatile("ldr r1, [pc, #-44]");

    asm volatile("ldr r1, [r2, r3]");
    asm volatile("ldr r4, [r5], r6");
    asm volatile("ldr r7, [pc, r8]");

    asm volatile("ldr r1, [r2, -r3]");
    asm volatile("ldr r4, [r5], -r6");
    asm volatile("ldr r7, [pc, -r8]");

    asm volatile("ldr r4, [r5, r6, lsl #1]");
    asm volatile("ldr r4, [r5, r6, ror #5]");

    asm volatile("ldrb r1, [r2, #-5]");
    asm volatile("ldrb r3, [r4, #-10]!");
    asm volatile("ldrb r5, [r6], #-20");

    asm volatile("ldrb r3, [pc, #30]");

    asm volatile("ldrb r3, [r4, r6]");
    asm volatile("ldrb r3, [r4, r5, ASR #30]");
    asm volatile("ldrb r3, [r4], r5, ASR  #30");

    asm volatile("ldm r4, {r0, r1, r2}");
    asm volatile("ldm r5!, {r6, r7, r8}");
    asm volatile("ldmda r4, {r0, r1, r2}");
    asm volatile("ldmda r7, {r8, r9, r10}");
    asm volatile("ldmdb r0, {r1, r2, r3, r4}");
    asm volatile("ldmib r2, {r5, r7, r8}");

    asm volatile("stm r4, {r1, r2}");
    asm volatile("stmda r4, {r1, r2}");
    asm volatile("stmdb r4, {r1, r2}");
    asm volatile("stmib r4, {r1, r2}");

    asm volatile("ldrh r4, [r5, #-30]");
    asm volatile("ldrh r6, [r7], #30");

    asm volatile("ldrh r6, [r7, r8]");
    asm volatile("ldrh r6, [r7], r8");

    asm volatile("ldrsh r2, [r4, #-20]");
    asm volatile("ldrsh r2, [r4], #20");

    asm volatile("mov r1, #500");
    asm volatile("mov r1, r2");

    asm volatile("asr r4, r5, #4");
    asm volatile("asr r4, r5, r6");

    asm volatile("lsl r7, r5, #4");
    asm volatile("lsl r7, r5, r6");

    asm volatile("lsr r8, r5, #4");
    asm volatile("lsr r8, r5, r6");

    asm volatile("ror r9, r5, #4");
    asm volatile("ror r9, r5, r6");

    asm volatile("rrx r10, r5");

    asm volatile("mvn r8, #200");
    asm volatile("mvn r5, r6, lsl #4");
    asm volatile("mvn r1, r2, lsl r4");

    asm volatile("movw r3, #10");

    asm volatile("str r1, [r0, #32]");
    asm volatile("str r2, [r3, #-32]");
    asm volatile("str r4, [r5], #-32");

    asm volatile("str r5, [r6, r7]");
    asm volatile("str r5, [r6, r7, ASR #30]");
    asm volatile("str r5, [r6], r7");

    asm volatile("strb r1, [r2, #-4]");
    asm volatile("strb r4, [r3, #-4]!");
    asm volatile("strb r5, [r4]");

    asm volatile("strb r6, [r7, r8, LSL #4]");
    asm volatile("strb r8, [r9, r10, LSR #4]!");
    asm volatile("strb r10, [r11], r12, asr #4");

    asm volatile("strd r2, r3, [r4, #-68]");
    asm volatile("strd r4, r5, [r4, #40]!");
    asm volatile("strd r6, r7, [r4], #20");

    asm volatile("strd r8, r9, [r4, +r5]");
    asm volatile("strd r10, r11, [r4, -r5]");
    asm volatile("strd r12, r13, [r4], +r5");

    asm volatile("strh r4, [r5, #-30]");
    asm volatile("strh r6, [r7], #20");

    asm volatile("strh r6, [r7, r9]");
    asm volatile("strh r6, [r7]");

    asm volatile("umull r4, r5, r6, r7");

    asm volatile("uxth r5, r4");

    asm volatile("vld1.16 {d16, d17, d18}, [r1]!\n\t");
    asm volatile("vld2.32 {d16, d17}, [r1]!\n\t");
    asm volatile("vld3.32 {d16, d17, d18}, [r1]!\n\t");
    asm volatile("vld4.8 {d10, d11, d12, d13}, [r7]!\n\t");
    asm volatile("vld4.32 {d16, d17, d18, d19}, [r1]!\n\t");

    asm volatile("vst1.32 {d3}, [r3]\n\t");
    asm volatile("vst1.32 {d16, d17, d18, d19}, [r1]!\n\t");
    asm volatile("vst2.32 {d16, d17}, [r1]!\n\t");
    asm volatile("vst3.32 {d16, d17, d18}, [r1]!\n\t");
    asm volatile("vst4.32 {d16, d17, d18, d19}, [r1]!\n\t");

    asm volatile("vld1.16 {d16[0]}, [r1]!\n\t");
    asm volatile("vld2.32 {d16[0], d18[0]}, [r1]!\n\t");
    asm volatile("vld3.32 {d16[0], d18[0], d20[0]}, [r1]!\n\t");
    asm volatile("vld4.32 {d10[0], d11[0], d12[0], d13[0]}, [r7]!");

    asm volatile("vst1.16 {d16[0]}, [r5]!\n\t");
    asm volatile("vst2.32 {d16[0], d18[0]}, [r5]!\n\t");
    asm volatile("vst3.32 {d16[0], d17[0], d18[0]}, [r5]!\n\t");
    asm volatile("vst4.32 {d16[0], d18[0], d20[0], d22[0]}, [r5]!\n\t");

    asm volatile("vld1.16 {d10[], d11[]}, [r4]");
    asm volatile("vld2.16 {d10[], d11[]}, [r4]");
    asm volatile("vld3.32 {d10[], d11[], d12[]}, [r4]");
    asm volatile("vld4.32 {d10[], d11[], d12[], d13[]}, [r4]");

    asm volatile("vaba.u32 q1, q2, q3");
    asm volatile("vaba.u16 d1, d2, d3");
    asm volatile("vabal.u16 q0, d2, d3");

    asm volatile("vabd.u16 q1, q2, q3");
    asm volatile("vabd.u16 d1, d2, d3");
    asm volatile("vabdl.u8 q1, d2, d3");

    asm volatile("vabd.f32 q1, q2, q3");
    asm volatile("vabd.f32 d1, d2, d3");

    asm volatile("vabs.f32 q1, q2");
    asm volatile("vabs.f32 d1, d2");

    asm volatile("vabs.f32 s1, s2");
    asm volatile("vabs.f64 d10, d20");

    asm volatile("vacge.f32 q1, q2, q3");
    asm volatile("vacge.f32 d0, d1, d2");

    asm volatile("vacgt.f32 q1, q2, q3");
    asm volatile("vacgt.f32 d10, d11, d12");

    asm volatile("vadd.i64 q4, q5, q6");
    asm volatile("vadd.i64 d5, d6, d7");

    asm volatile("vadd.f64 d0, d1, d2");
    asm volatile("vadd.f32 s0, s1, s2");

    asm volatile("vaddhn.i64 d0, q2, q3");

    asm volatile("vaddl.s16 q5, d16, d24\n\t");
    asm volatile("vaddw.s32 q5, q6, d24\n\t");

// Tested until here

    asm volatile("vand q0, q2, q4");
    asm volatile("vand d0, d2, d4");

    asm volatile("vcle.f32 d0, d2, #0");

    asm volatile("vsub.f64 d10, d14, d8\n\t");
    asm volatile("vsub.f32 s12, s28, s31\n\t");

    asm volatile("vmull.s16 q6, d4, d2[3]\n\t");
    asm volatile("vmlal.s16 q6, d5, d1[3]\n\t");
    asm volatile("vsubl.s16 q3, d16, d24\n\t");
    asm volatile("vorr q4, q6, q6\n\t");
    asm volatile("vorr.i32 q4, q4, #32\n\t");
    asm volatile("vmlsl.s16 q6, d26, d0[2]\n\t");
    asm volatile("vshl.i32 q3, q3, #0xd");
    asm volatile("vshl.u64 q1, q2, q3");
    asm volatile("vrshrn.i32 d18, q1, #0xb");

    asm volatile("vtrn.16 d28, d30");
    asm volatile("vshrn.i32 d23, q2, #0x10");
    asm volatile("vshll.s16 q3, d16, #0xd");
    asm volatile("vqrshrn.s16 d16, q8, #2");

    asm volatile("vmax.s32 d16, d16, d17\n\t");
    asm volatile("vmovn.i32 d20, q1");
    asm volatile("vmvn.i32 d20, #0xf");
    asm volatile("vmvn.i32 d20, d21");
    asm volatile("vdup.32 d16, r0");
    asm volatile("vdup.32 d16, d17[0]");
    asm volatile("vmov.f64 d8, d9\n\t");

    asm volatile("vcvt.s32.f64 s20, d10\n\t");
    asm volatile("vaddw.u8 q3, q1, d4\n\t");
    asm volatile("vqmovun.s16 d11, q10\n\t");

    asm volatile("vrev64.32 q14, q1\n\t");
    asm volatile("vabs.s16 q13, q1\n\t");
    asm volatile("vshr.s16 q3, q1, #0xf\n\t");
    asm volatile("veor q14, q14, q2\n\t");
    asm volatile("vneg.s16 q10, q12\n\t");

    asm volatile("vmovl.u8 q2, d10\n\t");
    asm volatile("vcvt.f64.u32 d7, s12\n\t");


#else // TEST_PROP_THUMB

INSTRUCTIONS:
    asm volatile("add r1, r2, r3\n\t");
    // asm volatile("strex r2, r3, [r4, #12]\n\t");
    // asm volatile("strex r2, r3, [r4]\n\t");
    // asm volatile("mov.w ip, sl, asr #31\n\t");
    // asm volatile("subw sp, sp, #0x534\n\t");
    // asm volatile("subw r2, r4, #0x100\n\t");
    // asm volatile("vld1.16 {d16, d17, d18, d19}, [r1 :0x80]!\n\t");
    // asm volatile("vmull.s16 q6, d4, d2[3]\n\t");
    // asm volatile("vld1.32 {d16[0]}, [r1]!\n\t");
    // asm volatile("vmlal.s16 q6, d5, d1[3]\n\t");
    // asm volatile("vsubl.s16 q3, d16, d24\n\t");
    // asm volatile("vorr q4, q6, q6\n\t");
    // asm volatile("vorr.i32 q4, q4, #32\n\t");
    // asm volatile("vmlsl.s16 q6, d26, d0[2]\n\t");
    // asm volatile("vshl.i32 q3, q3, #0xd");
    // asm volatile("vshl.u64 q1, q2, q3");
    // asm volatile("vrshrn.i32 d18, q1, #0xb");
    // asm volatile("vaddl.s16 q5, d16, d24\n\t");
    // asm volatile("vtrn.16 d28, d30");
    // asm volatile("vshll.s16 q3, d16, #0xd");
    // asm volatile("vqrshrn.s16 d16, q8, #2");

    // asm volatile("vst1.16 {d16[0]}, [r5]!\n\t");
    // asm volatile("vst1.32 {d16}, [r5]!\n\t");
    // asm volatile("vst1.32 {d16, d17, d18, d19}, [r1]!\n\t");
    // asm volatile("vmax.s32 d16, d16, d17\n\t");
    // asm volatile("vmovn.i32 d20, q1");
    // asm volatile("vmvn.i32 d20, #0xf");
    // asm volatile("vmvn.i32 d20, d21");
    // asm volatile("vdup.32 d16, r0");
    // asm volatile("vdup.32 d16, d17[0]");
    // asm volatile("vmov.f64 d8, d9\n\t");

    // asm volatile("vcvt.s32.f64 s20, d10\n\t");
    // asm volatile("addw r5, sp, #0x534\n\t");
    // asm volatile("vaddw.u8 q3, q1, d4\n\t");
    // asm volatile("vqmovun.s16 d11, q10\n\t");
    // asm volatile("vst4.16 {d10, d11, d12, d13}, [r7]!");
    // asm volatile("vst4.16 {d10[0], d11[0], d12[0], d13[0]}, [r7]!\n\t");

    // asm volatile("vrev64.32 q14, q1\n\t");
    // asm volatile("vabs.s16 q13, q1\n\t");
    // asm volatile("vshr.s16 q3, q1, #0xf\n\t");
    // asm volatile("veor q14, q14, q2\n\t");
    // asm volatile("vneg.s16 q10, q12\n\t");

    // asm volatile("vld1.16 {d10[], d11[]}, [r4]");
    // asm volatile("vld4.8 {d10, d11, d12, d13}, [r7]!\n\t");
    // asm volatile("vld4.32 {d10[0], d11[0], d12[0], d13[0]}, [r7]!");
    // asm volatile("vld4.32 {d10[], d11[], d12[], d13[]}, [r7]!");

    // asm volatile("vmovl.u8 q2, d10\n\t");
    // asm volatile("vcvt.f64.u32 d7, s12\n\t");

#endif // TEST_PROP_THUMB

END_OF_INSTRUCTIONS:

    return 0;
}
