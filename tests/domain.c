// #define EMU_BENCH
#include <anemu.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>

#define __NR_settaintmanager 376
#define __NR_taintprotectmemory 377

int main(int argc, char ** argv) {

    if (argc != 2) {
        printf("Usage: %s [test domain: 0/1]\n", argv[0]);
        return -1;
    }
    int test_domain = atoi(argv[1]);

    // Create buffer and protect it
    char *buf = mmap(NULL, PAGE_SIZE,
                     PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS,
                     -1, 0);

    // Need to perform an initial write to make sure the kernel
    // actually allocates the page.
    (*buf) = 0;

    // Page protections on!
    mprotect(buf, PAGE_SIZE, PROT_NONE);

    if (test_domain) {
        printf("Domain testing enabled\n");
        syscall(__NR_settaintmanager);
        syscall(__NR_taintprotectmemory, (unsigned long) buf, 1);
    }

    int value = 100;
    printf("Going to write %d into %p\n", value, buf);

    if (!test_domain) {
        printf("There should be a segfault\n");
    }

    (*buf) = value;
    printf("Done writing. %p=%d\n", buf, (*buf));

    return 0;
}
