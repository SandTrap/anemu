#include <sys/syscall.h>

#define __NR_settaintmanager    376
#define __NR_taintprotectmemory 377
#define ENABLE_MANAGER_MODE       1
#define PROTECT_MEMORY            1

//static int x = 0; // This will be located in the bss segment
int x;

int main()
{
    // Go into manager mode
    syscall(__NR_settaintmanager, ENABLE_MANAGER_MODE);

    // Set memory region into DOMAIN_TAINT
    syscall(__NR_taintprotectmemory, (unsigned long)&x, PROTECT_MEMORY);

    // Goodbye, cruel world!
    // I hardly knew ye ...
    printf("x is located at %p and has a value of %d\n", &x, x);
    x++;
    printf("x is now %d\n", x);

    // Because good coding practices matter
    return 0;
}
