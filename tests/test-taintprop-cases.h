// This file is only meant to be included in test-taintprop.h.

DECLARE_TEST_CASE(add,
                  {
                      // init(uint32_t[] core_regs, uint32_t[] core_reg_lbls);
                      core_regs[EMU_ARM_REG_R1] = 1;
                      core_regs[EMU_ARM_REG_R2] = 2;
                      core_regs[EMU_ARM_REG_R3] = 3;

                      core_reg_lbls[EMU_ARM_REG_R2] = 0x1;
                      core_reg_lbls[EMU_ARM_REG_R3] = 0x2;
                  },
                  {
                      // pre_run
                  },
                  {
                      // run()
                      asm volatile("add r1, r2, r3");
                  },
                  {
                      CHECK_CORE_REG_LABEL_EQUAL(EMU_ARM_REG_R1, (0x1 | 0x2));
                  }
    )

DECLARE_TEST_CASE(ldr,
                  {
                      // init(uint32_t[] core_regs, uint32_t[] core_reg_lbls);
                      emu_set_taint_array(MEM_ADDR, 4, 0x1);
                      core_reg_lbls[EMU_ARM_REG_R0] = 0x2;
                  },
                  {
                      // pre_run
                      asm volatile("mov r0, #"__stringify(MEM_ADDR));

                  },
                  {
                      // run()
                      asm volatile("ldr r0, [r0]");
                  },
                  {
                      CHECK_CORE_REG_LABEL_EQUAL(EMU_ARM_REG_R0, (0x3));
                  }
    )
