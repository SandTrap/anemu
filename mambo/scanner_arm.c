/*
  This file is part of MAMBO, a low-overhead dynamic binary modification tool:
      https://github.com/beehive-lab/mambo

  Copyright 2013-2016 Cosmin Gorgovan <cosmin at linux-geek dot org>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#include "dbm.h"
#include "common.h"
#include "scanner_common.h"

#include "pie/pie-arm-decoder.h"
#include "pie/pie-arm-encoder.h"
#include "pie/pie-arm-field-decoder.h"

#include "emu_intercept.h"
extern void emu_set_branch_inline(mambo_context *ctx, bool do_inline);

#include "emu_debug.h"
#ifdef DEBUG
  #define debug(...) emu_log_always(__VA_ARGS__)
#else
  #define debug(...)
#endif

#define copy_arm() *(write_p++) = *read_address;

#define ALLOWED_IHL_REGS (0x5FF8) // {R3 - R12, R14}

void arm_copy_to_reg_16bit(uint32_t **write_p, enum reg reg, uint32_t value) {
  arm_movw(write_p, reg, (value >> 12) & 0xF, value & 0xFFF);
  (*write_p)++;
}

void arm_cond_copy_to_reg_16bit(uint32_t **write_p, enum arm_cond_codes cond, enum reg reg, uint32_t value) {
  arm_movw_cond(write_p, cond, reg, (value >> 12) & 0xF, value & 0xFFF);
  (*write_p)++;
}

void arm_copy_to_reg_32bit(uint32_t **write_p, enum reg reg, uint32_t value) {
  arm_movw(write_p, reg, (value >> 12) & 0xF, value & 0xFFF);
  (*write_p)++;
  arm_movt(write_p, reg, (value >> 28), (value >> 16) & 0xFFF);
  (*write_p)++;
}

void arm_cond_copy_to_reg_32bit(uint32_t **write_p, enum arm_cond_codes cond, enum reg reg, uint32_t value) {
  arm_movw_cond(write_p, cond, reg, (value >> 12) & 0xF, value & 0xFFF);
  (*write_p)++;
  arm_movt_cond(write_p, cond, reg, (value >> 28), (value >> 16) & 0xFFF);
  (*write_p)++;
}

void arm_proc_i32bit(uint32_t **write_p, arm_instruction inst, enum reg rd, enum reg rn, uint32_t value) {
  int shift = 0;
  uint32_t op2 = INT_MAX;

  /* Ensure that at least one instruction is generated even for value == 0,
     by executing the loop at least once. */
  while (value || op2 == INT_MAX) {
    while (value && ((0x3 << shift) & value) == 0) {
      shift += 2;
    }

    op2 = ((((32 - shift) >> 1) & 0xF) << 8) | ((value >> shift) & 0xFF);
    switch (inst) {
      case ARM_ADD:
        arm_add(write_p, IMM_PROC, 0, rd, rn, op2);
        *write_p += 1;
        break;
      case ARM_SUB:
        arm_sub(write_p, IMM_PROC, 0, rd, rn, op2);
        *write_p += 1;
        break;
      default:
        emu_log_always("arm_proc_i32bit unknown insts: %d\n", inst);
        while(1);
    }

    rn = rd;
    value &= ~(0xFF << shift);
  }
}

void arm_add_sub_32_bit(uint32_t **write_p, enum reg rd, enum reg rn, int value) {
  arm_instruction inst = ARM_ADD;
  if (value < 0) {
    value = -value;
    inst = ARM_SUB;
  }

  arm_proc_i32bit(write_p, inst, rd, rn, value);
}

void arm_branch_save_context(dbm_thread *thread_data, uint32_t **o_write_p) {
  uint32_t *write_p = *o_write_p;

  arm_push_reg(r3);
  arm_copy_to_reg_32bit(&write_p, r3, (uint32_t)thread_data->scratch_regs);
  arm_stm(&write_p, r3, (1 << r0) | (1 << r1) | (1 << r2), 0, 1, 0, 0);
  write_p++;
  arm_pop_reg(r3);

  *o_write_p = write_p;
}

#define SETUP (1 << 0)
#define REPLACE_TARGET (1 << 1)
#define INSERT_BRANCH (1 << 2)

void arm_branch_jump(dbm_thread *thread_data, uint32_t **o_write_p, int basic_block,
                     uint32_t offset, uint32_t *read_address, uint32_t cond, uint32_t flags) {
  uint32_t *write_p = *o_write_p;

  int32_t  branch_offset;

  uint32_t *scratch_data;
  uint32_t scratch_offset;
  uint32_t condition_code = 0xE0000000;

  debug("ARM branch: read_addr: %p, offset: 0x%x\n", read_address, offset);

  if (flags & SETUP) {
    if (cond < 14) {
      arm_cond_copy_to_reg_32bit(&write_p, arm_inverse_cond_code[cond], r0, (uint32_t)read_address + 4);
    }
  }

  if (flags & REPLACE_TARGET) {
    branch_offset = (offset & 0x800000) ? 0xFC000000 : 0;
    branch_offset |= (offset<<2);

    arm_cond_copy_to_reg_32bit(&write_p, cond, r0, (uint32_t)read_address + 8 + branch_offset);
  }

  if (flags & INSERT_BRANCH) {
    arm_copy_to_reg_32bit(&write_p, r1, basic_block);

    arm_b(&write_p, (thread_data->dispatcher_addr - (uint32_t)write_p - 8) >> 2);
    write_p++;
  }

  *o_write_p = write_p;
}

void arm_check_free_space(dbm_thread *thread_data, uint32_t **write_p, uint32_t **data_p, uint32_t size) {
  int basic_block;

  if ((uint32_t)*write_p >= (uint32_t)*data_p) {
    emu_abort("In ARM scanner, write_p (0x%08x) has reached or exceeded data_p (0x%08x).\n",
              *write_p, *data_p);
  }

  if ((((uint32_t)*write_p)+size) >= (uint32_t)*data_p) {
    basic_block = allocate_bb(thread_data);
    arm_b(write_p, ((uint32_t)&thread_data->code_cache->blocks[basic_block] - (uint32_t)*write_p - 8) >> 2);
    *write_p = (uint32_t *)&thread_data->code_cache->blocks[basic_block];
    *data_p = (uint32_t *)*write_p;
    *data_p += BASIC_BLOCK_SIZE;
  }
}

void arm_branch_helper(uint32_t *write_p, uint32_t target, bool link, uint32_t cond) {
  if ((target & 3) == 0) {
    if (link) {
      arm_bl_cond(&write_p, cond, (target - (uint32_t)write_p - 8)>>2);
    } else {
      arm_b_cond(&write_p, cond, (target - (uint32_t)write_p - 8)>>2);
    }
  } else {
    emu_log_always("ERROR: Cannot insert branch from ARM to Thumb\n");
    while(1);
  }
}

void arm_adjust_b_bl_target(uint32_t *write_p, uint32_t dest_addr) {
  arm_instruction inst = arm_decode(write_p);

  if (inst != ARM_B) {
    emu_log_always("ARM: Trying to adjust target of invalid branch instruction.\n");
    while(1);
  }

  arm_branch_helper(write_p, dest_addr, inst == ARM_BL, *write_p >> 28);
}

void arm_b32_helper(uint32_t *write_p, uint32_t target, uint32_t cond) {
  arm_branch_helper(write_p, target, false, cond);
}

void arm_cc_branch(dbm_thread *thread_data, uint32_t *write_p, uint32_t target, uint32_t cond) {
  arm_b32_helper(write_p, target, cond);

  record_cc_link(thread_data, (uint32_t)write_p, target);
}

void arm_bl32_helper(uint32_t *write_p, uint32_t target, uint32_t cond) {
  arm_branch_helper(write_p, target, true, cond);
}

#define MIN_FSPACE 72

void pass1_arm(dbm_thread *thread_data, uint32_t *read_address, branch_type *bb_type) {
  uint32_t null, reglist, rd, dn, imm, offset;
  int32_t branch_offset;
  *bb_type = unknown;

  while(*bb_type == unknown) {
    arm_instruction inst = arm_decode(read_address);

    switch(inst) {
      case ARM_B:
      case ARM_BL:
        arm_b_decode_fields(read_address, &offset);

        if ((*read_address >> 28) == AL) {
#ifdef DBM_INLINE_UNCOND_IMM
          branch_offset = (offset & 0x800000) ? 0xFC000000 : 0;
          branch_offset |= (offset<<2);
          read_address = (uint32_t *)((int32_t)read_address + 8 + branch_offset) - 1; // read_address is incremented this iteration
#else
          *bb_type = uncond_imm_arm;
#endif
        } else {
          *bb_type = cond_imm_arm;
        }

        break;
      case ARM_BX:
      case ARM_BLX:
        *bb_type = ((*read_address >> 28) == AL) ? uncond_reg_arm : cond_reg_arm;
        break;

      case ARM_BLXI:
        *bb_type = ((*read_address >> 28) == AL) ? uncond_blxi_arm : cond_blxi_arm;
        break;

      case ARM_LDM:
        arm_ldm_decode_fields(read_address, &null, &reglist, &null, &null, &null, &null);

        if (reglist & (1 << pc)) {
          *bb_type = ((*read_address >> 28) == AL) ? uncond_reg_arm : cond_reg_arm;
        }
        break;

      case ARM_LDR:
        arm_ldr_decode_fields(read_address, &null, &rd, &null, &null, &null, &null, &null);

        if (rd == pc) {
          *bb_type = ((*read_address >> 28) == AL) ? uncond_reg_arm : cond_reg_arm;
        }
        break;

      case ARM_ADC:
      case ARM_ADD:
      case ARM_EOR:
      case ARM_MOV:
      case ARM_ORR:
      case ARM_SBC:
      case ARM_SUB:
      case ARM_RSC:
        arm_data_proc_decode_fields(read_address, &null, &null, &null, &rd, &null, &null);
        if (rd == pc) {
          *bb_type = ((*read_address >> 28) == AL) ? uncond_reg_arm : cond_reg_arm;
        }
        break;
    }

    read_address++;
  }
}

void arm_scanner_deliver_inline_callback(bool do_inline, dbm_thread *thread_data, uint32_t **o_write_p,
                                         uint32_t **o_data_p, int basic_block, arm_instruction inst) {
    uint32_t *write_p = *o_write_p;
    uint32_t *data_p = *o_data_p;

    mambo_context ctx;
    set_mambo_context(&ctx, thread_data, ARM_INST, -1, basic_block, inst, -1, NULL, write_p, NULL);
    emu_set_branch_inline(&ctx, do_inline);

    write_p = ctx.write_p;
    arm_check_free_space(thread_data, &write_p, &data_p, MIN_FSPACE);

    *o_write_p = write_p;
    *o_data_p = data_p;
}

bool arm_scanner_deliver_callbacks(dbm_thread *thread_data, mambo_cb_idx cb_id, uint32_t *read_address,
                                   arm_instruction inst, uint32_t **o_write_p, uint32_t **o_data_p,
                                   int basic_block, cc_type type, bool allow_write) {
  bool replaced = false;
#ifdef PLUGINS_NEW
  if (global_data.free_plugin > 0) {
    uint32_t *write_p = *o_write_p;
    uint32_t *data_p = *o_data_p;

    mambo_cond cond = (*read_address >> 28);
    if (cond == ALT) {
      cond = AL;
    }

    mambo_context ctx;
    set_mambo_context(&ctx, thread_data, ARM_INST, type, basic_block, inst, cond, read_address, write_p, NULL);

    for (int i = 0; i < global_data.free_plugin; i++) {
      if (global_data.plugins[i].cbs[cb_id] != NULL) {

        // The plugin might insert too many instructions. Let's make
        // sure we have at least MIN_FSPACE*2 instructions in the
        // block.
        arm_check_free_space(thread_data, &write_p, &data_p, MIN_FSPACE*2)
;
        ctx.write_p = write_p;
        ctx.plugin_id = i;
        ctx.replace = false;
        global_data.plugins[i].cbs[cb_id](&ctx);
        if (allow_write) {
          if (replaced && (write_p != ctx.write_p || ctx.replace)) {
            emu_log_always("MAMBO API WARNING: plugin %d added code for overridden"
                            "instruction (%p).\n", i, read_address);
          }
          if (ctx.replace) {
            if (cb_id == PRE_INST_C) {
              replaced = true;
            } else {
              emu_log_always("MAMBO API WARNING: plugin %d set replace_inst for "
                              "a disallowed event (at %p).\n", i, read_address);
            }
          }
          write_p = ctx.write_p;
          arm_check_free_space(thread_data, &write_p, &data_p, MIN_FSPACE);
        } else {
          emu_assert(ctx.write_p == write_p);
        }
      }
    }

    *o_write_p = write_p;
    *o_data_p = data_p;
  }
#endif
  return replaced;
}

void arm_inline_hash_lookup(dbm_thread *thread_data, uint32_t **o_write_p, int basic_block,
                            int reg1, int reg2, int reg3, uint32_t reglist, bool predictor,
                            unsigned int pc_incr, uint32_t **ret_branch) {
  uint32_t *write_p = *o_write_p;

  uint32_t *match_eq_br, arm_hash_lookup_loop;

  // MOVW+MOVT reg2, hash_table
  arm_copy_to_reg_32bit(&write_p, reg2, (uint32_t)thread_data->entry_address.entries);
  // MOVW+MOVT reg3, hash_mask
  arm_copy_to_reg_32bit(&write_p, reg3, CODE_CACHE_HASH_SIZE);

  // AND reg3, reg1, reg3
  arm_and(&write_p, REG_PROC, 0, reg3, reg1, reg3);
  write_p++;

  // ADD reg2, reg2, reg3, LSL #3
  arm_add(&write_p, REG_PROC, 0, reg2, reg2, reg3 | (LSL << 5) | (3 << 7));
  write_p++;

  // asm_hash_lookup_loop:
  arm_hash_lookup_loop = (uint32_t)write_p;
  // LDR reg3, [reg2], #8
  arm_ldr(&write_p, IMM_LDR, reg3, reg2, 8, 0, 1, 0);
  write_p++;

  // CMP reg3, reg1
  arm_cmp(&write_p, REG_PROC, reg3, reg1);
  write_p++;

  if (ret_branch) {
    *ret_branch = write_p;
  }

  // BEQ arm_hash_lookup_ret
  match_eq_br = write_p++;

  // CMP reg3, #0
  arm_cmp(&write_p, IMM_PROC, reg3, 0);
  write_p++;

  // BNE asm_hash_lookup_loop
  arm_b32_helper(write_p, arm_hash_lookup_loop, NE);
  write_p++;

  // arm_hash_lookup_fail:
  // MOV reg2, #thread_scratch_regs
  arm_copy_to_reg_32bit(&write_p, reg2, (uint32_t)thread_data->scratch_regs);

  // STMIA reg2, {R0-R2}
  arm_stm(&write_p, reg2, (1 << r0)|(1 << r1)|(1 << r2), 0, 1, 0, 0);
  write_p++;

  // MOV R0, reg1
  arm_mov(&write_p, REG_PROC, 0, r0, reg1);
  write_p++;

  // MOV R1, reg2
  arm_mov(&write_p, REG_PROC, 0, r1, reg2);
  write_p++;

  // POP {reglist}
  if((((1 << r0) | (1 << r1)) & reglist) != 0) {
    emu_log_always("[arm_inline_hash_lookup] scanner_arm going into infinite loop because reglist has r0 and r1!");
  }

  while((((1 << r0) | (1 << r1)) & reglist) != 0);

  // Restore the CPSR
  arm_pop_regs(1 << reg2);
  arm_msr(&write_p, reg2, 3);
  write_p++;

  arm_pop_regs(reglist & 0x7FFF);

  // STR R2, [R1, #8]
  arm_str(&write_p, IMM_LDR, r2, r1, 8, 1, 1, 0);
  write_p++;

  // MOV R1, #BB_ID
  arm_copy_to_reg_32bit(&write_p, r1, basic_block);

  // if the PC was to be POPed off the stack: ADD SP, SP, #4
  if ((reglist & (1 << pc)) && !(reglist & (1 << sp))) {
    emu_assert(pc_incr < 0x1000);
    arm_add(&write_p, IMM_PROC, 0, sp, sp, pc_incr);
    write_p++;
  }

  // B dispatcher_trampoline
  arm_b32_helper(write_p, (uint32_t)thread_data->dispatcher_addr, AL);
  write_p++;

  // saved_pc: .word
  write_p++;

  // arm_hash_lookup_ret:
  arm_b32_helper(match_eq_br, (uint32_t)write_p, EQ);
  // LDR reg1, [reg2, -4]
  arm_ldr(&write_p, IMM_LDR, reg1, reg2, 4, 1, 0, 0);
  write_p++;

  *o_write_p = write_p;
}

enum arm_ihl_branch {
  IHL_BRANCH_LDR_PC_PC,
  IHL_BRANCH_POP,
  IHL_BRANCH_LDM
};

int arm_ihl_result_branch(dbm_thread *thread_data, enum arm_ihl_branch type, uint32_t **o_write_p,
                          uint32_t reglist, uint32_t sr[3], bool ind_p_pred, unsigned int pc_incr) {
  uint32_t *write_p = *o_write_p;
  uint32_t *saved_target = write_p-2;

  switch (type) {
    case IHL_BRANCH_LDR_PC_PC:
      arm_str(&write_p, IMM_LDR, sr[0], pc, 16, 1, 0, 0);
      write_p++;

      // Restore the CPSR
      arm_pop_regs(1 << sr[1]);
      arm_msr(&write_p, sr[1], 3);
      write_p++;

      arm_pop_regs(reglist & 0x7FFF);

      if ((reglist & (1 << pc)) && !(reglist & (1 << sp))) {
        emu_assert(pc_incr < 0x1000);
        arm_add(&write_p, IMM_PROC, 0, sp, sp, pc_incr);
        write_p++;
      }

      arm_ldr(&write_p, IMM_LDR, pc, pc, (write_p+2-saved_target) << 2, 1, 0, 0);
      write_p++;
      break;

    case IHL_BRANCH_POP:
      arm_str(&write_p, IMM_LDR, sr[0], sp, (count_bits(reglist)-1) << 2, 1, 1, 0);
      write_p++;

      arm_pop_regs(reglist | (1 << pc));
      write_p++;
      break;

    case IHL_BRANCH_LDM:
      emu_assert((reglist & (1 << r12)) == 0);

      // MOV{W,T} sr2, #scratch_regs
      arm_copy_to_reg_32bit(&write_p, sr[2], (uint32_t)thread_data->scratch_regs);

      // STR R12, [sr2, #0]
      arm_str(&write_p, IMM_LDR, r12, sr[2], 0, 1, 1, 0);
      write_p++;

      // STR sr0, [sr2, #4] // CC target
      arm_str(&write_p, IMM_LDR, sr[0], sr[2], 4, 1, 1, 0);
      write_p++;

      // MOV r12, sr2
      arm_mov(&write_p, REG_PROC, 0, r12, sr[2]);
      write_p++;

      // POP {reglist - PC}
      arm_pop_regs(reglist & 0x7FFF);

      if ((reglist & (1 << pc)) && !(reglist & (1 << sp))) {
        emu_assert(pc_incr < 0x1000);
        // ADD SP, SP, #4
        arm_add(&write_p, IMM_PROC, 0, sp, sp, pc_incr);
	      write_p++;
      }

      // LDM R12, {R12, PC}
      arm_ldm(&write_p, r12, (1 << r12) | (1 << pc), 0, 1, 0, 0);
      write_p++;
      break;

    default:
      return -1;
  }

  *o_write_p = write_p;

  return 0;
}

uint32_t arm_ihl_static_sr(uint32_t **o_write_p, uint32_t *sr) {
  uint32_t to_push;
  uint32_t *write_p = *o_write_p;

  sr[0] = r4;
  sr[1] = r5;
  sr[2] = r6;

  to_push = (1 << sr[0]) | (1 << sr[1]) | (1 << sr[2]);
  arm_push_regs(to_push);

  *o_write_p = write_p;
  return to_push;
}

void arm_ihl_tr_rn_rm(uint32_t **o_write_p, uint32_t *read_address, uint32_t *sr, enum reg *rn, enum reg *rm, uint32_t *operand2) {
  uint32_t scratch_reg;
  uint32_t *write_p = *o_write_p;

  emu_assert(*rn != pc || *rm != pc);
  if (*rn == pc || *rm == pc) {
    if (*rn == pc) {
      scratch_reg = (*rm != sr[1]) ? sr[1] : sr[2];
      *rn = scratch_reg;
    } else if (*rm == pc) {
      scratch_reg = (*rn != sr[1]) ? sr[1] : sr[2];
      *rm = scratch_reg;
      *operand2 = (*operand2 & (~0xF)) | *rm;
    }
    arm_copy_to_reg_32bit(&write_p, scratch_reg, (uint32_t)read_address + 8);
  }

  *o_write_p = write_p;
}

size_t scan_arm(dbm_thread *thread_data, uint32_t *read_address, int basic_block, cc_type type, uint32_t *write_p) {
  bool stop = false;

  uint32_t *scratch_data;
  uint32_t scratch_offset;
  uint32_t condition_code;
  uint32_t scratch_reg;

  int32_t  branch_offset;
  uint32_t target;
  uint32_t return_addr;
  uint32_t *tr_start;

  if (write_p == NULL) {
    write_p = (uint32_t *)&thread_data->code_cache->blocks[basic_block];
  }
  uint32_t start_address = (uint32_t)write_p;

  uint32_t *data_p;
  if (type == mambo_bb) {
    data_p = write_p + BASIC_BLOCK_SIZE;
  } else {
    data_p = (uint32_t *)&thread_data->code_cache->traces + (TRACE_CACHE_SIZE/4);
  }

  debug("write_p: %p\n", write_p);

#ifdef DBM_TRACES
  branch_type bb_type;
  pass1_arm(thread_data, read_address, &bb_type);

  if (type == mambo_bb && bb_type == cond_imm_arm) {
    arm_push_regs((1 << r0) | (1 << r1) | (1 << r2) | (1 << lr));

    arm_copy_to_reg_32bit(&write_p, r0, basic_block);

    arm_bl32_helper(write_p, thread_data->trace_head_incr_addr-5, AL);
    write_p++;
  }
#endif

  while(!stop) {
    debug("arm scan read_address: %p\n", read_address);
    arm_instruction inst = arm_decode(read_address);
    debug("Instruction enum: %d\n", (inst == ARM_INVALID) ? -1 : inst);

    debug("instruction word: 0x%x\n", *read_address);
#ifdef PLUGINS_NEW
    bool skip_inst = arm_scanner_deliver_callbacks(thread_data, PRE_INST_C, read_address, inst,
                                                   &write_p, &data_p, basic_block, type, true);
    if (!skip_inst) {
#endif

    switch(inst) {
      /* Instructions which are allowed to use the PC */
      case ARM_ADC:
      case ARM_ADD:
      case ARM_EOR:
      case ARM_MOV:
      case ARM_ORR:
      case ARM_SBC:
      case ARM_SUB:
      case ARM_RSC: {
        uint32_t immediate, opcode, set_flags, rd, rn, operand2, rm = reg_invalid;
        arm_data_proc_decode_fields(read_address, &immediate, &opcode, &set_flags, &rd, &rn, &operand2);

        if(rd != pc && rn != pc && (immediate == IMM_PROC || (operand2 & 0xF) != pc)) {
          copy_arm();
        } else {
          if (immediate == REG_PROC) {
            rm = operand2 & 0xF;
          }
          if (rd == pc) {
            emu_assert(set_flags == 0);
#ifdef LINK_BX_ALT
            if ((*read_address >> 28) != AL) {
              target = lookup_or_stub(thread_data, (uint32_t)read_address + 4);
              arm_cc_branch(thread_data, write_p, target,
                            arm_inverse_cond_code[*read_address >> 28]);
              write_p++;
            }
#endif

            thread_data->code_cache_meta[basic_block].exit_branch_type = uncond_reg_arm;
            thread_data->code_cache_meta[basic_block].exit_branch_addr = (uint16_t *)write_p;

#ifdef DBM_D_INLINE_HASH
  #ifndef LINK_BX_ALT
    emu_assert(0);
  #endif
            uint32_t sr[3];
            uint32_t registers = 0;

            arm_ihl_static_sr(&write_p, sr);
            registers = (1 << sr[0]) | (1 << sr[1]) | (1 << sr[2]);

            arm_ihl_tr_rn_rm(&write_p, read_address, sr, &rn, &rm, &operand2);

            arm_data_proc(&write_p, immediate, opcode, set_flags, sr[0], rn, operand2);
            write_p++;

            arm_check_free_space(thread_data, &write_p, &data_p, 128);

            // Save CPSR
            arm_mrs(&write_p, sr[1]);
            write_p++;
            arm_push_regs(1 << sr[1]);

            arm_inline_hash_lookup(thread_data, &write_p, basic_block, sr[0], sr[1], sr[2], registers, false, 4, NULL);
            arm_ihl_result_branch(thread_data, IHL_BRANCH_LDR_PC_PC, &write_p, registers, sr, false, 4);

            stop = true;
            break;
#endif
            /* This is an indirect branch */
            arm_branch_save_context(thread_data, &write_p);
            arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), SETUP);
          }
          if (rn == pc && rm == pc) {
            emu_log_always("Unhandled ARM ADD, etc\n");
            while(1);
          }

          if (rn == pc || rm == pc) {
            /* If rd != PC and rd != rn && rd != rm, we can use rd as a scratch register */
            if (rd == pc || (rn == pc && rm == rd) || (rm == pc && rn == rd)) {
              scratch_reg = r0;
              while ((rm == scratch_reg) || (rn == scratch_reg) || (rd == scratch_reg)) {
                scratch_reg++;
              }
              // In this case the context hasn't been saved, we need to preserve the value of the scratch register
              if (rd != pc) {
                arm_cond_push_reg(*read_address >> 28, scratch_reg);
              } else {
                // r0, r1 (and optionally r2) are saved, but the value of r1 is set by the prev. call to arm_branch_jump
                emu_assert(scratch_reg == r0);
              }
            } else {
              scratch_reg = rd;
            }
            arm_cond_copy_to_reg_32bit(&write_p, *read_address >> 28, scratch_reg, (uint32_t)read_address + 8);
          }

          if (inst != ARM_MOV || set_flags == 1) {
            arm_data_proc_cond(&write_p, (*read_address >> 28), immediate, opcode, set_flags,
                          (rd == pc) ? r0 : rd, (rn == pc) ? scratch_reg : rn, (rm == pc) ? (scratch_reg | (operand2 & 0xFF0)) : operand2);
            write_p++;
          }

          // Restore the value of the scratch register
          if ((rn == pc || rm == pc) && rd != pc && scratch_reg != rd) {
            arm_cond_pop_reg(*read_address >> 28, scratch_reg);
          }

          if (rd == pc) {
            arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), INSERT_BRANCH);
            stop = true;
          }
        }
        break;
      }

      case ARM_B:
      case ARM_BL: {
        uint32_t offset;
        arm_b_decode_fields(read_address, &offset);

        branch_offset = (offset & 0x800000) ? 0xFC000000 : 0;
        branch_offset |= (offset<<2);
        target = (uint32_t)read_address + 8 + branch_offset;
        condition_code = (*read_address >> 28);

        uint32_t orig_lr = (uint32_t)read_address + 4;
#ifdef DBM_INLINE_UNCOND_IMM
        if (condition_code == AL) {
          emu_intercept_func_t* intercept_func = emu_intercept_lookup(target);
          if (intercept_func != NULL) {
            read_address = (uint32_t*)intercept_func->trampoline_read_addr - 1;
            emu_intercept_triggered(thread_data, intercept_func);
          } else {
            read_address = (uint32_t *)target - 1; // read_address is incremented this iteration
          }
          arm_scanner_deliver_inline_callback(true, thread_data, &write_p, &data_p, basic_block, inst);
          if (inst == ARM_BL) {
              arm_copy_to_reg_32bit(&write_p, lr, orig_lr);
          }

          break;
        }
#endif

        arm_scanner_deliver_inline_callback(false, thread_data, &write_p, &data_p, basic_block, inst);
        if (inst == ARM_BL) {
            arm_copy_to_reg_32bit(&write_p, lr, orig_lr);
        }

        thread_data->code_cache_meta[basic_block].exit_branch_type = (condition_code == AL) ? uncond_imm_arm : cond_imm_arm;
        thread_data->code_cache_meta[basic_block].exit_branch_addr = (uint16_t *)write_p;
        thread_data->code_cache_meta[basic_block].branch_taken_addr = target;
        thread_data->code_cache_meta[basic_block].branch_skipped_addr = (uint32_t)read_address + 4;
        thread_data->code_cache_meta[basic_block].branch_condition = condition_code;

        if (condition_code != AL) {
          // Reserve space for the conditional branch instruction
          arm_nop(&write_p);
          write_p++;
        }

        arm_branch_save_context(thread_data, &write_p);
        arm_branch_jump(thread_data, &write_p, basic_block, offset, read_address, condition_code, SETUP|REPLACE_TARGET|INSERT_BRANCH);
        stop = true;

        break;
      }

      case ARM_BX:
      case ARM_BLX: {
        uint32_t link, rn;
        arm_bx_t_decode_fields(read_address, &link, &rn);
        emu_assert(rn != pc);

#ifdef LINK_BX_ALT
        if ((*read_address >> 28) != AL) {
          debug("w: %p, r: %p, bb: %d\n", write_p, read_address, basic_block);
          target = lookup_or_stub(thread_data, (uint32_t)read_address + 4);
          debug("stub: %p\n", target);
          arm_cc_branch(thread_data,write_p, target,
                        arm_inverse_cond_code[(*read_address >> 28)]);
          write_p++;
        }
#endif // LINK_BX_ALT

        if (inst == ARM_BLX) {
          arm_copy_to_reg_32bit(&write_p, lr, (uint32_t)read_address + 4);
        }
        thread_data->code_cache_meta[basic_block].exit_branch_type = uncond_reg_arm;
        thread_data->code_cache_meta[basic_block].exit_branch_addr = (uint16_t *)write_p;
        thread_data->code_cache_meta[basic_block].rn = rn;

#ifdef DBM_D_INLINE_HASH
  #ifndef LINK_BX_ALT
        emu_assert(0);
  #endif
          uint32_t sr[3];
          uint32_t to_push;
          uint32_t registers = 0;
          uint32_t *branch_addr;

          sr[0] = r4;
          sr[1] = r5;
          sr[2] = r6;
          to_push = (1 << sr[0]) | (1 << sr[1]) | (1 << sr[2]);

          if (to_push) {
            arm_push_regs(to_push);
            registers |= to_push;
          }

          // MOV sr[0], rn
          if (sr[0] != rn) {
            arm_mov(&write_p, REG_PROC, 0, sr[0], rn);
            write_p++;
          }

          arm_check_free_space(thread_data, &write_p, &data_p, 140);

          // Save CPSR
          arm_mrs(&write_p, sr[1]);
          write_p++;
          arm_push_regs(1 << sr[1]);

          arm_inline_hash_lookup(thread_data, &write_p, basic_block, sr[0], sr[1], sr[2], registers, false, 4, &branch_addr);
          arm_ihl_result_branch(thread_data, IHL_BRANCH_LDR_PC_PC, &write_p, registers, sr, false, 4);
          stop = true;
          break;
#endif // DBM_D_INLINE_HASH

#if !defined(DBM_D_INLINE_HASH)
        arm_branch_save_context(thread_data, &write_p);

        arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), SETUP);

        // Branch taken (not taken MOV is inserted by arm_branch_jump(SETUP))
        arm_mov_cond(&write_p, (*read_address >> 28), REG_PROC, false, r0, rn);
        write_p++;

        arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), INSERT_BRANCH);
#endif

        stop = true;
        break;
      }

      case ARM_BLXI: {
        uint32_t h, offset;
        arm_blxi_decode_fields(read_address, &h, &offset);

        branch_offset = ((h << 1) | (offset << 2)) + 1;
        if (branch_offset & 0x2000000) { branch_offset |= 0xFC000000; }

        arm_copy_to_reg_32bit(&write_p, lr, (uint32_t)read_address + 4);

        thread_data->code_cache_meta[basic_block].exit_branch_type = uncond_blxi_arm;
        thread_data->code_cache_meta[basic_block].exit_branch_addr = (uint16_t *)write_p;

        arm_branch_save_context(thread_data, &write_p);
        arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), SETUP);

        arm_copy_to_reg_32bit(&write_p, r0, (uint32_t)read_address + 8 + branch_offset);

        arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), INSERT_BRANCH);
        stop = true;

        break;
      }

      case ARM_LDM: {
        uint32_t rn, registers, prepostindex, updown, writeback, psr;
        int ret;
        arm_ldm_decode_fields(read_address, &rn, &registers, &prepostindex, &updown, &writeback, &psr);

        if ((registers & (1 << 15)) == 0) {
          copy_arm();
        } else {
          condition_code = *read_address & 0xF0000000;
#ifdef LINK_BX_ALT
          if ((condition_code >> 28) != AL) {
            target = lookup_or_stub(thread_data, (uint32_t)read_address + 4);
            arm_cc_branch(thread_data, write_p, target,
                          arm_inverse_cond_code[condition_code >> 28]);
            write_p++;
          }
#else
          emu_assert((condition_code >> 28) == AL);
#endif

          thread_data->code_cache_meta[basic_block].exit_branch_type = uncond_reg_arm;
          thread_data->code_cache_meta[basic_block].exit_branch_addr = (uint16_t *)write_p;

#ifdef DBM_D_INLINE_HASH
            uint32_t to_push = 0, sr[3];
            if (rn != sp || !updown || (!writeback && !(registers & (1 << sp))) || prepostindex || psr) {
              emu_log_always("Panic: LDM ...{PC} not a POP\n");
              while(1);
            }

            // ALLOWED_IHL_REGS is {R3 - R12, R14}
            ret = get_n_regs(registers & ALLOWED_IHL_REGS, sr, 3);
            if (ret != 3) {
              // If this is a POP and enough low registers can be pushed...
              if (rn == sp && (writeback || (registers & (1 << sp)))
                  && ((int)next_reg_in_list(registers, 0) - 3 + count_bits(registers & ALLOWED_IHL_REGS)) >= 3) {
                scratch_reg = r3 - 1; // it's incremented by at least 1 before use
                for (int r = 0; r < 3; r++) {
                  if (sr[r] == reg_invalid) {
                    do {
                      scratch_reg++;
                    } while ((1 << scratch_reg) & registers);
                    if (ret > 0) {
                      emu_assert(scratch_reg < sr[0]);
                    }
                    sr[r] = scratch_reg;
                    to_push |= 1 << scratch_reg;
                  }
                }
              } else {
                emu_assert((registers & (1 << rn)) == 0);
                arm_ldm(&write_p, rn, registers & 0x7FFF, prepostindex, updown, writeback, psr);
                write_p++;

                registers = 1 << pc;

                sr[0] = r4;
                sr[1] = r5;
                sr[2] = r6;
                to_push = (1 << sr[0]) | (1 << sr[1]) | (1 << sr[2]);
              }
            }

            if (to_push) {
              arm_push_regs(to_push);
              registers |= to_push;
            }

            // LDR SR[0], [SP, #pc_offset]
            arm_ldr(&write_p, IMM_LDR, sr[0], rn, (count_bits(registers)-1) << 2, 1, 1, 0);
            write_p++;

            arm_check_free_space(thread_data, &write_p, &data_p, 136);

            // If this LDM instruction is also going to pop r0 and/or
            // r1, let's do that separately here for
            // correctness. We'll also modify the register list and
            // mask off r0 and r1. We're doing this here because
            // arm_inline_hash_lookup saves r0-r1 into the scratch
            // space and then uses those values for its own
            // purposes. So by doing this here, we're ensuring that
            // only the latest values of r0 and r1 get saved to the
            // scratch space.
            if ((registers & 0b11) != 0) {
              arm_pop_regs(registers & 0b11);
              registers = registers & ~0b11;
            }

            // Save CPSR
            arm_mrs(&write_p, sr[1]);
            write_p++;
            arm_push_regs(1 << sr[1]);

            arm_inline_hash_lookup(thread_data, &write_p, basic_block, sr[0], sr[1], sr[2], registers, false, 4, NULL);
            arm_ihl_result_branch(thread_data, IHL_BRANCH_LDR_PC_PC, &write_p, registers, sr, false, 4);

            stop = true;
            break;
#endif
          if ((registers & 0x7FFF) && (registers & (1 << rn)) == 0) {
            arm_ldm_cond(&write_p, (condition_code >> 28), rn, registers & 0x7FFF, prepostindex, updown, writeback, psr);
            write_p++;
          }

#if !defined(DBM_D_INLINE_HASH)
          arm_branch_save_context(thread_data, &write_p);
          arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), SETUP);

          if ((registers & (1 << rn)) == 0) {
            arm_ldm_cond(&write_p, (*read_address >> 28), rn, (1 << r0), prepostindex, updown, writeback, psr);
            write_p++;
          } else {
            emu_assert (prepostindex == 0 && updown == 1); // untested

            arm_ldr_cond(&write_p, (*read_address >> 28), IMM_LDR, r0, rn, (count_bits(registers) - 1) << 2, 1, 1, 0);
            write_p++;

            if (registers & 0x7FFF) {
              arm_ldm_cond(&write_p, (*read_address >> 28), rn, registers & 0x7FFF, prepostindex, updown, writeback, psr);
              write_p++;
            }
          }

          arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), INSERT_BRANCH);
#endif
          stop = true;
        }
        break;
      }

      case ARM_LDRB:
      case ARM_LDR: {
        uint32_t immediate, rd, rn, offset, prepostindex, updown, writeback, rm = reg_invalid;
#ifdef DBM_D_INLINE_HASH
        uint32_t sr[3], registers, target_offset;
#endif
        switch (inst) {
          case ARM_LDRB:
            arm_ldrb_decode_fields(read_address, &immediate, &rd, &rn, &offset, &prepostindex, &updown, &writeback);
            break;
          case ARM_LDR:
            arm_ldr_decode_fields(read_address, &immediate, &rd, &rn, &offset, &prepostindex, &updown, &writeback);
            break;
        }
        if (immediate == LDR_REG) {
          rm = offset & 0xF;
          emu_assert(rm != pc);
        }

        condition_code = *read_address & 0xF0000000;

#ifdef LINK_BX_ALT
        if (rd == pc && (*read_address >> 28) != AL) {
          target = lookup_or_stub(thread_data, (uint32_t)read_address + 4);
          arm_cc_branch(thread_data, write_p, target,
                        arm_inverse_cond_code[*read_address >> 28]);
          write_p++;
        }
#endif

        if (rd == pc || rn == pc) {
          if (rd == pc) {
            emu_assert(inst == ARM_LDR);
            thread_data->code_cache_meta[basic_block].exit_branch_type = uncond_reg_arm;
            thread_data->code_cache_meta[basic_block].exit_branch_addr = (uint16_t *)write_p;

#ifdef DBM_D_INLINE_HASH
  #ifndef LINK_BX_ALT
            emu_assert(0);
  #endif
            arm_ihl_static_sr(&write_p, sr);
            arm_ihl_tr_rn_rm(&write_p, read_address, sr, &rn, &rm, &offset);
            registers = (1 << sr[0]) | (1 << sr[1]) | (1 << sr[2]);

            if (rn == sp) {
              emu_assert(!immediate && updown);

              target_offset = prepostindex ? offset : 0;
              target_offset += count_bits(registers) << 2;
              emu_assert(target_offset < 0x1000);
              arm_ldr(&write_p, IMM_LDR, sr[0], rn, target_offset, 1, 1, 0);

              if (prepostindex) {
                emu_assert(!writeback);
              } else {
                registers |= (1 << pc);
              }
            } else { // rn != sp
              arm_ldr(&write_p, immediate, sr[0], rn, offset, prepostindex, updown, writeback);
              if (writeback) {
                // Ensure the writeback is not applied to one of the previously saved scratch registers
                emu_assert(((1 << rn) & registers) == 0);
              }
            }
            write_p++;

            arm_check_free_space(thread_data, &write_p, &data_p, 136);

            // Save CPSR
            arm_mrs(&write_p, sr[1]);
            write_p++;
            arm_push_regs(1 << sr[1]);

            arm_inline_hash_lookup(thread_data, &write_p, basic_block, sr[0], sr[1], sr[2], registers, false, offset, NULL);
            arm_ihl_result_branch(thread_data, IHL_BRANCH_LDR_PC_PC, &write_p, registers, sr, false, offset);

            stop = true;
            break;
#endif
            arm_branch_save_context(thread_data, &write_p);
            arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), SETUP);
          }
          scratch_reg = r0;
          if (rn == pc) {
            while ((rd != pc && scratch_reg == rd) || (immediate == LDR_REG && ((offset & 0xF) == scratch_reg))) {
              scratch_reg++;
            }
            if (rd != pc) {
              arm_cond_push_reg(condition_code >> 28, scratch_reg);
            }
            arm_cond_copy_to_reg_32bit(&write_p, condition_code >> 28, scratch_reg, (uint32_t)read_address + 8);
          }

          switch (inst) {
            case ARM_LDRB:
              arm_ldrb_cond(&write_p, (condition_code >> 28), immediate, (rd == pc) ? r0 : rd, (rn == pc) ? scratch_reg : rn, offset, prepostindex, updown, writeback);
              break;
            case ARM_LDR:
              arm_ldr_cond(&write_p, (condition_code >> 28), immediate, (rd == pc) ? r0 : rd, (rn == pc) ? scratch_reg : rn, offset, prepostindex, updown, writeback);
              break;
          }
          *write_p |= condition_code;
          write_p++;

          // TODO: fixme
          /* LDR is used to load a value into PC, with writeback.
             If the base register is R0, R1, or R2, it would have
             already been saved before it is written back.
             A proper fix should refactor the branching code to save */
          if (rd == pc && writeback) {
            switch (rn) {
              case r0:
              case r1:
              case r2:
                emu_log_always("LDR with writeback");
                while(1);
                break;
            }
          }

          if (rd == pc) {
            arm_branch_jump(thread_data, &write_p, basic_block, 0, read_address, (*read_address >> 28), INSERT_BRANCH);
            stop = true;
          } else {
            arm_cond_pop_reg(condition_code >> 28, scratch_reg);
          }
        } else {
          copy_arm();
        }
        break;
      }

      case ARM_STM: {
        uint32_t rn, registers, prepostindex, updown, writeback, psr, offset;
        arm_stm_decode_fields(read_address, &rn, &registers, &prepostindex, &updown, &writeback, &psr);
        emu_assert(rn != pc);

        if (registers & (1 << pc)) {
          /* Example :            STMFD SP!, {SP, LR, PC}

             is translated to:    STMFD SP!, {SP, LR, PC}
                                  MOW LR, #(spc & 0xFFFF)
                                  MOVT LR, #(spc >> 16)
                                  STR LR, [SP, #8]
                                  LDR LR, [SP, #4]

             Note that if rn is in the reglist, then its value must be stored first.
             This example was encountered in libgcc.
          */
          condition_code = (*read_address >> 28);
          if (condition_code != AL) {
            tr_start = write_p;
            write_p++;
          }

          scratch_reg = ~((1 << pc) | (1 << rn)) & registers;
          emu_assert(scratch_reg);
          copy_arm();

          // Overwrite the saved TPC
          scratch_reg = next_reg_in_list(scratch_reg, 0);
          emu_assert(scratch_reg < pc && scratch_reg != rn);
          arm_copy_to_reg_32bit(&write_p, scratch_reg, (uint32_t)read_address + 8);
          if (writeback) {
            offset = (count_bits(registers) - 1) << 2;
          }
          if (!prepostindex) {
            offset -= 4;
          }
          arm_str(&write_p, IMM_LDR, scratch_reg, rn, offset, 1, 1, 0);
          write_p++;

          // Calculate the offset of the location where the SR was saved
          offset = (scratch_reg > rn && (registers & (1 << rn))) ? 4 : 0;
          offset += prepostindex ? 0 : 4;

          arm_ldr(&write_p, IMM_LDR, scratch_reg, rn, offset, 1, updown ? 0 : 1, 0);
          write_p++;

          while (!writeback || !prepostindex || updown); // implement this

          if (condition_code != AL) {
            arm_b32_helper(tr_start, (uint32_t)write_p, arm_inverse_cond_code[condition_code]);
          }

          while(rn != sp); // Check me
        } else {
          copy_arm();
        }
        break;
      }

      case ARM_STRB:
      case ARM_STR: {
        uint32_t immediate, rd, rn, offset, prepostindex, updown, writeback;
        switch (inst) {
          case ARM_STRB:
            arm_strb_decode_fields(read_address, &immediate, &rd, &rn, &offset, &prepostindex, &updown, &writeback);
            break;
          case ARM_STR:
            arm_str_decode_fields(read_address, &immediate, &rd, &rn, &offset, &prepostindex, &updown, &writeback);
            break;
        }
        if (immediate == 1) emu_assert((offset & 0xF) != pc);

        if (rd == pc) {
          condition_code = (*read_address & 0xF0000000) >> 28;
          emu_assert(condition_code == AL && rn == sp & prepostindex && !updown && writeback); // PUSH {PC}

          // SUB SP, SP, #8
          arm_add_sub_32_bit(&write_p, sp, sp, -8);

          // STR R0, [SP, #0]
          arm_str(&write_p, IMM_LDR, r0, sp, 0, 1, 1, 0);
          write_p++;

          // MOV{W,T} R0, addr
          arm_copy_to_reg_32bit(&write_p, r0, (uint32_t)read_address + 8);

          // STR R0, [SP, #4]
          arm_str(&write_p, IMM_LDR, r0, sp, 4, 1, 1, 0);
          write_p++;

          // POP {R0}
          arm_pop_reg(r0);
        } else if(rn == pc) {
          condition_code = *read_address & 0xF0000000;

          scratch_reg = r0;
          while (rd == scratch_reg || (offset & 0xF) == scratch_reg) {
            scratch_reg++;
          }

          arm_cond_push_reg(condition_code >> 28, scratch_reg);
          arm_cond_copy_to_reg_32bit(&write_p, condition_code >> 28, scratch_reg, (uint32_t)read_address + 8);

          switch (inst) {
            case ARM_STRB:
              arm_strb_cond(&write_p, (condition_code >> 28), immediate, rd, scratch_reg, offset, prepostindex, updown, writeback);
              break;
            case ARM_STR:
              arm_str_cond(&write_p, (condition_code >> 28), immediate, rd, scratch_reg, offset, prepostindex, updown, writeback);
              break;
          }
          *write_p |= condition_code;
          write_p++;

          arm_cond_pop_reg(condition_code >> 28, scratch_reg);
        } else {
          copy_arm();
        }
        break;
      }

      /* Other translated sensitive instructions */
      case ARM_MCR:
      case ARM_MRC: {
        uint32_t opc1, load_store, crn, rd, coproc, opc2, crm;
        arm_coproc_trans_decode_fields(read_address, &opc1, &load_store, &crn, &rd, &coproc, &opc2, &crm);

        // thread id
        if (coproc == 15 && opc1 == 0 && crn == 13 && crm == 0 && opc2 == 3 && load_store == 1 && rd != pc) {
          condition_code = (*read_address >> 28);
          if (condition_code != AL) {
            tr_start = write_p++;
          }
          arm_copy_to_reg_32bit(&write_p, rd, (uint32_t)(&thread_data->tls));
          arm_ldr(&write_p, IMM_LDR, rd, rd, 0, 1, 1, 0);
          write_p++;
          if (condition_code != AL) {
            arm_b32_helper(tr_start, (uint32_t)write_p, condition_code ^ 1);
          }

        // NEON / FP VMRS/VMSR
        } else if (coproc == 10 && opc1 == 7 && crn == 1 && rd != pc) {
          copy_arm();
        // Performance counter
        // This is used in OpenSSL in OPENSSL_cpuid_setup, a constructor. WTF
        } else if (coproc == 15 && opc1 == 0 && crn == 9 && crm == 13 && opc2 == 0 && load_store == 1 && rd != pc) {
          copy_arm();
        } else {
          emu_log_always("unknown coproc: %d %d %d %d %d %d\n", opc1, crn, rd, coproc, opc2, crm);
          while(1);
        }
        break;
      }

      case ARM_SVC: {
        condition_code = (*read_address >> 28) & 0xF;

        if (condition_code != AL) {
          tr_start = write_p;
          write_p++;
        }

        arm_push_reg(r0);

        arm_copy_to_reg_32bit(&write_p, r0, (uint32_t)thread_data->scratch_regs);
        arm_stm(&write_p, r0, (1 << r8) | (1 << r9) | (1 << r14), 0, 1, 0, 0);
        write_p++;

        arm_mov(&write_p, REG_PROC, 0, r9, r0);
        write_p++;

        arm_pop_reg(r0);

        arm_copy_to_reg_32bit(&write_p, r8, (uint32_t)read_address + 4);

        arm_bl(&write_p, (thread_data->syscall_wrapper_addr - (uint32_t)write_p - 8) >> 2);
        write_p++;

        if (condition_code != AL) {
          arm_b32_helper(tr_start, (uint32_t)write_p, condition_code);
        }
        break;
      }

      case ARM_UDF: {
        if (start_address != (uint32_t)write_p) {
          arm_cc_branch(thread_data, write_p, lookup_or_stub(thread_data, (uint32_t)read_address), AL);
          write_p++;
          stop = true;
        } else {
          emu_log_always("TODO: generate SIGILL\n");
        }
        break;
      }

      /* Instructions which could access the PC, but shouldn't. */
      case ARM_AND:
      case ARM_BIC:
      case ARM_RSB: {
        uint32_t immediate, set_flags, rd, rn, operand2;
        arm_and_decode_fields(read_address, &immediate, &set_flags, &rd, &rn, &operand2);
        emu_assert(rd != pc && rn != pc);
        if (immediate == 0) emu_assert((operand2 & 0xF) != pc);
        copy_arm();
        break;
      }

      case ARM_CMN:
      case ARM_CMP:
      case ARM_TEQ:
      case ARM_TST: {
        uint32_t immediate, rn, operand2;
        arm_cmn_decode_fields(read_address, &immediate, &rn, &operand2);
        emu_assert(rn != pc);
        if (immediate == 0) emu_assert((operand2 & 0xF) != pc);
        copy_arm();
        break;
      }

      case ARM_MOVW:
      case ARM_MOVT: {
        uint32_t immediate, opcode, set_flags, rd, rn, operand2;
        arm_data_proc_decode_fields(read_address, &immediate, &opcode, &set_flags, &rd, &rn, &operand2);
        // Rn is actually the top 4 bits of the immediate value
        emu_assert(rd != pc);
        copy_arm();
        break;
      }

      case ARM_MVN: {
        uint32_t immediate, set_flags, rd, operand2;
        arm_mvn_decode_fields(read_address, &immediate, &set_flags, &rd, &operand2);
        emu_assert(rd != pc);
        if (immediate == REG_PROC) emu_assert((operand2 & 0xF) != pc);
        copy_arm();
        break;
      }

      case ARM_MUL:
      case ARM_MLA:
      case ARM_MLS: {
        uint32_t accumulate, set_flags, rd, rm, rs, rn;
        arm_multiply_decode_fields(read_address, &accumulate, &set_flags, &rd, &rm, &rs, &rn);
        emu_assert(rd != pc && rm != pc && rs != pc && rn != pc);
        copy_arm();
        break;
      }

      case ARM_LDRH:
      case ARM_LDRHT:
      case ARM_STRH:
      case ARM_LDRD:
      case ARM_STRD:
      case ARM_LDRSH: {
        uint32_t opcode, size, opcode2, immediate, rd, rn, rm, imm4h, prepostindex, updown, writeback;
        arm_h_data_transfer_decode_fields(read_address, &opcode, &size, &opcode2, &immediate, &rd, &rn, &rm, &imm4h, &prepostindex, &updown, &writeback);
        emu_assert(rd != pc && rn != pc);
        if (immediate == REG_PROC) emu_assert(rm != pc);
        copy_arm();
        break;
      }

      case ARM_LDREX:
      case ARM_LDREXB:
      case ARM_LDREXD: {
        uint32_t rd, rn;
        arm_ldrex_decode_fields(read_address, &rd, &rn);
        emu_assert(rd != pc && rn != pc);
        copy_arm();
        break;
      }

      case ARM_STREX:
      case ARM_STREXB:
      case ARM_STREXD: {
        uint32_t rd, rn, rm;
        arm_strex_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_REV:
      case ARM_REV16:
      case ARM_CLZ: {
        uint32_t opcode, size, opcode2, immediate, rd, rn, rm, imm4h, prepostindex, updown, writeback;
        arm_h_data_transfer_decode_fields(read_address, &opcode, &size, &opcode2, &immediate, &rd, &rn, &rm, &imm4h, &prepostindex, &updown, &writeback);
        emu_assert(rd != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_PLD: {
        uint32_t imm, updown, readonly, rn, operand2, rm = reg_invalid;
        arm_pld_decode_fields(read_address, &imm, &updown, &readonly, &rn, &operand2);
        if (imm == LDR_REG) {
          rm = operand2 & 0xF;
        }
        /* The cost of obtaining a scratch register and copying the source PC
             is *probably* too high to be worth translating this. */
        if (rn != pc && rm != pc ) {
          copy_arm();
        }
        break;
      }

      case ARM_STC: {
        uint32_t p, updown, d, writeback, load_store, rn, vd, opcode, immediate;
        arm_vfp_ldm_stm_decode_fields(read_address, &p, &updown, &d, &writeback, &load_store, &rn, &vd, &opcode, &immediate);
        emu_assert(rn != pc);
        copy_arm();
        break;
      }

      case ARM_CDP: {
        uint32_t opc1, crn, crd, coproc, opc2, crm;
        arm_coproc_dp_decode_fields(read_address, &opc1, &crn, &crd, &coproc, &opc2, &crm);
        copy_arm();
        emu_log_always("Untested CDP\n");
        while(1);
        break;
      }

      case ARM_UMLAL:
      case ARM_UMULL:
      case ARM_SMULL:
      case ARM_SMLAL: {
        uint32_t opcode, set_flags, rdhi, rdlo, rm, opcode2, setting, rn;
        arm_dsp_long_res_decode_fields(read_address, &opcode, &set_flags, &rdhi, &rdlo, &rm, &opcode2, &setting, &rn);
        emu_assert(rdhi != pc && rdlo != pc && rm != pc && rn != pc);
        copy_arm();
        break;
      }

      case ARM_MRS: {
        uint32_t rd;
        arm_mrs_decode_fields(read_address, &rd);
        emu_assert(rd != pc);
        copy_arm();
        break;
      }

      case ARM_MSR: {
        uint32_t rn, mask;
        arm_msr_decode_fields(read_address, &rn, &mask);
        emu_assert(rn != pc);
        copy_arm();
        break;
      }

      case ARM_UDIV:
      case ARM_SDIV: {
        uint32_t opcode, rd, rn, rm;
        arm_divide_decode_fields(read_address, &opcode, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_SXTB:
      case ARM_SXTH:
      case ARM_SXTAH:
      case ARM_UXTB:
      case ARM_UXTB16:
      case ARM_UXTH:
      case ARM_UXTAH:
      case ARM_UXTAB:
      case ARM_UXTAB16: {
        uint32_t opcode, rd, rn, rm, rotate;
        arm_extend_decode_fields(read_address, &opcode, &rd, &rn, &rm, &rotate);
        // if rn == pc, it's the version without add which doesn't use pc
        emu_assert(rd != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_LDRSB: {
        uint32_t immediate, rd, rn, rm, imm4h, prepostindex, updown, writeback;
        arm_ldrsb_decode_fields(read_address, &immediate, &rd, &rn, &rm, &imm4h, &prepostindex, &updown, &writeback);
        emu_assert(rd != pc && rn != pc);
        if (immediate == REG_PROC) emu_assert(rm != pc);
        copy_arm();
        break;
      }

      case ARM_LDRBT:
      case ARM_LDRT: {
        uint32_t immediate, rd, rn, updown, operand2;
        arm_ldrt_decode_fields(read_address, &immediate, &rd, &rn, &updown, &operand2);
        emu_assert(rd != pc && rn != pc);
        if (immediate == LDR_REG) emu_assert((operand2 & 0xF) != pc);
        copy_arm();
        break;
      }

      case ARM_BFI: {
        uint32_t rd, rn, lsb, msb;
        arm_bfi_decode_fields(read_address, &rd, &rn, &lsb, &msb);
        emu_assert(rd != pc && rn != pc);
        copy_arm();
        break;
      }

      case ARM_UBFX:
      case ARM_SBFX: {
        uint32_t rd, rn, lsb, width;
        arm_ubfx_decode_fields(read_address, &rd, &rn, &lsb, &width);
        emu_assert(rd != pc && rn != pc);
        copy_arm();
        break;
      }

      case ARM_UQSUB8: {
        uint32_t rd, rn, rm;
        arm_uqsub8_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_BFC: {
      //case ARM_BFI: {
        uint32_t rd, lsb, msb;
        arm_bfc_decode_fields(read_address, &rd, &lsb, &msb);
        emu_assert(rd != pc);
        copy_arm();
        break;
      }

      case ARM_MRRC: {
        uint32_t coproc, opc1, rd, rd2, crm;
        arm_mrrc_decode_fields(read_address, &coproc, &opc1, &rd, &rd2, &crm);
        emu_assert(rd != pc && rd2 != pc);
        copy_arm();
        break;
      }

      case ARM_UMAAL: {
        uint32_t rd, rd2, rn, rm;
        arm_umaal_decode_fields(read_address, &rd, &rd2, &rn, &rm);
        emu_assert(rd != pc && rd2 != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_SMULBB:
      case ARM_SMULTT: {
        uint32_t rd, rn, rm;
        arm_smulbb_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_SMULWB:
      case ARM_SMULWT: {
        uint32_t rd, rn, rm;
        arm_smulwb_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_RBIT: {
        uint32_t rd, rm;
        arm_rbit_decode_fields(read_address, &rd, &rm);
        emu_assert(rd != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_SMLABB: {
        uint32_t rd, rn, rm, ra;
        arm_smlabb_decode_fields(read_address, &rd, &rn, &rm, &ra);
        emu_assert(rd != pc && rn != pc && rm != pc && ra != pc);
        copy_arm();
        break;
      }

      case ARM_SMLAWB:
      case ARM_SMLAWT: {
        uint32_t rd, rn, rm, ra;
        arm_smlawb_decode_fields(read_address, &rd, &rn, &rm, &ra);
        emu_assert(rd != pc && rn != pc && rm != pc && ra != pc);
        copy_arm();
        break;
      }

      case ARM_PKH: {
        uint32_t rd, rn, rm, tb, imm5;
        arm_pkh_decode_fields(read_address, &rd, &rn, &rm, &tb, &imm5);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_SSAT:
      case ARM_USAT: {
        uint32_t rd, sat_imm, rn, sh, imm5;
        arm_usat_decode_fields(read_address, &rd, &sat_imm, &rn, &sh, &imm5);
        emu_assert(rd != pc && rn != pc);
        copy_arm();
        break;
      }

      case ARM_SADD16: {
        uint32_t rd, rn, rm;
        arm_sadd16_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_SSUB16: {
        uint32_t rd, rn, rm;
        arm_ssub16_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_USADA8: {
        uint32_t rd, rn, rm, ra;
        arm_usada8_decode_fields(read_address, &rd, &rn, &rm, &ra);
        emu_assert(rd != pc && rn != pc && rm != pc && ra != pc);
        copy_arm();
        break;
      }

      case ARM_USAT16: {
        uint32_t rd, sat_imm, rn;
        arm_usat16_decode_fields(read_address, &rd, &sat_imm, &rn);
        emu_assert(rd != pc && rn != pc);
        copy_arm();
        break;
      }

      case ARM_UADD8:
      case ARM_UQADD8: {
        uint32_t rd, rn, rm;
        arm_uadd8_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_UADD16: {
        uint32_t rd, rn, rm;
        arm_uadd16_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
        break;
      }

      case ARM_SEL: {
        uint32_t rd, rn, rm;
        arm_sel_decode_fields(read_address, &rd, &rn, &rm);
        emu_assert(rd != pc && rn != pc && rm != pc);
        copy_arm();
	      break;
      }

      case ARM_RRX: {
        uint32_t set_flags, rd, rm;
        arm_rrx_decode_fields(read_address, &set_flags, &rd, &rm);
        emu_assert(rd != pc && rm != pc);
        copy_arm();
        break;
      }

      /* ARM instructions which can be copied directly */
      case ARM_DMB:
      case ARM_ISB:
      case ARM_MSRI:
      case ARM_NOP:
        copy_arm();
        break;

      /* Discarded ARM instructions */
      case ARM_PLII:
        /* Discard instruction preload hints, since they would otherwise only pollute our icache */
        break;

      /* NEON and VFP instructions which might access the PC */
      case ARM_VFP_VSTM_DP:
      case ARM_VFP_VSTM_SP:
      case ARM_VFP_VLDM_SP:
      case ARM_VFP_VLDM_DP:
      case ARM_VFP_VSTR_DP:
      case ARM_VFP_VSTR_SP:
      case ARM_VFP_VLDR_DP:
      case ARM_VFP_VLDR_SP: {
        uint32_t p, updown, d, writeback, load_store, rn, vd, opcode, immediate;
        arm_vfp_ldm_stm_decode_fields(read_address, &p, &updown, &d, &writeback, &load_store, &rn, &vd, &opcode, &immediate);

        if (rn == pc) {
          emu_assert(writeback == 0);

          condition_code = *read_address & 0xF0000000;
          arm_cond_push_reg(condition_code >> 28, r0);

          arm_cond_copy_to_reg_32bit(&write_p, condition_code >> 28, r0, (uint32_t)read_address + 8);
          arm_vfp_ldm_stm_cond(&write_p, (*read_address) >> 28, p, updown, d, writeback, load_store, r0, vd, opcode, immediate);
          write_p++;

          arm_cond_pop_reg(condition_code >> 28, r0);
        } else {
          copy_arm();
        }
        break;
      }

      case ARM_NEON_VLDX_M:
      case ARM_NEON_VLDX_S_O:
      case ARM_NEON_VLDX_S_A:
      case ARM_NEON_VSTX_M:
      case ARM_NEON_VSTX_S_O: {
        uint32_t opcode, opcode2, opcode3, opcode4, params, d, vd, rn, rm;
        arm_v_trans_mult_decode_fields(read_address, &opcode, &opcode2, &opcode3, &opcode4, &params, &d, &vd, &rn, &rm);
        emu_assert(rn != pc); // rm is guaranteed not to be pc
        copy_arm();
        break;
      }

      case ARM_VFP_VMOV_2CORE_DP: {
        uint32_t opcode, rd, rd2, m, vm;
        arm_vfp_vmov_2core_dp_decode_fields(read_address, &opcode, &rd, &rd2, &m, &vm);
        emu_assert(rd != pc && rd2 != pc); // rm is guaranteed not to be pc
        copy_arm();
        break;
      }

      case ARM_VFP_VMOV_CORE_SCAL: {
        uint32_t d, vd, opcode, opcode2, rd;
        arm_vfp_vmov_core_scal_decode_fields(read_address, &d, &vd, &opcode, &opcode2, &rd);
	      emu_assert(rd != pc);
	      copy_arm();
        break;
      }

      case ARM_VFP_VMOV_CORE_SP: {
        uint32_t opcode, rd, n, vn;
        arm_vfp_vmov_core_sp_decode_fields(read_address, &opcode, &rd, &n, &vn);
        emu_assert(rd != pc);
        copy_arm();
        break;
      }

      case ARM_NEON_VDUP_CORE: {
        uint32_t b, e, q, d, vd, rd;
        arm_neon_vdup_core_decode_fields(read_address, &b, &e, &q, &d, &vd, &rd);
        emu_assert(rd != pc);
        copy_arm();
        break;
      }

      case ARM_VFP_VMOV_SCAL_CORE: {
        uint32_t opcode, rd, n, vn, opcode2, opcode3;
        arm_vfp_vmov_scal_core_decode_fields(read_address, &opcode, &rd, &n, &vn, &opcode2, &opcode3);
        emu_assert(rd != pc);
        copy_arm();
        break;
      }

      case ARM_VFP_VMSR: {
        uint32_t rd;
        arm_vfp_vmsr_decode_fields(read_address, &rd);
        emu_assert(rd != pc);
        copy_arm();
        break;
      }

      /* NEON and VFP instructions which can't access the PC */
      case ARM_NEON_VABAL:
      case ARM_NEON_VABD_I:
      case ARM_NEON_VABS:
      case ARM_NEON_VABDL:
      case ARM_NEON_VADD_F:
      case ARM_NEON_VADD_I:
      case ARM_NEON_VADDL:
      case ARM_NEON_VADDW:
      case ARM_NEON_VAND:
      case ARM_NEON_VBIC:
      case ARM_NEON_VBICI:
      case ARM_NEON_VBSL:
      case ARM_NEON_VCEQ_I:
      case ARM_NEON_VCEQZ:
      case ARM_NEON_VCGE_F:
      case ARM_NEON_VCGE_I:
      case ARM_NEON_VCGEZ:
      case ARM_NEON_VCGT_F:
      case ARM_NEON_VCGT_I:
      case ARM_NEON_VCGTZ:
      case ARM_NEON_VCLEZ:
      case ARM_NEON_VCLTZ:
      case ARM_NEON_VCLZ:
      case ARM_NEON_VCVT_F_FP:
      case ARM_NEON_VCVT_F_I:
      case ARM_NEON_VDUP_SCAL:
      case ARM_NEON_VEOR:
      case ARM_NEON_VEXT:
      case ARM_NEON_VHADD:
      case ARM_NEON_VMAX_I:
      case ARM_NEON_VMIN_I:
      case ARM_NEON_VMLA_F:
      case ARM_NEON_VMLA_I:
      case ARM_NEON_VMLAL_I:
      case ARM_NEON_VMLAL_SCAL:
      case ARM_NEON_VMLA_SCAL:
      case ARM_NEON_VMLS_F:
      case ARM_NEON_VMLSL_I:
      case ARM_NEON_VMLSL_SCAL:
      case ARM_NEON_VMLS_SCAL:
      case ARM_NEON_VMOVI:
      case ARM_NEON_VMOVL:
      case ARM_NEON_VMOVN:
      case ARM_NEON_VMUL_F:
      case ARM_NEON_VMUL_I:
      case ARM_NEON_VMULL_I:
      case ARM_NEON_VMULL_SCAL:
      case ARM_NEON_VMUL_SCAL:
      case ARM_NEON_VMVN:
      case ARM_NEON_VMVNI:
      case ARM_NEON_VNEG:
      case ARM_NEON_VORN:
      case ARM_NEON_VORR:
      case ARM_NEON_VORRI:
      case ARM_NEON_VPADD_F:
      case ARM_NEON_VPADD_I:
      case ARM_NEON_VPADAL:
      case ARM_NEON_VPADDL:
      case ARM_NEON_VPMAX_I:
      case ARM_NEON_VQADD:
      case ARM_NEON_VQDMULH_I:
      case ARM_NEON_VQDMULH_SCAL:
      case ARM_NEON_VQMOVUN:
      case ARM_NEON_VQRSHRN:
      case ARM_NEON_VQRSHRUN:
      case ARM_NEON_VQSHRN:
      case ARM_NEON_VQSHRUN:
      case ARM_NEON_VQSUB:
      case ARM_NEON_VREV32:
      case ARM_NEON_VREV64:
      case ARM_NEON_VRHADD:
      case ARM_NEON_VRSHL:
      case ARM_NEON_VRSHR:
      case ARM_NEON_VRSHRN:
      case ARM_NEON_VSHL:
      case ARM_NEON_VSHLI:
      case ARM_NEON_VSHLL:
      case ARM_NEON_VSHLL2:
      case ARM_NEON_VSHR:
      case ARM_NEON_VSHRN:
      case ARM_NEON_VSLI:
      case ARM_NEON_VSRA:
      case ARM_NEON_VSUB_F:
      case ARM_NEON_VSUB_I:
      case ARM_NEON_VSUBL:
      case ARM_NEON_VSUBW:
      case ARM_NEON_VSWP:
      case ARM_NEON_VTRN:
      case ARM_NEON_VTST:
      case ARM_NEON_VUZP:
      case ARM_NEON_VZIP:
      case ARM_VFP_VABS:
      case ARM_VFP_VADD:
      case ARM_VFP_VCMP:
      case ARM_VFP_VCMPE:
      case ARM_VFP_VCMPEZ:
      case ARM_VFP_VCMPZ:
      case ARM_VFP_VCVT_DP_SP:
      case ARM_VFP_VCVT_F_FP:
      case ARM_VFP_VCVT_F_I:
      case ARM_VFP_VDIV:
      case ARM_VFP_VFMA:
      case ARM_VFP_VMLA_F:
      case ARM_VFP_VMLS_F:
      case ARM_VFP_VMOV:
      case ARM_VFP_VMOVI:
      case ARM_VFP_VMRS:
      case ARM_VFP_VMUL_F:
      case ARM_VFP_VNEG:
      case ARM_VFP_VNMLA:
      case ARM_VFP_VNMLS:
      case ARM_VFP_VNMUL:
      case ARM_VFP_VPOP_DP:
      case ARM_VFP_VPUSH_DP:
      case ARM_VFP_VSQRT:
      case ARM_VFP_VSUB_F:
        copy_arm();
        break;

      default:
        emu_log_always("Unknown arm instruction: %d at %p\n", inst, read_address);
        while(1);
        exit(EXIT_FAILURE);
    }
#ifdef PLUGINS_NEW
    } // if (!skip_inst)
#endif

    if (write_p >= data_p) {
      emu_log_always("w: %p r: %p\n", write_p, data_p);
    }
    emu_assert (write_p < data_p);

    if (!stop) arm_check_free_space(thread_data, &write_p, &data_p, MIN_FSPACE);

#ifdef PLUGINS_NEW
    arm_scanner_deliver_callbacks(thread_data, POST_INST_C, read_address, inst, &write_p, &data_p, basic_block, type, !stop);
#endif

    debug("write_p: %p\n", write_p);

    read_address++;
    debug("\n");
  }

  // We haven't strictly enforced updating write_p after the last instruction
  return ((uint32_t)write_p - start_address + 4);
}

void arm_encode_stub_bb(dbm_thread *thread_data, int basic_block, uint32_t target) {
  uint32_t *write_p = (uint32_t *)&thread_data->code_cache->blocks[basic_block];
  uint32_t *data_p = (uint32_t *)write_p;
  data_p += BASIC_BLOCK_SIZE;

  debug("Stub BB: %p\n", write_p);
  debug("ARM stub target: 0x%x\n", target);

  arm_branch_save_context(thread_data, &write_p);
  arm_branch_jump(thread_data, &write_p, basic_block, 0, (uint32_t *)(target - 8), AL, SETUP|REPLACE_TARGET|INSERT_BRANCH);
}
