#ifndef EMU_MEM_MGMT_H
#define EMU_MEM_MGMT_H

#include <sys/mman.h>

/* This is a custom mmap flag to ensure that the page does not get
 * cached in the CPU's L1 cache. This flag is introduced in the
 * "tt-no-l1-cache" branch of our custom TaintTrap kernel for the
 * Galaxy Nexus and it is defined in
 * $KERNEL_ROOT/include/asm-generic/mman-common.h.  */
#define MAP_NO_L1_CACHE 0x40

uint32_t align_page(uint32_t);
uint32_t align_word(uint32_t);
uint32_t page_number(uint32_t);
uint32_t page_number_relative(uint32_t, uint32_t);
uint32_t num_pages(uint32_t);
uint32_t num_pages_relative(uint32_t, uint32_t);
void *emu_alloc(size_t);
void *emu_alloc_custom(size_t size, int prot, int flags);
int emu_free(void *addr, size_t size);

#endif
