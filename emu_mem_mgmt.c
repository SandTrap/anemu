#include <inttypes.h>
#include "emu_debug.h"
#include "emu_mem_mgmt.h"

#define PAGE_SIZE 4096
#define PAGE_SHIFT 12
#define WORD_SIZE 4


/**
 * Calculate and return the address of the beginning of the page that contains addr.
 *
 * Assume all integers are uint32_t
 * PAGE_SIZE = 4096 or 		0000 0000 0000 0000 0001 0000 0000 0000 (binary)
 * PAGE_SIZE - 1 = 4095 or 	0000 0000 0000 0000 0000 1111 1111 1111 (binary)
 * ~ (PAGE_SIZE -1) = 		1111 1111 1111 1111 1111 0000 0000 0000 (binary)
 *
 * For example, the first page is from 0(32) to 0(20) 1111 1111 1111. Any address
 * that falls within this ranges is going to return 0(32), i.e., the address of the
 * beginning of that page.
 *
 * This implementation assumes that first page address start with 0. All page headers
 * are multiples of PAGE_SIZE. Page size in ARM is 4096 bytes.
 *
 * @param addr an 32-bit address
 *
 * @return an 32-bit address of the beginning of the page that contains addr
 */
inline uint32_t
align_page(uint32_t addr) {
    return addr & ~ (PAGE_SIZE - 1);
}


/**
 * Following the same math as in get_page_starting_addr(), this function returns
 * the address of the beginning of the word that contains addr.
 * Memory is assumed to be 32-bit and word size is 4 bytes.
 */
inline uint32_t
align_word(uint32_t addr) {
    return addr & ~ (WORD_SIZE - 1);
}


/**
 *
 * Page number starts with 0.
 */
inline uint32_t
page_number(uint32_t addr) {
    return addr >> PAGE_SHIFT;
}


/**
 *
 * Page number starts with 0
 */
inline uint32_t
page_number_relative(uint32_t addr, uint32_t start) {
    emu_assert(addr >= start);
    return (addr - start) >> PAGE_SHIFT;
}


/**
 * Calculate how many pages length bytes can contain
 *
 * length should be multiples of PAGE_SIZE. When it is not,
 * a warning is generated and the result is rounded up. For
 * example, when the length is about 1.5 pages, the result
 * returned is 2.
 *
 * @param length number of bytes
 *
 * @return the number of pages length bytes can contain
 */
inline uint32_t
num_pages(uint32_t length) {
    if (length % PAGE_SIZE != 0) {
        emu_log_debug("Partial pages in %u\n", length);
    }
    return (length >> PAGE_SHIFT) + 1;

}


/**
 * number of pages between start and end
 *
 * Assuming start (32-bit address) is the beginning of a page,
 * calculate how many pages there are between start and end
 * (32-bit address). end is not not included.
 */
inline uint32_t
num_pages_relative(uint32_t start, uint32_t end) {
    uint32_t length = end - start;

    return num_pages(length);
}


void *
emu_alloc(size_t size) {

    emu_assert(size > 0);

    return emu_alloc_custom(size,
                            PROT_READ | PROT_WRITE,
                            MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE);
}

void *
emu_alloc_custom(size_t size, int prot, int flags) {

    emu_assert(size > 0);

    void *ret = mmap(NULL, size, prot, flags, -1, 0);

    if (ret == MAP_FAILED) {
        emu_log_always("mmap failed on size: %u\n", size);
        return NULL;
    }

    emu_log_debug("%s: %x - %x length: %5d\n",
                  __func__, (intptr_t)ret, (intptr_t)ret + size, size);

    return ret;
}


int emu_free(void *addr, size_t size) {
    emu_assert(addr && size > 0);
    int ret = munmap(addr, size);
    if (ret) {
        emu_abort("munmap");
    }
    emu_log_debug("%s: %x - %x length: %5d\n", __func__, (intptr_t)addr, (intptr_t)addr + size, size);
    return ret;
}
