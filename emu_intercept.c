#include <dlfcn.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

#include "emu_debug.h"
#include "emu_intercept.h"
#include "emu_intercept_gl.h"
#include "emu_intercept_jni.h"


/*
    ____      __                            __
   /  _/___  / /____  _____________  ____  / /_____  _____
   / // __ \/ __/ _ \/ ___/ ___/ _ \/ __ \/ __/ __ \/ ___/
 _/ // / / / /_/  __/ /  / /__/  __/ /_/ / /_/ /_/ / /
/___/_/ /_/\__/\___/_/   \___/\___/ .___/\__/\____/_/
                                 /_/
*/

extern void calloc_trampoline();
extern void malloc_trampoline();
extern void realloc_trampoline();
extern void free_trampoline();
extern void send_trampoline();
extern void pthread_create_trampoline();
extern void pthread_join_trampoline();
extern void emu_test_save_regs_trampoline();

extern void glDeleteTextures_trampoline();
extern void glGetIntegerv_trampoline();
extern void glGenTextures_trampoline();
extern void glBindTexture_trampoline();
extern void glTexParameteri_trampoline();
extern void glTexImage2D_trampoline();
extern void glGetError_trampoline();
extern void glCreateShader_trampoline();
extern void glShaderSource_trampoline();
extern void glCompileShader_trampoline();
extern void glGetShaderiv_trampoline();
extern void glGetShaderInfoLog_trampoline();
extern void glDeleteShader_trampoline();
extern void glCreateProgram_trampoline();
extern void glAttachShader_trampoline();
extern void glBindAttribLocation_trampoline();
extern void glLinkProgram_trampoline();
extern void glGetProgramiv_trampoline();
extern void glGetProgramInfoLog_trampoline();
extern void glDeleteProgram_trampoline();
extern void glReadPixels_trampoline();
extern void glActiveTexture_trampoline();
extern void glBindBuffer_trampoline();
extern void glBindFramebuffer_trampoline();
extern void glBindRenderbuffer_trampoline();
extern void glBufferData_trampoline();
extern void glClear_trampoline();
extern void glClearColor_trampoline();
extern void glDeleteBuffers_trampoline();
extern void glDeleteFramebuffers_trampoline();
extern void glDeleteRenderbuffers_trampoline();
extern void glDepthMask_trampoline();
extern void glDetachShader_trampoline();
extern void glDisable_trampoline();
extern void glDrawArrays_trampoline();
extern void glDrawElements_trampoline();
extern void glEnableVertexAttribArray_trampoline();
extern void glFramebufferRenderbuffer_trampoline();
extern void glFramebufferTexture2D_trampoline();
extern void glGenBuffers_trampoline();
extern void glGenFramebuffers_trampoline();
extern void glGenRenderbuffers_trampoline();
extern void glPixelStorei_trampoline();
extern void glRenderbufferStorage_trampoline();
extern void glUniform1f_trampoline();
extern void glUniform1fv_trampoline();
extern void glUniform1i_trampoline();
extern void glUniform1iv_trampoline();
extern void glUniform2f_trampoline();
extern void glUniform2fv_trampoline();
extern void glUniform3f_trampoline();
extern void glUniform3fv_trampoline();
extern void glUniform4f_trampoline();
extern void glUniform4fv_trampoline();
extern void glUniformMatrix2fv_trampoline();
extern void glUniformMatrix3fv_trampoline();
extern void glUseProgram_trampoline();
extern void glVertexAttribPointer_trampoline();
extern void glViewport_trampoline();
extern void glGetAttribLocation_trampoline();
extern void glGetUniformLocation_trampoline();
extern void glGetString_trampoline();

extern ssize_t emu_trampoline_read(int fd, void *buf, size_t count);
extern void emu_taint_read_trampoline();
extern ssize_t emu_taint_read(int fd, void *buf, size_t count);

extern ssize_t emu_trampoline_write(int fd, void *buf, size_t count);
extern void emu_taint_write_trampoline();
extern ssize_t emu_taint_write(int fd, void *buf, size_t count);

extern uint32_t emu_get_taint_array(uint32_t addr, uint32_t length);
extern void emu_get_taint_array_trampoline();
extern void emu_set_taint_array(uint32_t addr, uint32_t length, uint32_t tag);
extern void emu_set_taint_array_trampoline();

extern void emu_test_save_regs();
extern void emu_test_save_regs_impl(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3,
                                    uint32_t r4, uint32_t r5, uint32_t r6, uint32_t r7,
                                    uint32_t r8, uint32_t r9, uint32_t r10, uint32_t r11);

ssize_t send_wrapper(int socket, const void *buffer, size_t length, int flags) {
    ssize_t ret = send(socket, buffer, length, flags);
    uint32_t tag = emu_get_taint_array((uint32_t)buffer, length);

    struct sockaddr_in addr;
    socklen_t addr_size = sizeof(struct sockaddr_in);
    int res = getpeername(socket, (struct sockaddr *)&addr, &addr_size);
    char clientip[20];
    memset(clientip, 0, sizeof(clientip));
    sprintf(clientip, "%s", inet_ntoa(addr.sin_addr));

    emu_log_always("Intercepting send()! "
                   "Buffer address: %p, Length: %d, Tag: 0x%08x, "
                   "Destination IP: %s, Return value of send(): %d.",
                   buffer, length, tag,
                   clientip, ret);
    return ret;
}

emu_intercept_func_t libc_funcs[] = {
    {
        .name = "calloc",
        .id = calloc_ID,
        .orig_func_addr = (uint32_t)&calloc,
        .trampoline_read_addr = (uint32_t)&calloc_trampoline,
        .target_func_addr = (uint32_t)&calloc,
    },
    {
        .name = "malloc",
        .id = malloc_ID,
        .orig_func_addr = (uint32_t)&malloc,
        .trampoline_read_addr = (uint32_t)&malloc_trampoline,
        .target_func_addr = (uint32_t)&malloc,
    },
    {
        .name = "realloc",
        .id = realloc_ID,
        .orig_func_addr = (uint32_t)&realloc,
        .trampoline_read_addr = (uint32_t)&realloc_trampoline,
        .target_func_addr = (uint32_t)&realloc,
    },
    {
        .name = "free",
        .id = free_ID,
        .orig_func_addr = (uint32_t)&free,
        .trampoline_read_addr = (uint32_t)&free_trampoline,
        .target_func_addr = (uint32_t)&free,
    },
    {
        .name = "emu_trampoline_read",
        .id = emu_trampoline_read_ID,
        .orig_func_addr = (uint32_t)&emu_trampoline_read,
        .trampoline_read_addr = (uint32_t)&emu_taint_read_trampoline,
        .target_func_addr = (uint32_t)&emu_taint_read,
    },
    {
        .name = "emu_trampoline_write",
        .id = emu_trampoline_write_ID,
        .orig_func_addr = (uint32_t)&emu_trampoline_write,
        .trampoline_read_addr = (uint32_t)&emu_taint_write_trampoline,
        .target_func_addr = (uint32_t)&emu_taint_write,
    },
    {
        .name = "emu_get_taint_array",
        .id = emu_get_taint_array_ID,
        .orig_func_addr = (uint32_t)&emu_get_taint_array,
        .trampoline_read_addr = (uint32_t)&emu_get_taint_array_trampoline,
        .target_func_addr = (uint32_t)&emu_get_taint_array,
    },
    {
        .name = "emu_set_taint_array",
        .id = emu_set_taint_array_ID,
        .orig_func_addr = (uint32_t)&emu_set_taint_array,
        .trampoline_read_addr = (uint32_t)&emu_set_taint_array_trampoline,
        .target_func_addr = (uint32_t)&emu_set_taint_array,
    },
    {
        .name = "send",
        .id = send_ID,
        .orig_func_addr = (uint32_t)&send,
        .trampoline_read_addr= (uint32_t)&send_trampoline,
        .target_func_addr = (uint32_t)&send_wrapper,
    },
    {
        .name = "pthread_create",
        .id = pthread_create_ID,
        .orig_func_addr = (uint32_t)&pthread_create,
        .trampoline_read_addr = (uint32_t)&pthread_create_trampoline,
        .target_func_addr = (uint32_t)&pthread_create,
    },
    {
        .name = "pthread_join",
        .id = pthread_join_ID,
        .orig_func_addr = (uint32_t)&pthread_join,
        .trampoline_read_addr = (uint32_t)&pthread_join_trampoline,
        .target_func_addr = (uint32_t)&pthread_join,
    },
#ifdef TEST_TOOLS
    {
        .name = "emu_test_save_regs",
        .id = emu_test_save_regs_ID,
        .orig_func_addr = (uint32_t)&emu_test_save_regs,
        .trampoline_read_addr = (uint32_t)&emu_test_save_regs_trampoline,
        .target_func_addr = (uint32_t)&emu_test_save_regs_impl,
    }
#endif // TEST_TOOLS
};


// The signature of this wrapper should match the typedef
// "wrapper_cb_t", which is declared in <dlfcn.h>.
unsigned long emu_dlwrapper_cb(const char *lib_name, const char *slib_name, char *sym_name, unsigned long sym_addr) {

    // "lib_name" is the name of the library the linker is loading
    // "slib_name" is the name of the shared library which has the required symbol
    // "sym_name" is the symbol name
    // "sym_addr" is the original address to the symbol


    // At the moment, we only care about the OpenGL library.
    if ((slib_name == NULL) ||
        (strstr(slib_name, "libGLESv2.so") != slib_name)) {
        return sym_addr;
    }

    // Return the appropriate wrapper for each OpenGL function.
    //
    // TODO: This is ugly and will slow down as the number of
    // comparisons increase. But the slowdown is okay at the moment
    // because this is only done when the linker loads a library for
    // the first time.
    CHECK_AND_RETURN(glDeleteTextures);
    CHECK_AND_RETURN(glGetIntegerv);
    CHECK_AND_RETURN(glGenTextures);
    CHECK_AND_RETURN(glBindTexture);
    CHECK_AND_RETURN(glTexParameteri);
    CHECK_AND_RETURN(glTexImage2D);
    CHECK_AND_RETURN(glGetError);
    CHECK_AND_RETURN(glCreateShader);
    CHECK_AND_RETURN(glShaderSource);
    CHECK_AND_RETURN(glCompileShader);
    CHECK_AND_RETURN(glGetShaderiv);
    CHECK_AND_RETURN(glGetShaderInfoLog);
    CHECK_AND_RETURN(glDeleteShader);
    CHECK_AND_RETURN(glCreateProgram);
    CHECK_AND_RETURN(glAttachShader);
    CHECK_AND_RETURN(glBindAttribLocation);
    CHECK_AND_RETURN(glLinkProgram);
    CHECK_AND_RETURN(glGetProgramiv);
    CHECK_AND_RETURN(glGetProgramInfoLog);
    CHECK_AND_RETURN(glDeleteProgram);
    CHECK_AND_RETURN(glReadPixels);
    CHECK_AND_RETURN(glActiveTexture);
    CHECK_AND_RETURN(glBindBuffer);
    CHECK_AND_RETURN(glBindFramebuffer);
    CHECK_AND_RETURN(glBindRenderbuffer);
    CHECK_AND_RETURN(glBufferData);
    CHECK_AND_RETURN(glClear);
    CHECK_AND_RETURN(glClearColor);
    CHECK_AND_RETURN(glDeleteBuffers);
    CHECK_AND_RETURN(glDeleteFramebuffers);
    CHECK_AND_RETURN(glDeleteRenderbuffers);
    CHECK_AND_RETURN(glDepthMask);
    CHECK_AND_RETURN(glDetachShader);
    CHECK_AND_RETURN(glDisable);
    CHECK_AND_RETURN(glDrawArrays);
    CHECK_AND_RETURN(glDrawElements);
    CHECK_AND_RETURN(glEnableVertexAttribArray);
    CHECK_AND_RETURN(glFramebufferRenderbuffer);
    CHECK_AND_RETURN(glFramebufferTexture2D);
    CHECK_AND_RETURN(glGenBuffers);
    CHECK_AND_RETURN(glGenFramebuffers);
    CHECK_AND_RETURN(glGenRenderbuffers);
    CHECK_AND_RETURN(glPixelStorei);
    CHECK_AND_RETURN(glRenderbufferStorage);
    CHECK_AND_RETURN(glUniform1f);
    CHECK_AND_RETURN(glUniform1fv);
    CHECK_AND_RETURN(glUniform1i);
    CHECK_AND_RETURN(glUniform1iv);
    CHECK_AND_RETURN(glUniform2f);
    CHECK_AND_RETURN(glUniform2fv);
    CHECK_AND_RETURN(glUniform3f);
    CHECK_AND_RETURN(glUniform3fv);
    CHECK_AND_RETURN(glUniform4f);
    CHECK_AND_RETURN(glUniform4fv);
    CHECK_AND_RETURN(glUniformMatrix2fv);
    CHECK_AND_RETURN(glUniformMatrix3fv);
    CHECK_AND_RETURN(glUseProgram);
    CHECK_AND_RETURN(glVertexAttribPointer);
    CHECK_AND_RETURN(glViewport);
    CHECK_AND_RETURN(glGetAttribLocation);
    CHECK_AND_RETURN(glGetUniformLocation);
    CHECK_AND_RETURN(glGetString);


    emu_abort("Unsatisfied symbol found in callback,"
              " lib_name: %s, slib_name: %s, sym_name: %s, sym_addr: 0x%08lx",
              lib_name, slib_name, sym_name, sym_addr);

    return sym_addr;
}

void *emu_dlopen(const char *filename, int flags) {
    return dlopen_wrap(filename, flags, &emu_dlwrapper_cb);
}

void emu_intercept_init(emu_intercept_func_t **intercept_tbl) {
    emu_intercept_func_t *intercept_func = NULL;
    void* method_sym = NULL;
    void* lib_handle = NULL;
    int array_length = 0;
    int i = 0;


    // Add libc intercepts
    array_length = sizeof(libc_funcs) / sizeof(emu_intercept_func_t);
    for (i = 0; i < array_length; i++) {
        emu_intercept_func_t* libc_func = &libc_funcs[i];
        HASH_ADD_INT(*intercept_tbl, orig_func_addr, libc_func);
    }

    // Add libgles intercepts
    lib_handle = dlopen("/system/lib/libGLESv2.so", RTLD_NOW);
    OPENGL_INTERCEPT_ALL;

    // Add libdvm intercepts
    lib_handle = dlopen("/system/lib/libdvm.so", RTLD_NOW);
    JNI_INTERCEPT_ALL;

}

emu_intercept_func_t* emu_intercept_lookup_impl(emu_intercept_func_t **intercept_tbl, uint32_t address) {
    emu_intercept_func_t *intercept_func = NULL;
    HASH_FIND_INT(*intercept_tbl, &address, intercept_func);

    if (intercept_func != NULL) {
        if (intercept_func->trampoline_read_addr == 0) {
            emu_abort("Intercept trampoline address NULL for %s.\n", intercept_func->name);
        }

        if (intercept_func->id == Invalid_ID) {
            emu_log_always("ID not set for intercept function %s.\n", intercept_func->name);
        }
    }

    return intercept_func;
}
