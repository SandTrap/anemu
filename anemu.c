#include "anemu-private.h"
#include "emu_profiler.h"

emu_global_t emu_global_data = {
    .target = 0,
    .taint_prop_off = false,
    .taint_handler_off = false,
    .mutex = PTHREAD_MUTEX_INITIALIZER,
};

emu_global_t *emu_global = &emu_global_data;


const char *get_signame(int sig) {
    switch(sig) {
    case SIGSEGV:    return "SIGSEGV";
    case SIGTRAP:    return "SIGTRAP";
    case SIGILL:     return "SIGILL" ;
    case SIGABRT:    return "SIGABRT";
    default:         return "? UNKNOWN SIGNAME";
    }
}


const char *get_sigcode(int signo, int code) {
    switch (signo) {
    case SIGSEGV:
        switch (code) {
        case SEGV_MAPERR: return "SEGV_MAPERR";
        case SEGV_ACCERR: return "SEGV_ACCERR";
        }
        break;
    case SIGTRAP:
        switch (code) {
        case TRAP_BRKPT:  return "TRAP_BRKPT";
        case TRAP_TRACE:  return "TRAP_TRACE";
        }
        break;
    case SIGILL:
        switch (code) {
        case ILL_ILLOPC:  return "ILL_ILLOPC";
        case ILL_ILLTRP:  return "ILL_ILLTRP";
        }
        break;
    }
    return "? UNKNOWN SIGCODE";
}


const char *get_ssname(int code) {
    switch(code) {
    case SS_DISABLE: return "SS_DISABLE";
    case SS_ONSTACK: return "SS_ONSTACK";
    default:         return "? UNKNOWN SS NAME";
    }
}

void emu_print_siginfo(int sig, siginfo_t *si, ucontext_t *uc) {

    uint32_t pc = 0;
    uint32_t addr_fault = 0;
    char cmdline[128];
    stack_t oss;

    pc = uc->uc_mcontext.arm_pc;
    addr_fault = uc->uc_mcontext.fault_address;
    emu_parse_cmdline(cmdline, sizeof(cmdline));

    emu_log_always(LOG_BANNER_SIG);
    emu_log_always("signal %d (%s) %d (%s).\n",
                   sig, get_signame(sig),
                   si->si_code, get_sigcode(sig, si->si_code));

    emu_log_always("pid: %5d (cmdline: %s) tid: %5d (threadname: %s).\n",
                   getpid(), cmdline,
                   gettid(), emu_parse_threadname());

    emu_log_always("fault address: 0x%08x, pc: 0x%08x.\n", addr_fault, pc);
    dbg_print_method_info(pc);
    emu_log_always("SandTrap global thread count: %d\n", emu_global->thread_count);

    dbg_dump_ucontext(uc, NULL);
#ifdef WITH_VFP
    dbg_dump_ucontext_vfp(uc);
#endif

    if (sigaltstack(NULL, &oss) == -1) {
        emu_log_always("sandTrap does not have alternate stack!\n");
    } else {
        emu_log_always("alternate sp: %p size: %d flags: %s (%d)\n", oss.ss_sp, oss.ss_size, get_ssname(oss.ss_flags), oss.ss_flags);
    }
}


void emu_handler_segv(int sig, siginfo_t *si, void *ucontext) {

    ucontext_t *uc = NULL;
    emu_thread_t *emu = NULL;
    bool was_obeying_protections = false;

    stack_t oss;
    uint32_t addr_fault = 0;
    uint32_t altstack_base = 0;
    size_t useable_size = 0;
    pthread_internal_t *thread = NULL;

#ifdef ENABLE_THREAD_ISOLATION
    syscall(__NR_sandtrapsettrackedmode, THREAD_TRACKED);
#endif

    was_obeying_protections = emu_ignore_protections();
    emu_log_debug("Switched to tracked mode.");

    uc = (ucontext_t *)ucontext;

    // Verify that the thread is not trapping despite ignoring
    // sandtrap protections. Do this only if we get a SIGSEGV, If we
    // trap because of SIGTRAP, it's because we forced a trap with
    // EMU_MARKER_START.
    if (sig == SIGSEGV) {
        if (!was_obeying_protections) {
            emu_print_siginfo(sig, si, uc);
            emu_abort("Unexpected trap while thread was ignoring sandtrap protections!\n");
        }
    }

    // Verify that the TLS structure used by SandTrap is fine.
    emu = emu_tls_get();
    emu_assert(emu);
    emu_assert(emu_initialized());

    emu->curr_app_ctx = *uc;
    emu->regs = (uint32_t *)&emu->curr_app_ctx.uc_mcontext.arm_r0;
    emu->ext_regs = (void*)&emu->curr_app_ctx + UCONTEXT_FPREGS;

    // Make sure we are not trapping while we are emulating
    if (emu->running) {
        emu_print_siginfo(sig, si, uc);
        emu_abort("re-trap detected!\n");
    }

    // Verify that we have the right signal
    switch (sig) {
    case SIGSEGV:
        emu_log_debug("[emu_handler_segv] signal that triggered emulation is SIGSEGV.\n");
        break;
    case SIGTRAP:
        emu_log_debug("[emu_handler_segv] signal that triggered emulation is SIGTRAP.\n");
        if (emu_thumb_mode(emu) == 1) {
            emu->regs[EMU_ARM_REG_PC] += 2;
        } else {
            emu->regs[EMU_ARM_REG_PC] += 4;
        }
        break;
    default:
        emu_print_siginfo(sig, si, uc);
        emu_abort("[emu_handler_segv] emulation triggered by unexpected signal %d!\n");
        break;
    }

    // Check that SandTrap's stack is fine
    if (sigaltstack(NULL, &oss) == -1) {
        emu_print_siginfo(sig, si, uc);
        emu_abort("SandTrap does not have a valid stack!\n");
    }

    addr_fault = uc->uc_mcontext.fault_address;
    thread = (pthread_internal_t *)pthread_self();
    useable_size = thread->altstack_size - thread->altstack_guard_size;
    altstack_base = (uint32_t)thread->altstack + thread->altstack_guard_size;
    emu_assert(oss.ss_sp == (void *)altstack_base && oss.ss_flags == SS_ONSTACK && oss.ss_size == useable_size);

    // Verify that we are not segfaulting on SandTrap's stack!
    if (addr_fault < altstack_base && addr_fault > (uint32_t)thread->altstack) {
        emu_abort("[-] fault on thread->altstack guard page!\n");
    }

    if (sig == SIGSEGV) {
        // SIGSEGV happens when we're testing real apps. SIGTRAP
        // happens when we force an emulation via EMU_MARKER_START
        uint32_t tag = emu_get_taint_array(addr_fault, 4);
        if (tag != 0) {
            emu_log_always("Thread trapping because of tainted data access (tag: 0x%08x, address: 0x%08x).\n", tag, addr_fault);
        } else {
            emu_log_always("Thread trapping falsely (address: 0x%08x).\n", addr_fault);
        }
    }

    // emu_start(...) will not return.
    emu_start(emu);
}


ssize_t emu_trampoline_read(int fd, void *buf, size_t count) {
    register ssize_t ret asm("r0") = -1;
    if (emu_target()) {

        if (emu_running()) {
            goto no_taint;
        }
        ret = emu_taint_read(fd, buf, count);
        return ret;
    }

no_taint: ;
    return check_read(fd, buf, count);
}

ssize_t emu_trampoline_write(int fd, void *buf, size_t count) {
    register ssize_t ret asm("r0") = -1;

    if (emu_target()) {

        if (emu_running()) {
            goto no_taint ;
        }

        ret = emu_taint_write(fd, buf, count);
        return ret;
    }

no_taint: ;
    return check_write(fd, buf, count);
}


ssize_t emu_taint_read(int fd, void *buf, size_t count) {

    uint32_t taint_file = emu_get_taint_file(fd);
    uint32_t taint_buf  = emu_get_taint_array((uint32_t)buf, count);

    ssize_t ret = -1;
    if (!taint_file && !taint_buf) {
        ret = check_read(fd, buf, count);
        return ret;
    }

    if (taint_buf) {
        // need temp buffer to avoid EFAULT
        /* LOGD("read(%d, %p, %d) tainted buf - sneaking data...\n", fd, buf, count); */
        emu_log_debug("read(%d, %p, %d) tainted buf - sneaking data...\n", fd, buf, count);
        ret = check_read(fd, buf, count);
        if (ret) {
            emu_set_taint_array((uint32_t)buf, ret, taint_file);
        }
    } else {
        // can perform read directly
        ret = check_read(fd, buf, count);
        if (ret && taint_file) {
            /* LOGD("read(%d, %p, %d) tainted ret: %ld\n", fd, buf, count, ret); */
            emu_log_debug("read(%d, %p, %d) tainted ret: %ld\n", fd, buf, count, ret);
            emu_set_taint_array((uint32_t)buf, ret, taint_file);
        }
    }
    return ret;
}


ssize_t emu_taint_write(int fd, void *buf, size_t count) {
    ssize_t ret = -1;
    uint32_t taint_file = emu_get_taint_file(fd);
    uint32_t taint_buf  = emu_get_taint_array((uint32_t)buf, count);

    if (!taint_file && !taint_buf) {
        // let write go through - this must be Dalvik setting taint
        ret = check_write(fd, buf, count);
        /* LOGD("write(%d, %p, %d) ret: %ld\n", fd, buf, count, ret); */
     } else if (!taint_file && taint_buf) {
        /* LOGD("write(%d, %p, %d) tainted - sneaking data...\n", fd, buf, count); */
        emu_log_debug("write(%d, %p, %d) tainted - sneaking data...\n", fd, buf, count);
        ret = check_write(fd, buf, count);
        if (fd != STDOUT_FILENO && fd != STDERR_FILENO) {
            emu_set_taint_file(fd, taint_buf);
        }
    } else { // taint_file
        // NOTE: we never clear file taint if we're writing a non-tainted buffer
        // this is because other parts of the file could still be tainted...

        // TODO: look into pass bench reporting clear

        emu_log_debug("write(%d, %p, %d) tainted file but clear buffer...\n", fd, buf, count);
        ret = check_write(fd, buf, count);
    }
    return ret;
}


__attribute__((always_inline))
uint8_t emu_disabled() {
    return !emu_global->target;
}

void emu_init_properties() {
    emu_log_debug("[+] init properties through property_get\n");
    char prop[PROP_VALUE_MAX]; // max is 92

    property_get("emu.taintpropoff", prop);
    emu_global->taint_prop_off = (atoi(prop) == 1);
    emu_log_debug("taintpropoff value: %d\n", emu_global->taint_prop_off);

    property_get("emu.tainthandleroff", prop);
    emu_global->taint_handler_off = (atoi(prop) == 1);
    emu_log_always("tainthandleroff value: %d\n", emu_global->taint_handler_off);
}

static void emu_init() {

    pthread_mutex_lock(&emu_global->mutex);

    if (emu_initialized()) {
        pthread_mutex_unlock(&emu_global->mutex);
        return;
    }

    emu_log_debug("initializing emu state ...\n");

    emu_parse_maps(emu_global);
    if (emu_init_taintmaps() != 0) {
        emu_abort("Taintmap init failed\n");
    }

    emu_init_properties();
    emu_intercept_init(&emu_global->intercept_func_table);
    emu_taint_prop_plugin_register();

    emu_global->initialized = true;
    pthread_mutex_unlock(&emu_global->mutex);
}


void
emu_start(emu_thread_t *emu) {

    emu_log_debug("starting emulation ...\n");
    emu->running = true;

#ifndef TEST_TOOLS
    // wipe register taint (only if we're not in testing mode)
    emu_clear_taintregs(emu);
#else
    // In testing mode, ensure we enable the taint handler before
    // proceeding further.
    emu->taint_handler_off = false;
#endif // TEST_TOOLS

    // initialize offset to thread data storage in the dispatcher
    global_data.disp_thread_data_off = (uint32_t)&disp_thread_data - (uint32_t)th_to_arm+1;

    // allocate and initialize mambo's thread data struct
    if (!allocate_thread_data(&emu->dbm_thread_data)) {
        emu_abort("Failed to allocate initial dbm thread data.\n");
    }
    init_thread(emu->dbm_thread_data);
    emu->dbm_thread_data->tid = gettid();
    emu->dbm_thread_data->tls = (uint32_t)__get_tls();
    emu->dbm_thread_data->sandtrap_handle = (void*)emu;

    emu_taint_prop_plugin_init(emu);

    // Get the first basic block from mambo
    if (CPSR_T) {
        emu->regs[EMU_ARM_REG_PC] |= 0x1;
    }

    uint32_t block_address = scan(emu->dbm_thread_data, (uint16_t *) emu->regs[EMU_ARM_REG_PC], ALLOCATE_BB);

    // Go to that basic block
    // This will not return
    emu_go_to_address(emu, block_address);
}

void emu_stop(emu_thread_t *emu) { // Hammertime!
    emu_log_debug("##stopping emulation\n");

#ifdef ENABLE_THREAD_ISOLATION
    syscall(__NR_sandtrapsettrackedmode, THREAD_UNTRACKED);
#endif

    if (!emu->ignore_protections_on_exit) {
        emu_obey_protections();
        emu_log_always("##ANEMU obeying sandtrap protections\n");
    }

    // Note that the only way the emulation is going to be stopped is
    // if emu_terminate(...) is called, and emu_terminate(...) will be
    // compiled as an ARM function. R0 at this point contains the
    // first argument to emu_terminate(...).
    //
    // By manually changing R0 to 1, we overwrite the original
    // argument, and we will return 1 to the caller. Now the caller
    // can tell that SandTrap was actively emulating the thread.
    CPU(curr_app_ctx, r0) = 1;

    emu->running = 0;
    emu_log_debug(LOG_BANNER_SIG);

    emu_log_debug("##About to setcontext back to cpp\n");
    IF_DEBUG(dbg_dump_ucontext(&emu->curr_app_ctx, emu));

    // When we finish emulating a thread, we have to free up the mambo
    // and taint prop data structures because we have no guarantee
    // that this thread will be emulated again. And even if it will
    // be, there's no guarantee that it will execute the same native
    // function and that it will use the exact same basic blocks.
    emu_taint_prop_plugin_free(emu);
    free_thread_data(&emu->dbm_thread_data);

    setcontext((const ucontext_t *)&emu->curr_app_ctx); /* never returns */
}


int emu_terminate(int ret) {
    // this is a special method. if it's called while emulation is not
    // being done, it will have no effect. If it's being called during
    // emulation, mambo will intercept it to ensure the right thing is
    // done. see the comments for emu_terminate(...) in anemu.h and
    // the implementation of emu_stop(...).

    // emu_log_always("[emu_terminate] emulation has ended\n");
    return ret;
}

void emu_terminate_at_address(uint32_t address) {

    emu_thread_t *emu = emu_tls_get();
    emu->regs[EMU_ARM_REG_PC] = address;
    emu_stop(emu);
}


void
emu_init_handler(int sig, void (*handler)(int, siginfo_t *, void *),
                 void *stack, size_t stack_size)
{

    emu_log_debug("[+] registering %s (%d) handler sigaltstack sp: %p "
                  "size: %d  threads: %d ...\n",
                    get_signame(sig), sig,
                    stack, stack_size,
                    emu_global->thread_count
                 );

    emu_assert(stack != NULL && stack_size >= MINSIGSTKSZ);

    if (stack == NULL) {
        emu_abort("unallocated stack");
    }
    // memset(stack, 0, stack_size);
    //stack_t ss = malloc(sizeof(stack_t));
    //stack_t oss = malloc(sizeof(stack_t));
    stack_t ss, oss;
    ss.ss_sp = stack;
    ss.ss_size = stack_size;
    ss.ss_flags = 0;
    if (sigaltstack(&ss, &oss) == -1) {
        emu_abort("sigaltstack");
    }

    if (oss.ss_sp != NULL || oss.ss_size != 0 || oss.ss_flags == SA_ONSTACK) {
        emu_log_debug("[-] %7s sigaltstack previously setup! tid: %d oss ss_sp: %p "
                     "ss_flags: %d ss_size: %d\n",
                         get_signame(sig),
                        ((pthread_internal_t *)pthread_self())->kernel_id,
                        oss.ss_sp,
                        oss.ss_flags,
                        oss.ss_size
                    );
    }

    struct sigaction sa;
    // SA_NODEFER to detect traps within the handler itself (bugs)
    sa.sa_flags = SA_SIGINFO | SA_ONSTACK | SA_NODEFER;
    sigemptyset(&sa.sa_mask);
    // sigaddset(&sa.sa_mask, SIGQUIT);
    sa.sa_sigaction = handler;
    if (sigaction (sig, &sa, NULL) == -1) {
        emu_abort("sigaction");
    }
}

void
emu_parse_maps(emu_global_t *emu_global) {

    emu_log_debug("[+] parse process maps\n");
    char buf[1024];

    /**
     * The file /proc/[pid]/maps contains the currently mapped memory
     * and their information. The format of the file is:
     *
     * address              perms   offset      dev      inode     pathname
     * 00400000-00452000    r-xp    00000000    08:02    173521    /usr/lib64/libc-2.15.so
     *
     * See: http://man7.org/linux/man-pages/man5/proc.5.html
     */
    FILE *file = fopen("/proc/self/maps", "r");

    if (!file) {
        emu_abort("could not open /proc/self/maps.\n");
    }

    map_t *m = &emu_global->main_thread_stack;
    memset(m, 0, sizeof(map_t));

    /* Read /proc/self/maps line by line */
    while (fgets(buf, sizeof(buf), file) != NULL) {

        /* string to match /proc/self/maps lines */
        sscanf(buf, "%x-%x %c%c%c%c %llx %x:%x %u %127s\n",
                   &m->vm_start,
                   &m->vm_end,
                   &m->r, &m->w, &m->x, &m->s,
                   &m->pgoff,
                   &m->major, &m->minor,
                   &m->ino,
                   m->name);

        if (strncmp(m->name, "[stack]", 7) != 0) {
            continue;
        }

        break;
    }

    if (strncmp(m->name, "[stack]", 7) != 0) {
        emu_abort("Could not find stack while reading /proc/self/maps.\n");
    }

    emu_log_debug("Process's memory maps parsed\n");
    fclose(file);
}

/* NOTE: return value only valid until next invocation of function */
void emu_parse_cmdline(char *cmdline, size_t size) {
    // emu_log_debug("processing cmdline...\n");
    char filename[64];
    int bytes = snprintf(filename, sizeof(filename), "/proc/%d/cmdline", (uint32_t)getpid());
    emu_assert(bytes > 0 && (uint32_t)bytes < sizeof(filename));

    /* int file = open("/proc/self/cmdline", O_RDONLY); */
    int file = open(filename, O_RDONLY);

    if (!file) {
        emu_abort("open failed\n");
    }

    emu_assert(size > 0);
    ssize_t ret = __read(file, cmdline, size - 1); // one less to allow space for null terminator
    close(file);
    if (ret == -1) {
        emu_abort("read failed\n");
    }

    // expect we read everything in one shot
    emu_assert(ret > 0 && (size_t)ret < size);

    cmdline[ret] = '\0';
    emu_log_debug("cmdline: %s\n", cmdline);
}

// see man(2) prctl, specifically the section about PR_GET_NAME
#define MAX_TASK_NAME_LEN (32)
/* NOTE: return value only valid until next invocation of function */
// WARNING: not thread safe, currently used with emu.lock held so should be fine
char* emu_parse_threadname() {
    static char threadname[MAX_TASK_NAME_LEN + 1]; // one more for termination
    if (prctl(PR_GET_NAME, (unsigned long)threadname, 0, 0, 0) != 0) {
        strcpy(threadname, "<name unknown>");
    } else {
        // short names are null terminated by prctl, but the manpage
        // implies that 16 byte names are not.
        threadname[MAX_TASK_NAME_LEN] = 0;
    }
    return threadname;
}


int emu_ignore_protections() {
    // We'll return the previous value of
    // thread->obeying_protections. This allows the caller to know if
    // the thread was already ignoring protections.

    int ret = 0;
    pthread_internal_t *thread = NULL;

    syscall(__NR_sandtrapsetprotmode, IGNORE_SANDTRAP_PROTECTIONS);
    thread = (pthread_internal_t*)pthread_self();
    ret = thread->obeying_protections;
    thread->obeying_protections = 0;
    return ret;
}

void emu_obey_protections() {
    if (emu_running()) {
        emu_abort("[pid: %d, tid:% d] Error! Trying to obey sandtrap protections while being emulated!\n",
                  getpid(), gettid())
    }
    pthread_internal_t *thread = (pthread_internal_t*)pthread_self();
    thread->obeying_protections = 1;
    syscall(__NR_sandtrapsetprotmode, OBEY_SANDTRAP_PROTECTIONS);
}

void emu_set_protections_mode_on_exit(bool ignore_protections_on_exit) {
    emu_thread_t *emu = emu_tls_get();
    if (emu != NULL) {
        emu->ignore_protections_on_exit = ignore_protections_on_exit;
    }
}



// TODO: hierarchy of taint pages, first by page then by addr
// TODO: ranges instead?
// Level 1: array indexed by (Align(addr) % getPageSize())
//          0 : no taint
//          1 : page fully tainted
//          2 : partial taint -> need further lookup (Level 2)
// Level 2: taintmap->data[offset]

/*
 * Mark the memory region between [addr, addr + length - 1] with taint tag
 *
 * This function should be called by JNI layer code. The implementation of
 * this function tried to make as less assumptions as possible. For example,
 * it doesn't assume that addr is word aligned, meaning addr doesn't have
 * to be the beginning of a word. It could be some random address. Also,
 * length could be bigger than PAGE_SIZE.
 *
 * All the taint information is saved in emu_global->taintmpas. For detailed
 * explanation on taint management, please consult README or the project's
 * git repo
 *
 * @param addr:     Address to a block of memory that is of PAGE_SIZE
 * @param tag:        taint tag. Used to identify what types of taint this is
 * @param length:    Num of bytes of the variable that need to be tainted.
 */
void
emu_set_taint_array(uint32_t addr, uint32_t length, uint32_t tag) {

    if (emu_disabled()) {
        return;
    }

    emu_assert(addr != 0 && length > 0);

    /* Making sure emu_global->taintmaps are initialized */
    if (!emu_initialized()) emu_init();

    taintmap_t *taintmap = emu_get_taintmap(addr);
    /* TODO: Is there guarantee that if an addr and the page header of addr are
     * associated with the same taintmap?
     * Also, are start and end in taintmap_t both are page edges? Would /proc/self/maps
     * contain partial pages?
     */

    /* Most of the time, [addr + length] would not cover multiple pages.
     * But the for loop is in place to make sure all cases are covered.
     */
    uint32_t p;
    for (p = align_page(addr); p < (addr + length); p += PAGE_SIZE) {

        taintcount_t *tp = emu_get_taintcount_quick(p);

        if (!(*tp == 0 && tag == TAINT_CLEAR)) {
            uint32_t p_start;
            uint32_t p_end;
            // safety check if current page would cross the taint array end
            if (p + PAGE_SIZE < (addr + length)) {
                p_end = p + PAGE_SIZE;
            } else {
                p_end = addr + length;
            }

            if (p < addr) {
                p_start = Align(addr, 4);
            } else {
                p_start = p;
            }

            uint32_t x;
            int16_t increment = 0;
            for (x = p_start; x < p_end; x+=4) {
                uint32_t offset = (x - taintmap->start) >> 2;

                bool update = false;
                if (tag == TAINT_CLEAR && taintmap->data[offset] != TAINT_CLEAR) {
                    increment--;
                    update = true;
                } else if (tag != TAINT_CLEAR && taintmap->data[offset] == TAINT_CLEAR) {
                    increment++;
                    update = true;
                }
                if (update) {
                    taintmap->data[offset] = tag;  /* word (32-bit) based tag storage */
                }
            }
            if (increment) { // if taint toggled
                emu_update_taintpage(p, increment);
            }
        }
    }

}

uint32_t
emu_get_taint_array(uint32_t addr, uint32_t length) {
    // TODO: aquire lock first before checking flag? extremely unlikely case
    if (emu_disabled() || !emu_initialized()) return TAINT_CLEAR;

    emu_assert(addr != 0 && length > 0);

    uint32_t ret = TAINT_CLEAR;
    uint32_t p;
    // taintmap can only be the same space for a single array range
    taintmap_t *taintmap = emu_get_taintmap(addr);
    for (p = Align(addr, 4); p < (addr + length); p += PAGE_SIZE) {
        taintcount_t *tp = emu_get_taintcount_quick(p);
        if (*tp) { // is page tainted?
            uint32_t p_end;
            if (p + PAGE_SIZE < (addr + length)) {
                p_end = p + PAGE_SIZE;
            } else {
                p_end = addr + length;
            }
            uint32_t offset;
            uint32_t x;
            for (x = p; x < p_end; x+=4) {
                uint32_t offset = (x - taintmap->start) >> 2;
                uint32_t tag = taintmap->data[offset]; /* word (32-bit) based tag storage */
                ret |= tag;
            }
        }
    }
    return ret;
}

__attribute__((always_inline))
bool emu_running() {
    emu_thread_t *emu = emu_tls_get();
    if (emu) return emu->running;
    return 0;
}

bool emu_untracked() {
    emu_thread_t *emu = emu_tls_get();
    pthread_internal_t *thread = (pthread_internal_t*)pthread_self();
    if (emu) {
        return (!emu->running & thread->obeying_protections);
    }

    return 0;
}

bool emu_initialized() {
    return emu_global->initialized;
}

__attribute__((always_inline))
uint32_t emu_target() {
    return emu_global->target;
}

inline
void emu_set_target(pid_t pid) {
    emu_init();
    emu_global->target = pid;
}

// copied over from libc/bionic/pthread.c
static
void *mkstack(size_t size, size_t guard_size) {
    void *stack;

    stack = emu_alloc(size);
    if (stack == NULL) {
        emu_log_always("Allocation failed for mkstack\n");
        emu_abort("mkstack\n");
    }

    if(mprotect(stack, guard_size, PROT_NONE)){
        emu_free(stack, size);
        stack = NULL;
    }

    return stack;
}

inline
int32_t emu_thread_count_up() {
    return atomic_inc(&emu_global->thread_count);
}

inline
int32_t emu_thread_count_down() {
    return atomic_dec(&emu_global->thread_count);
}

/**
 * Hook a pthread with our signal handler.
 */
void
emu_hook_thread_entry(void *arg)
{
    int res;
    pthread_internal_t *thread = (pthread_internal_t *)arg;
    emu_assert(thread == (pthread_internal_t *)pthread_self());
    thread->target = emu_target();
    if (thread->target) {

        // The various data structures we create for the emulator
        // below may be allocated in a protected page. Therefore, it's
        // important that we have ignore sandtrap protections mode
        // here.
        emu_ignore_protections();
        emu_init();

        emu_log_debug("Target process set. Hooking pthread %lu to anemu.\n", pthread_self());
        // atomic increment a emu->thread_count and decrement it in __pthread_internal_free()
        // which would provide a live thread count equal to total running threads for target pid
        // can then match it against /proc/self/task
        emu_thread_count_up();

        size_t guard_size = PAGE_SIZE;
        size_t altstack_size = guard_size + 4 * SIGSTKSZ; /* ensures useable altstack is SIGSTKSZ */

        if (!thread->altstack) {
            thread->altstack = mkstack(altstack_size, guard_size);
            if (thread->altstack == NULL) {
                // kill thread/process
                emu_abort("mkstack failed alstack_size: %d guard_size: %d\n", altstack_size, guard_size);
            }
            thread->altstack_size = altstack_size;
            thread->altstack_guard_size = guard_size;
            // emu_log_debug("altstack alloc: %p\n", thread->altstack);
        } else {
            emu_abort("altstack already exists: %p\n", thread->altstack);
        }

        emu_log_debug("Altstack allocation and set up succeed\n");

        // allocated PAGE_SIZE for guard + SIGSTKSZ thread->altstack
        // handler must use only the SIGSTKSZ portion
        size_t useable_size = altstack_size - guard_size;
        uint32_t altstack_base = (uint32_t)thread->altstack + guard_size;

        emu_log_debug("Registering signal handler function");
        emu_init_handler(SIGSEGV, emu_handler_segv, (void *)altstack_base, useable_size);
        // NOTE! for standalone emu, we use START and STOP markers
        // we purposely want the same handler for SIGTRAP as SIGSEGV to simplify matters
        emu_init_handler(SIGTRAP, emu_handler_segv, (void *)altstack_base, useable_size);
        emu_init_handler(SIGILL,  emu_handler_segv, (void *)altstack_base, useable_size);

        emu_thread_t *emu = calloc(1, sizeof(emu_thread_t));
        if (emu == NULL) {
            emu_log_always("Initial allocation failed. Terminating the system\n");
            emu_abort("emu_thread_t\n");
        }

        emu->heap_tm = &emu_global->taintmaps[TAINTMAP_LIB];
        emu->stack_tm = &emu_global->taintmaps[TAINTMAP_STACK];
        emu->stack_base = emu_global->stack_base;

        emu->tid = gettid();
        emu->ignore_protections_on_exit = true;

        // If taint propagation is globally turned off, then we'll
        // turn it off for the thread too. This is, at the moment, a
        // micro-optimization because instead of having the
        // taint_prop_off field in emu_thread_t, we could just check
        // emu_global->taint_prop_off wherever it counts. But it might
        // be cleaner (and ever-so-slightly faster) to do this.
        emu->taint_prop_off = emu_global->taint_prop_off;
        emu->taint_handler_off = emu_global->taint_handler_off;

        emu_assert(emu->tid == thread->kernel_id);
        emu_tls_set(emu);
        emu_log_debug("[+] Added emu_thread_t to TLS\n");
    }

}

void emu_hook_pthread_internal_free(void *arg) {

    // TODO: In emu_hook_thread_entry, we needed to ignore sandtrap
    // protections because the various emu data structures may be
    // allocated in a protected page. For similar reasons, would we
    // also need to ignore sandtrap protections mode here?

    pthread_internal_t *thread = (pthread_internal_t *)arg;

    if (thread->altstack) {
        emu_assert(thread->target);
        emu_log_debug("altstack free: %p\n", thread->altstack);
        emu_free(thread->altstack, thread->altstack_size);

        emu_thread_t *emu = emu_tls_get();
        if (emu) {

            if (emu->tid == thread->kernel_id) {
                emu_log_debug("emu free: %p", emu);
                free(emu);
                emu_tls_set(NULL);
            } else {
                // TODO: This branch is taken when a thread A is doing
                // a pthread_join() on thread B. In this case, thread
                // B's resources are released by B. However, the
                // emu_thread_t structure is located on the TLS and
                // thread A may not access thread B's TLS. Since we're
                // not handling this at the moment, this is a mem
                // leak.
                //
                // The correct fix is to have two pthread hooks: one
                // to free the altstack and one to free up data on the
                // TLS. That way, thread B will destroy its own
                // emu-thread_t struct and thread A may destroy B's
                // altstack.
            }
        }
        emu_thread_count_down();
    }
}

void emu_hook_bionic_clone_entry() {
    emu_thread_count_up();

    emu_abort("not implemented yet");
}

void emu_hook_exit_thread(int ret) {
    emu_log_debug("exit_thread: %d %s\n", ret, ret ? "EXIT_FAILURE" : "EXIT_SUCCESS");
    emu_thread_count_down();
}

void emu_hook_bionic_atfork_run_child(void *arg) {
    UNUSED pthread_internal_t *thread = (pthread_internal_t *)arg;

    // emu_log_debug("fork:\n");
    char name[16];
    int ret = prctl(PR_GET_NAME, (unsigned long) name, 0, 0, 0);
    if (ret != 0) {
        emu_log_debug("fork prctl name: %s\n", name);
    }
    // NOTE: if target check is done after work, we'll be with a new pid so can't ever be target?
    // move hook to right before the kernel adjusts id __pthread_settid(pthread_self(), gettid());
    //
    // TODO: Understand the domain implications of fork. We might have to modify fork().
    if (emu_target()) {
        emu_abort("App is calling fork()! This is an unsupported feature. Parent pid: %d.\n", emu_global->target);
    }
}

void emu_hook_Zygote_forkAndSpecializeCommon(void *arg) {
    pid_t pid = (pid_t)arg;
    emu_assert(!emu_global->target);
    // paranoid
    // memset(emu_global, 0, sizeof(emu_global_t));
    emu_global->target = pid;

    char name[16];
    int ret = prctl(PR_GET_NAME, (unsigned long) name, 0, 0, 0);
    if (ret != 0) {
        emu_log_debug("prctl name: %s\n", name);
    }

    // reuse handler setup code
    emu_hook_thread_entry((void *)pthread_self());
}

ssize_t check_read(int fd, void *buf, size_t count) {
    /* if (fd > 3) emu_log_debug("%s(%d, %p, %d)\n", __func__, fd, buf, count); */
    ssize_t ret = __read(fd, buf, count);
    if (ret == -1) {
        if (errno != ENOENT && errno != EAGAIN) {
            //TODO: Why are there numerous EAGAIN errors?
            // emu_log_always("read(%d, %p, %d) failed with ret: %ld and errno: %d and strerror: %s\n", fd, buf, count, ret, errno, strerror(errno));
        }
        // this should never happen outside emu
        emu_assert(errno != EFAULT);
    }
    return ret;
}

ssize_t check_write(int fd, void *buf, size_t count) {
    ssize_t ret = __write(fd, buf, count);
    if (ret == -1) {
        //emu_log_always("write(%d, %p, %d) failed ret: %ld and errno: %d and sterror: %s\n", fd, buf, count, ret, errno, strerror(errno));
        // __write(STDERR_FILENO, buf, count);
        // emu_log_always("debug: %s\n", buf);
        // gdb_wait();
        // this should never happen outside emu
        emu_assert(errno != EFAULT);
    }
    return ret;
}

void emu_set_jni_return_taint(uint32_t tag, size_t length) {
    emu_thread_t *emu = emu_tls_get();
    if (emu == NULL) {
        emu_abort("Emulator not available in emu_set_jni_return_taint!\n");
    }

    if (length != 4 && length != 8) {
        emu_abort("Unexpected return value length in emu_set_jni_return_taint!. Tag: %d, Length: %d\n", tag, length);
    }

    if (emu->jni_return_taint_set == true) {
        emu_abort("jni_return_taint_set is true when it should be false, possibly because the JNI return taint was not handled correctly.");
    }

    emu->jni_return_taint_set = true;
    emu->jni_return_taint_tag = tag;
    emu->jni_return_taint_length = length;
}

emu_intercept_func_t* emu_intercept_lookup(uint32_t address) {
    return emu_intercept_lookup_impl(&emu_global->intercept_func_table, address);
}

char* emu_get_last_status() {
    emu_thread_t *emu = emu_tls_get();
    if (emu == NULL) {
        return NULL;
    }

    return &emu->last_status;
}
