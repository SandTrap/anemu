#ifndef EMU_DEBUG_H
#define EMU_DEBUG_H

#include <android/log.h>
#include <stdio.h>

#define LOG_BANNER_SIG   "\n### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###\n"
#define LOG_BANNER_INSTR "*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***\n\n"

#define TT_LOG_TAG "anemu"
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, 		TT_LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG,		TT_LOG_TAG, __VA_ARGS__))
#define LOGV(...) ((void)__android_log_print(ANDROID_LOG_VERBOSE,	TT_LOG_TAG, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR,		TT_LOG_TAG, __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN,		TT_LOG_TAG, __VA_ARGS__))

#define emu_log_always      LOGE

#ifdef NO_DEBUG

#define IF_DEBUG(...) (void)(NULL)
#define emu_log_debug(...) (void)(NULL)

#else // NO_DEBUG

#define IF_DEBUG(line) line
#define emu_log_debug  LOGD

#endif // NO_DEBUG

#define UNUSED __attribute__((unused))

#define emu_abort(...) {                        \
        emu_log_always("EMU ABORT!");           \
        emu_log_always(__VA_ARGS__);            \
        gdb_wait();                             \
    }

#ifdef ASSERTS_ON
#define emu_assert(x) if (!(x)) { emu_abort("ASSERTION (%s) FAILED file: %s line: %d\n", #x, __FILE__, __LINE__); }
#else
#define emu_assert(x) (void)(NULL)
#endif

void gdb_wait();
void dbg_print_method_info(uint32_t pc);
void dbg_print_dumb_backtrace(uint32_t *sp, int crawl_amount);

// Note that the stack crawl is done from -crawl_amount to
// +crawl_amount
void dbg_print_stack(uint32_t *sp, int crawl_amount);

#endif
