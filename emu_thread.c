#include "emu_thread.h"
#include "emu_debug.h"

/**
 * Decide if the CPU is in thumb mode by looking at the
 * Thumb bit of the CPSR
 */
uint8_t emu_thumb_mode(emu_thread_t *emu) {
    return CPSR_T;              /* 0: ARM, 1: Thumb */
}

__attribute__((always_inline))
emu_thread_t* emu_tls_get() {
    void**  tls = (void**)__get_tls();
    return tls[TLS_SLOT_EMU_THREAD];
}

inline
void emu_tls_set(emu_thread_t *emu) {
    void**  tls = (void**)__get_tls();
    tls[TLS_SLOT_EMU_THREAD] = emu;
}

void dbg_dump_ucontext(ucontext_t *uc, emu_thread_t *emu) {
    mcontext_t *r = &uc->uc_mcontext;

    emu_log_debug("dump gp regs:\n");
    emu_log_debug("fault addr %8x\n",
                 (uint32_t)r->fault_address);

    emu_log_debug("%s: %8x  %s: %8x  %s: %8x  %s: %8x\n"
                 "%s: %8x  %s: %8x  %s: %8x  %s: %8x\n"
                 "%s: %8x  %s: %8x  %s: %8x  %s: %8x\n"
                 "%s: %8x  %s: %8x  %s: %8x  %s: %8x  cpsr: %08x\n\n",
                 emu && emu->taintreg[EMU_ARM_REG_R0] ? " R0" : " r0", (uint32_t)r->arm_r0,
                 emu && emu->taintreg[EMU_ARM_REG_R1] ? " R1" : " r1", (uint32_t)r->arm_r1,
                 emu && emu->taintreg[EMU_ARM_REG_R2] ? " R2" : " r2", (uint32_t)r->arm_r2,
                 emu && emu->taintreg[EMU_ARM_REG_R3] ? " R3" : " r3", (uint32_t)r->arm_r3,
                 emu && emu->taintreg[EMU_ARM_REG_R4] ? " R4" : " r4", (uint32_t)r->arm_r4,
                 emu && emu->taintreg[EMU_ARM_REG_R5] ? " R5" : " r5", (uint32_t)r->arm_r5,
                 emu && emu->taintreg[EMU_ARM_REG_R6] ? " R6" : " r6", (uint32_t)r->arm_r6,
                 emu && emu->taintreg[EMU_ARM_REG_R7] ? " R7" : " r7", (uint32_t)r->arm_r7,
                 emu && emu->taintreg[EMU_ARM_REG_R8] ? " R8" : " r8", (uint32_t)r->arm_r8,
                 emu && emu->taintreg[EMU_ARM_REG_R9] ? " R9" : " r9", (uint32_t)r->arm_r9,
                 emu && emu->taintreg[EMU_ARM_REG_R10] ? "R10" : "r10", (uint32_t)r->arm_r10,
                 emu && emu->taintreg[EMU_ARM_REG_FP] ? " FP" : " fp", (uint32_t)r->arm_fp,
                 emu && emu->taintreg[EMU_ARM_REG_IP] ? " IP" : " ip", (uint32_t)r->arm_ip,
                 emu && emu->taintreg[EMU_ARM_REG_SP] ? " SP" : " sp", (uint32_t)r->arm_sp,
                 emu && emu->taintreg[EMU_ARM_REG_LR] ? " LR" : " lr", (uint32_t)r->arm_lr,
                 emu && emu->taintreg[EMU_ARM_REG_PC] ? " PC" : " pc", (uint32_t)r->arm_pc,
                 (uint32_t)r->arm_cpsr);

    emu_log_debug("\n");
}

void
emu_dump_cpsr(emu_thread_t *emu)
{
    emu_log_debug("cpsr [%c%c%c%c %c %c]\n",
                  CPSR_N ? 'N' : 'n',
                  CPSR_Z ? 'Z' : 'z',
                  CPSR_C ? 'C' : 'c',
                  CPSR_V ? 'V' : 'v',
                  CPSR_I ? 'I' : 'i',
                  CPSR_T ? 'T' : 't'
                  );
    return;
}

void dbg_dump_ucontext_vfp(ucontext_t *uc) {
    emu_log_debug("dump vfp regs:\n");
    /* dump VFP registers from uc_regspace */
    struct aux_sigframe *aux;
    aux = (struct aux_sigframe *) uc->uc_regspace;
    struct vfp_sigframe *vfp = &aux->vfp;

    uint64_t magic = vfp->magic;
    uint64_t size  = vfp->size;
    emu_assert(magic == VFP_MAGIC && size == VFP_STORAGE_SIZE);

    struct user_vfp vfp_regs = vfp->ufp;
    int i;
    for (i = 0; i < NUM_VFP_REGS; i += 2) {
        emu_log_debug("d%-2d: %16llx  d%-2d: %16llx\n",
                      i,   vfp_regs.fpregs[i],
                      i+1, vfp_regs.fpregs[i+1]);
    }
    // Floating-point Status and Control Register
    emu_log_debug("fpscr:   %08lx\n", vfp_regs.fpscr);

    // exception registers
    struct user_vfp_exc vfp_exc = vfp->ufp_exc;
    // Floating-Point Exception Control register
    emu_log_debug("fpexc:   %08lx\n", vfp_exc.fpexc);
    // Floating-Point Instruction Registers
    // FPINST contains the exception-generating instruction
    emu_log_debug("fpinst:  %08lx\n", vfp_exc.fpinst);
    // FPINST2 contains the bypassed instruction
    emu_log_debug("fpinst2: %08lx\n", vfp_exc.fpinst2);

    // sanitise exception registers
    // based on $KERNEL/arch/arm/kernel/signal.c
    // see: int restore_vfp_context(struct vfp_sigframe __user *frame)
    // ensure the VFP is enabled
    emu_assert(vfp_exc.fpexc & FPEXC_EN);
    // ensure FPINST2 is invalid and the exception flag is cleared
    emu_assert(!(vfp_exc.fpexc & (FPEXC_EX | FPEXC_FP2V)));
}

void emu_dump(emu_thread_t *emu) {
    dbg_dump_ucontext(&emu->curr_app_ctx, emu);
#ifdef WITH_VFP
    dbg_dump_ucontext_vfp(&emu->curr_app_ctx);
#endif
}

#ifdef TEST_TOOLS

void emu_test_save_regs() {
    // this will be intercepted during emulation and
    // emu_test_save_regs_impl() will be called instead.
}

void emu_test_save_regs_impl(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3,
                             uint32_t r4, uint32_t r5, uint32_t r6, uint32_t r7,
                             uint32_t r8, uint32_t r9, uint32_t r10, uint32_t r11) {

    emu_thread_t *emu = emu_tls_get();

    emu->saved_regs[EMU_ARM_REG_R0] = r0;
    emu->saved_regs[EMU_ARM_REG_R1] = r1;
    emu->saved_regs[EMU_ARM_REG_R2] = r2;
    emu->saved_regs[EMU_ARM_REG_R3] = r3;
    emu->saved_regs[EMU_ARM_REG_R4] = r4;
    emu->saved_regs[EMU_ARM_REG_R5] = r5;
    emu->saved_regs[EMU_ARM_REG_R6] = r6;
    emu->saved_regs[EMU_ARM_REG_R7] = r7;
    emu->saved_regs[EMU_ARM_REG_R8] = r8;
    emu->saved_regs[EMU_ARM_REG_R9] = r9;
    emu->saved_regs[EMU_ARM_REG_R10] = r10;
    emu->saved_regs[EMU_ARM_REG_R11] = r11;
}

void emu_test_get_regs(uint32_t core_regs[]) {
    emu_thread_t *emu = emu_tls_get();

    core_regs[EMU_ARM_REG_R0] = emu->saved_regs[EMU_ARM_REG_R0];
    core_regs[EMU_ARM_REG_R1] = emu->saved_regs[EMU_ARM_REG_R1];
    core_regs[EMU_ARM_REG_R2] = emu->saved_regs[EMU_ARM_REG_R2];
    core_regs[EMU_ARM_REG_R3] = emu->saved_regs[EMU_ARM_REG_R3];
    core_regs[EMU_ARM_REG_R4] = emu->saved_regs[EMU_ARM_REG_R4];
    core_regs[EMU_ARM_REG_R5] = emu->saved_regs[EMU_ARM_REG_R5];
    core_regs[EMU_ARM_REG_R6] = emu->saved_regs[EMU_ARM_REG_R6];
    core_regs[EMU_ARM_REG_R7] = emu->saved_regs[EMU_ARM_REG_R7];
    core_regs[EMU_ARM_REG_R8] = emu->saved_regs[EMU_ARM_REG_R8];
    core_regs[EMU_ARM_REG_R9] = emu->saved_regs[EMU_ARM_REG_R9];
    core_regs[EMU_ARM_REG_R10] = emu->saved_regs[EMU_ARM_REG_R10];
    core_regs[EMU_ARM_REG_R11] = emu->saved_regs[EMU_ARM_REG_R11];
}

#endif // TEST_TOOLS
