#include <stdlib.h>
#include "emu_emulate.h"
#include "emu_debug.h"
#include <endian.h>
#include "emu_thread.h"

/**
 * @brief based on the emu_arm_reg, return the value of the specified register
 *        in the ucontext.
 */
uint32_t
emu_read_reg(emu_thread_t *emu, emu_arm_reg reg)
{
    emu_assert(reg <= EMU_ARM_REG_R16);
    if (reg == EMU_ARM_REG_PC) {

        // In ARMv7 CPUs, the PC register in the CPU does *not*
        // contain the same value as the address of the current
        // instruction. In ARM mode, the PC register is equal to the
        // address of the current instruction + 8, while in Thumb
        // mode, the PC is equal to the address of the current
        // instruction + 4. (See ARM DDI 0406B, Page B1-14, R[] -
        // non-assignment form.) Loosely put, the PC is always
        // pointing to an instruction that is two instructions ahead
        // of the one being executed right now. This is the PC value
        // that should be used if an instruction uses the PC as a
        // source register.
        //
        // On the other hand, the PC that we keep in emu->regs (in the
        // ucontext structure) is the address of the current
        // instruction. To match the semantics of the ARMv7 CPU, when
        // reading the value of the PC, we must increment it by 4 or
        // 8, depending on the current execution mdoe.
        if (emu_thumb_mode(emu) == 1) {
            return emu->regs[reg] + 4;
        } else {
            return emu->regs[reg] + 8;
        }
    }
    return emu->regs[reg];
}

bool is_cond_pass(emu_thread_t *emu, emu_arm_instr_cc cond) {
    switch (cond) {
    case EMU_ARM_CC_EQ: return CPSR_Z;
    case EMU_ARM_CC_NE: return !CPSR_Z;
    case EMU_ARM_CC_CS: return CPSR_C;
    case EMU_ARM_CC_CC: return !CPSR_C;
    case EMU_ARM_CC_MI: return CPSR_N;
    case EMU_ARM_CC_PL: return !CPSR_N;
    case EMU_ARM_CC_VS: return CPSR_V;
    case EMU_ARM_CC_VC: return !CPSR_V;
    case EMU_ARM_CC_HI: return (CPSR_C && !CPSR_Z);
    case EMU_ARM_CC_LS: return (!CPSR_C && CPSR_Z);
    case EMU_ARM_CC_GE: return (CPSR_N == CPSR_V);
    case EMU_ARM_CC_LT: return (CPSR_N != CPSR_V);
    case EMU_ARM_CC_GT: return (!CPSR_Z && (CPSR_N == CPSR_V));
    case EMU_ARM_CC_LE: return (CPSR_Z || (CPSR_N != CPSR_V));
    case EMU_ARM_CC_AL: return true;
    case EMU_ARM_CC_ALT: return true;
    default:
        emu_abort("[is_cond_pass] Unsupported condition type (%d).\n", cond);
        return false;
    }
}

void emu_go_to_address(emu_thread_t * emu, uint32_t address) {
    CPU(curr_app_ctx, pc) = (uint32_t)address;
    asm volatile("dsb\n\t");
    setcontext((const ucontext_t *)&emu->curr_app_ctx);
}
